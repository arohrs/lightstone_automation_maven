/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Reporting;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author sbachan
 */
public class DB_TestResult_Logging extends BaseClass
{

    private String TestPack = null;//Required
    private String Environment = null;
    private String Browser = null;
    private String Device = null;
    private String DeviceBuild = null;
    private String RunDate = null;//Required
    private String RunDuration = null;//Required
    private String Status = null;//Required
    private String FailReason = "none";
    private String FailError = null;
    private String ReportPath = "none";
    private String ProjectID_FK = null;//Required
    private String PC_Name = getComputerName();

    public boolean insertIntoDatabase(List<TestResult> testResults)
    {
        try
        {
            print("Entered into insertIntoDatabase() method");
            currentDatabase = Enums.Database.ProjectReporting_Lightstone;
            print("SET Current Database - " + this.currentDatabase.name());

            setDeviceBuild();
            setDevice();
            setEnvironment();
            setBrowser();
            setProjectID_FK();
            setStatus(testResults);
            setFailError(testResults);
            setRunDuration(testResults);
            setRunDate(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));

            String sqlQuery = "EXEC dbo.PROC_AddTestResult \n"
                    + "	@TestPack  = N'" + getTestPack() + "',\n"
                    + "	@Environment = N'" + getEnvironment() + "',\n"
                    + "	@Browser = N'" + getBrowser() + "',\n"
                    + "	@Device = N'" + getDevice() + "',\n"
                    + "	@DeviceBuild = N'" + getDeviceBuild() + "',\n"
                    + "	@RunDate = N'" + getRunDate() + "',\n"
                    + "	@RunDuration = N'" + getRunDuration() + "',\n"
                    + "	@Status = N'" + getStatus() + "',\n"
                    + "	@FailReason = N'" + getFailReason() + "',\n"
                    + "	@FailError = N'" + getFailError() + "',\n"
                    + "	@ReportPath = N'" + getReportPath() + "',\n"
                    + "	@ProjectID_FK = N'" + getProjectID_FK() + "', \n"
                    + "	@PC_Name = N'" + getPC_Name() + "'";

            print("SQL Insert Query: " + sqlQuery);
            DataBaseInstance.RunQuery(sqlQuery);
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Failed to Insert Test Result into SQL DB - " + e.getMessage());
            print("ERROR running Insert statement - " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public void setTestPack(String TestPack)
    {
        if (this.TestPack == null || !TestPack.toLowerCase().contains(this.TestPack.toLowerCase()))
        {
            this.TestPack = TestPack.replace("TestPacks\\", "").replace("TestPacks//", "");
            print("\nSET Testpack - " + this.TestPack);
        }
    }

    public void setRunDuration(List<TestResult> testResults)
    {
        int totalSeconds = 0;
        //Add up the run duration from each test result
        for (TestResult result : testResults)
        {
            totalSeconds += result.testDuration;
        }
        this.RunDuration = Long.toString(totalSeconds);
        print("SET RunDuration - " + this.RunDuration);
    }

    public void setStatus(List<TestResult> testResults)
    {
        if (testResults.size() > 0)
        {
            this.Status = (testResults.stream().allMatch(s -> s != null && s.testStatus == Enums.ResultStatus.PASS)) ? "PASS" : "FAIL";
        } else
        {
            this.Status = "FAIL";
        }
        print("SET Status - " + this.Status);
    }

    public void setFailError(List<TestResult> testResults)
    {
        this.FailError = (getStatus().equalsIgnoreCase("PASS")) ? "none" : testResults.stream().filter(s -> s != null && s.testStatus != Enums.ResultStatus.PASS).collect(Collectors.toList()).get(0).errorMessage.replaceAll("'", "''");
        print("SET FailError - " + this.FailError);
    }

    public void setProjectID_FK()
    {
        this.ProjectID_FK = currentDatabase.projectID;
        print("SET ProjectID_FK - " + this.ProjectID_FK);
    }

//
    // <editor-fold defaultstate="collapsed" desc="Other Getters and Setters">
    public String getTestPack()
    {
        return TestPack;
    }

    public String getEnvironment()
    {
        return Environment;
    }

    public void setEnvironment()
    {
        this.Environment = (currentEnvironment != null) ? currentEnvironment.name() : "none";
        print("SET Environment - " + this.Environment);
    }

    public String getBrowser()
    {
        return Browser;
    }

    public void setBrowser()
    {
        this.Browser = (currentBrowser != null) ? currentBrowser.name() : "none";
        print("SET Browser - " + this.Browser);
    }

    public String getDevice()
    {
        return Device;
    }

    public void setDevice()
    {
        this.Device = (currentDevice != null) ? currentDevice.name() : "none";
        print("SET Device - " + this.Device);
    }

    public String getDeviceBuild()
    {
        return DeviceBuild;
    }

    public void setDeviceBuild()
    {
        this.DeviceBuild = (currentDeviceConfig != null) ? currentDeviceConfig.name() : "none";
        print("SET DeviceBuild - " + this.DeviceBuild);
    }

    public String getRunDate()
    {
        return RunDate;
    }

    public void setRunDate(String RunDate)
    {
        this.RunDate = RunDate;
        print("SET RunDate - " + this.RunDate);
    }

    public String getRunDuration()
    {
        return RunDuration;
    }

    public String getStatus()
    {
        return Status;
    }

    public String getFailReason()
    {
        return FailReason;
    }

    public void setFailReason(String FailReason)
    {
        this.FailReason = FailReason;
        print("SET FailReason - " + this.FailReason);
    }

    public String getFailError()
    {
        return FailError;
    }

    public String getReportPath()
    {
        return ReportPath;
    }

    public void setReportPath(String ReportPath)
    {
        this.ReportPath = ReportPath;
        print("SET ReportPath - " + this.ReportPath);
    }

    public String getProjectID_FK()
    {
        return ProjectID_FK;
    }

    public String getPC_Name()
    {
        return PC_Name;
    }

    public void setPC_Name(String PC_Name)
    {
        this.PC_Name = PC_Name;
        print("SET PC_Name - " + this.PC_Name);
    }
    // </editor-fold>
//

    public static String getComputerName()
    {
        String hostname = "";

        try
        {
            InetAddress address;
            address = InetAddress.getLocalHost();
            hostname = address.getHostName();
        } catch (UnknownHostException ex)
        {
            System.out.println("Hostname can not be resolved - " + ex.getMessage());
        }

        return hostname;
    }

    private void print(String msg)
    {
        System.out.println("[DB_TestResult_Logging] - " + msg);
    }
}
