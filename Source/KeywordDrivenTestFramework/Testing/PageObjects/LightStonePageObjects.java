/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author fnell
 */
public class LightStonePageObjects extends BaseClass {

	public static String lightstoneStagingURL() {
		return Enums.Environment.LightStone.pageUrl;
	}

	// Xpaths for the login tab
	public static String username() {
		return "//input[@id='idnumber']";
	}

	public static String password() {
		return "//input[@id='password'] ";
	}

	public static String clickLogin() {
		return "//input[@id='submit'] ";
	}

	public static String searchCriteria() {
		return "//div[@id='headingOne']";
	}

	public static String loginErrorMessage() {
		return "//div[@class='login_message']";
	}

	// Xpaths for the E_Application tab
	public static String idNumberLabel() {
		return "//label[@id='customerIdNumberTitle']";
	}

	public static String applicationFormTab() {
		return "//div[@id='eAppFormWidgetContent']";
	}

	public static String applicationDropDown() {
		return "//a[text()='Application']";
	}

	public static String newApplication() {
		return "//a[@target='_blank']";
	}

	public static String applicationFirstname() {
		return "//input[@id='customerFirstName']";
	}

	public static String title() {
		return "//span[contains(text(),'DVT DVT')]";
	} // span[contains(text(),'DVT DVT')]

	public static String applicationSurname() {
		return "//input[@id='customerSurname']";
	}

	public static String applicationIdNumber() {
		return "//input[@id='customerIdNumber']";
	}

	public static String applicationVerifiedDropDown() {
		return "//select[@id='customerIdVerified']";
	}

	public static String clickAbsaBank() {
		return "//input[@id='isAbsa']";
	}

	public static String clickMfc() {
		return "//input[@id='isMfc']";
	}

	public static String clickStandardBank() {
		return "//input[@id='isStandardBank']";
	}

	public static String clickWesbank() {
		return "//input[@id='isWesbank']";
	}

	public static String indicationTypeDropDown() {
		return "//select[@id='customerIdType']";
	}

	public static String driversLicenseDropDown() {
		return "//select[@id='customerDriversLicenseVerified']";
	}

	public static String absaDealerCodeDropDown() {
		return "//select[@id='dealerCodeABSA']";
	}

	public static String mfcDealerCodeDropDown() {
		return "//select[@id='dealerCodeMFCFox']";
	}

	public static String standardBankDealerCodeDropDown() {
		return "//select[@id='dealerCodeSBSA']";
	}

	public static String wesBankDealerCodeDropDown() {
		return "//select[@id='dealerCodeWesbank']";
	}

	public static String branchDropDown() {
		return "//select[@id='dealerJVBranchCode']";
	}

	/* xpaths for SBSAInternalDetails */
	public static String internalDetailsTab() {
		return "//div[@id='sbsaInternalDetailsPanel']";
	}

	public static String payoutDealerCode() {
		return "//input[@id='sbsaOnBehalfOfDealerCode']";
	}

	public static String payoutDealerName() {
		return "//input[@id='sbsaOnBehalfOfDealerName']";
	}

	public static String accountExecutive() {
		return "//input[@id='sbsaAccountExecutive']";
	}

	public static String bdoNumber() {
		return "//input[@id='sbsaBDONumber']";
	}

	public static String marketSegmentDropDown() {
		return "//select[@id='marketSubsegment']";
	}

	public static String marketPlan() {
		return "//input[@id='sbsaMarketPlan']";
	}

	public static String mWisePremiumOptionsDropDown() {
		return "//select[@id='MWisePremiumOption']";
	}

	public static String mwiDropDown() {
		return "//select[@id='sbsaMWI']";
	}

	public static String mwiNumberOfYearsDropDown() {
		return "//select[@id='sbsaMWIPeriod']";
	}

	public static String vafBusinessUnit() {
		return "//select[@id='vafBranch']";
	}

	public static String originCode() {
		return "//select[@id='origin']";
	}

	public static String sbsaBranchIbt() {
		return "//select[@id='sbsaBranch']";
	}

	/* xpaths for Vehicle Details */
	public static String vehicleDetailsTab() {
		return "//div[@id='VehicleContent']";
	}

	public static String articleNewUsedDrop() {
		return "//label[text() = 'Article New/Used']/..//select";
	}

	public static String articleUsedOption() {
		return "//label[text() = 'Article New/Used']/..//select//option[text()= 'Used']";
	}

	public static String mmCodeLookUp() {
		return "//input[@id = 'vehicleLookup_']";
	}

	public static String manufacturerDropdown() {
		return "//select[@id = 'selectMake']";
	}

	public static String modelDropdown() {
		return "//select[@id = 'selectModel']";
	}

	public static String yearDropdown() {
		return "//select[@id = 'selectYear']";
	}

	public static String okButtonOnVehicleInfo() {
		return "//input[@id = 'cmdVehicleOk']";
	}

	public static String regTrackDropdown() {
		return "//select[@id ='regtrackIndicator']";
	}

	public static String purchasePriceTextfield() {
		return "//input[@id = 'purchasePrice']";
	}

	public static String actualKilometersTextfield() {
		return "//input[@id = 'kilometerReading']";
	}

	public static String kilometerCategoryLabel() {
		return "//label[text()='Kilometer Category']";
	}

	public static String chassisNumberTextfield() {
		return "//input[@id='chassisNo']";
	}

	public static String stockNumberTextfield() {
		return "//input[@id='stockNumber']";
	}

	public static String kilometersCategoryTextfield() {
		return "//input[@id = 'kilometerCategory']";
	}

	// xpaths for dealer extras
	public static String dealerExtrasTab() {
		return "//div[@id='dealerExtrasPanel']";
	}

	public static String addNewExtra() {
		return "//input[@id='addExtraButton']";
	}

	public static String description1DropDown() {
		return "//select[@id='extraDescription_1']";
	}

	public static String amount1() {
		return "//input[@id='extraAmtInclVat_1']";
	}

	public static String description2DropDown() {
		return "//select[@id='extraDescription_2']";
	}

	public static String amount2() {
		return "//input[@id='extraAmtInclVat_2']";
	}

	public static String description3DrowDown() {
		return "//select[@id='extraDescription_3']";
	}

	public static String amount3() {
		return "//input[@id='extraAmtInclVat_3']";
	}

	public static String total() {
		return "//input[@id='extrasAmount']";
	}

	// xpaths for finance details
	public static String financeDetailsTab() {
		return "//div[@id='financeDetailsPanel']";
	}

	public static String rateIndicatorDropDown() {
		return "//select[@id='rateIndicator']";
	}

	public static String residualBalloonPercentage() {
		return "//input[@id='residualPercent']";
	}

	public static String residualBalloonValue() {
		return "//input[@id='residualValue']";
	}

	public static String depositTrade() {
//        return "//input[@id='deposit']";
		return "//input[@id='cashDeposit']";
	}

	public static String cashDepositLabel() {
		return "//label[contains(text(),'Cash Deposit Amount')]";
	}

	public static String dealTypeWesbank() {
		return "//select[@id='dealType']";
	}

	public static String financeInitiationFees() {
		return "//select[@id='financeInitiationFees']";
	}

	// xpaths for absa premium collection details
	public static String absaPremiumCollectionTab() {
		return "//div[@id='absaPremium']";
	}

	public static String absaPremiumCollectionDropDown() {
		return "//select[@id='ddPremiumVendor']";
	}

	public static String absaPremiumVendorDropDown() {
		return "//select[@id='PremiumVendorCode']";
	}

	// xpaths for dealer vaps
	public static String dealerVapsTab() {
		return "//div[contains(@id,'dealerVAPContent')]";
	}

	public static String dofRegistrationDropDown() {
		return "//input[@id='firstRegistrationDate']";
	}

	public static String monthDropDown() {
		return "//select[@class='ui-datepicker-month']";
	}

	public static String yearDropDown() {
		return "//select[@class='ui-datepicker-year']";
	}

	public static String selectDay() {
		return "//a[@class='ui-state-default']";
	}

	public static String manufacturerWarrantyDropDown() {
		return "//select[@id='isCheckManufacturerWarranty']";
	}

	public static String addButton() {
		return "//input[@id='addVap']";
	}

	public static String categoryDropDown() {
		return "//select[@id='vapCategory_0']";
	}

	public static String supplierDropDown() {
		return "//select[@id='vapSupplier_0']";
	}

	public static String productDropDown() {
		return "//select[@id='vapProduct_0']";
	}

	public static String productOptionDropDown() {
		return "//select[@id='vapProductOption_0']";
	}

	// xpaths for personal details
	public static String personalDetailsTab() {
		return "//div[@id='personalDetailsPanel']";
	}

	public static String raceDropDown() {
		return "//select[@id='customerRaceEthnicGroup']";
	}

	public static String titleDropDown() {
		return "//select[@id='customerTitle']";
	}

	public static String maritalStatusDropDown() {
		return "//select[@id='customerMaritalStatus']";
	}

	public static String mobileNumber() {
		return "//input[@id='customerMobilePhoneNumber']";
	}

	public static String mobileTypeDropDown() {
		return "//select[@id='mobileType']";
	}

	public static String phoneTypeDropDown() {
		return "//select[@id='customerWorkPhoneNumberType']";
	}

	public static String workNumber() {
		return "//input[@id='customerWorkPhoneNumber']";
	}

	public static String emailAdress() {
		return "//input[@id='customerEmail']";
	}

	public static String countryOfBirthDropDown() {
		return "//select[@id='customerCountryOfBirth']";
	}

	public static String countryOfNationality() {
		return "//select[@id='customerNationality']";
	}

	public static String countryOfResidentialAdressDropDown() {
		return "//select[@id='residentialAddressCountry']";
	}

	public static String taxObligations() {
		return "//select[@id='isForeignOrTaxObligation']";
	}

	public static String multipleNationalitiesDropDown() {
		return "//select[@id='isMultipleNationalities']";
	}

	public static String wasSACitizenDropDown() {
		return "//select[@id='wasSouthAfricanCitizen']";
	}

	public static String countryOfCitizenship() {
		return "//select[@id='customerCountryOfCitizenship']";
	}

	public static String clientType() {
		return "//select[@id='mfcClientType']";
	}

	// xpaths for residential details
	public static String residentialDetailsTab() {
		return "//div[@id='resDetailsPanel']";
	}

	public static String address1() {
		return "//input[@id='customerResidentialAddressLine1']";
	}

	public static String suburb() {
		return "//input[@id='customerResidentialAddressLookup']";
	}

	public static String searchSuburb() {
		return "//input[@id='searchSuburb']";
	}

	public static String searchButton() {
		return "//input[@id='cmdSearchSuburb']";
	}

	public static String selectSuburbDropDown() {
		return "//select[@id='selectSuburb']";
	}

	public static String selectCodeDropDown() {
		return "//select[@id='selectCode']";
	}

	public static String selectOK() {
		return "//input[@id='cmdAddressOk']";
	}

	public static String postalAdressDropDown() {
		return "//select[@id='sameAsRes']";
	}

	public static String periodYear() {
		return "//input[@id='customerPeriodAtCurrentAddressYears']";
	}

	public static String periodMonth() {
		return "//input[@id='customerPeriodAtCurrentAddressMonths']";
	}
	///////////////////////////////////////////////////////////////////////////

	// xpaths for employer details
	public static String employerDetailsTab() {
		return "//div[@id='employerDetailsPanel']";
	}

	public static String employerName() {
		return "//input[@id='employerName']";
	}

	public static String occupationDropDown() {
		return "//select[@id='customerOccupation']";
	}

	public static String employerIndustryTypeDropDown() {
		return "//select[@id='employerIndustryType']";
	}

	public static String employerType() {
		return "//select[@id='customerEmploymentStatus']";
	}

	public static String employerTypeList() {
		return "//select[@id='customerEmploymentStatus']//option";
	}

	public static String employerType(String type) {
		return "//select[@id='customerEmploymentStatus']//option[text()='" + type + "']";
	}

	public static String employerLevel() {
		return "//select[@id='employmentLevel']";
	}

	public static String employerClientType() {
		return "//select[@id='customerType']";
	}

	public static String employerAddress() {
		return "//input[@id='employerAddressLine1']";
	}

	public static String employerSuburbButton() {
		return "//input[@id='employerAddressLookup']";
	}

	public static String employerSuburbSearch() {
		return "//input[@id='searchSuburb']";
	}

	public static String employerSearchButton() {
		return "//input[@id='cmdSearchSuburb']";
	}

	public static String selectEmployerSuburbDropDown() {
		return "//select[@id='selectSuburb']";
	}

	public static String employerSuburbCodeDropDown() {
		return "//select[@id='selectCode']";
	}

	public static String employerButtonOK() {
		return "//input[@id='cmdAddressOk']";
	}

	public static String employerPeriodYear() {
		return "//input[@id='customerPeriodAtCurrentEmployerYears']";
	}

	public static String employerPeriodMonth() {
		return "//input[@id='customerPeriodAtCurrentEmployerMonths']";
	}
	///////////////////////////////////////////////////////////////////////////

	// xpaths for applicant income details
	public static String incomeDetailsTab() {
		return "//div[@id='applicantIncomeDetailsPanel']";
	}

	public static String grossRenumeration() {
		return "//input[@id='sumIncomeCustomerGrossRemuneration']";
	}

	public static String netIncome() {
		return "//input[@id='addIncomeCustomerNetTakeHomePay']";
	}

	public static String tmI() {
		return "//input[@id='customerTotalMonthlyIncome']";
	}
	///////////////////////////////////////////////////////////////////////////

	// xpaths for applicant expenses
	public static String applicantExpensesTab() {
		return "//div[@id='applicantExpensesPanel']";
	}

	public static String rent() {
		return "//input[@id='sumExpensesCustomerRent']";
	}

	public static String telephonePayments() {
		return "//input[@id='sumExpensesCustomerTelephonePayments']";
	}

	public static String clothingAccounts() {
		return "//input[@id='sumExpensesCustomerClothingAccounts']";
	}

	public static String educationCost() {
		return "//input[@id='sumExpensesCustomerEducationCosts']";
	}

	public static String customerTotalExpenses() {
		return "//input[@id='customerTotalExpenses']";
	}

	public static String foodAndEntertainmentLabel() {
		return "//label[contains(text(),'Food and Entertainment')]";
	}
	///////////////////////////////////////////////////////////////////////////

	// xpaths for source of funds
	public static String sourceOfFundsTab() {
		return "//div[@id='tblSourceOfFunds']";
	}

	public static String pension() {
		return "//input[@id='sourceOfFundsPension']";
	}

	public static String salary() {
		return "//input[@id='sourceOfFundsSalary']";
	}

	public static String beeIncome() {
		return "//input[@id='sourceOfFundsBEE']";
	}

	public static String pensionAmount() {
		return "//input[@id='sourceOfFundsPensionAmount']";
	}

	public static String beeAmount() {
		return "//input[@id='sourceOfFundsBEEAmount']";
	}
	//////////////////////////////////////////////////////////////////////////

	// xpaths for payment history
	public static String paymentHistoryTab() {
		return "//div[@id='paymentHistoryPanel']";
	}

	public static String debtReviewDropDown() {
		return "//select[@id='debtReviewIndicator']";
	}

	public static String debtCounsellingDropDown() {
		return "//select[@id='debtCounselling']";
	}

	public static String curatorBonusDropDown() {
		return "//select[@id='administrationOrderIndicator']";
	}

	public static String adminOrderDropDown() {
		return "//select[@id='DebtAdminHist']";
	}

	public static String judgementDropDown() {
		return "//select[@id='previousJudgementIndicator']";
	}

	public static String creditBureauDropDown() {
		return "//select[@id='DebtDisp']";
	}

	public static String sequastrationOrderDropDown() {
		return "//select[@id='debtSeqOrder']";
	}
	///////////////////////////////////////////////////////////////////////////

	// xpaths for relative details
	public static String relativeDetailsTab() {
		return "//div[@id='relativeDetailsPanel']";
	}

	public static String relativeFirstname() {
		return "//input[@id='relativeFirstNames']";
	}

	public static String relativeSurname() {
		return "//input[@id='relativeSurname']";
	}

	public static String relationDropDown() {
		return "//select[@id='relativeRelation']";
	}

	public static String preferedContactMethodDropDown() {
		return "//select[@id='relativePreferredContactMethod']";
	}

	public static String relativeAddress() {
		return "//input[@id='relativeAddressLine1']";
	}

	public static String relativeCellNumber() {
		return "//input[@id='relativeCellphone']";
	}

	public static String relativeSuburb() {
		return "//input[@id='relativeAddressLookup']";
	}

	public static String relativeSuburbSearch() {
		return "//input[@id='searchSuburb']";
	}

	public static String relativeSearchButton() {
		return "//input[@id='cmdSearchSuburb']";
	}

	public static String relativeSuburbDropDown() {
		return "//select[@id='selectSuburb']";
	}

	public static String relativePostalCodeDropDown() {
		return "//select[@id='selectCode']";
	}

	public static String relative_Suburb() {
		return "//input[@id='relativeAddressSuburb']";
	}

	public static String relativeCityTown() {
		return "//input[@id='relativeAddressCity']";
	}

	public static String relativeOK() {
		return "//input[@id='cmdAddressOk']";
	}

	///////////////////////////////////////////////////////////////////////////
	// xpaths for banking details
	public static String bankingDetailsTab() {
		return "//*[@id='bankingDetailsPanel']";
	}

	public static String accountHolder() {
		return "//input[@id='customerBankAccountHolder']";
	}

	public static String bankNameDropDown() {
		return "//select[@id='customerBankAccountBank']";
	}

	public static String bankBranch() {
		return "//input[@id='customerBankAccountBranchName']";
	}

	public static String validAccountNumber() {
		return "//select[@id='validBankAccount']";
	}

	public static String accountNumber() {
		return "//input[@id='customerBankAccountNumber']";
	}

	public static String accountType() {
		return "//select[@id='customerBankAccountType']";
	}

	public static String branchCode() {
		return "//input[@id='customerBankAccountBranchCode']";
	}

	public static String paymentMethod() {
		return "//select[@id='paymentMethod']";
	}

	public static String settleExisting() {
		return "//select[@id='settleExistingInstallment']";
	}
	//////////////////////////////////////////////////////////////////////////

	// xpaths for statement
	public static String statementTab() {
		return "//div[@id='statementPanel']";
	}

	public static String statementDeliveryMethod() {
		return "//select[@id='customerStatementDeliveryMethod']";
	}

	public static String statementDeliveryMethodList() {
		return "//select[@id='customerStatementDeliveryMethod']//option";
	}
	///////////////////////////////////////////////////////////////////////////

	// xpaths for application details
	public static String applicationDetailsTab() {
		return "//div[@id='appDetailsPanel']";
	}

	public static String shareInformation() {
		return "//select[@id='marketingConsentIndicator']";
	}

	public static String sharingInformationToAlpheraGroup() {
		return "//select[@id='insuranceConsentIndicator']";
	}

	///////////////////////////////////////////////////////////////////////////
	// xpaths for secure x details
	public static String secureXTab() {
		return "//div[@id='application_data']";
	}

	public static String bankStatement() {
		return "//select[@id='customerBankStatementConsentIndicator']";
	}

	public static String incomeVerification() {
		return "//select[@id='customerIncomeVerificationConsentIndicator']";
	}

	// xpaths for mfc my contract details
	public static String myContractTab() {
		return "//div[@id='myContractMFCPanel']";
	}

	public static String myContract() {
		return "//select[@id='myContractIndicator']";
	}

	// xpaths for F & I information
	public static String informatioTab() {
		return "//div[@id='fandIDetailsPanel']";
	}

	public static String registeredOfficialName() {
		return "//select[@id='registeredOfficial']";
	}

	public static String submitApplication() {
		return "//input[@id='cmdSubmit']";
	}

	public static String pageLoaderArticle() {
		return "//article[@id='pageloader']";
	}

	public static String maskSweetOverlay() {
		return "//div[@class='sweet-overlay']";
	}

	public static String success() {
		return "//button[@class= 'confirm']";
	}

	public static String consentModal() {
		return "//span[text()='No Consent given for insurance leads']";
	}

	public static String okConsentModal() {
		return "//button//span[text()='Ok']";
	}

	public static String proofOfResidence() {
		return "//select[@id='proofOfResidenceConsentIndicator']";
	}

	public static String logoutButtonXpath() {
		return "//a//i[@class='fa fa-sign-out']/..";
	}

	public static String mask_InboxScreen() {
		return "//div[@class='spinner middle'][@style='display: block;']";
	}

	public static String boardRoomLogoXpath() {
		return "//img[@id='navbarDealerLogo']";
	}

	public static String boardroomLogoEnlargeXpath() {
		return "//div[@id='navigationModalContent']//img";
	}

	public static String logoModalCloseButtonXpath() {
		return "//div[@id='navigationModalContent']//button[text()='Close']";
	}

	public static String tableInboxColumnXpath(String columnName) {
		return "//thead//th[text()='" + columnName + "']";
	}

	public static String tableInboxListPerColumn(String columnName, int index) {
		return tableInboxColumnXpath(columnName) + "/../../..//tbody//tr//td[" + index + "]";
	}

	public static String columnButtonXpath(String columnName, int index) {
		return tableInboxListPerColumn(columnName, index) + "/input";
	}

	public static String columnLinkXpath(String columnName, int index) {
		return tableInboxListPerColumn(columnName, index) + "/a";
	}

	public static String podiumValidation(String podiumName) {// be careful if changing this xpath.
		return "//*[contains(text(), '" + podiumName + " ')] | " + "//*[contains(text(), '" + podiumName.toLowerCase()
				+ " ')] |" + " //*[contains(text(), '" + LightstoneScriptingInstance.ucFirst(podiumName) + " ')]";
	}

	public static String leadsPageTitle() {
		return "//title[contains(text(),'Leads Results')]";
	}// return "//title[contains(text(),'Tracking Installation')]";

	public static String showDealsForLast() {
		return "//select[@id='numberOfDaysTarget']";
	}

	public static String searchByXpath() {
		return "//select[@id='searchTarget']";
	}

	public static String criteriaField() {
		return "//input[@id='searchParameter']";
	}

	public static String searchButtonInboxFilter() {
		return "//button[@id='searchButton']";
	}

	public static String suppDocs(String columnName, int index) {
		return tableInboxListPerColumn(columnName, index) + "/a";
	}

	public static String costSheet() {
		return "//span[text() ='Costing Sheet']";
	}

	public static String clearSearchButton() {
		return "//button[text()='Clear']";

	}

	public static String submitCostSheetModal() {
		return costSheet() + "/../..//div[@id='costingSheetModal']//input[@value='Submit']";
	}

	public static String confirmOverrideCostSheetModal() {
		return "//span[text()='Confirm Override?']";
	}

	public static String yesConfirmOverrideCostSheetModal() {
		return confirmOverrideCostSheetModal() + "/../..//button[text() = 'Yes']";
	}

	public static String compareModal() {
		return "//span[text()='Compare Selector']";
	}

	public static String compareModalCompareButton() {
		return compareModal() + "/../..//input[@value = 'Compare']";
	}

	public static String supportDropdownXpath() {
		return "//li/a[contains(text(), 'Support')]";
	}

	public static String helpItem() {
		return "//li/a[text()= 'Help']";
	}

	public static String aContainsText(String text) {
		return "//a[contains(text(), '" + text + "')]";
	}

	public static String helpPanel(String text) {
		return "//h4" + aContainsText(text);
	}

	public static String inboxMenu() {
		return aContainsText("Inbox");
	}

	public static String termsAndConditions() {
		return "//a/h6[text()='Terms and Conditions']";
	}

	public static String embeddedTerms() {
		return "//embed[@src='http://www.lightstoneauto.co.za/downloads/Lightstone-Auto-Usage-Agreement.pdf']";
	}

	public static String archiveMenu() {
		return aContainsText("Archive");
	}

	public static String signioArchivePage() {
		return "//h1/em[text()='Signio Archive']";
	}

	public static String faqForVAPSLink() {
		return "//span[text()='FAQ'S FOR VAPS.PDF']/..";
	}

	public static String columnExtactValue(int index) {
		return "//input[contains(translate(@id, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'leads')]/../../td["
				+ index + "]";
//        return "//input[contains(@id,'leads')]/../../td[" + index + "]";
	}

	public static String supportDocumentTitle() {
		return "//title[text() ='Supporting Documents']";
	}

	public static String submitDocuments() {
		return "//a[text()='Submit Document']";
	}

	public static String CostSheetPage() {
		return "//*[contains(text(), 'Costing Sheet')]";
	}

	public static String podiumComparePage() {
		return "//*[contains(text(), 'Podium Change')]";
	}

	public static String DocPage() {
		return "//*[contains(text(), 'DOC Details')]";
	}

	private static String pageHandle;

	public static String getPageHandle() {
		return pageHandle;
	}

	public static void setPageHandle(String pageHandle) {
		LightStonePageObjects.pageHandle = pageHandle;
	}

	public static String wesbankReApplication() {
		return "//a[contains(text(),'WesBank Re-Application')]";
	}

	public static String wesbank_Re_ApplicationHeader() {
		return "//input[@id='clientIdNumber']";
	}

	public static String clientIdNumber() {
		return "//input[@id='clientIdNumber']";
	}

	public static String fiFirstName() {
		return "//input[@id='fiFirstName']";
	}

	public static String fiSurName() {
		return "//input[@id='fiSurname']";
	}

	public static String fiContactNumber() {
		return "//input[@id='phone']";
	}

	public static String fiFaxNumber() {
		return "//input[@id='fax']";
	}

	public static String fiEmailAddress() {
		return "//input[@id='fiEmailAddress']";
	}

	public static String fiIdNumber() {
		return "//input[@id='fiIdNumber']";
	}

	public static String fiNotificationCriteria() {
		return "//select[@id='returnAnswerVia']";
	}

	public static String fiUserType() {
		return "//select[@id='fiUserType']";
	}

	public static String fiBranchCode() {
		return "//select[@id='dealerBranch']";
	}

	public static String fiSearchButton() {
		return "//input[@id='searchButton']";
	}
	/////////////////////////////////////////////////////////////////////////
	// Xpaths for the archive tab

	public static String archive() {
		return "//a[contains(text(),'Archive')]";
	}

//    public static String signioArchiveSearch(String textField)
//    {
//        return "//input[@id= '" + textField + "']";
//    }
	public static String signioArchiveIdNumber() {
		return "//input[@id= 'idNumber']";
	}

	public static String signioArchiveClientName() {
		return "//input[@id= 'clientName']";
	}

	public static String signioArchiveClientSurname() {
		return "//input[@id= 'clientSurname']";
	}

	public static String signioArchiveMobileNumber() {
		return "//input[@id= 'mobileNumber']";
	}

	public static String signioArchiveEmailAddress() {
		return "//input[@id= 'emailAddress']";
	}

	public static String signioArchiveChassisNumber() {
		return "//input[@id= 'vinChassis']";
	}

	public static String signioArchiveStockNumber() {
		return "//input[@id= 'stockNumber']";
	}

	public static String signioArchiveReagistrationNumber() {
		return "//input[@id= 'registrationNumber']";
	}

	public static String signioLogo() {
		return "//div[@id='signioLogo']/img";
	}

	public static String dateRangeCheckBox() {
		return "//input[@id='dateRange']";
	}

	public static String searchArchive() {
		return "//input[@id='cmdSearch']";
	}

	public static String signioArchiveFromDate() {
		return "//input[@id= 'fromDate']";
	}

	public static String signioArchiveToDate() {
		return "//input[@id= 'toDate']";
	}

	public static String searchResults(String idNumber) {
		return "//a[text()='" + idNumber + "']/../..//a[text()='Open']";
	}// a[text()='7208085654087']/../..//a[text()='Open']

	public static String searchResultsGenericRow() {
		return "//tr[@class='even']";
	}

	public static String archiveProxyError() {
		return "//h1[text()='Proxy Error']";
//        return "//div[text()='HTTP ERROR 504']";
	}

	public static String archive_DateResult_DateCannotBeGreater() {
		return "//div[contains(text(),'be greater than the To date!')]";
	}

	public static String submitDocumentTab() {
		return "//a[@id='submissions']";
	}

	public static String clickIDNumber(String idNumber) {
		return "//a[text()='" + idNumber + "']";
	}

	public static String pdfFileOpener(String uniqueID) {
		return "//..//.//div[@id='subDiv_" + uniqueID + "']/table/tbody/tr/td[3]/a";
	}// ..//.//div[@id="subDiv_1112018091409550006"]/table/tbody/tr/td[3]/a

	public static String dealFileId(String uniqueID) {
		return "//td//b[contains(text(),'Deal File ID')]/../..//span[contains(text(),'" + uniqueID + "')]";
	}

	public static String pdfApplicationFormTab() {
		return "//title[text()='V5 Application Form']";
	}

	// td//b[contains(text(),'Deal File
	// ID')]/../..//span[contains(text(),'1112018091409550006')]
	// -----------------------------//
	// MY PROFILE //
	// -----------------------------//
	public static String mySignioDropDown() {
		return "//a[contains(text(),'My Signio')]";
	}

	public static String myProfileDropDown() {
		return "//a[contains(text(),'My Profile')]";
	}

	public static String myPreferencesDropDown() {
		return "//a[contains(text(),'My Preferences')]";
	}

	public static String eComplianceHeader() {
		return "//div[@id='navbar']";
	}

	public static String profileTab() {
		return "//a[@id='profileTabLink1']";
	}

	public static String identificationType() {
		return "//select[@id='fiIdType']";
	}

	public static String identificationNumber() {
		return "//input[@id='fiIdNumber']";
	}

	public static String firstName() {
		return "//input[@id='fiFirstName']";
	}

	public static String lastName() {
		return "//input[@id='fiLastName']";
	}

	public static String personalDetailSaveButton() {
		return "//button[@id='btnSavePersonalDetails']";
	}

	public static String manageMyPreferences() {
		return "//h5[@id='myModalPreferenceLabel']";
	}

	public static String myBoardroomPreferences() {
		return "//a[@class='collapsed']";
	}

	public static String regtrackLater(String val) {
		return "//select[@id='regtrackIndicator']//option[@value='" + val + "']";
	}

	public static String carTypedropdown() {
		return "//select[@id='selectCondition']";
	}

	public static String carTypedropdownClick() {
		return "//select[@id='selectCondition']//option[text()='Used']";
	}// select[@id='selectCondition']//option[text()='Used']

	public static String mnMCode() {
		return "//div[contains(text(),'M&M Code:')]/..//input";
	}// input[@id='searchMMCode']

	public static String selectVehicleButton() {
		return "//input[@id='vehicleLookup_']";
	}

	public static String searchMMcode() {
		return "//input[@id='cmdMMCode']";
	}

	public static String ok() {
		return "//input[@id='cmdVehicleOk']";
	}

	public static String searchCriteriaPod() {
		return "//a[contains(.,'Search Criteria')]";
	}

	/////////////////////////////////////
	// Xpaths for podium
	public static String criteria() {
		return "//input[@id='searchParameter']";
	}

	public static String searchByDropdown() {
		return "//select[@id='searchTarget']";
	}

	public static String searchByClick() {
		return "//select[@id='searchTarget']//option[@value='ID Number']";
	}

	public static String searchByClick(String searchBy) {
		return "//select[@id='searchTarget']//option[@value='" + searchBy + "']";
	}

	public static String searchBtn() {
		return "//button[@id='searchButton']";
	}

	public static String PodiumClick() {
//        return "//div[@id='bankDiv_7074902_2018093580100063031']/../input[@compareorigid='3']";
		return "//div/../input[@compareorigid='3']";
	}

	public static String okBtn() {
		return "//span[@class='ui-button-text' and contains(text(),'Ok')]";
	}

	// div/../input[@compareorigid='3']
	// -----------------------------//
	// DOC //
	// -----------------------------//
	public static String myMerchant() {
		return "//a[contains(text(),'My Merchant')]";
	}

	public static String myProfile() {
		return "//a[contains(text(),'My Profile')]";
	}

	public static String signioValidation() {
		return "//strong[text()='SIGNIO (PTY) LTD']";
	}

	public static String changeMerchantHeader() {
		return "//h4[@class='panel-title']";
	}

	public static String currentMerchant(String merchant) {
		return "//label[contains(text(),'" + merchant + "')]//input[@id='selectedInstitution']";
	}

	public static String selectInstitution() {
		return "//input[@id='selectMerch']";
	}

	public static String epsonMotorsLabel() {
		return "//div[@class='text-right']//p//strong[contains(text(),'EPSON MOTORS')]";
	}

	public static String epsonMototsValidation() {
		return "//strong[text()='EPSON MOTORS']";
	}

	public static String myPreferenceValidation() {
		return "//a[text()='My Preferences']";
	}

	public static String docCurrentMonthValidation() {
		return "//div[text()='DOC Current Month']";
	}

	public static String docDropDown() {
		return "//a[contains(text(),'DOC')]";
	}

	public static String monthlyDoc() {
		return "//a[contains(text(),'Monthly DOC')]";
	}

	public static String recurringIncome() {
		return "//a[contains(text(),'Recurring Income')]";
	}

	public static String manageSalesPersons() {
		return "//a[contains(text(),'Manage Salespersons')]";
	}

	public static String currentMonthDOC() {
		return "//div[@id='currentMonthHeader']";
	}

	public static String previousMonthDOC() {
		return "//div[@id='prevMonthHeader']";
	}

	public static String salesPersonsHeader() {
		return "//div[@id='salespersonsHeader']";
	}

	// -----------------------------//
	// MONTHLY DOC //
	// -----------------------------//
	public static String searchClientName() {
		return "//input[@id='clientNameSearchFilterValue'][@type='text']";
	}

	public static String searchClientSurname() {
		return "//input[@id='clientSurnameSearchFilterValue'][@type='text']";
	}

	public static String searchClientIDNumber() {
		return "//input[@id='clientIDNumberSearchFilterValue'][@type='text']";
	}

	public static String searchBankName() {
		return "//select[@id='bankSearchFilterValue']";
	}

	public static String searchDealStatus() {
		return "//select[@id='dealSearchFilterValue']";
	}

	public static String searchReferenceNumber() {
		return "//input[@id='referenceNumberSearchFilterValue'][@type='text']";
	}

	public static String clearSearch() {
		return "//input[@id='clear']";
	}

	public static String searchCriteriaButton() {
		return "//input[@id='Search']";
	}

	public static String searchLoadMask() {
		return "//div[@class='loadmask-msg']";
	}

	public static String resultRow() {
		return "//tr[contains(@class,'docRecord')]";
	}

	public static String fAndI() {
		return "//select[@id='fAndISearchValue']";
	}

	public static String displayTotal() {
		return "//select[@id='max']";
	}

	public static String paginationIndex() {
		return "//div[@class='pagination']//a[@class='step'][1]";
	}

	public static String addNew() {
		return "//input[@class='osx formee-button']";
	}

	public static String captureDocHeader() {

		return "//div[@id='osx-modal-title']";
	}

	public static String clientDetailsHeader() {

		return "//div[@id='clientDetailsHeader']";
	}

	public static String validRSAID() {

		return "//div[@id='clientDetailsHeader']";
	}

	public static String clientName() {

		return "//input[@id='clientName']";
	}

	public static String clientSurname() {

		return "//input[@id='clientSurname']";
	}

	public static String clientID() {

		return "//input[@id='clientID']";
	}

	public static String clientEmail() {

		return "//input[@id='customerEmail']";
	}

	public static String clientCellPhone() {

		return "//input[@id='customerCell']";
	}

	public static String clientworkPhone() {

		return "//input[@id='customerWorkTel']";
	}

	public static String isCompany() {

		return "//input[@id='isCompany']";
	}

	public static String companyName() {

		return "//input[@id='companyName']";
	}

	public static String companyRegistrationNumber() {

		return "//input[@id='registrationNumber']";
	}

	public static String vehicleDetailsHeader() {

		return "//div[@id='vehicleDetailsHeader']";
	}

	public static String articleCondition() {

		return "//select[@id='articleCondition']";
	}

	public static String yearModel() {

		return "//select[@id='vehicleYear']";
	}

	public static String vehicleMake() {

		return "//select[@id='vehicleMake']";
	}

	public static String vehicleModel() {

		return "//select[@id='vehicleModel']";
	}

	public static String mmCode() {

		return "//input[@id='mmCode']";
	}

	public static String retailPrice() {

		return "//input[@id='retailValue']";
	}

	public static String costPrice() {

		return "//input[@id='vehicleCostPrice']";
	}

	public static String overAllowance() {

		return "//input[@id='overAllowance']";
	}

	public static String factoryClaim() {

		return "//input[@id='factoryClaim']";
	}

	public static String purchasePrice() {

		return "//input[@class='valid'][@id='purchasePrice']";
	}

	public static String vehicleAccessories() {

		return "//div[@id='vehicleAccessoriesHeader']";
	}

	public static String accessoriesRetailPrice() {

		return "//input[@id='accessoriesRetailPrice']";
	}

	public static String accessoriesCostPrice() {

		return "//input[@id='accessoriesCostPrice']";
	}

	public static String docDetailsHeader() {

		return "//div[@id='captureDocDetailsHeader']";
	}

	public static String sightedCheckBox() {

		return "//input[@id='isSighted']";
	}

	/**
	 * ************************************************************************
	 *************************** 
	 * @ASwaphi*************************************** ************************************************************************
	 */
	public static String vehicleDetailsTabHeader() {
		return "//div[@name='vehicleDetails']";
	}

	public static String submitPodiumApplication() {
		return "//input[@id='submitButton']";
	}

	public static String podiumSubmitionTextMessage() {
		return "//label[text()='Podium Successfully Submitted!']";
	}

	public static String purchasingPrice() {
		return "//select[@id= 'articleUse']";
	}

	public static String selectVehicleUse(String vehicleUse) {
		return "//select[@id= 'articleUse']//option[text()='" + vehicleUse + "']";
	}// select[@id= 'articleType']//option[text()='Private']

	public static String cashPrice() {
		return "//label[contains(text(),'Cash Price INCL VAT ')]/..//input";
	}

	public static String actualKilometers() {
		return "//label[contains(text(),'Actual Kilometers ')]/..//input";
	}

	public static String chassisNumber() {
		return "//label[contains(text(),'Chassis Number')]/..//input";
	}

	public static String regNumber() {
		return "//label[contains(text(),'Registration Number')]/..//input";
	}

	public static String podiumFinancialDetailsTab() {
		return "//div[contains(text(),'Finance Details')]";
	}

	public static String podiumResidentialPercentage() {
		return "//label[text()='Residual/Balloon % ']/..//input";
	}

	public static String podiumResidentialValue() {
		return "//label[text()='Residual/Balloon Amount ']/..//input";
	}

	public static String podiumDeposit() {
		return "//label[text()='Deposit']/..//input";
	}

	public static String podiumVehicleExtrasTab() {
		return "//div[contains(text(),'Vehicle Extras')]";
	}

	public static String podiumAddNewExtra() {
		return "//input[@id='addExtraButton']";
	}

	public static String selectFogLights() {
		return "//select[@id='extraDescription_1']";
	}

	public static String fogLightsAmount() {
		return "//input[@id='extraAmtInclVat_1']";
	}

	public static String selectBootSpoilers() {
		return "//select[@id='extraDescription_1']";
	}

	public static String bootSpoilerAmount() {
		return "//input[@id='extraAmtInclVat_1']";
	}

	public static String selectEngineUpgrade() {
		return "//select[@id='extraDescription_1']";
	}

	public static String EngineUpgradeAmount() {
		return "//input[@id='extraAmtInclVat_1']";
	}

	public static String podiumHeaders(String headerName) {
		return "//div[contains(text(),'" + headerName + "')]";
	}// div[contains(text(),'Dealer VAPs')]

	public static String addNewDealerVap() {
		return "//input[@id='addDealerVapButton']";
	}

	public static String selectionActionDescription() {
		return "//select[@id='absaVapCategoryCode_1']";
	}

	public static String searchDateErrorMessage() {
		return "//h1[contains(text(),'Search')]/../div";
	}

	public static String searchIdErrorMessage() {
		return "//div[text()='You need to enter a search criteria!']";

	}

	public static String zeroResults() {
		return "//div[text()='Results: 0']";
	}

	/**
	 * ************************************************************************
	 *************************** 
	 * @ASwaphi*************************************** ************************************************************************
	 */
	public static String podiumRent() {
		return "//input[@id='totalCustomerBondAndRentPayment']";
	}

	public static String podiumRelativeCellphone() {
		return "//label[text()='Contact Number']/..//input";
	}

	public static String podiumBankingDetails() {
		return "//div[@id='bankingDetails']";
	}

	public static String bankBrachText() {
		return "//a[text()='632005 : Universal']";
	}

	public static String podiumSalesPerson() {
		return "//label[text()='Sales Person']/..//input";
	}

	public static String podiumMarketingPurposes() {
		return "//select[@id='marketingConsentIndicator']";
	}

	public static String mmCodeClick() {
		return "//input[@id='vehicleLookup_']";

	}

	public static String repeatCustomer() {

		return "//input[@id='isRepeatCustomer']";
	}

	public static String docBankName() {

		return "//select[@id='financeBank']";
	}

	public static String dealer() {

		return "//select[@id='merchantID']";
	}

	public static String fAndIName() {

		return "//select[@id='fiName']";
	}

	public static String typeOfSale() {

		return "//select[@id='typeOfSale']";
	}

	public static String jointVenture() {

		return "//select[@id='jv']";
	}

	public static String stockNumber() {

		return "//input[@id='stockNumber']";
	}

	public static String interestRate() {

		return "//input[@id='interestRate']";
	}

	public static String principalDebt() {

		return "//input[@id='principalDebt']";
	}

	public static String docInvoiceAmount() {

		return "//input[@id='invoiceDOCAmount']";
	}

	public static String invoicedDate() {

		return "//input[@id='invoiceDate']";
	}

	public static String payoutDate() {

		return "//input[@id='payoutDate']";
	}

	public static String signedDate() {

		return "//input[@id='signedDate']";
	}

	public static String vehicleDeposit() {

		return "//input[@id='vehicleDeposit']";
	}

	public static String dic() {

		return "//input[@id='dic']";
	}

	public static String specialDic() {

		return "//input[@id='specialDic']";
	}

	public static String rateType() {

		return "//select[@id='rateIndicator']";
	}

	public static String incomeDetailsHeader() {

		return "//div[contains(text(),'Income Details')][@class='ui-widget-header']";
	}

	public static String creditLifeIncome() {

		return "//select[@id='creditLifeIncomeTick']";
	}

	public static String creditLifeIncomeAmount() {

		return "//input[@id='creditLifeIncome']";
	}

	public static String maintenancePlan() {

		return "//select[@id='maintenanceServicePlanIncomeTick']";
	}

	public static String maintenancePlanAmount() {

		return "//input[@id='maintenanceServicePlanIncome']";
	}

	public static String paintGlass() {

		return "//select[@id='paintGlassProtectionIncomeTick']";
	}

	public static String paintGlassAmount() {

		return "//input[@id='paintGlassProtectionIncome']";
	}

	public static String adminFee() {

		return "//input[@id='adminFeeIncome']";
	}

	public static String creditShortFall() {

		return "//select[@id='creditShortfallIncomeTick']";
	}

	public static String creditShortFallAmount() {

		return "//input[@id='creditShortfallIncome']";
	}

	public static String bodyInsurance() {

		return "//select[@id='bodyInsuranceIncomeTick']";
	}

	public static String bodyInsuranceAmount() {

		return "//input[@id='bodyInsuranceIncome']";
	}

	public static String tyreRim() {

		return "//select[@id='tyreRimWarrantyIncomeTick']";
	}

	public static String tyreRimAmount() {

		return "//input[@id='tyreRimWarrantyIncome']";
	}

	public static String warrantyIncome() {

		return "//select[@id='warrantyIncomeTick']";
	}

	public static String warrantyIncomeAmount() {

		return "//input[@id='warrantyIncome']";
	}

	public static String trackingIncome() {

		return "//select[@id='trackingIncomeTick']";
	}

	public static String trackingIncomeAmount() {

		return "//input[@id='trackingIncome']";
	}

	public static String inflationProctection() {

		return "//select[@id='inflationProtectorIncomeTick']";
	}

	public static String inflationProctectionAmount() {

		return "//input[@id='inflationProtectorIncome']";
	}

	public static String depositCover() {

		return "//select[@id='depositCoverIncomeTick']";
	}

	public static String depositCoverAmount() {

		return "//input[@id='depositCoverIncome']";
	}

	public static String roadCover() {

		return "//select[@id='roadCoverIncomeTick']";
	}

	public static String roadCoverAmount() {

		return "//input[@id='roadCoverIncome']";
	}

	public static String otherIncomeAmount() {

		return "//input[@id='otherIncome']";
	}

	public static String compInsBroker() {

		return "//input[@id='compInsuranceName']";
	}

	public static String compInsIncome() {

		return "//select[@id='compInsuranceIncomeTick']";
	}

	public static String compInsIncomeAmount() {

		return "//input[@id='compInsuranceIncome']";
	}

	public static String firstGrossAmount() {

		return "//input[@id='firstGrossIncome']";
	}

	public static String expenseDetailsHeader() {

		return "//div[contains(text(),'Expense Details')]";
	}

	public static String dealerAdjustmentAmount() {

		return "//input[@id='dealerAdjustment']";
	}

	public static String igfInvoiceFee() {

		return "//input[@id='invoiceCostIGF']";
	}

	public static String bankInvoiceFee() {

		return "//input[@id='invoiceCostBank']";
	}

	public static String totalExpenseExclVat() {

		return "//input[@id='invoiceCostBank']";
	}

	public static String totalExpenseInclVat() {

		return "//input[@id='invoiceCostBank']";
	}

	public static String createDOCButton() {

		return "//input[@value='Create DOC']";
	}

	/**
	 * ********************************* Recurring Income
	 * ********************************
	 */
	public static String recurringIncomeHeader() {
		return "//div[@id='recurringIncomeHeader']";
	}

	public static String addRecurringIncome() {
		return "//input[@id='DOCRecurringAdd']";
	}

	public static String recurringIncomeFI_BaseRow_Xpath = "";

	public static void setRecurringIncomeFI_BaseRow(String index) {
		recurringIncomeFI_BaseRow_Xpath = "//div[@class='grid-12-12']//select[@id='" + index + "']/../..";
	}// div[@class='grid-12-12']//select[@id='fnis_0']/../..

	public static String fnI(String index) {
		return recurringIncomeFI_BaseRow_Xpath + "//select[@id='" + index + "']";
	}// div[@class='grid-12-12']//select[@id='fnis_2']/../..//select[@id='fnis_2']

	public static String fnIDropdown(String index) {
		return recurringIncomeFI_BaseRow_Xpath + "//select[@id='" + index + "']//option";
	}

	public static String supplier(String index) {
		return recurringIncomeFI_BaseRow_Xpath + "//select[@name='suppliers']";
	}// div[@class='grid-12-12']//select[@id='fnis_2']/../..//select[@name='suppliers']

	public static String supplierDropdown(String index) {
		return recurringIncomeFI_BaseRow_Xpath + "//select[@name='suppliers']//option";
	}

	public static String department(String index) {
		return recurringIncomeFI_BaseRow_Xpath + "//select[@name='articleCondition']";
	}

	public static String departmentDropdown(String index) {
		return recurringIncomeFI_BaseRow_Xpath + "//select[@name='articleCondition']//option";
	}

	public static String typeSale(String index) {
//        return "//select[@name='typeOfSale']//option[contains(text(),'Please Select Type of Sale')]";
		return recurringIncomeFI_BaseRow_Xpath + "//select[@name='typeOfSale']";

	}

	public static String typeSaleDropdown(String index) {
//        return "//select[@name='typeOfSale']//option[contains(text(),'Please Select Type of Sale')]";
		return recurringIncomeFI_BaseRow_Xpath + "//select[@name='typeOfSale']//option";

	}

	public static String vapCategory(String index) {
//        return "//select[@name='vapCategory']//option[contains(text(),'Please Select Vap Category')]";
		return recurringIncomeFI_BaseRow_Xpath + "//select[@name='vapCategory']";
	}

	public static String vapCategoryDropdown(String index) {
//        return "//select[@name='vapCategory']//option[contains(text(),'Please Select Vap Category')]";
		return recurringIncomeFI_BaseRow_Xpath + "//select[@name='vapCategory']";
	}

	public static String recurringIncomeFI() {
		return "//div[@class='grid-12-12']//select[contains(@id,'fnis_')]";
	}

	public static String vapIncome(String index) {
		return recurringIncomeFI_BaseRow_Xpath + "//input[@name='vapIncome'][@value='0.00']";
	}

	public static String compIns_Income(String index) {
		return recurringIncomeFI_BaseRow_Xpath + "//input[@name='compInsIncome'][@value='0.00']";
	}

	public static String recurrinIncomeTotExclVat() {
		return "//input[@id='total_recurring_vat_exc']";
	}

	public static String recurrinIncomeTotInclVat() {
		return "//input[@id='total_recurring_vat_incl']";
	}

	public static String monthHeader() {
		return "//div[@class='grid-4-12']//div//h6//b";
	}

	public static String removeRecurringIncome(String index) {
		return "//input[contains(@id,'remove_')]";
	}

	public static String save_RecurringIncome() {
		return "//input[@id='saveRecurringIncome']";
	}

	public static String prevMonthRecurringIncome() {
		return "//input[@id='prevMonth']";
	}

	public static String nextMonthRecurringIncome() {
		return "//input[@id='nextMonth']";
	}

	public static String prePopulateRecurringIncome() {
		return "//input[@id='prepopulateBtn']";
	}

	public static String error_RecurringIncome() {
		return "//div[@id='recurringIncomeDetails']//div//select[contains(@class,'error')]";
	}

	public static String applicationSuccessMessage() {
		return "//div[@class='logoContainer landscape']//label";
	}

	public static String clickAlpheraFinance() {
		return "//input[@id='isAlpheraFinance']";
	}

	public static String clickAriva() {
		return "//input[@id='isAriva']";
	}

	public static String clickCapitecBank() {
		return "//input[@id='isCapitecBank']";
	}

	public static String clickSaTaxiFinance() {
		return "//input[@id='isSaTaxiFinance']";
	}

	public static String clickPrintApplication() {
		return "//input[@id='isPrintApplication']";
	}

	public static String clickTrackingApproved() {
		return "//input[@id='isTrackingLeads']";
	}

	public static String clickBeame() {
		return "//input[@id='leadProduct_9']";
	}

	public static String applicationMiddleName() {
		return "//input[@id='customerMiddleName']";
	}

	public static String pdfAbsaBankOpener(String uniqueID) {
		return "//div[@id='subDiv_" + uniqueID + "']//td[@align='right']//img";
	}

	public static String vehicleRetailValue() {
		return "//input[@id='retailValue']";
	}

	public static String articleColour() {
		return "//input[@id='articleColour']";
	}

	public static String serialNumber() {
		return "//input[@id='serialNumber']";
	}

	public static String motorBikeSize() {
		return "//input[@id='vehicleEngineSize']";
	}

	public static String numberOfSeat() {
		return "//input[@id='vehicleNumberOfSeats']";
	}

	public static String packageNumber() {
		return "//input[@id='schemeCodeAbsa']";
	}

	public static String schemeCodeMFC() {
		return "//input[@id='schemeCodeMFC']";
	}

	public static String schemeCodeWesbank() {
		return "//input[@id='schemeCodeWesbank']";
	}

	public static String dealerVAPSComment() {
		return "//input[contains(@id,'vapFAISComment')]";
	}

	public static String engineNumber() {
		return "//input[@id='engineNumber']";
	}

	public static String phoneNumber() {
		return "//input[@id='customerHomePhoneNumber']";
	}

	public static String registrationNumber() {
		return "//input[@id='registrationNumber']";
	}

	public static String address2() {
		return "//input[@id='customerResidentialAddressLine2']";
	}

	public static String relativeAddress2() {
		return "//input[@id='relativeAddressLine1']";
	}

	public static String agreement() {
		return "//select[@id='agreementType']";
	}

	public static String financeCalculatorForm() {
		return "//div[@id='financeCalculatorPanel']";
	}

	public static String commencementDate() {
		return "//input[@id='commencementDate']";
	}

	public static String instalmentDate() {
		return "//input[@id='instalmentDate']";
	}

	public static String saTaxiRouteForm() {
		return "//div[@id='taxiRoutesAppPanel']";
	}

	public static String taxiAssociation() {
		return "//input[@id='taxiAssociation']";
	}

	public static String routeFrom() {
		return "//select[@id='routeFromTo']";
	}

	public static String driverWages() {
		return "//input[@id='driverWagePerMonth']";
	}

	public static String numberOfPassengers() {
		return "//input[@id='averageNumberOfPassengersPerTrip']";
	}

	public static String passengerFare() {
		return "//input[@id='farePerPassenger']";
	}

	public static String daysWorked() {
		return "//select[@id='dayWorkedPerMonth']";
	}

	public static String taxiArea() {
		return "//select[@id='taxiAssociationProvince']";
	}

	public static String tripKM() {
		return "//input[@id='kmPerTrip']";
	}

	public static String tripsPerDay() {
		return "//input[@id='noOfTripsPerDay']";
	}

	public static String taxiNumberOfSeats() {
		return "//input[@id='monIncNoOfSeats']";
	}

	public static String saTaxiInsuranceForm() {
		return "//div[@id='tblTaxiInsuranceVAPs']";
	}

	public static String insuranceCompany() {
		return "//select[@id='supplier']";
	}

	public static String insuranceBroker() {
		return "//select[@id='insuranceBroker']";
	}

	public static String insuranceBrokerCode() {
		return "//input[@id='insuranceBrokerCode']";
	}

	public static String insuranceAmount() {
		return "//input[@id='insuranceAmount']";
	}

	public static String insuranceCover() {
		return "//input[@id='saTaxiNoCreditLife']";
	}

	public static String bankAccountStatus() {
		return "//select[@id='customerBankAccountStatus']";
	}

	public static String consentToAlphera() {
		return "//select[@id='consentToAlphera']";
	}

	public static String alpheraFI() {
		return "//select[@id='bmwOutletID']";
	}

	public static String alpheraSpecificInfoForm() {
		return "//div[@id='alpheraInfoPanel']";
	}

	public static String alpheraBrandCode() {
		return "//select[@id='bmwBrandCode']";
	}

	public static String alpheraPayDay() {
		return "//select[@id='bmwPayDay']";
	}

	public static String daysWorkedLabel() {
		return "//label[contains(text(),'Days worked per month')]";
	}

	public static String taxiAssociationName(String name) {
		return "//a[contains(text(),'" + name + "')]";
	}

	public static String wesbankErrorMessage() {
		return "//div[text()='Wesbank Re-Application']/..//p[contains(@class,error)]";
	}

	public static String archiveSearchMask() {
		return "//div[@id='divBusy']";
	}

	public static String absaDeclinePodium(String ref) {
		// return "//div[@id='subDiv_" + ref + "']//tr//td[@align='right']//img";
		return "//div[contains(@id,'subDiv_')]//tr//td[@align='right']//img";
	}

	/*
	 * AS - Xpaths for managing salesperson.
	 */
	public static String doc() {
		return "//a[text()='DOC']";
	}

	public static String manageSalesperson() {
		return "//a[text()='Manage Salespersons']";
	}

	public static String addNewSalesPerson() {
		return "//input[@value='ADD NEW']";
	}

	public static String nameOfSalesPerson() {
		return "//label[text()='Name']/../..//input[@id='firstNameModal']";
//        return "//label[text()='Name']/../..//input[@name='firstName']";
	}

	public static String surnameOfSalesPerson() {
		return "//label[text()='Name']/../..//input[@id='lastNameModal']";
//        return "//label[text()='Name']/../..//input[@name='lastName']";
	}

	public static String identificationTypeOfSalesPerson() {
		return "//select[@id='memberIdentificationtype']";
	}

	public static String idNumberOfSalesPerson() {
		return "//label[text()='ID Number']/../..//input[@id='memberIdNumberModal']";
//        return "//label[text()='ID Number']/../..//input[@name='idNumber']";
	}

	public static String selectIdentificationType(String identificationType) {
		return "//option[text()='" + identificationType + "']";
	}// option[text()='South African Valid ID']

	public static String salepersonSavedValidation(String idNumber) {
		return "//input[@id='idNumber'][@value='" + idNumber + "']";
	}// input[@id="idNumber"][@value='7906125160086']

	public static String emailAddressOfSalesPerson() {
		return "//label[text()='Email Address']/../..//input[@id='emailModal']";
//        return "//input[@class='email required']";
	}

	public static String cellphoneOfSalesPerson() {
		return "//label[text()='Cell Phone']/../..//input[@id='cellphoneNumberModal']";
//        return "//input[@class='mobileRSA required']";
	}

	public static String titleOfSalesPerson() {
		return "//div[@id='clientDetailsContent']//select[@id='titleModal']";
//        return "//div[@id='clientDetailsContent']//select[@id='title']";
	}

	public static String selectTitle(String title) {
		return "//select[@name='title']/option[text()='" + title + "']";
	}// select[@name='title']/option[text()='Advocate']

	public static String epsonMototorsCheckBox() {
		return "//div[text()='Dealerships']/..//input[@title='EPSON MOTORS']";
	}

	public static String signioPtyLtdCheckBox() {
		return "//div[text()='Dealerships']/..//input[@title='SIGNIO (PTY) LTD']";
	}

	public static String saveSalesPerson() {
		return "//input[@value='Save Salesperson']";
	}

	public static String clientDetails() {
		return "//div[text()='Client Details']";
	}

	public static String ValidateUsingBackGroundColor() {
		return "//div[@id='clientDetailsContent']//*[contains(@style,'rgb')]";
	}

	public static String ValidateUsingBackGroudColorForTitle() {
		return "//select[@style='background-color: rgb(255, 119, 119); color: rgb(0, 0, 0);']";
	}

	public static String closeButtonSalesPerson() {
		return "//input[@value='Close']";
	}

	////////////////////////////////////////////////////////////////////////
	public static String dateSearchMessage() {
		return "//div[@class='message']";
	}

	public static String docTransaction() {
		return "//span[contains(text(),'DOC Transactions')]";
	}

	public static String docShowButton() {
		return "//div[@id='DOCSelectModal']//input[@value='Show']";
	}

	public static String alpheraDealerCode() {
		return "//select[@id='dealerCodeBMW']";
	}

	public static String arivaDealerCode() {
		return "//select[@id='dealerCodeAriva']";
	}

	public static String capitecDealerCode() {
		return "//select[@id='dealerCodeCapitec']";
	}

	public static String saTaxiDealerCode() {
		return "//select[@id='dealerCodeSaTaxi']";
	}

	public static String beeActStatusHeader() {
		return "//div[@id='BEEActStatus']";
	}

	public static String beeActStatus() {
		return "//select[@id='isBEEActBeneficiary']";
	}

	public static String vehicleTrackingDevice() {
		return "//select[@id='isFinanceTrackUnit']";
	}

	public static String annualMileage() {
		return "//input[@id='maxAnnualMileage']";
	}

	public static String shockAbsorber() {
		return "//select[@id='shockAbsorberWesbank']";
	}

	public static String consentSalesPerson() {
		return "//select[@id='salespersonSelect']";
	}

	public static String applicantConsent() {
		return "//select[@id='itcConcentIndicator']";
	}

	public static String applicantSpouseConsent() {
		return "//select[@id='itcConcentSpouseIndicator']";
	}

	public static String jointIncomeConsent() {
		return "//select[@id='jointIncomeIndicator']";
	}

	public static String motorInsuranceDetailsheader() {
		return "//div[@id='motorInsuranceDetailsPanel']";
	}

	public static String carRadio() {
		return "//input[@id='carRadioCI']";
	}

	public static String claimFreeYear() {
		return "//select[@id='claimFreeYears']";
	}

	public static String motorAlarm() {
		return "//select[@id='alarm']";
	}

	public static String gearLock() {
		return "//select[@id='gearlock']";
	}

	public static String yearsInsured() {
		return "//select[@id='yearsInsured']";
	}

	public static String typeOfCover() {
		return "//select[@id='typeOfCover']";
	}

	public static String overnightParking() {
		return "//select[@id='overnightParking']";
	}

	public static String immobiliser() {
		return "//select[@id='immobiliser']";
	}

	public static String motorTrackingDevice() {
		return "//select[@id='trackingDevice']";
	}

	public static String sendQuote() {
		return "//select[@id='sendQuoteTo']";
	}

	public static String driverLicenseCode() {
		return "//select[@id='driversLicenseCode']";
	}

	public static String driverFirstname() {
		return "//input[@id='firstNameCI']";
	}

	public static String indentificationType() {
		return "//select[@id='idTypeCI']";
	}

	public static String driverDOB() {
		return "//input[@id='dobCI']";
	}

	public static String driverGender() {
		return "//select[@id='genderCI']";
	}

	public static String employementType() {
		return "//select[@id='empTypeCI']";
	}

	public static String driverDetails() {
		return "//select[@id='driverDetails']";
	}

	public static String driverSurname() {
		return "//input[@id='surnameCI']";
	}

	public static String driverIDNumber() {
		return "//input[@id='idnumberCI']";
	}

	public static String licenseDate() {
		return "//input[@id='driversLicenseIsValidFromDate']";
	}

	public static String driverMaritalStatus() {
		return "//select[@id='maritalCI']";
	}

	public static String absaBankVaps() {
		return "//div[contains(@id,'ABSABankVaps')]";
	}

	public static String absaEmployeeIndicator() {
		return "//select[contains(@id,'bankEmployeeIndicator')]";
	}

	public static String academyCertified() {
		return "//select[contains(@id,'adaCertified')]";
	}

	public static String addNewAbsaVap() {
		return "//input[contains(@id,'addAbsaBankVapButton')]";
	}

	public static String absaVapsSupplier() {
		return "//select[contains(@id,'bankVapOrigin')]";
	}

	public static String absaVapsProduct() {
		return "//select[contains(@id,'bankVapCode')]";
	}

	public static String wesbankVaps() {
		return "//div[@id='WesBankBankVaps']";
	}

	public static String addNewWesbankVaps() {
		return "//input[@id='addBankVapButton']";
	}

	public static String valueAddedModal() {
		return "//span[text()='Value Added Products']";
	}

	public static String wesbankVapOwner() {
		return "//select[@id='vapOwner']";
	}

	public static String wesbankVapCategory() {
		return "//select[@id='vapDescriptionCustomer']";
	}

	public static String wesbankVapAmount() {
		return "//input[@id='vapAmount']";
	}

	public static String vapOkButton() {
		return "//input[@id='vapOKButton']";
	}

	public static String spouseBankStatement() {
		return "//select[@id='spouseCreditBureauConsent']";
	}

	public static String newAbsaBankVapsAmount() {
		return "//input[contains(@id,'bankVapAmount')]";
	}

	public static String absaEmployeeNumber() {
		return "//input[contains(@id,'employeeNumber')]";
	}

	public static String tradeInDeposit() {
		return "//input[@id='tradeInDeposit']";
	}

	public static String afterMarket() {
		return "//input[@id='isAftermarket']";
	}

	public static String companyDetailsHeader() {
		return "//div[@id='companyDetailsPanel']";
	}

	public static String companyTransaction() {
		return "//input[@id='isCompanyApp']";
	}

	public static String vatNumber() {
		return "//input[@id='businessVatNumber']";
	}

	public static String businessRegistrationNumber() {
		return "//input[@id='businessRegistrationNumber']";
	}

	public static String salesPerson() {
		return "//select[@id='salespersonSelect']";
	}

	public static String vehicleLookup() {
		return "//div//span[contains(text(),'Vehicle Lookup')]";
	}

	public static String depositSource() {
		return "//select[@id='sourceOfDeposit']";
	}

	public static String fspNumber() {
		return "//select[@id='fspNumber']";
	}

	public static String manufactureService() {
		return "//select[@id='isCheckManufacturerMaintenance']";
	}

	public static String fullServiceHistory() {
		return "//select[@id='isCheckFullServiceHistory']";
	}

	public static String maintenanceOrServicePlan() {
		return "//select[@id='maintenancePlanOrServicePlan']";
	}

	public static String maintenanceOrServiceExpiryDate() {
		return "//input[@id='manufacturerMaintenancePlanExpiryDate']";
	}

	public static String maintenanceOrServiceExpiryKm() {
		return "//input[@id='manufacturerMaintenancePlanExpiryKilometers']";
	}

	public static String priceList() {
		return "//input[@id='cmdTreeView']";
	}

	public static String expandAll() {
		return "//span[contains(text(),'Expand All')]";
	}

	public static String collapseAll() {
		return "//span[contains(text(),'Collapse All')]";
	}

	public static String closeTreeview() {
		return "//span[text() = 'Tree View']/..//button[@role = 'button']";
	}

	public static String occupantType() {
		return "//select[@id='ownerTenantLodger']";
	}

	public static String bondedVia() {
		return "//select[@id='bondLenders']";
	}

	public static String typeOfHouse() {
		return "//select[@id='residentialType']";
	}

	public static String resizableIcon() {
		return "//div[contains(@class,'ui-resizable ui-dialog-buttons')]//div[@class='ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se']";
	}

	public static String occupantTypeList() {
		return "//select[@id='ownerTenantLodger']//option";
	}

	public static String occupantTypeList(String occupant) {
		return "//select[@id='ownerTenantLodger']//option[text()='" + occupant + "']";
	}

	public static String bondBalance() {
		return "//input[@id='outstandingBondBalance']";
	}

	public static String propertyValue() {
		return "//input[@id='propertyCurrentValue']";
	}

	public static String previousAddressYear() {
		return "//input[@id='customerPeriodAtPreviousAddressYears']";
	}

	public static String previousAddressMonth() {
		return "//input[@id='customerPeriodAtPreviousAddressMonths']";
	}

	public static String motorInsuranceLead() {
		return "//input[@id='_isMotorLeadInsurance'][@disabled]";
	}

	public static String shortTermInsurers() {
		return "//select[@id='leadDelay']";
	}

	public static String testInsuranceCheckBox() {
		return "//label[text()='TEST INSURANCE']/..//input[@id='_leadProduct_6']";
	}

	public static String budgetDealerQuote() {
		return "//label[text()='Budget Dealer Quote']/..//input[@id='leadProduct_0']";
	}

	public static String budgetLink() {
		return "//label[text()='Budget link']/..//input[@id='leadProduct_1']";
	}

	public static String compTesting() {
		return "//label[text()='Comp Testing']/..//input[@id='leadProduct_2']";
	}

	public static String kingPrice() {
		return "//label[text()='King Price']/..//input[@id='leadProduct_3']";
	}

	public static String miWay() {
		return "//label[text()='MiWay']/..//input[@id='leadProduct_4']";
	}

	public static String standardBankInsuranceLeads() {
		return "//label[contains(text(),'Standardbank')]/..//input[@id='leadProduct_5']";
	}

	public static String teleSure() {
		return "//label[text()='Telesure']/..//input[@id='leadProduct_7']";
	}

	public static String iPlatForm() {
		return "//label[text()='iPlatform']/..//input[@id='leadProduct_8']";
	}

	public static String drivaCover() {
		return "//label[text()='DRIVA Cover']/..//input[@id='drivaCoverIndicator']";
	}

	public static String cTrack() {
		return "//label[text()='C-TRACK']/..//input[@id='leadProduct_10']";
	}

	public static String carTrack() {
		return "//label[text()='CARTRACK']/..//input[@id='leadProduct_11']";
	}

	public static String matrix() {
		return "//label[text()='MATRIX']/..//input[@id='leadProduct_12']";
	}

	public static String netstar() {
		return "//label[text()='NETSTAR']/..//input[@id='leadProduct_13']";
	}

	public static String tracker() {
		return "//label[text()='TRACKER']/..//input[@id='leadProduct_14']";
	}

	public static String trackingInfinity() {
		return "//label[contains(text(),'TRACKING')]/..//input[@id='leadProduct_15']";
	}

	public static String beamePlan() {
		return "//select[@id='leadProductOption_9']";
	}

	public static String cTrackPlan() {
		return "//select[@id='leadProductOption_10']";
	}

	public static String carTrackPlan() {
		return "//select[@id='leadProductOption_11']";
	}

	public static String netStarPlan() {
		return "//select[@id='leadProductOption_13']";
	}

	public static String trackerPlan() {
		return "//select[@id='leadProductOption_14']";
	}

	public static String trackingInfinityPlan() {
		return "//select[@id='leadProductOption_15']";
	}

	public static String callCenterLeads() {
		return "//input[@id='isCallCentreLeads']";
	}

	public static String ccTest() {
		return "//label[text()='CC Test']/..//input[@id='leadProduct_16']";
	}

	public static String keyFind() {
		return "//label[text()='KEYFIND']/..//input[@id='leadProduct_17']";
	}

	public static String zestLifeTest() {
		return "//label[text()='ZESTLIFE TEST']/..//input[@id='leadProduct_18']";
	}

	public static String fordSchemeDeal() {
		return "//select[@id='TCMIndicator']";
	}

	public static String carAllowance() {
		return "//input[@id='sumIncomeCustomerCarAllowance']";
	}

	public static String overTime() {
		return "//input[@id='sumIncomeCustomerOvertime']";
	}

	public static String rentalIncome() {
		return "//input[@id='sumIncomeCustomerRental']";
	}

	public static String maintenanceIncome() {
		return "//input[@id='sumIncomeCustomerMaintenance']";
	}

	public static String otherIncome() {
		return "//input[@id='addIncomeCustomerOtherIncome']";
	}

	public static String otherIncomeDescription() {
		return "//input[@id='addIncomeCustomerOtherIncomeDescription']";
	}

	public static String spouseGrossRenumeration() {
		return "//input[@id='sumIncomeSpouseGrossRemuneration']";
	}

	public static String spouseCarAllowance() {
		return "//input[@id='sumIncomeSpouseCarAllowance']";
	}

	public static String spouseNetTakeHome() {
		return "//input[@id='addIncomeSpouseNetTakeHomePay']";
	}

	public static String spouseRentalIncome() {
		return "//input[@id='sumIncomeSpouseRental']";
	}

	public static String spouseMaintenance() {
		return "//input[@id='sumIncomeSpouseMaintenance']";
	}

	public static String spouseOtherIncome() {
		return "//input[@id='addIncomeSpouseOtherIncome']";
	}

	public static String sourceOfIncome() {
		return "//select[@id='sourceOfIncome']";
	}

	public static String articleType() {
		return "//select[@id='articleType']";
	}

	public static String useOfArticle() {
		return "//select[@id='articleUse']";
	}

	public static String repaymentPeriod() {
		return "//select[@id='repaymentPeriod']";
	}

	public static String paymentFrequency() {
		return "//select[@id='paymentFrequency']";
	}

	public static String packageIndicator() {
		return "//select[@id='absaSchemeIndicator']";
	}

	public static String payInAdvance() {
		return "//select[@id='advance']";
	}

	public static String previousPeriodEmployedYear() {
		return "//input[@id='customerPeriodAtPreviousEmployerYears']";
	}

	public static String previousPeriodEmployedMonth() {
		return "//input[@id='customerPeriodAtPreviousEmployerMonths']";
	}

	public static String employerSecondAddress() {
		return "//input[@id='employerAddressLine2']";
	}

	public static String waterElectricity() {
		return "//input[@id='sumExpensesCustomerRates']";
	}

	public static String vehicleInstallment() {
		return "//input[@id='sumExpensesCustomerVehicleInstallments']";
	}

	public static String loanRepayment() {
		return "//input[@id='sumExpensesCustomerLoanRepayments']";
	}

	public static String furnitureAccount() {
		return "//input[@id='sumExpensesCustomerFurnitureAccounts']";
	}

	public static String overDraftRepayment() {
		return "//input[@id='sumExpensesCustomerOverdraftRepayments']";
	}

	public static String insuranceRepayment() {
		return "//input[@id='sumExpensesCustomerInsurancePayments']";
	}

	public static String liabiltiesDetails() {
		return "//div[@id='liabilityDetailsPanel']";
	}

	public static String surety() {
		return "//select[@id='surety']";
	}

	public static String guarantor() {
		return "//select[@id='guarantor']";
	}

	public static String coDebtor() {
		return "//select[@id='coDebtor']";
	}

	public static String suretyDescription() {
		return "//input[@id='suretyDescription']";
	}

	public static String guarantorDescription() {
		return "//input[@id='guarantorDescription']";
	}

	public static String coDebtorDescription() {
		return "//input[@id='coDebtorDescription']";
	}

	public static String accountNumberToSettle() {
		return "//input[@id='accountNumberToSettle']";
	}

	public static String settlementAmount() {
		return "//input[@id='settlementAmount']";
	}

	public static String bankNameAccToSettle() {
		return "//select[@id='bankNameAccToSettle']";
	}

	public static String monthlyInstalmentAmount() {
		return "//input[@id='monthlyInstallmentOnAccountToBeSettled']";
	}

	public static String preferredEmailAddress() {
		return "//input[@id='customerPreferredEmailAddress']";
	}

	public static String alternativeEmailAddress() {
		return "//input[@id='customerAlternateEmailAddress']";
	}

	public static String preferredDeliveryMethod() {
		return "//select[@id='customerPreferredDeliveryMethod']";
	}

	public static String highlightedInputField(String name) {
		return "//input[@id='" + name + "'][contains(@class,'error')]";
	}

	public static String highlightedSelectField(String name) {
		return "//select[@id='" + name + "'][contains(@class,'error')]";
	}

	public static String readOnlyInputField(String name) {
		return "//input[@id='" + name + "'][@readonly='readonly']";
	}

	public static String vapIncomeList() {
		return "//input[contains(@name,'vapIncome')]";
	}

	public static String compInsIncomeList() {
		return "//input[contains(@name,'compInsIncome')]";
	}

	// JURISTIC Application
	public static String juristicApplication() {
		return "//a[text()='Juristic Application']";
	}

	public static String tradingName() {
		return "//input[@id='businessTradingName']";
	}

	public static String establishedDate() {
		return "//input[@id='companyEstablishedDate']";
	}

	public static String companyYearEnd() {
		return "//select[@id='companyYearEnd']";
	}

	public static String companyType() {
		return "//select[@id='companyType']";
	}

	public static String vatIndicator() {
		return "//select[@id='vatRegistered']";
	}

	public static String businessVatNumber() {
		return "//input[@id='vatNumber']";
	}

	public static String companyDetailsTab() {
		return "//div[@id='companyDetails']";
	}

	public static String clientSubType() {
		return "//select[@id='customerPermitSubType']";
	}

	public static String isCompanySolvent() {
		return "//select[@id='indicatorInsolvent']";
	}

	public static String isCreditSecure() {
		return "//select[@id='businessCreditMortgageSecured']";
	}

	public static String blackOwned() {
		return "//select[@id='beeBlackControl']";
	}

	public static String principalMembersAge() {
		return "//input[@id='principalMemberAveAge']";
	}

	public static String durationOfDirectorship() {
		return "//input[@id='durationCurrentDirectorship']";
	}

	public static String registeredTaxPayer() {
		return "//select[@id='taxRegistered']";
	}

	public static String profitRingfenced() {
		return "//select[@id='assetsProfitsRingfenced']";
	}

	public static String companyPreferredContactMethod() {
		return "//select[@id='customerPreferredContactMethod']";
	}

	public static String companyCellNumber() {
		return "//input[@id='businessMobileNumber']";
	}

	public static String companyEmail() {
		return "//input[@id='businessEmailAddress']";
	}

	public static String companyCountryOfOperation() {
		return "//select[@id='businessCountryOfOperation']";
	}

	public static String companyCountryOfHeadOffice() {
		return "//select[@id='businessCountryOfHeadOffice']";
	}

	public static String sectorOfIndusrty() {
		return "//select[@id='businessSectorOfIndustryJuristic']";
	}

	public static String industryType() {
		return "//select[@id='businessSectorOfIndustryTypeJuristic']";
	}

	public static String holdingCompanyName() {
		return "//input[@id='holdingCompanyName']";
	}

	public static String holdingRegistrationNumber() {
		return "//input[@id='holdingBusinessRegistrationNumber']";
	}

	public static String businessContactTitle() {
		return "//select[@id='businessContactTitle']";
	}

	public static String businessContactDesignation() {
		return "//select[@id='businessContactDesignation']";
	}

	public static String businessContactName() {
		return "//input[@id='businessContactName']";
	}

	public static String yearTimeInBusiness() {
		return "//input[@id='businessPeriodTimeInbusinessYears']";
	}

	public static String monthTimeInBusiness() {
		return "//input[@id='businessPeriodTimeInbusinessMonths']";
	}

	public static String businessMonthlyIncome() {
		return "//input[@id='businessMonthlyIncome']";
	}

	public static String companyExpenses() {
		return "//input[@id='businessCompanyExpenses']";
	}

	public static String companyPreferredLanguage() {
		return "//select[@id='businessLanguage']";
	}

	public static String companySourceOfIncome() {
		return "//select[@id='sourceofIncomeJuristic']";
	}

	public static String companyForeignTaxObligation() {
		return "//select[@id='companyHasForeignIdentity']";
	}

	public static String geographicalTab() {
		return "//div[@id='geographicalDetails']";
	}

	public static String addressDetailsTab() {
		return "//div[contains(text(),'Address Details')]";
	}

	public static String propertyDetailsTab() {
		return "//div[contains(text(),'Property Details')]";
	}

	public static String businessPhysicalAddress1() {
		return "//input[@id='businessPhysicalAddressLine1']";
	}

	public static String businessPhysicalAddress2() {
		return "//input[@id='businessPhysicalAddressLine2']";
	}

	public static String businessPhysicalAddressSuburbButton() {
		return "//input[@id='businessPhysicalAddressLookup']";
	}

	public static String physicalAddressPopulateButton() {
		return "//input[@id='businessPostal']";
	}

	public static String businessOperatingAddress1() {
		return "//input[@id='businessOperatingAddressLine1']";
	}

	public static String businessOperatingAddress2() {
		return "//input[@id='businessOperatingAddressLine2']";
	}

	public static String businessOperatingAddressSuburbButton() {
		return "//input[@id='businessOperatingAddressLookup']";
	}

	public static String operatingAddressPopulateButton() {
		return "//input[@id='businessOperating']";
	}

	public static String businessRegisteredAddress1() {
		return "//input[@id='businessRegisteredAddressLine1']";
	}

	public static String businessRegisteredAddress2() {
		return "//input[@id='businessRegisteredAddressLine2']";
	}

	public static String businessRegisteredAddressSuburbButton() {
		return "//input[@id='businessRegisteredAddressLookup']";
	}

	public static String registeredAddressPopulateButton() {
		return "//input[@id='businessRegistered']";
	}

	public static String businessPropertyAddress1() {
		return "//input[@id='businessPropertyAddressLine1']";
	}

	public static String businessPropertyAddress2() {
		return "//input[@id='businessPropertyAddressLine2']";
	}

	public static String businessPropertyAddressSuburbButton() {
		return "//input[@id='businessPropertyAddressLookup']";
	}

	public static String businessPropertyOwned() {
		return "//select[@id='businessPropertyOwned']";
	}

	public static String typeOfOwnership() {
		return "//select[@id='ownerTenantLodger']";
	}

	public static String propertyStandNumber() {
		return "//input[@id='businessPropertyStandNumber']";
	}

	public static String bondHolderName() {
		return "//input[@id='bondHolder']";
	}

	public static String bondHolderBankName() {
		return "//select[@id='businessPropertyBondHolderBankName']";
	}

	public static String propertyPurchaseDate() {
		return "//input[@id='propertyPurchaseDate']";
	}

	public static String propertyRentAmountLabel() {
		return "//label[text()='Rent Amount']";
	}

	public static String propertyRentAmount() {
		return "//input[@id='businessPropertyRentAmount']";
	}

	public static String propertyOriginalBondAmount() {
		return "//input[@id='originalBondAmount']";
	}

	public static String propertyBondRepayment() {
		return "//input[@id='businessPropertyBondRepayment']";
	}

	public static String propertyBondBalance() {
		return "//input[@id='businessPropertyBondBalance']";
	}

	public static String propertyBondOutstanding() {
		return "//input[@id='flexiAccessBondTotalOutstanding']";
	}

	public static String propertyCurrentValue() {
		return "//input[@id='propertyCurrentValue']";
	}

	public static String developmentFunding() {
		return "//input[@id='isDevelopmentFundingJuristic']";
	}

	public static String commissionCheckBox() {
		return "//input[@id='sourceOfFundsCompanyCommissionsEarned']";
	}

	public static String commissionAmount() {
		return "//input[@id='sourceOfFundsCompanyCommissionsEarnedAmount']";
	}

	public static String salesPersonTab() {
		return "//div[@id='salespersonPanel']";
	}

	public static String addNewSalesPersonButton() {
		return "//input[@id='addSalesPerson']";
	}

	public static String salesPersonFirstname() {
		return "//input[@id='salesPersonFirstName']";
	}

	public static String salesPersonSurname() {
		return "//input[@id='salesPersonLastName']";
	}

	public static String salesPersonIdType() {
		return "//select[@id='salesPersonIdType']";
	}

	public static String salesPersonIdNumber() {
		return "//input[@id='salesPersonIdNumber']";
	}

	public static String salesPersonTitle() {
		return "//select[@id='salesPersonTitle']";
	}

	public static String addSalesPerson() {
		return "//input[@id='saveSalesPerson']";
	}

	public static String infoSharingConsent() {
		return "//select[@id='customerCreditInfoSharingConsentIndicator']";
	}

	public static String consentTab() {
		return "//div[@id='bankStatementConsentPanel']";
	}

	public static String financialDetailsTab() {
		return "//div[@id='financialDetails']";
	}

	public static String companyBankName() {
		return "//select[@id='businessBankAccountBank']";
	}

	public static String companyBankBranchName() {
		return "//input[@id='businessBankAccountBranchName']";
	}

	public static String companyBankBranchCode() {
		return "//input[@id='businessBankAccountBranchCode']";
	}

	public static String companyBankAccountType() {
		return "//select[@id='businessBankAccountType']";
	}

	public static String companyBankAccountNumber() {
		return "//input[@id='businessBankAccountNumber']";
	}

	public static String companyBankAccountName() {
		return "//input[@id='businessAccountHolderName']";
	}

	public static String companyBankOverDraftLimit() {
		return "//input[@id='businessOverdraftLimit']";
	}

	public static String companyBankOverDrafBalance() {
		return "//input[@id='businessOverdraftBalance']";
	}

	public static String addAccountToBeSettled() {
		return "//input[@id='addcreditProvider']";
	}

	public static String addCompanyAccount() {
		return "//select[contains(@id,'creditProviderName_')]";
	}

	public static String addCompanyAccountNumber() {
		return "//input[contains(@id,'creditProviderAccountNumber_')]";
	}

	public static String addCompanyAccountMonthlyInstalment() {
		return "//input[contains(@id,'creditProviderMonthlyInstalment_')]";
	}

	public static String addCompanyAccountBalance() {
		return "//input[contains(@id,'creditProviderOutstandingBalance_')]";
	}

	public static String addCompanyAccountOpen() {
		return "//select[contains(@id,'creditProviderAccountOpen_')]";
	}

	public static String addCompanyAccountSettlementIntent() {
		return "//select[contains(@id,'creditProviderSettlementIntent_')]";
	}

	public static String removeCompanyAccount() {
		return "//input[contains(@name,'removeproviders_')]";
	}

	public static String companyBalanceSheetYear() {
		return "//input[contains(@id,'businessBalanceSheetYears_')]";
	}

	public static String companyBalanceSheetAssetAmount() {
		return "//input[contains(@id,'businessTotalAssetAmount_')]";
	}

	public static String companyBalanceSheetTurnover() {
		return "//input[contains(@id,'businessBalanceTurnOver_')]";
	}

	public static String signatoriesTab() {
		return "//div[@id='signatories']";
	}

	public static String signAsSurety() {
		return "//select[contains(@id,'customerSignSurety_')]";
	}

	public static String signatoryFirstName() {
		return "//input[contains(@id,'customerFirstName_')]";
	}

	public static String signatorySurName() {
		return "//input[contains(@id,'customerSurname_')]";
	}

	public static String signatoryContactType() {
		return "//select[contains(@id,'businessContactType_')]";
	}

	public static String signatoryIdType() {
		return "//select[contains(@id,'customerIdType_')]";
	}

	public static String signatoryIdNumber() {
		return "//input[contains(@id,'customerIdNumber_')]";
	}

	public static String signatoryDateofBirth() {
		return "//input[contains(@id,'customerDateOfBirth_')]";
	}

	public static String signatoryClientSubType() {
		return "//select[contains(@id,'customerSubType_')]";
	}

	public static String signatoryPreferredLanguage() {
		return "//select[contains(@id,'customerPreferredLanguage_')]";
	}

	public static String signatoryGender() {
		return "//select[contains(@id,'customerGender_')]";
	}

	public static String signatoryContactTitle() {
		return "//select[contains(@id,'customerTitle_')]";
	}

	public static String signatoryEthnicity() {
		return "//select[contains(@id,'customerRaceEthnicGroup_')]";
	}

	public static String signatoryEmploymentSector() {
		return "//select[contains(@id,'employmentSector_')]";
	}

	public static String signatoryClientType() {
		return "//select[contains(@id,'customerTypeJuristic_')]";
	}

	public static String signatoryMaritalStatus() {
		return "//select[contains(@id,'customerMaritalStatus_')]";
	}

	public static String signatoryMaritalContract() {
		return "//select[contains(@id,'customerMaritalContract_')]";
	}

	public static String signatoryDateMarried() {
		return "//input[contains(@id,'dateMarried_')]";
	}

	public static String signatoryIndustryType() {
		return "//select[contains(@id,'businessSectorOfIndustryTypeJuristic_')]";
	}

	public static String signatoryResidenceYear() {
		return "//input[contains(@id,'customerPeriodAtCurrentAddressYears_')]";
	}

	public static String signatoryResidenceMonth() {
		return "//input[contains(@id,'customerPeriodAtCurrentAddressMonths_')]";
	}

	public static String signatoryEmployerYear() {
		return "//input[contains(@id,'customerPeriodAtCurrentEmployerYears_')]";
	}

	public static String signatoryEmployerMonth() {
		return "//input[contains(@id,'customerPeriodAtCurrentEmployerMonths_')]";
	}

	public static String signatoryRegisteredTaxPayer() {
		return "//select[contains(@id,'taxRegistered_')]";
	}

	public static String signatoryTaxNumber() {
		return "//input[contains(@id,'taxNumber_')]";
	}

	public static String signatoryAddressLine1() {
		return "//input[contains(@id,'customerResidentialAddressLine1_')]";
	}

	public static String signatoryAddressLine2() {
		return "//input[contains(@id,'customerResidentialAddressLine2_')]";
	}

	public static String signatoryAddressLookup() {
		return "//input[contains(@id,'customerResidentialAddressLookup_')]";
	}

	public static String signatoryPopulateAddressButton() {
		return "//input[contains(@id,'signatoryPostal_')]";
	}

	public static String signatoryHomePhone() {
		return "//input[contains(@id,'customerHomePhoneNumber_')]";
	}

	public static String signatoryWorkPhone() {
		return "//input[contains(@id,'customerWorkPhoneNumber_')]";
	}

	public static String signatoryCellPhone() {
		return "//input[contains(@id,'customerCellPhoneNumber_')]";
	}

	public static String signatoryEmailAddress() {
		return "//input[contains(@id,'customerEmailAddress_')]";
	}

	public static String signatoryTypeOfOwnership() {
		return "//select[contains(@id,'ownerTenantLodger_')]";
	}

	public static String signatoryPropertyCurrentValue() {
		return "//input[contains(@id,'propertyCurrentValue_')]";
	}

	public static String signatoryPropertyBondBalance() {
		return "//input[contains(@id,'customerPropertyBondBalance_')]";
	}

	public static String signatorySalutation() {
		return "//select[contains(@id,'customerSalutation_')]";
	}

	public static String signatoryBankName() {
		return "//select[contains(@id,'customerBankAccountBank_')]";
	}

	public static String signatoryAccountType() {
		return "//select[contains(@id,'customerBankAccountType_')]";
	}

	public static String signatoryAccHolderName() {
		return "//input[contains(@id,'businessMemberAccountHolder_')]";
	}

	public static String signatoryAccNumber() {
		return "//input[contains(@id,'businessMemeberAccountNumber_')]";
	}

	public static String signatoryBranchCode() {
		return "//input[contains(@id,'businessMemberBranchCode_')]";
	}

	public static String signatoryOccupation() {
		return "//select[contains(@id,'customerOccupation_')]";
	}

	public static String signatoryEmployerName() {
		return "//input[contains(@id,'employerName_')]";
	}

	public static String signatoryEmployerWorkPhone() {
		return "//input[contains(@id,'employerPhoneNumber_')]";
	}

	public static String signatorySourceofIncome() {
		return "//select[contains(@id,'customerSourceOfIncome_')]";
	}

	public static String signatoryMonthlyIncome() {
		return "//input[contains(@id,'customerTotalMonthlyIncome_')]";
	}

	public static String signatorySharePercentage() {
		return "//input[contains(@id,'customerSharePercentage_')]";
	}

	public static String signatoryNetworkType() {
		return "//select[contains(@id,'businessNetworkType_')]";
	}

	public static String signatorySuretyType() {
		return "//select[contains(@id,'businessSuretyType_')]";
	}

	public static String signatoryYearInPosition() {
		return "//input[contains(@id,'businessNumberYearsPosition_')]";
	}

	public static String signatoryForeignIdentity() {
		return "//select[contains(@id,'customerHasForeignIdentity_')]";
	}

	public static String signatoryMultipleNationalities() {
		return "//select[contains(@id,'customerMultiNational_')]";
	}

	public static String signatoryNationality() {
		return "//select[contains(@id,'customerNationalityJuristic_')]";
	}

	public static String signatoryNationality1() {
		return "//select[contains(@id,'customerNationality_')]";
	}

	public static String signatoryNationality2() {
		return "//select[contains(@id,'customerNationality2_')]";
	}

	public static String signatoryCountryofResidence() {
		return "//select[contains(@id,'customerCountryOfResidence_')]";
	}

	public static String signatoryCountryOfBirth() {
		return "//select[contains(@id,'customerCountryOfBirth_')]";
	}

	public static String signatoryContactDesignation() {
		return "//select[contains(@id,'businessContactDesignation_')]";
	}

	public static String signatoryEmployeeNumber() {
		return "//input[contains(@id,'employeeNo_')]";
	}

	public static String signatoryMFCConsentTab() {
		return "//div[text()='MFC Consent']";
	}

	public static String signatoryInfoSharing() {
		return "//select[contains(@id,'customerCreditInfoSharingConsentIndicator_')]";
	}

	public static String signatoryItcConsent() {
		return "//select[contains(@id,'itcConcentIndicator_')]";
	}

	public static String signatoryMarketingConsent() {
		return "//select[contains(@id,'businessIncludeInMarketingList_')]";
	}

	public static String signatorySuretyTab() {
		return "//div[@id='signatoryIndividualSuretyPanel_0']";
	}

	public static String suretyEmploymentType() {
		return "//select[contains(@id,'customerEmploymentType_')]";
	}

	public static String suretyPeriodEmployedYear() {
		return "//input[contains(@id,'customerYearsEmployed_')]";
	}

	public static String suretyPeriodEmployedMonth() {
		return "//input[contains(@id,'customerMonthsEmployed_')]";
	}

	public static String suretyResidentialStatus() {
		return "//select[contains(@id,'customerResidentialStatus_')]";
	}

	public static String suretySpouseFirstName() {
		return "//input[contains(@id,'customerSpouseFirstName_')]";
	}

	public static String suretySpouseSurname() {
		return "//input[contains(@id,'customerSpouseSurname_')]";
	}

	public static String suretyNextofKinFirstname() {
		return "//input[contains(@id,'customerNextOfKinFirstName_')]";
	}

	public static String suretyNextofKinSurname() {
		return "//input[contains(@id,'customerNextOfKinSurname_')]";
	}

	public static String suretyNextofKinRelation() {
		return "//input[contains(@id,'customerNextOfKinRelation_')]";
	}

	public static String suretyNextofKinTelephoneNumber() {
		return "//input[contains(@id,'customerNextOfKinPhoneNumber_')]";
	}

	public static String suretyWorkAddressLine1() {
		return "//input[contains(@id,'WorkAddressLine1_')]";
	}

	public static String suretyWorkAddressLine2() {
		return "//input[contains(@id,'WorkAddressLine2_')]";
	}

	public static String suretyWorkAddressLookup() {
		return "//input[contains(@id,'WorkAddressLookup_')]";
	}

	public static String suretyNextofKinWorkAddressLine1() {
		return "//input[contains(@id,'customerNextOfKinAddressLine1_')]";
	}

	public static String suretyNextofKinWorkAddressLine2() {
		return "//input[contains(@id,'customerNextOfKinAddressLine2_')]";
	}

	public static String suretyNextofKinWorkAddressLookup() {
		return "//input[contains(@id,'nextOfKinAddressAddressLookup_')]";
	}

	public static String signatoryIncomeDetailsTab() {
		return "//div[@id='signatoryIncomeDetailsPanel_0']";
	}

	public static String signatorySourceofIncomeDetails() {
		return "//select[contains(@id,'sourceOfIncome_')]";
	}

	public static String signatoryGrossRenumeration() {
		return "//input[contains(@id,'sumIncomeCustomerGrossRemuneration_')]";
	}

	public static String signatorySpouseGrossRenumeration() {
		return "//input[contains(@id,'sumIncomeSpouseGrossRemuneration_')]";
	}

	public static String signatoryMonthlyCommission() {
		return "//input[contains(@id,'sumIncomeCustomerMonthlyCommission_')]";
	}

	public static String signatorySpouseMonthlyCommission() {
		return "//input[contains(@id,'sumIncomeSpouseMonthlyCommission_')]";
	}

	public static String signatoryCarAllowance() {
		return "//input[contains(@id,'sumIncomeCustomerCarAllowance_')]";
	}

	public static String signatorySpouseCarAllowance() {
		return "//input[contains(@id,'sumIncomeSpouseCarAllowance_')]";
	}

	public static String signatoryOverTime() {
		return "//input[contains(@id,'sumIncomeCustomerOvertime_')]";
	}

	public static String signatoryReimbursement() {
		return "//input[contains(@id,'reimbursement_')]";
	}

	public static String signatoryNetTakeHome() {
		return "//input[contains(@id,'addIncomeCustomerNetTakeHomePay_')]";
	}

	public static String signatorySpouseNetTakeHome() {
		return "//input[contains(@id,'addIncomeSpouseNetTakeHomePay_')]";
	}

	public static String signatoryOtherIncome() {
		return "//input[contains(@id,'addIncomeCustomerOtherIncome_')]";
	}

	public static String signatorySpouseOtherIncome() {
		return "//input[contains(@id,'addIncomeSpouseOtherIncome_')]";
	}

	public static String signatoryTotalMonthlyIncome() {
		return "//input[contains(@id,'customerSignatoryTotalMonthlyIncome_')]";
	}

	public static String signatorySpouseTotalMonthlyIncome() {
		return "//input[contains(@id,'spouseTotalMonthlyIncome_')]";
	}

	public static String signatoryExpenseDetails() {
		return "//div[@id='signatoryExpensesPanel_0']";
	}

	public static String signatoryBondPayment() {
		return "//input[contains(@id,'sumExpensesCustomerBondPayment_')]";
	}

	public static String signatorSpouseBondPayment() {
		return "//input[contains(@id,'sumExpensesSpouseBondPayment_')]";
	}

	public static String signatoryRent() {
		return "//input[contains(@id,'sumExpensesCustomerRent_')]";
	}

	public static String signatorySpouseRent() {
		return "//input[contains(@id,'sumExpensesSpouseRent_')]";
	}

	public static String signatoryRatesAndWater() {
		return "//input[contains(@id,'sumExpensesCustomerRates_')]";
	}

	public static String signatorySpouseRatesAndWater() {
		return "//input[contains(@id,'sumExpensesSpouseRates_')]";
	}

	public static String signatoryVehicleInstalment() {
		return "//input[contains(@id,'sumExpensesCustomerVehicleInstallments_')]";
	}

	public static String signatorySpouseVehicleInstalment() {
		return "//input[contains(@id,'sumExpensesSpouseVehicleInstallments_')]";
	}

	public static String signatoryPersonalLoan() {
		return "//input[contains(@id,'sumExpensesCustomerLoanRepayments_')]";
	}

	public static String signatorySpousePersonalLoan() {
		return "//input[contains(@id,'sumExpensesSpouseLoanRepayments_')]";
	}

	public static String signatoryCreditCardRepayment() {
		return "//input[contains(@id,'sumExpensesCustomerCreditCardRepayments_')]";
	}

	public static String signatorySpouseCreditCardRepayment() {
		return "//input[contains(@id,'sumExpensesSpouseCreditCardRepayments_')]";
	}

	public static String signatoryFurnitureAccount() {
		return "//input[contains(@id,'sumExpensesCustomerFurnitureAccounts_')]";
	}

	public static String signatorySpouseFurnitureAccount() {
		return "//input[contains(@id,'sumExpensesSpouseFurnitureAccounts_')]";
	}

	public static String signatoryClothingAccount() {
		return "//input[contains(@id,'sumExpensesCustomerClothingAccounts_')]";
	}

	public static String signatorySpouseClothingAccount() {
		return "//input[contains(@id,'sumExpensesSpouseClothingAccounts_')]";
	}

	public static String signatoryOverdraftRepayment() {
		return "//input[contains(@id,'sumExpensesCustomerOverdraftRepayments_')]";
	}

	public static String signatorySpouseOverdraftRepayment() {
		return "//input[contains(@id,'sumExpensesSpouseOverdraftRepayments_')]";
	}

	public static String signatoryInsurancePayment() {
		return "//input[contains(@id,'sumExpensesCustomerInsurancePayments_')]";
	}

	public static String signatorySpouseInsurancePayment() {
		return "//input[contains(@id,'sumExpensesSpousePolicyInsurancePayments_')]";
	}

	public static String signatoryTelephonePayment() {
		return "//input[contains(@id,'sumExpensesCustomerTelephonePayments_')]";
	}

	public static String signatorySpouseTelephonePayment() {
		return "//input[contains(@id,'sumExpensesSpouseTelephonePayments_')]";
	}

	public static String signatoryTransportCost() {
		return "//input[contains(@id,'sumExpensesCustomerTransport_')]";
	}

	public static String signatorySpouseTransportCost() {
		return "//input[contains(@id,'sumExpensesSpouseTransport_')]";
	}

	public static String signatoryFoodEntertainment() {
		return "//input[contains(@id,'sumExpensesCustomerFoodAndEntertainment_')]";
	}

	public static String signatorySpouseFoodEntertainment() {
		return "//input[contains(@id,'sumExpensesSpouseFoodAndEntertainment_')]";
	}

	public static String signatoryEducationCost() {
		return "//input[contains(@id,'sumExpensesCustomerEducationCosts_')]";
	}

	public static String signatorySpouseEducationCost() {
		return "//input[contains(@id,'sumExpensesSpouseEducationCosts_')]";
	}

	public static String signatoryMaintenance() {
		return "//input[contains(@id,'sumExpensesCustomerMaintenance_')]";
	}

	public static String signatorySpouseMaintenance() {
		return "//input[contains(@id,'sumExpensesSpouseMaintenance_')]";
	}

	public static String signatoryHouseholdExpenses() {
		return "//input[contains(@id,'sumExpensesCustomerHouseholdExpenses_')]";
	}

	public static String signatorySpouseHouseholdExpenses() {
		return "//input[contains(@id,'sumExpensesSpouseHouseholdExpenses_')]";
	}

	public static String signatoryOtherExpenses() {
		return "//input[contains(@id,'sumExpensesCustomerOtherExpenses_')]";
	}

	public static String signatorySpouseOtherExpenses() {
		return "//input[contains(@id,'sumExpensesSpouseOtherExpenses_')]";
	}

	public static String signatoryTotalExpenses() {
		return "//input[contains(@id,'customerTotalExpenses_')]";
	}

	public static String signatorySpouseTotalExpenses() {
		return "//input[contains(@id,'spouseTotalExpenses_')]";
	}

	public static String signatoryOtherIncomeTab() {
		return "//div[@id='otherIncomePanel']";
	}

	public static String signatorypayDay() {
		return "//select[contains(@id,'customerSalaryDay_')]";
	}

	public static String signatoryHomephoneLabel() {
		return "//label[text()='Home Phone']";
	}

	public static String transactionsTab() {
		return "//div[@id='transactions']";
	}

	public static String transactionArticleType() {
		return "//select[contains(@id,'articleType_')]";
	}

	public static String transactionArticleCondition() {
		return "//select[contains(@id,'articleCondition_')]";
	}

	public static String transactionArticleUse() {
		return "//select[contains(@id,'articleUse_')]";
	}

	public static String transactionActualKilometers() {
		return "//input[contains(@id,'kilometerReading_')]";
	}

	public static String transactionChassisNumber() {
		return "//input[contains(@id,'chassisNo_')]";
	}

	public static String transactionRegistrationNumber() {
		return "//input[contains(@id,'registrationNumber_')]";
	}

	public static String transactionEngineNumber() {
		return "//input[contains(@id,'engineNumber_')]";
	}

	public static String transactionExpectedMileage() {
		return "//input[contains(@id,'maxAnnualMileage_')]";
	}

	public static String transactionFinanceType() {
		return "//select[contains(@id,'agreementTypeJuristic_')]";
	}

	public static String transactionPeriodofContact() {
		return "//select[contains(@id,'repaymentPeriod_')]";
	}

	public static String transactionPaymentFrequency() {
		return "//select[contains(@id,'paymentFrequency_')]";
	}

	public static String transactionPaymentMethod() {
		return "//select[contains(@id,'paymentMethod_')]";
	}

	public static String transactionRateType() {
		return "//select[contains(@id,'rateIndicator_')]";
	}

	public static String transactionInterestRate() {
		return "//input[contains(@id,'interestRate_')]";
	}

	public static String transactionPurchasePrice() {
		return "//input[contains(@id,'purchasePrice_')]";
	}

	public static String transactionRetailPrice() {
		return "//input[contains(@id,'retailPrice_')]";
	}

	public static String transactionInitiationFees() {
		return "//select[contains(@id,'financeInitiationFees_')]";
	}

	public static String transactionBallonPercent() {
		return "//input[contains(@id,'residualPercent_')]";
	}

	public static String transactionBallonAmount() {
		return "//input[contains(@id,'residualValue_')]";
	}

	public static String transactionCashDeposit() {
		return "//input[contains(@id,'cashDeposit_')]";
	}

	public static String transactionTradeInDeposit() {
		return "//input[contains(@id,'tradeInDeposit_')]";
	}

	public static String transactionDeposit() {
		return "//input[contains(@id,'deposit_')]";
	}

	public static String transactionPackageIndicator() {
		return "//select[contains(@id,'absaSchemeJuristicIndicator_')]";
	}

	public static String transactionPackageNumber() {
		return "//input[contains(@id,'schemeCodeJuristicAbsa_')]";
	}

	public static String transactionSchemeCode() {
		return "//input[contains(@id,'dealerSubsidy_')]";
	}

	public static String transactionDealType() {
		return "//select[contains(@id,'dealType_')]";
	}

	public static String transactionBreakMonth() {
		return "//select[contains(@id,'takeABreakMonth_')]";
	}

	public static String transactionStatementMethod() {
		return "//select[contains(@id,'businessPreferredStatementMethod_')]";
	}

	public static String transactionSelectVehicleButton() {
		return "//input[contains(@id,'vehicleLookup_')]";
	}

	public static String transactionVehicleDetailsTab() {
		return "//div[text()='Vehicle Details']";
	}

	public static String transactionFinanceDetailsTab() {
		return "//div[text()='Finance']";
	}

	public static String transactionDealerExtraTab() {
		return "//div[text()='Dealer Extras']";
	}

	public static String transactionAddNewExtra() {
		return "//input[contains(@id,'addExtraButton_')]";
	}

	public static String transactionExtraDescription() {
		return "//select[contains(@id,'extraDescription_')]";
	}

	public static String transactionExtraAmount() {
		return "//input[contains(@id,'extraAmtInclVat_')]";
	}

	public static String transactionExtraTotalAmount() {
		return "//input[contains(@id,'extrasAmount_')][@readonly='readonly']";
	}

	public static String transactionStatementType() {
		return "//select[@id='businessBankStatementMethod']";
	}

	public static String transactionPreferredEmail() {
		return "//input[@id='businessPreferredStatementEmailAddress']";
	}

	public static String statementTypeList() {
		return "//select[@id='businessBankStatementMethod']//option";
	}

	public static String marketingTab() {
		return "//div[text()='Marketing']";
	}

	public static String telemarketingCampaign() {
		return "//select[contains(@id,'businessIncludeInTelemarketingCampaign')]";
	}

	public static String includeMarketingList() {
		return "//select[@id='businessIncludeInMarketingList']";
	}

	public static String marketingMethod() {
		return "//select[contains(@id,'businessMarketingCommsMethod')]";
	}

	public static String massDistribution() {
		return "//select[contains(@id,'businessIncludeInMassDistribution')]";
	}

	public static String throughReconnect() {
		return "//select[contains(@id,'trackingDeviceInd')]";
	}

	public static String comprehensiveInsurance() {
		return "//select[contains(@id,'compInsurance')]";
	}

	public static String fIDealerTab() {
		return "//div[@id='dealerInformation']";
	}

	public static String registeredOffical() {
		return "//select[@id='ncaFaisRegisteredOfficial']";
	}

	public static String notificationMethod() {
		return "//select[@id='returnAnswerVia']";
	}

	public static String submitApplicationButton() {
		return "//input[@id='submitAction']";
	}

	public static String sourceOfFundsCheckBoxes() {
		return "//div[@class='grid-12-12 mySourceOfFunds']//div//p//input[contains(@class,'error')]";
	}

	public static String beeIncomeCheckBox() {
		return "//input[@id='sourceOfFundsBEE'][@disabled]";
	}

	public static String winningsCheckBox() {
		return "//input[@id='sourceOfFundsWinnings'][@disabled]";
	}

	public static String allowanceCheckBox() {
		return "//input[@id='sourceOfFundsAllowance']";
	}

	public static String financialInstitutionAlert() {
		return "//div[contains(@class,'sweet-alert')]";
	}

	public static String alertConfirmButton() {
		return "//button[contains(@class,'confirm')]";
	}

	// xpath for compare podium.
	public static String CompareButton() {
		return "//button[contains(@class,'compare')]";
	}
	
	public static String InterestRate() {
		return "//button[contains(@class,'interestRate')]";
	}
	
	public static String SubmitButton() {
		return "//button[contains(@class,'submitAll')]";
	}

}
