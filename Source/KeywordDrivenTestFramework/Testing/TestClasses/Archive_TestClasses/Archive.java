/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Archive_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.openqa.selenium.Keys;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Archive",
        createNewBrowserInstance = false
)

public class Archive extends BaseClass
{

    String error = "";
    String currentTabHandle = SeleniumDriverInstance.Driver.getWindowHandle();
    String archiveTabHandle = "";
    String dealFileTabHandle = "";
    String declinePodium = "";
    String absaDocRefNumber = getData("ABSA BANK");
    String idNumber = testData.getData("ID Number");
    String clientName = testData.getData("Client Name");
    String clientSurname = testData.getData("Surname");
    String cellNumber = testData.getData("Cellphone Number");
    String emailAddress = testData.getData("Email Address");
    String chassisNumber = testData.getData("Chassis Number");
    String stockNumber = testData.getData("Stock Number");
    String registrationNumber = testData.getData("Registration Number");
    String fromDate = testData.getData("From Date");
    String toDate = testData.getData("To Date");
    String uniqueID = testData.getData("Unique ID");
    String invalidDate = testData.getData("Invalid Date");
    String fileName = testData.getData("File Name");
    String fileExtension = testData.getData("File Extension");
    String finalFileExtensionName = fileName + fileExtension;

    public Archive()
    {
    }

    public TestResult executeTest()
    {
        if (!archive())
        {
            return narrator.testFailed("Failed to download the pdf form of the application from archive - " + error);
        }

        return narrator.finalizeTest("Successfully downloaded the pdf file of the application from archive.");
    }

    public boolean archive()
    {

        /*Entering archive details*/
        List<EnterData> enterDataList = new ArrayList<>();

        enterDataList.add(new EnterData(LightStonePageObjects.signioArchiveIdNumber(), idNumber));
        enterDataList.add(new EnterData(LightStonePageObjects.signioArchiveClientName(), clientName));
        enterDataList.add(new EnterData(LightStonePageObjects.signioArchiveClientSurname(), clientSurname));
        enterDataList.add(new EnterData(LightStonePageObjects.signioArchiveMobileNumber(), cellNumber));
        enterDataList.add(new EnterData(LightStonePageObjects.signioArchiveEmailAddress(), emailAddress));
        enterDataList.add(new EnterData(LightStonePageObjects.signioArchiveChassisNumber(), chassisNumber));
        enterDataList.add(new EnterData(LightStonePageObjects.signioArchiveStockNumber(), stockNumber));
        enterDataList.add(new EnterData(LightStonePageObjects.signioArchiveReagistrationNumber(), registrationNumber));

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.archive()))
        {
            error = "Failed to wait for the archive tab to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.archive()))
        {
            error = "Failed to click the archive tab.";
            return false;
        }
        pause(2500);
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to the archive tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.signioLogo()))
        {
            SeleniumDriverInstance.pressKey(Keys.F5);

            if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.signioLogo()))
            {
                error = "Failed to wait for the signio logo to appear.";
                return false;
            }
        }
        archiveTabHandle = SeleniumDriverInstance.Driver.getWindowHandle();

        narrator.stepPassedWithScreenShot("Successfully landed on archive tab.");

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.searchArchive())) //might be an unneccessary wait.
        {
            error = "Failed to wait for the search button to appear.";
            return false;
        }

        for (EnterData currentEnterDataObj : enterDataList)
        {
            String currentDataToEnter = currentEnterDataObj.getDataToEnter();
            if (!SeleniumDriverInstance.enterTextByXpath(currentEnterDataObj.getXpath(), currentDataToEnter))
            {
                error = "Failed to enter " + currentDataToEnter + " from the text field.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.dateRangeCheckBox()))
        {
            error = "Failed to click the use date range check box.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signioArchiveFromDate(), fromDate))
        {
            error = "Failed to enter " + fromDate + " into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signioArchiveToDate(), toDate))
        {
            error = "Failed to enter " + toDate + " into the text field.";
            return false;
        }

        narrator.stepPassed("ID Number : " + idNumber);
        narrator.stepPassed("Name: " + clientName);
        narrator.stepPassed("Surname : " + clientSurname);
        narrator.stepPassed("Email Address  : " + emailAddress);
        narrator.stepPassed("Chassis Number  : " + chassisNumber);
        narrator.stepPassed("Stock Number  : " + stockNumber);
        narrator.stepPassed("Registration Number  : " + registrationNumber);
        narrator.stepPassed("From Date : " + fromDate);
        narrator.stepPassed("To Date : " + toDate);

        narrator.stepPassedWithScreenShot("Successfully entered applicant's details on archive.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchArchive()))
        {
            error = "Failed to click search.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchResults(idNumber)))
        {
            error = "Failed to wait for the archived client with this id number - " + idNumber + ".";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully returned search results.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchResults(idNumber)))
        {
            error = "Failed to click open.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to a deal file tab.";
            return false;
        }

        dealFileTabHandle = SeleniumDriverInstance.Driver.getWindowHandle();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealFileId(uniqueID)))
        {
            error = "Failed to load the deal file tab.";
            narrator.stepFailedWithScreenShot(error);
            closeCurrentTab();

            //return false; // must continue with the test.
        }

        narrator.stepPassedWithScreenShot("Successfully opened the deal file ID : " + uniqueID);
        if (!closeCurrentTab())
        {
            error = "Failed to close current tab.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clickIDNumber(idNumber)))
        {
            error = "Failed to wait for the archived client.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully returned to the archive search result ");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickIDNumber(idNumber)))
        {
            error = "Failed to click on the id number  - " + idNumber + ".";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.absaDeclinePodium(absaDocRefNumber)))
        {
            error = "Failed to wait for the- " + absaDocRefNumber + " decline Podium file.";
            return false;
        }

        if (!SeleniumDriverInstance.deleteAllFilesInFolder(SeleniumDriverInstance.DIR_DOWNLOADS, fileName))
        {
            error = "Failed to delete the file in the download directory";

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.absaDeclinePodium(absaDocRefNumber)))
        {
            error = "Failed to click for the- " + absaDocRefNumber + " decline Podium file.";
            return false;
        }

        boolean fileFound = false;//SeleniumDriverInstance.isFileInFolder(SeleniumDriverInstance.DIR_DOWNLOADS, name);
        int timeOut = 60;
        int count = 0;
        while (!fileFound && count < timeOut)
        {
            if (!SeleniumDriverInstance.isFileInFolder(SeleniumDriverInstance.DIR_DOWNLOADS, fileName))
            {
                pause(3000);
                count++;
            } else if (SeleniumDriverInstance.isFileInFolder(SeleniumDriverInstance.DIR_DOWNLOADS, fileName))
            {
                fileFound = true;
                narrator.stepPassed("Successfully downloaded the  pdf file.");
            }

        }

        String name = getTheNewestFile(fileName);

        if (!SeleniumDriverInstance.moveFileToCurrentDirectory(name))
        {
            error = "Failed to move file " + fileName + " to the report directory.";
            narrator.stepFailedWithScreenShot(error);
            closeCurrentTab();
        }

        if (!SeleniumDriverInstance.ReadReportFile(reportDirectory + "/" + name).contains("Application for Finance (Declined)"))
        {
            error = "Failed to validate the Pdf file header message.";
            return false;
        }
        narrator.stepPassed("Successfully validated the PDF file header is: Application for Finance (Declined)");
        if (!closeTab())
        {
            error = "Failed to close the pdf downloadable pdf file page..";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully moved back to the home page.");
        return true;
    }

    private boolean closeCurrentTab()
    {
        if (!SeleniumDriverInstance.closeCurrentTab(archiveTabHandle))
        {
            error = "Failed to close the current tab.";
            return false;
        }
        return true;
    }

    private boolean closeTab()
    {
        if (!SeleniumDriverInstance.closeTab(currentTabHandle))
        {
            error = "Failed to close other tabs.";
            return false;
        }
        return true;
    }

    public String ReadFile(String filePath)
    {
        String file = filePath;

        String line = null;

        String text = "";

        try
        {
            FileReader fileReader = new FileReader(file);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null)
            {
                text += line;
            }

            bufferedReader.close();
        } catch (FileNotFoundException ex)
        {
            System.out.println("Unable to open file '" + file + "'");
        } catch (IOException ex)
        {
            System.out.println("Error reading file '" + file + "'");
        }
        return text;
    }

    class EnterData
    {

        private String xpath = null;
        private String dataToEnter = null;

        public EnterData(String xpath, String dataToEnter)
        {
            this.xpath = xpath;
            this.dataToEnter = dataToEnter;
        }

        public String getXpath()
        {
            return xpath;
        }

        public String getDataToEnter()
        {
            return dataToEnter;
        }

    }

    private String getTheNewestFile(String fileName)
    {
        try
        {
            String latestFile = "";
            ArrayList<File> fileList = new ArrayList<>(Arrays.asList(new File(SeleniumDriverInstance.DIR_DOWNLOADS).listFiles()));

            for (File file : fileList)
            {
                if (file.getName().contains(fileName))
                {
                    latestFile = file.getName();
                    break;
                }

            }
            return latestFile;

        } catch (Exception e)
        {
            System.err.println("Failed to get downloaded file name- " + e.getMessage());
            return "";
        }
    }

}
