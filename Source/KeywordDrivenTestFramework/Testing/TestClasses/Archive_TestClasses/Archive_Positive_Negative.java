/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Archive_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Archive_Positive_Negative",
        createNewBrowserInstance = false
)

public class Archive_Positive_Negative extends BaseClass
{

    String error = "";

    String currentTabHandle = SeleniumDriverInstance.Driver.getWindowHandle();

    String xpathFieldToEnter = testData.getData("xpathFieldToEnter");
    String dataToEnter = testData.getData("dataToEnter");
    String xpathFieldToValidate = testData.getData("xpathFieldToValidate");
    String field = getData("Field");

    String fromDate = testData.getData("From Date");
    String toDate = testData.getData("To Date");
    boolean isTestPositive;

    public Archive_Positive_Negative()
    {
    }

    public TestResult executeTest()
    {

        isTestPositive = Boolean.parseBoolean(getData("isTestPositive"));
        String testType = isTestPositive ? "Positive" : "Negative";

        if (!navToArchive())
        {
            return narrator.testFailed("Failed to search for the archived client using " + field + " - " + error);
        }

        switch (field)
        {
            case "Date":
                //Date specific
                if (!searchDate())
                {
                    return narrator.testFailed("Failed to do " + testType + " search " + field + " - " + error);
                }
                break;
            default:
                if (!searchField())
                {
                    return narrator.testFailed("Failed to do " + testType + " search " + field + " - " + error);
                }
                break;
        }

        return narrator.finalizeTest("Successfully searched applicants Archive files using " + field);
    }

    public boolean navToArchive()
    {
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.archive()))
        {
            error = "Failed to wait for the archive tab to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.archive()))
        {
            error = "Failed to click the archive tab.";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to the archive tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.signioLogo()))
        {
            SeleniumDriverInstance.pressKey(Keys.F5);
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.signioLogo()))
            {
                error = "Failed to wait for the signio logo to appear.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Successfully landed on archive for positive/negative search using " + field + ".");
        return true;
    }

    public boolean searchDate()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.dateRangeCheckBox()))
        {
            error = "Failed to click the use date range button'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signioArchiveFromDate(), fromDate))
        {
            error = "Failed to enter " + fromDate + " into text field.";
            return false;
        }

        narrator.stepPassed("From Date : " + fromDate);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signioArchiveToDate(), toDate))
        {
            error = "Failed to enter " + toDate + " into the text field.";
            return false;
        }

        narrator.stepPassed("To Date : " + toDate);

        narrator.stepPassedWithScreenShot("Successfully entered date.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchArchive()))
        {
            error = "Failed to click search.";
            return false;
        }

        if (isTestPositive)
        {
            if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.archiveProxyError(), 120))
            {
//                error = "Received HTTP Proxy Error";
                narrator.stepFailedWithScreenShot("Received HTTP Proxy Error");
                closeTab();
                return false;
            }

//            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchResultsGenericRow()))
//            {
//                error = "Failed to load results given the date range.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully returned results using " + field);
            //Cont. positive test after this works.
        } else
        {//Negative Test

            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchArchive()))
            {
                error = "Failed to click search.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.archive_DateResult_DateCannotBeGreater()))
            {
                error = "Failed to load the message: from date can't be greater to date.";
                return false;
            }
            String errorMessage = SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.archive_DateResult_DateCannotBeGreater());
            narrator.stepPassedWithScreenShot("Successfully checked the search return an error message : " + errorMessage);
        }

        if (!closeTab())
        {
            error = "Failed to close Tabs.";
            return false;
        }

        return true;
    }

    public boolean searchField()
    {
        String result = "";
        if (!SeleniumDriverInstance.waitForElementByXpath(xpathFieldToEnter))
        {
            error = "Failed to wait for " + dataToEnter;
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(xpathFieldToEnter, dataToEnter))
        {
            error = "Failed to enter " + dataToEnter + " into text field - " + xpathFieldToEnter;
            return false;
        }

        narrator.stepPassed(field + " : " + dataToEnter);
        narrator.stepPassedWithScreenShot("Successfully entered " + field);

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchArchive()))
        {
            error = "Failed to click search.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(xpathFieldToValidate))
        {
            error = "Failed to wait for the results.";
            closeTab();
            return false;
        }
        result = SeleniumDriverInstance.retrieveTextByXpath(getData("Result"));
        narrator.stepPassedWithScreenShot("Successfully returned : " + result);
        if (!closeTab())
        {
            error = "Failed to close tab.";
            return false;
        }
        narrator.stepPassed("Successfully moved back to the inbox home page.");
        return true;
    }

    private boolean closeTab()
    {
        if (!SeleniumDriverInstance.closeTab(currentTabHandle))
        {
            error = "Failed to close the current tab.";
            return false;
        }

        return true;
    }

}
