/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inbox_TestClasses;

import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Costing Sheet",
        createNewBrowserInstance = false
)

public class CostingSheetPodium extends BaseClass
{

    String error = "";

    public TestResult executeTest()
    {
        if (!csPodium())
        {
            return narrator.testFailed("Failed to validate the Costing Sheet Podium -" + error);
        }

        return narrator.finalizeTest("Successfully validated Inbox dashboard page.");
    }

    public boolean csPodium()
    {
        // Get Current tab title
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to wait for search criteria.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to click on search criteria.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearchButton()))
        {
            error = "Failed to click on clear.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.columnLinkXpath("CS", 17)))
        {
            error = "Failed to click on 'Costing Sheet' button.";
            return false;
        }

        narrator.stepPassed("Successfully validated that the 'Costing Sheet' button is present.");

        // Clicking on 'CS' button
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.columnLinkXpath("CS", 17)))
        {
            error = "Failed to click on 'Costing Sheet' button.";
            return false;
        }

        // Waiting for 'Cost Sheet' modal
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.costSheet()))
        {
            error = "Failed to wait for 'Cost Sheet' modal";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully opened the Costing Sheet modal.");

        /**
         * Not going according to test cases.
         */
        // Clicking on 'Submit' from 'cost sheet' modal
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.submitCostSheetModal()))
        {
            error = "Failed to click on 'Submit' from 'cost sheet' modal.";
            return false;
        }

        // Waiting for 'Confirm Override?' modal
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.confirmOverrideCostSheetModal()))
        {
            error = "Failed to wait for 'Confirm Override?' modal";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully showing an option to cancel or continue.");

        // Clicking on 'Yes' button to confirm Override of cost sheet
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.yesConfirmOverrideCostSheetModal()))
        {
            error = "Failed to click on 'Yes' button to confirm Override of cost sheet.";
            return false;
        }

        // Switch New tab
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to Switch to a new tab.";
            return false;
        }

        // Validate that tab open is from column
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.CostSheetPage()))
        {
            error = "Failed to Validate that tab open is from 'Costing Sheet' column.";
            return false;
        }

        //extracting new tab Title page name to extends report
        testData.extractParameter("Cost Sheet page Title", SeleniumDriverInstance.Driver.getTitle(), "");
        narrator.stepPassedWithScreenShot("Successfully opened the 'Cost Sheet' column.");

        // Close the new Tab and switch back to original tab
        if (!SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle()))
        {
            error = "Failed to close the current tab.";
            return false;
        }
        return true;
    }

}
