/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inbox_TestClasses;

import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Leads",
        createNewBrowserInstance = false
)

public class LeadsPodium extends BaseClass
{

    String error = "";

    public TestResult executeTest()
    {
        if (!leadsPodium())
        {
            return narrator.testFailed("Failed to validate the Leads Podium -" + error);
        }

        return narrator.finalizeTest("Successfully validated Inbox dashboard page.");
    }

    public boolean leadsPodium()

    {
        // Clicking on 'lead' link
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.columnButtonXpath("LEADS", 14)))
        {
            error = "Failed to wait for the 'Leads' button.";
            return false;
        }
        narrator.stepPassed("Successfully validated that the 'Leads' button is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.columnButtonXpath("LEADS", 14)))
        {
            error = "Failed to click on the 'leads' button";
            return false;
        }

        // Get Current tab title
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        // Switch New tab
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to Switch to a new tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.leadsPageTitle()))
        {
            error = "Failed to Validate that tab opened is from 'Leads' column";
            return false;
        }

        //extracting new tab Title page name to extends report
        testData.extractParameter("Leads page Title", SeleniumDriverInstance.Driver.getTitle(), "");
        narrator.stepPassedWithScreenShot("Successfully opened the 'Leads' column.");

        // Close the new Tab and switch back to original tab
        if (!SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle()))
        {
            error = "Failed to close the current tab.";
            return false;
        }

        return true;
    }

}
