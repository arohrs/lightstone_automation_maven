/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inbox_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.NavigateAndLogin;
import KeywordDrivenTestFramework.Testing.TestClasses.Logout;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebElement;

/**
 *
 * @author nkelechi
 */
@KeywordAnnotation(
        Keyword = "Inbox",
        createNewBrowserInstance = false
)

public class Inbox extends BaseClass
{

    String error = "", expectedFileName, path;
    String originalHandle = "";

    public Inbox()
    {
    }

    public TestResult executeTest()
    {
        if (!inbox())
        {
            // Delete file from download folder in c
            if (new File(path).exists())
            {
                new File(path).delete();
            }
            return narrator.testFailed("Failed to validate Inbox dashboard page - " + error);
        }
        return narrator.finalizeTest("Successfully validated Inbox dashboard page.");
    }

    public boolean setup()
    {
        // Waiting for 'Search criteria'
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to wait for 'Search criteria'.";
            return false;
        }

        // Clicking on 'Search Criteria' accordion tab
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to click on 'Search Criteria' accordion tab.";
            return false;
        }

        // Waiting for 'Show deals for the last' dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.showDealsForLast()))
        {
            error = "Failed to wait for 'Show deals for the last' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully showing the search criteria bar.");

        // Select 3 Days as default 'Show deals for the last' dropdown
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.showDealsForLast(), testData.getData("Show Deal for 3")))
        {
            error = "Failed to Select '" + testData.getData("Show Deal for 1") + "' as default 'Show deals for the last' dropdown";
            return false;
        }

        // Waiting for inbox table to load
        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }

        // Waiting for 'Search criteria'
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to wait for 'Search criteria'";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to click on 'Search Criteria' accordion tab";
            return false;
        }

        return true;
    }

    public boolean inbox()
    {
        // Waiting for Boardroom logo
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.boardRoomLogoXpath()))
        {
            error = "Failed to wait for Boardroom logo";
            return false;
        }
        narrator.stepPassed("Successfully validated that the 'Boardroom Logo' is available.");
        // Clicking on board Room logo
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.boardRoomLogoXpath()))
        {
            error = "Failed to click on board Room logo";
            return false;
        }

        // Waiting for Boardroom logo enlarged
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.boardroomLogoEnlargeXpath()))
        {
            error = "Failed to wait for Boardroom logo enlarged";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully enlarged the boardroom logo.");

        // Clicking on close modal
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.logoModalCloseButtonXpath()))
        {
            error = "Failed to click on close modal";
            return false;
        }

        Logout loggingOut = new Logout();
        if (!loggingOut.logout())
        {
//            error = "Inbox Test case - " + loggingOut.error;
            error = "Inbox Test case - Log Out";
            return false;
        }

        NavigateAndLogin login = new NavigateAndLogin();
        if (!login.navigateToLightStonePage())
        {
//            error = "Inbox Test case - " + login.error;
            error = "Inbox Test case - Login";
            return false;
        }
        //narrator.stepPassed("Successfully logged back in to the site.");

        if (!setup())
        {
            error = "Environmment not ready for testing - " + error;
            return false;
        }
        // getting Column headers from testpack
        ArrayList<String> columnHeaders = new ArrayList<>();
        columnHeaders.add(testData.getData("Column1"));
        columnHeaders.add(testData.getData("Column2"));
        columnHeaders.add(testData.getData("Column3"));
        columnHeaders.add(testData.getData("Column4"));

        for (int i = 0; i < columnHeaders.size(); i++)
        {
            String currentColumn = columnHeaders.get(i);
            if (!validateColumnSorting(currentColumn, (i + 1)))
            {
                error = "Validating column '" + currentColumn + "' - " + error;
                //narrator.stepFailedWithScreenShot(error);

                return false;
            }
            // narrator.stepPassedWithScreenShot("Successfully validated that column '" + columnHeaders.get(i) + "' can be sorted in alphabetical order.");
        }

        /**
         * Validate Podium1 = ABSA BANK
         */
        if (!validatePodium(testData.getData("Podium1"), 6))
        {
            error = "Failed while validating podium '" + testData.getData("Podium1") + "' - " + error;
            return false;
        }

        /**
         * Validate podium3 = WESBANK
         */
        if (!validatePodium(testData.getData("Podium2"), 7))
        {
            error = "Failed while validating podium '" + testData.getData("Podium2") + "' - " + error;
            return false;
        }

        /**
         * Validate podium3 = Standard Bank
         */
        if (!validatePodium(testData.getData("Podium3"), 8))
        {
            error = "Failed while validating podium '" + testData.getData("Podium3") + "' - " + error;
            return false;
        }

        /**
         * Validate podium4 = MFC
         */
        if (!validatePodium(testData.getData("Podium4"), 11))
        {
            error = "Failed while validating podium '" + testData.getData("Podium4") + "' - " + error;
            return false;
        }

        /**
         * Validate podium5 = SIGNIO (PTY) LTD
         */
        // Validate podium
        if (!validatePodium(testData.getData("Podium5"), 14))
        {
            error = "Failed while validating podium '" + testData.getData("Podium5") + "' - " + error;
            return false;
        }

        /**
         * Validating Search Criteria functionality
         */
        // retrieve list of WebElement of podium button 
        List<WebElement> columnIDNumber = SeleniumDriverInstance.retrieveWebElementByXpath(LightStonePageObjects.columnExtactValue(3));
        String id_number = getData("ID Number"); // columnIDNumber.get(1).getText();
        testData.extractParameter("ID Number used:", id_number, "");

        columnIDNumber = SeleniumDriverInstance.retrieveWebElementByXpath(LightStonePageObjects.columnExtactValue(1));
        String surname = getData("Surname"); //columnIDNumber.get(1).getText();
        testData.extractParameter("Surname used:", surname, "");

        String companyRegistrationNumber = "";

        // Waiting for 'Search criteria'
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to wait for 'Search criteria'";
            return false;
        }

        // Clicking on 'Search Criteria' accordion tab
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to click on 'Search Criteria' accordion tab";
            return false;
        }

        // Waiting for 'Show deals for the last' dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.showDealsForLast()))
        {
            error = "Failed to wait for 'Show deals for the last' dropdown";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully showing deals.");
        // Select 35 Days as default 'Show deals for the last' dropdown
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.showDealsForLast(), testData.getData("Show Deal for 35")))
        {
            error = "Failed to Select '" + testData.getData("Show Deal for 35") + "' as default 'Show deals for the last' dropdown";
            return false;
        }

        // Waiting for inbox table to load
        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }

        if (!validateSearchCriteria(testData.getData("Search By 1"), id_number, 3))
        {
            error = "Failed while filter for '" + testData.getData("Search By") + "' - " + error;
            return false;
        }

        if (!validateSearchCriteria(testData.getData("Search By 2"), surname, 1))
        {
            error = "Failed while filter for '" + testData.getData("Search By") + "' - " + error;
            return false;
        }

        /**
         * Impediments on 'company registration number ' field - Field does not
         * appear on the table to validate sorting.
         */
//        if (!validateSearchCriteria(testData.getData("Search By 3"), companyRegistrationNumber, 1)) {
//            error = "Failed while filter for '" + testData.getData("Search By") + "' - " + error;
//            return false;
//        }
        return true;
    }

//        if (!validateSearchCriteria(testData.getData("Search By 2"), getData("Criteria"), 1)) {
//            error = "Failed while filter for '" + testData.getData("Search By") + "' - " + error;
//            return false;
//        }
    // Clicking on 'DOC'
//        if (!validateSearchCriteria(testData.getData("Search By 2"), getData("Criteria"), 1))
//        {
//            error = "Failed while filter for '" + testData.getData("Search By") + "' - " + error;
//            return false;
//        }
    public boolean supportDropDown()
    {
        // Clicking on 'Support' dropdown
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.supportDropdownXpath()))
        {
            error = "Failed to click on 'Support' dropdown";
            return false;
        }

        // Waiting for 'Help' dropdown item
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpItem()))
        {
            error = "Failed to wait for 'Help' dropdown item";
            return false;
        }

        // Clicking on 'Help' dropdown Item
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.helpItem()))
        {
            error = "Failed to click on 'Help' dropdown Item";
            return false;
        }

        // Waiting for 'Software links' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpPanel("Software links")))
        {
            error = "Failed to wait for 'Software links' panel";
            return false;
        }

        // Waiting for 'Vaps' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpPanel("VAPS")))
        {
            error = "Failed to wait for 'Vaps' panel";
            return false;
        }

        // Waiting for 'Test bank doc' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpPanel("test bank doc")))
        {
            error = "Failed to wait for 'Test bank doc' panel";
            return false;
        }

        // Waiting for 'Help' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpPanel("Help")))
        {
            error = "Failed to wait for 'Help' panel";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to 'Help' page.");

        // Downlaod File embedded on the link button
        // Download 'FAQs FOR VAPS.PDF' file
        if (!SeleniumDriverInstance.downloadLinkText("FAQ'S FOR VAPS.PDF"))
        {
            error = "Failed to Download 'FAQs FOR VAPS.PDF' file";
            return false;
        }

        // delay of 5s for download to complete
        pause(5000);

        expectedFileName = "FAQ'S FOR VAPS.PDF";
        // Check if 'epad devices.exe' is in the downloads folder
        // Construct path
        path = System.getProperty("user.home") + "/Downloads/" + expectedFileName;
        if (!new File(path).exists())
        {
            error = "Failed to download 'ePad devices.exe' file.";
            return false;
        }

        // Move Expected File to current report directory
        try
        {
            File downloadFolder = new File(reportDirectory + "/" + testCaseId + "/file");
            downloadFolder.mkdir();
            FileUtils.moveFileToDirectory(new File(path), downloadFolder, false);
        } catch (IOException ex)
        {
            error = "Failed to move '" + expectedFileName + "' file to report folder";
            return false;
        }

        // Clicking on 'Inbox' menu
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.inboxMenu()))
        {
            error = "Failed to click on 'Inbox' menu";
            return false;
        }

        // Waiting for inbox table to load
        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }

        // Waiting for 'Search criteria'
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to wait for 'Search criteria'";
            return false;
        }

        // Clicking on 'Terms and Conditions'
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.termsAndConditions()))
        {
            error = "Failed to click on 'Terms and Conditions'";
            return false;
        }

        // Switch New tab
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to Switch New tab";
            return false;
        }

        // Validate that tab open is from column
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.embeddedTerms()))
        {
            error = "Failed to Validate that tab open is from 'Leads' column";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully opened 'Terms and conditions' page.");

        //extracting new tab Title page name to extends report
        testData.extractParameter("Terms and condition page Title", SeleniumDriverInstance.Driver.getTitle(), "");

        // Close the new Tab and switch back to original tab
        SeleniumDriverInstance.closeTab(originalHandle);

        /**
         * Archives Page not working (19/09/2018)
         */
        // Clicking on 'Archives' menu
//        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.archiveMenu())) {
//            error = "Failed to click on 'Archives' menu";
//            return false;
//        }
//
//        // Switch New tab
//        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
//            error = "Failed to Switch New tab";
//            return false;
//        }
//
//        // Validate that tab open is from column
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.signioArchivePage())) {
//            error = "Failed to Validate that tab open is from 'Leads' column";
//            return false;
//        }
//
//        //extracting new tab Title page name to extends report
//        testData.extractParameter("Signio Archive page Title", SeleniumDriverInstance.Driver.getTitle(), "");
//        narrator.stepPassedWithScreenShot("Successfully opened 'Signio Archive' page.");
//
//        // Close the new Tab and switch back to original tab
//        SeleniumDriverInstance.closeTab(originalHandle);
        return true;
    }

    public boolean validateColumnSorting(String columnName, int index)
    {
        if (!columnName.isEmpty())
        {

            // Retrieve list of column before sorting
            ArrayList<String> columnRowsBefore = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.tableInboxListPerColumn(columnName, index));

            // Clicking on 'Surname Colummn'
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.tableInboxColumnXpath(columnName)))
            {
                error = "Failed to click on '" + columnName + "' Colummn";
                return false;
            }

            // Retrieve list of column after sorting
            ArrayList<String> columnRowsAfter = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.tableInboxListPerColumn(columnName, index));

            // Sort element extract before clicking to compare with element sorted after clicking on the column header
            Collections.sort(columnRowsBefore);

            // Comparing list before and after sorting
            for (int i = 0; i < columnRowsBefore.size(); i++)
            {
                if (!columnRowsBefore.get(i).equals(columnRowsAfter.get(i)))
                {
                    error = "Failed to Validate Order of Column";
                    return false;
                }
            }

            narrator.stepPassedWithScreenShot("Successfully validated that column '" + columnName + "' can be sorted in alphabetical order.");

            return columnRowsAfter.equals(columnRowsBefore);

        }
        return true;
    }

    public boolean validatePodium(String podiumName, int index)
    {
        try
        {
            // retrieve list of WebElement of podium button s
            List<WebElement> columnButtons = SeleniumDriverInstance.retrieveWebElementByXpath(LightStonePageObjects.columnButtonXpath(podiumName, index));

            // Click on first button
            /**
             * To click on the first podium button, two condition need to be
             * met: 1.Button should not be hidden 2. class attribute should not
             * contains 'no_click' So the next line filter for those condition
             * through the lsit of WebElements and click on the first button
             */
            WebElement elementToClick = columnButtons.stream().filter(element -> (!element.getAttribute("type").equals("hidden") && !element.getAttribute("class").contains("no_click"))).findFirst().get();
            // Click podium button
            if (!SeleniumDriverInstance.clickWebElementUsingActions(elementToClick))
            {
                error = "Failed to Click podium button";
                return false;
            }

            // accept alert
            SeleniumDriverInstance.alertHandlerUsingJavaScript();

            // Get Current tab title
            String originalHandle = SeleniumDriverInstance.getWindowHandle();

            // Switch New tab
            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to Switch New tab.";
                return false;
            }

            //extracting new tab Title page name to extends report
            testData.extractParameter("Podium page Title", SeleniumDriverInstance.Driver.getTitle());
            narrator.stepPassedWithScreenShot("Successfully opened '" + podiumName + "' podium.");

            // Close the new Tab and switch back to original tab
            SeleniumDriverInstance.closeTab(originalHandle);
            return true;
        } catch (Exception e)
        {
            error = "Failed to extract '" + podiumName + "' for validation - ";
            return false;
        }

    }

    public boolean validateSearchCriteria(String searchCrieria, String criteriaValue, int index)
    {
        // Wait for 'Search Criteria'
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to wait for the table to appear";
            return false;
        }

        // Clicking on 'Search Criteria' accordion tab
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to click on 'Search Criteria' accordion tab";
            return false;
        }

        // Selecting from 'Search By' dropdown
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.searchByXpath(), searchCrieria))
        {
            error = "Failed to Select '" + searchCrieria + "' from 'Search By' dropdown";
            return false;
        }

        // Entering in 'Criteria' field
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.criteriaField(), criteriaValue))
        {
            error = "Failed to enter '" + criteriaValue + "' in 'Criteria' field";
            return false;
        }

        // Clicking on 'search' button
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchButtonInboxFilter()))
        {
            error = "Failed to click on 'search' button";
            return false;
        }

        // Retrieve list of column before sorting
        ArrayList<String> criteriaColumnValues = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.tableInboxListPerColumn(searchCrieria.toUpperCase(), index));

        for (String criteriaColumnValue : criteriaColumnValues)
        {
            if (!criteriaColumnValue.equals(criteriaValue))
            {
                error = "Failed to validate filter result - Found: '" + criteriaValue + "' but expected: '" + criteriaValue + "'";
                return false;
            }
        }
        return true;
    }
}
