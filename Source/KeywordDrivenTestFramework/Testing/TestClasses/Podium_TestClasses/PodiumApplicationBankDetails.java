/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Podium_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author aswaphi
 */
@KeywordAnnotation(
        Keyword = "LIG51_PodiumDetails_S5",
        createNewBrowserInstance = false
)

public class PodiumApplicationBankDetails extends BaseClass
{
    
    String error = "";
    
    public PodiumApplicationBankDetails()
    {
    }
    
    public TestResult executeTest()
    {
        
        if (!podiumApplicationBankDetails())
        {
            return narrator.testFailed("Failed to update podium  bank details. - " + error);
        }
        if (!podiumApplicationMarketConsentDetails())
        {
            return narrator.testFailed("Failed to update podium application market consent details details. - " + error);
        }
        
        return narrator.finalizeTest("Successfully updated podium application bank details on the Form.");
    }
    
    public boolean podiumApplicationBankDetails()
    {
        /*updating podium bank details*/
        
        String validAccount = getData("Valid Account");
        String accountHolder = getData("Account Holder");
        String bankName = getData("Bank Name");
        String accountNumber = getData("Account Number");
        String accountType = getData("Account Type");
        String bankBranch = getData("Bank Branch");
        
        narrator.stepPassed("Bank Branch  :" + bankBranch);
        narrator.stepPassed("Account Holder  : " + accountHolder);
        narrator.stepPassed("Bank Name :" + bankName);
        narrator.stepPassed("Valid Account  :" + " " + getData("Valid Account"));
        narrator.stepPassed(" Account Number :" + accountNumber);
        narrator.stepPassed("Account Type :" + accountType);
        
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumBankingDetails()))
        {
            error = "Failed to scroll to the banking details heading.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountHolder(), accountHolder))
        {
            error = "Failed to enter account holder details into the text field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankNameDropDown(), bankName))
        {
            error = "Failed to select the 'bank name' from the list.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.bankBranch(), bankBranch))
        {
            error = "Failed to select bank branch.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.bankBrachText()))
        {
            error = "Failed to select bank branch.";
            return false;
        }
        
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.validAccountNumber(), validAccount))
        {
            error = "Failed to select Yes/No to confirm if account is valid.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountNumber(), accountNumber))
        {
            error = "Failed to enter an account number into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.accountType(), accountType))
        {
            error = "Failed to select account type from the list.";
            return false;
        }
        
        return true;
    }

    public boolean podiumApplicationMarketConsentDetails()
    {
        
        String salesPerson = getData("Sales Person");
        String marketingConsent = getData("Marketing Purposes");
        
        narrator.stepPassed("Sales Person : " + salesPerson);
        
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.podiumSalesPerson(), salesPerson))
        {
            error = "Failed to enter sales person into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.podiumMarketingPurposes(), marketingConsent))
        {
            error = "Failed to Select yes/No for market Consent.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
        {
            error = "Failed to submit application.";
            return false;
        }
        return true;
        
    }
    
}
