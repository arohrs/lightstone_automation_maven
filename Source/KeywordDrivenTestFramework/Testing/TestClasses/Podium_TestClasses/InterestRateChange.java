package KeywordDrivenTestFramework.Testing.TestClasses.Podium_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author arohrs
 */
@KeywordAnnotation(Keyword = "LIG186_SBSA_Interest", createNewBrowserInstance = false)

public class InterestRateChange extends BaseClass {

	String error = "";

	public TestResult executeTest() {
		
		String interestRate = getData("Interest Rate");

		if (!podiumCompare()) {
			return narrator.testFailed("Failed to update podium  bank details. - " + error);
		}

		return narrator.finalizeTest("Successfully updated interest rate.");
	}

	public boolean podiumCompare() {
		/* updating podium bank details */

		String InterestRate = getData("InterestRate");

		narrator.stepPassed("Interest Rate:" + InterestRate);
		
		 if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.CompareButton()))
	        {
	            error = "Failed to click the 'submit application' button.";
	            return false;
	        }
		if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.InterestRate(), InterestRate)) {
			error = "Failed to enter account holder details into the text field.";
			return false;
		}
		 if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
	        {
	            error = "Failed to click the 'submit application' button.";
	            return false;
	        }
		return true;

	}
}
