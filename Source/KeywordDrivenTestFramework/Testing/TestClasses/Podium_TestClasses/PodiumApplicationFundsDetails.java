/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Podium_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author aswaphi
 */
@KeywordAnnotation(
        Keyword = "LIG51_PodiumDetails_S3",
        createNewBrowserInstance = false
)

public class PodiumApplicationFundsDetails extends BaseClass
{

    String error = "";

    public PodiumApplicationFundsDetails()
    {
    }

    public TestResult executeTest()
    {

        if (!podiumApplicationSourceOfFunds())
        {
            return narrator.testFailed("Failed to update podium source of funds details. - " + error);
        }
        if (!podiumApplicationIncomeDetails())
        {
            return narrator.testFailed("Failed to update podium application income details details. - " + error);
        }
        if (!podiumApplicationExpensesDetails())
        {
            return narrator.testFailed("Failed to update podium application expenses details. - " + error);
        }
        return narrator.finalizeTest("Successfully updated podium application details on the Form.");
    }

    public boolean podiumApplicationSourceOfFunds()
    {
        /*failed to update podium source of funds*/
        String sourceOfFundsHeader = testData.getData("Source Of Funds Header");

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumHeaders(sourceOfFundsHeader)))
        {
            error = "Failed to scroll to the source of funds container.";
            return false;
        }
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.pension()))
        {
            error = "Failed to check if the pension check - box is selected.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully selected pension check box.");
        return true;
    }

    public boolean podiumApplicationIncomeDetails()
    {
        /*updating podium application details*/

        String gross = testData.getData("Gross Renumeraton");
        String netIncome = testData.getData("Net Pay");
        String incomeHeader = testData.getData("Income Header");

        narrator.stepPassed("Gross Renumeration : " + gross);
        narrator.stepPassed("Net Income : " + netIncome);

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumHeaders(incomeHeader)))
        {
            error = "Failed to scroll to the income details container.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.grossRenumeration(), gross))
        {
            error = "Failed to enter gross renumeration into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.netIncome(), netIncome))
        {
            error = "Failed to enter net income into the text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered income details.");

        return true;
    }

    public boolean podiumApplicationExpensesDetails()
    {
        /*updating podium expenses details*/

        String expensesHeaders = testData.getData("Expense Header");
        String rent = testData.getData("Rent");
        String telephonePayment = testData.getData("Telephone Payment");
        String clothingAccount = testData.getData("Clothing Account");
        String educationCosts = testData.getData("Education Costs");

        narrator.stepPassed("Rent Amount : " + rent);
        narrator.stepPassed("Telephone Amount : " + telephonePayment);
        narrator.stepPassed("Clothing Amount : " + clothingAccount);
        narrator.stepPassed("Educational cost : " + educationCosts);

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumHeaders(expensesHeaders)))
        {
            error = "Failed to scroll to the expenses container.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.podiumRent(), rent))
        {
            error = "Failed to enter rent amount into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clothingAccounts(), clothingAccount))
        {
            error = "Failed to enter clothing account into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.telephonePayments(), telephonePayment))
        {
            error = "Failed to enter telephone payment amount into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.educationCost(), educationCosts))
        {
            error = "Failed to enter education costs into the text field";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully entered expenses details.");
        return true;
    }

}
