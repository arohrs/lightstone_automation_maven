/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Podium_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author aswaphi
 */
@KeywordAnnotation(
        Keyword = "LIG51_PodiumDetails_S1",
        createNewBrowserInstance = false
)

public class PodiumApplicationVehicleDetails extends BaseClass
{

    String error = "";

    public PodiumApplicationVehicleDetails()
    {
        this.testData = testData;
    }

    public TestResult executeTest()
    {
        if (!searchApplication())
        {
            return narrator.testFailed("Failed to search podium application details. - " + error);
        }
        if (!podiumApplicationDetails())
        {
            return narrator.testFailed("Failed to update podium application details. - " + error);
        }
        if (!podiumVehicleDetails())
        {
            return narrator.testFailed("Failed to update podium vehicle details details. - " + error);
        }
        if (!podiumFinancialDetails())
        {
            return narrator.testFailed("Failed to update podium financial details. - " + error);
        }
        if (!podiumDealerVapsDetails())
        {
            return narrator.testFailed("Failed to update podium dealer vaps details. - " + error);
        }

        return narrator.finalizeTest("Successfully updated podium application vehicle details on the Form.");
    }

    public boolean searchApplication()
    {
        /*searching an existing application name*/

        String id = testData.getData("ID");
        String searchBy = testData.getData("Search By");
        narrator.stepPassed("Search By : " + searchBy);
        narrator.stepPassed("ID : " + id);

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaPod()))
        {
            error = "Failed to wait for search criteria.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaPod()))
        {
            error = "Failed to click on search criteria.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.criteria()))
        {
            error = "Failed to wait for criteria text box.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully loaded the search criteria fields.");

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.searchByDropdown(), searchBy))
        {
            error = "Failed to select a searching method.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.criteria(), id))
        {
            error = "Failed to enter id into criteria text field.";
            return false;
        }
        
        //screenshot
        
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchBtn()))
        {
            error = "Failed to click search button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.PodiumClick()))
        {
            error = "Failed to wait for podium absa status.";
            return false;
        }
        
        //provide search validation.
        //did you receive the results you were looking for?
        
        narrator.stepPassedWithScreenShot("Successfully searched an applicant.");

        return true;

    }

    public boolean podiumApplicationDetails()
    {
        /*updating podium application details*/

        String firstname = testData.getData("Firstname");
        String surname = testData.getData("Surname");
        String regTrack = testData.getData("RequestRegTrack");//post as message
        narrator.stepPassed("Firstname : " + firstname);
        narrator.stepPassed("Surname : " + surname);

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.PodiumClick()))
        {
            error = "Failed to click on podium absa status button.";
            return false;
        }

        SeleniumDriverInstance.switchToTabOrWindow();//if statement

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.title()))
        {
            error = "Failed to wait for the title text to appear.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully switched to a new window.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.applicationFirstname()))
        {
            error = "Failed to wait for the firstname text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationFirstname(), firstname))
        {
            error = "Failed to enter firstname into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationSurname(), surname))
        {
            error = "Failed to enter surname into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.regTrackDropdown()))
        {
            error = "Failed to click request regtrack later dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.regtrackLater(regTrack)))
        {
            error = "Failed to click Request RegTrack Later dropdown list value.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered podium application details");

        return true;
    }

    public boolean podiumVehicleDetails()
    {
        /*updating podium vehicle details*/

        String mmCode = testData.getData("MMCode");
        String purchasePrice = testData.getData("Cash Price");
        String regNumber = testData.getData("Registration Number");
        String chassisNumber = testData.getData("Chassis Number");
        String actualKilometers = testData.getData("Actual Kilometers");

        narrator.stepPassed("Cash Price : " + purchasePrice);
        narrator.stepPassed("M&M Code : " + mmCode);
        narrator.stepPassed("Registration Number " + regNumber);
        narrator.stepPassed("Actual Kilometers : " + actualKilometers );
        
        
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsTabHeader()))
        {
            error = "Failed to scroll to the vehicle container.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.mmCodeClick()))
        {
            error = "Failed to click  vehicle lookup button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully loaded the m&m code frame.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.carTypedropdown()))
        {
            error = "Failed to wait for element new/used dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.carTypedropdown()))
        {
            error = "Failed to click new/used dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.carTypedropdownClick()))
        {
            error = "Failed to click vehicle state.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.mnMCode()))
        {
            error = "Failed to wait for m&m code text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mnMCode(), mmCode))
        {
            error = "Failed to enter M&M code into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchMMcode()))
        {
            error = "Failed to click the search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.ok()))
        {
            error = "Failed to click the ok lookup button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okBtn()))
        {
            error = "Failed to click the ok pop button.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.cashPrice()))
        {
            error = "Failed to clear the text on cash price text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.cashPrice(), purchasePrice))
        {
            error = "Failed to enter purchase price into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.actualKilometers()))
        {
            error = "Failed to click on actual kilometres text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okBtn()))
        {
            error = "Failed to click the ok button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okBtn()))
        {
            error = "Failed to click the ok button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.actualKilometers(), actualKilometers))
        {
            error = "Failed to enter the number of kilometers into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.chassisNumber()))
        {
            error = "Failed to click on chassisNumber.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okBtn()))
        {
            error = "Failed to click the ok button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okBtn()))
        {
            error = "Failed to click the ok button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.chassisNumber(), chassisNumber))
        {
            error = "Failed to enter the chassis number into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.regNumber(), regNumber))
        {
            error = "Failed to enter the registration number into the text field.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully entered podium vehicle details.");
        
        return true;
    }

    public boolean podiumFinancialDetails()
    {
        /*updating podium financial details*/
        String residentialValue = testData.getData("Residential Value");
        String residentialPercentage = testData.getData("Residential Percentage");
        String deposit = testData.getData("Deposit");
        
        narrator.stepPassed("Residential value : " + residentialValue);
        narrator.stepPassed("Residential Percentage : " + residentialPercentage);
        narrator.stepPassed("Residential value : " + deposit);

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumFinancialDetailsTab()))
        {
            error = "Failed to scroll to the finance details container.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.podiumResidentialValue()))
        {
            error = "Failed to wait for the residential value text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.podiumResidentialValue(), residentialValue))
        {
            error = "Failed to enter residential value into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.podiumResidentialPercentage(), residentialPercentage))
        {
            error = "Failed to enter residential percentage into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.podiumDeposit()))
        {
            error = "Failed to clear the deposit text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.podiumDeposit(), deposit))
        {
            error = "Failed to clear the deposit text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.podiumResidentialPercentage()))
        {
            error = "Failed to click on residential value.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okBtn()))
        {
            error = "Failed to click the ok button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully entered finance details.");

        return true;
    }

    public boolean podiumVehicleExtras()
    {
        /*updating podium vehicle extras details*/
        String fogLightsAmount = testData.getData("Fog Amount");
        String boopSpoilersAmount = testData.getData("Spoiler Amount");
        String engineUpgradeAmount = testData.getData("Engine Amount");
        String fogLights = testData.getData("Action Description1");
        String bootSpoilers = testData.getData("Action Description2");
        String engineUpgrade = testData.getData("Action Description3");
        
        narrator.stepPassed("FOG LIGHTS amount : " + fogLightsAmount);
        narrator.stepPassed("BOOT SPOILER amount : " + bootSpoilers);
        narrator.stepPassed("ENGINE UPGRADE : " + engineUpgradeAmount);
       
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumVehicleExtrasTab()))
        {
            error = "Failed to scroll to the vehicle extras container.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.podiumAddNewExtra()))
        {
            error = "Failed to wait for the add new extra button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.podiumAddNewExtra()))
        {
            error = "Failed to click add new extra button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectFogLights()))
        {
            error = "Failed to wait for the fog lights selection field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectFogLights(), fogLights))
        {
            error = "Failed to select fog lights from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.fogLightsAmount(), fogLightsAmount))
        {
            error = "Failed to enter fog light amount.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.podiumAddNewExtra()))
        {
            error = "Failed to click add new extra button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectBootSpoilers()))
        {
            error = "Failed to wait for the boot spoilers selection filed to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectBootSpoilers(), bootSpoilers))
        {
            error = "Failed to select boot spoilers from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.bootSpoilerAmount(), boopSpoilersAmount))
        {
            error = "Failed to enter boot spoiler amount into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.podiumAddNewExtra()))
        {
            error = "Failed to click add new extra button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectEngineUpgrade()))
        {
            error = "Failed to wait for engine upgrade selection field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectEngineUpgrade(), engineUpgrade))
        {
            error = "Failed to select engine upgrade from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.EngineUpgradeAmount(), engineUpgradeAmount))
        {
            error = "Failed to enter engine upgrade amount into the text field.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully entered vehicle extras details.");
        
        return true;
    }

    public boolean podiumDealerVapsDetails()
    {
        String dealerHeader = testData.getData("Podium Header");
        String dealerCatergory = testData.getData("Action Catergory");
        
        narrator.stepPassed("Dealer VAPs : " + dealerCatergory);
        
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumHeaders(dealerHeader)))
        {
            error = "Failed to scroll to the dealer vaps podium container.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewDealerVap()))
        {
            error = "Failed to click the add new dealer vap button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectionActionDescription()))
        {
            error = "Failed to wait for the action description selection field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectionActionDescription(), dealerCatergory))
        {
            error = "Failed to select dealer vaps from the list.";
            return false;
        }
        
        return true;
        
    }

}
