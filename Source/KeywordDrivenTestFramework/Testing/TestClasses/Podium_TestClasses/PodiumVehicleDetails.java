/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Podium_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author aswaphi
 */
@KeywordAnnotation(
        Keyword = "Vehicle Details ABSA",
        createNewBrowserInstance = false
)

public class PodiumVehicleDetails extends BaseClass
{

    String error = "";

    public PodiumVehicleDetails()
    {
    }

    public TestResult executeTest()
    {

        if (!podiumVehicleDetails())
        {
            return narrator.testFailed("Failed to update podium vehicle details. - " + error);
        }
        return narrator.finalizeTest("Successfully updated podium vehicle details on the Form.");
    }

    public boolean podiumVehicleDetails()
    {
        /*updating podium vehicle details*/
        String vehicleState = testData.getData("new/used");
        String mmCode = testData.getData("mnmCode");
        String purchasePrice = testData.getData("Purchase Price");
        String regNumber = testData.getData("Registration Number");
        String chassisNumber = testData.getData("Chassis Number");
        String actualKilometers = testData.getData("Actual Kilometers");

//        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.selectVehicleButton()))
//        {
//            error = "Failed to click the select vehicle button";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectVehicleButton()))
//        {
//            error = "Failed to click slect vehicle button.";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.carTypedropdown()))
        {
            error = "Failed to wait for element new/used dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.carTypedropdown()))
        {
            error = "Failed to click new/used dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.carTypedropdownClick()))
        {
            error = "Failed to click new/used dropdown list item.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mnMCode(), mmCode))
        {
            error = "Failed to enter M&M code into the textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchMMcode()))
        {
            error = "Failed to click the search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.ok()))
        {
            error = "Failed to click the ok button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okBtn()))
        {
            error = "Failed to click the ok button.";
            return false;
        }
//        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.purchasingPrice(), purchasePrice))
//        {
//            error = "Failed to enter the purchase price into the text field.";
//            return false;
//        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.actualKilometers(), actualKilometers))
        {
            error = "Failed to enter the number of kilometers into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.chassisNumber(), chassisNumber))
        {
            error = "Failed to enter the chassis number into the text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.regNumber(), regNumber))
        {
            error = "Failed to enter the registration number into the text field";
            return false;
        }
         if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitPodiumApplication()))
        {
            error = "Failed to click 'submit' to submit podium application details.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.podiumSubmitionTextMessage()))
        {
            error = "Failed to wait for podium success message.";
            return false;
        }
        return true;
    }
}
