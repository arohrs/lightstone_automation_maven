/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Podium_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author aswaphi
 */
@KeywordAnnotation(
        Keyword = "LIG51_PodiumDetails_S2",
        createNewBrowserInstance = false
)

public class PodiumApplicationPersonalDetails extends BaseClass
{

    String error = "";

    public PodiumApplicationPersonalDetails()
    {
    }

    public TestResult executeTest()
    {

        if (!podiumPersonalDetails())
        {
            return narrator.testFailed("Failed to update podium personal details. - " + error);
        }
        if (!podiumEmployerDetails())
        {
            return narrator.testFailed("Failed to update podium employer details. - " + error);
        }
        return narrator.finalizeTest("Successfully updated podium application personal details.");
    }

    public boolean podiumPersonalDetails()
    {

        /*Entering podium personal details*/
        String race = testData.getData("Race");
        String title = testData.getData("Title");
        String maritialStatus = testData.getData("Maritial Status");
        String mobileNumber = testData.getData("Mobile Number");
        String mobileType = testData.getData("Mobile Type");
        String emailAddress = testData.getData("Email Address");
        String nationality = testData.getData("Nationality");

        narrator.stepPassed("Title : " + title);
        narrator.stepPassed("Maritial Status : " + maritialStatus);
        narrator.stepPassed("Mobile Number : " + mobileNumber);
        narrator.stepPassed("Email Address : " + emailAddress);
        narrator.stepPassed("Nationality : " + nationality);

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.raceDropDown()))
        {
            error = "Failed to scroll to race drop down selection";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.raceDropDown()))
        {
            error = "Failed to wait for the payout date of first registration text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.raceDropDown(), race))
        {
            error = "Failed to select race from the drop down list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.titleDropDown(), title))
        {
            error = "Failed to select a title from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfNationality(), nationality))
        {
            error = "Failed to select country of birth from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mobileNumber(), mobileNumber))
        {
            error = "Failed to enter mobile number from the drop down list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mobileTypeDropDown(), mobileType))
        {
            error = "Failed to select mobile number type from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.emailAdress(), emailAddress))
        {
            error = "Failed to enter email adress from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maritalStatusDropDown(), maritialStatus))
        {
            error = "Failed to select marital status from the list.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered personal details.");

        return true;
    }

    public boolean podiumEmployerDetails()
    {

        String emplopyerName = getData("Employer Name");
        String occupation = getData("Occupation");
        String employerIndustryType = getData("Employer Industry Type");
        String employerType = getData("Employment Type");
        String clientType = getData("Employer Client Type");
        String employerAddress = getData("Employer Address1");
        String employerSuburb = getData("Search Suburb2");
        String postalCode = getData("Postal Code");
        String periodYear = getData("Period(Year)");
        String periodMonth = getData("Period(Month)");
        String selectSuburb = getData("Select Suburb2");
        String employerHeader = getData("Employer Header");

        narrator.stepPassed("Employer Name:" + emplopyerName);
        narrator.stepPassed("Occupation :" + " " + occupation);
        narrator.stepPassed("Employer Industry Type :" + employerIndustryType);
        narrator.stepPassed("Employment Type :" +  employerType);
        narrator.stepPassed("Employer Client Type :"  + clientType);
        narrator.stepPassed("Employer Suburb:" +  selectSuburb);
        narrator.stepPassed("Employer Postal Code:" + postalCode);

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumHeaders(employerHeader)))
        {
            error = "Failed to scroll to employer name text field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.podiumHeaders(employerHeader)))
        {
            error = "Failed to wait for the employer name text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerName(), emplopyerName))
        {
            error = "Failed to enter employer name on the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.occupationDropDown(), occupation))
        {
            error = "Failed to select occupation from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerIndustryTypeDropDown(), employerIndustryType))
        {
            error = "Failed to select employer industry type from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerType(), employerType))
        {
            error = "Failed to select employer type from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerClientType(), clientType))
        {
            error = "Failed to select employer client type from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerAddress(), employerAddress))
        {
            error = "Failed to enter employer address into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.employerSuburbButton()))
        {
            error = "Failed to click suburb look up button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerSuburbSearch(), employerSuburb))
        {
            error = "Failed to enter suburb into the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.employerSearchButton()))
        {
            error = "Failed to click suburb search button.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectEmployerSuburbDropDown(), selectSuburb))
        {
            error = "Failed to select suburb from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerSuburbCodeDropDown(), postalCode))
        {
            error = "Failed to select postal code from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectOK()))
        {
            error = "Failed to click the OK button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPeriodYear(), periodYear))
        {
            error = "Failed to enter period(year) into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPeriodMonth(), periodMonth))
        {
            error = "Failed to enter period(month) from the list.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered employer details.");
        
        return true;
    }

}
