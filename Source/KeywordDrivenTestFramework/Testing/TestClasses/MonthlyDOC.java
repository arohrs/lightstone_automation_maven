/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Monthly DOC",
        createNewBrowserInstance = false
)

public class MonthlyDOC extends BaseClass
{

    String error = "";

    public MonthlyDOC()
    {
    }

    public TestResult executeTest()
    {
        if (!monthlyDOC())
        {
            return narrator.testFailed("Failed to enter customer's details on the 'e-application' Form - " + error);
        }
        if (!captureCompanyDetails())
        {
            return narrator.testFailed("Failed to enter customer's details on the 'e-application' Form - " + error);
        }
        if (!captureVehicleDetails())
        {
            return narrator.testFailed("Failed to enter customer's details on the 'e-application' Form - " + error);
        }
        if (!captureVehicleAccessories())
        {
            return narrator.testFailed("Failed to enter customer's details on the 'e-application' Form - " + error);
        }
        if (!captureDOCDetails())
        {
            return narrator.testFailed("Failed to enter customer's details on the 'e-application' Form - " + error);
        }
        return narrator.finalizeTest("Successfully entered customer's details on the 'e-application' Form.");
    }

    public boolean monthlyDOC()
    {

        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to click 'DOC' dropdown button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.monthlyDoc())))
        {
            error = "Failed to wait for the 'Monthly DOC ' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.monthlyDoc())))
        {
            error = "Failed to click 'Monthly DOC' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to wait for the 'Monthly DOC page' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchClientName()))
        {
            error = "Failed to wait for the 'Client Name' Textfield.";
            return false;
        }
        String clientName = getData("Client Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.searchClientName(), clientName))
        {
            error = "Failed to enter 'Client Name' in the Textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to wait for the 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        searcMaskHandling();
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for the 'searched DOC' current Month.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchClientSurname()))
        {
            error = "Failed to wait for the 'searched DOC' current Month.";
            return false;
        }
        String clientSurname = getData("Client Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.searchClientSurname(), clientSurname))
        {
            error = "Failed to enter 'Client Surname' in the Textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to wait for the 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        searcMaskHandling();
//         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
//        {
//            error = "Failed to wait for the 'searched DOC' current Month.";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchClientIDNumber()))
        {
            error = "Failed to wait for the 'client ID' textfield.";
            return false;
        }
        String clientID = getData("Client ID Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.searchClientIDNumber(), clientID))
        {
            error = "Failed to enter 'Client ID Number' in the Textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to wait for the 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for the 'searched DOC' current Month.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchBankName()))
        {
            error = "Failed to wait for the 'Bank Name' comboBox.";
            return false;
        }
        String bankName = getData("Bank Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.searchBankName(), bankName))
        {
            error = "Failed to select 'ABSA BANK' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        searcMaskHandling();
//        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
//        {
//            error = "Failed to wait for the 'searched DOC' current Month.";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }
        ArrayList<String> dealStatus = new ArrayList<>();
        dealStatus.add(getData("Status1"));
        dealStatus.add(getData("Status2"));

        for (int i = 0; i < dealStatus.size(); i++)
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchDealStatus()))
            {
                error = "Failed to wait for the 'deal status' list.";
                return false;
            }
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.searchDealStatus(), dealStatus.get(i)))
            {
                error = "Failed to select the'deal status' from the list.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaButton()))
            {
                error = "Failed to wait for the 'Search' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
            {
                error = "Failed to click for the 'Search' button.";
                return false;
            }
            searcMaskHandling();
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
            {
                error = "Failed to wait for the 'searched DOC' result .";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
            {
                error = "Failed to wait for the 'clear' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
            {
                error = "Failed to click for the 'clear' button.";
                return false;
            }

        }

        ArrayList<String> fAndI = new ArrayList<>();
        fAndI.add(getData("F&I1"));
        fAndI.add(getData("F&I2"));
        fAndI.add(getData("F&I3"));
        fAndI.add(getData("F&I4"));
        fAndI.add(getData("F&I5"));
        fAndI.add(getData("F&I6"));
        fAndI.add(getData("F&I7"));
        fAndI.add(getData("F&I8"));
        fAndI.add(getData("F&I9"));
        fAndI.add(getData("F&I10"));
        fAndI.add(getData("F&I11"));
        fAndI.add(getData("F&I12"));

        for (int i = 0; i < fAndI.size(); i++)
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fAndI()))
            {
                error = "Failed to wait for the 'F&I' list.";
                return false;
            }

            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fAndI(), fAndI.get(i)))
            {
                error = "Failed to select 'F&I' from the dropdown list.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
            {
                error = "Failed to click  the 'Search' button.";
                return false;
            }
            searcMaskHandling();
//            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
//            {
//                error = "Failed to wait for the 'searched DOC' current Month.";
//                return false;
//            }
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
            {
                error = "Failed to wait for the 'clear' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
            {
                error = "Failed to click  the 'clear' button.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fAndI()))
            {
                error = "Failed to wait for the 'F&I' list.";
                return false;
            }

        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to wait for the 'Current DOC' Header .";
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to double click  the 'Current DOC' Header.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click  the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.previousMonthDOC()))
        {
            error = "Failed to wait for the 'Previous DOC' Header.";
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(LightStonePageObjects.previousMonthDOC()))
        {
            error = "Failed to double click for the 'Previous DOC' Header.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }

        return true;
    }

    public boolean searcMaskHandling()
    {
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchLoadMask(), 5))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchLoadMask(), 120))
            {
                error = "Failed to handle the search mask";
                return false;
            }

        }
        return true;
    }

    public boolean captureCompanyDetails()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.captureDocHeader()))
        {
            error = "Failed to wait for the 'Capture DOC' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientDetailsHeader()))
        {
            error = "Failed to wait for the 'Client Details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.validRSAID()))
        {
            error = "Failed to wait for the 'Valid RSA ID' checkBox.";
            return false;
        }

        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.validRSAID()))
        {
            error = "Failed to check if the 'Valid RSA ID' is selected.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientName()))
        {
            error = "Failed to wait for the 'Client Name'.";
            return false;
        }
        String clientName = getData("Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientName(), clientName))
        {
            error = "Failed to enter  the 'client name'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientSurname()))
        {
            error = "Failed to wait for the 'client surname'.";
            return false;
        }
        String clientSurname = getData("Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientSurname(), clientSurname))
        {
            error = "Failed to enter  the 'client surname'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientID()))
        {
            error = "Failed to wait for the 'client ID' .";
            return false;
        }
        String clientID = getData("ID Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientID(), clientID))
        {
            error = "Failed to enter  the ' ID'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientEmail()))
        {
            error = "Failed to wait for the ' email' .";
            return false;
        }
        String email = getData("Email");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientEmail(), email))
        {
            error = "Failed to enter  the ' email'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientCellPhone()))
        {
            error = "Failed to wait for the 'Cell Phone' .";
            return false;
        }
        String cellPhone = getData("Cell Phone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientCellPhone(), cellPhone))
        {
            error = "Failed to enter  the 'Cell Phone'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientworkPhone()))
        {
            error = "Failed to wait for the 'work phone'.";
            return false;
        }
        String workPhone = getData("Work Phone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientworkPhone(), workPhone))
        {
            error = "Failed to enter  the 'work phone'.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.isCompany()))
        {
            error = "Failed to wait for the 'is Company' checkBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.isCompany()))
        {
            error = "Failed to click for the 'Company' checkBox.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyName()))
        {
            error = "Failed to wait for the 'Company name' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyRegistrationNumber()))
        {
            error = "Failed to wait for the 'Company registration number'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.isCompany()))
        {
            error = "Failed to wait for the 'is Company' checkBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.isCompany()))
        {
            error = "Failed to click for the 'is Company' checkBox.";
            return false;
        }

        return true;
    }

    public boolean captureVehicleDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsHeader()))
        {
            error = "Failed to wait for the 'Vehicle details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.articleCondition()))
        {
            error = "Failed to wait for the 'article condition' .";
            return false;
        }
        String vehicleCondition = getData("Condition");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.articleCondition(), vehicleCondition))
        {
            error = "Failed to select 'used' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.yearModel()))
        {
            error = "Failed to wait for the 'Year Model'.";
            return false;
        }
        String yearModel = getData("Year Model");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.yearModel(), yearModel))
        {
            error = "Failed to select '2010' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleMake()))
        {
            error = "Failed to wait for the 'vehicle make' .";
            return false;
        }
        String vehicleMake = getData("Vehicule Make");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vehicleMake(), vehicleMake))
        {
            error = "Failed to select for the 'vehicle make' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleModel()))
        {
            error = "Failed to wait for the 'vehicle model' .";
            return false;
        }
        String vehicleModel = getData("Vehicle Model");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vehicleModel(), vehicleModel))
        {
            error = "Failed to select for the 'vehicle model' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.mmCode()))
        {
            error = "Failed to wait for the 'M&M Code' .";
            return false;
        }
        String mmCode = getData("M&M");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.mmCode(), mmCode))
        {
            error = "Failed to enter for the 'M&M Code' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.retailPrice()))
        {
            error = "Failed to wait for the 'retail Price'.";
            return false;
        }
        String vehicleRetailPrice = getData("Retail Price");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.retailPrice(), vehicleRetailPrice))
        {
            error = "Failed to enter for the 'retail Price'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.costPrice()))
        {
            error = "Failed to wait for the 'cost Price'.";
            return false;
        }
        String vehicleCostPrice = getData("Cost Price");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.costPrice(), vehicleCostPrice))
        {
            error = "Failed to enter for the 'costPrice'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.overAllowance()))
        {
            error = "Failed to wait for the 'over Allowance'.";
            return false;
        }
        String overAllowance = getData("Over Allowance");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.overAllowance(), overAllowance))
        {
            error = "Failed to enter for the 'over Allowance' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.factoryClaim()))
        {
            error = "Failed to wait for the 'factory Claim' .";
            return false;
        }
        String factoryClaim = getData("Factory Claim");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.factoryClaim(), factoryClaim))
        {
            error = "Failed to enter for the 'factory Claim' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.purchasePrice()))
        {
            error = "Failed to wait for the 'purchase Price'.";
            return false;
        }
        String purchasePrice = getData("Purchase Price");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.purchasePrice(), purchasePrice))
        {
            error = "Failed to enter for the 'purchase Price'.";
            return false;
        }

        return true;
    }

    public boolean captureVehicleAccessories()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleAccessories()))
        {
            error = "Failed to wait for the 'vehicle Accessories' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.accessoriesRetailPrice()))
        {
            error = "Failed to wait for the 'accessories RetailPrice'.";
            return false;
        }
        String accessoriesRetailPrice = getData("Accessories Retail Price");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.accessoriesRetailPrice(), accessoriesRetailPrice))
        {
            error = "Failed to enter for the 'accessories RetailPrice'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.accessoriesCostPrice()))
        {
            error = "Failed to wait for the 'accessories Cost Price'.";
            return false;
        }
        String accessoriesCostPrice = getData("Accessories cost Price");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.accessoriesCostPrice(), accessoriesCostPrice))
        {
            error = "Failed to enter for the 'accessories Cost Price' header.";
            return false;
        }

        return true;
    }

    public boolean captureDOCDetails()
    {
        Date date = new Date();
        Calendar now = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        now.add(Calendar.DATE, 9);

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.docDetailsHeader()))
        {
            error = "Failed to wait for the 'doc Details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.sightedCheckBox()))
        {
            error = "Failed to wait for the 'sighted' CheckBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.sightedCheckBox()))
        {
            error = "Failed to click for the 'sighted' CheckBox.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.repeatCustomer()))
        {
            error = "Failed to wait for the 'repeat customer' CheckBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.repeatCustomer()))
        {
            error = "Failed to click for the 'repeat customer' CheckBox.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.docBankName()))
        {
            error = "Failed to wait for the 'Bank Name' .";
            return false;
        }
        String bankName = getData("Bank Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.docBankName(), bankName))
        {
            error = "Failed to select for  'ABSA' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealer()))
        {
            error = "Failed to wait for the 'dealer' .";
            return false;
        }
        String dealer = getData("Dealer");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.dealer(), dealer))
        {
            error = "Failed to select for the 'dealer' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fAndIName()))
        {
            error = "Failed to wait for the 'F&I'.";
            return false;
        }
        String fandI = getData("F&I6");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fAndIName(), fandI))
        {
            error = "Failed to select for the 'F&I' list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.typeOfSale()))
        {
            error = "Failed to wait for the 'type Of Sale'.";
            return false;
        }
        String typeOfSale = getData("Type of Sale");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeOfSale(), typeOfSale))
        {
            error = "Failed to select for the 'type Of Sale'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.jointVenture()))
        {
            error = "Failed to wait for the 'joint Venture'.";
            return false;
        }
        String jointAdventure = getData("Joint Venture");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.jointVenture(), jointAdventure))
        {
            error = "Failed to select for the 'joint venture' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.stockNumber()))
        {
            error = "Failed to wait for the 'stock Number'.";
            return false;
        }
        String stockNumber = getData("Stock Number");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.stockNumber(), stockNumber))
        {
            error = "Failed to enter for the 'stock Number' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.interestRate()))
        {
            error = "Failed to wait for the 'interest Rate'.";
            return false;
        }
        String interestRate = getData("Interest Rate");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.interestRate(), interestRate))
        {
            error = "Failed to enter for the 'interest Rate' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.principalDebt()))
        {
            error = "Failed to wait for the 'principal Debt'.";
            return false;
        }
        String principalDebt = getData("Principal Dept");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.principalDebt(), principalDebt))
        {
            error = "Failed to enter for the 'principal Debt'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.invoicedDate()))
        {
            error = "Failed to wait for the 'invoiced Date'.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.invoicedDate(), dateFormat.format(date)))
        {
            error = "Failed to enter for the 'invoiced Date'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.payoutDate()))
        {
            error = "Failed to wait for the 'invoiced Date' .";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.payoutDate(), dateTimeFormat.format(now.getTime())))
        {
            error = "Failed to enter for the 'invoiced Date'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.signedDate()))
        {
            error = "Failed to wait for the 'signed Date'.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.signedDate(), dateTimeFormat.format(date)))
        {
            error = "Failed to enter for the 'signed Date' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleDeposit()))
        {
            error = "Failed to wait for the 'vehicle Deposit' .";
            return false;
        }
        String deposit = getData("Deposit");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.vehicleDeposit(), deposit))
        {
            error = "Failed to enter for the 'vehicle Deposit' header.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dic()))
        {
            error = "Failed to wait for the 'dic'.";
            return false;
        }
        String dic = getData("DIC");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.dic(), dic))
        {
            error = "Failed to enter for the 'dic'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.specialDic()))
        {
            error = "Failed to wait for the 'special Dic'.";
            return false;
        }
        String specialDic = getData("Special DIC");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.specialDic(), specialDic))
        {
            error = "Failed to enter for the 'special Dic'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.rateType()))
        {
            error = "Failed to wait for the 'rateType'.";
            return false;
        }
        String rateType = getData("Rate Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.rateType(), rateType))
        {
            error = "Failed to select for the 'Linked' from the list.";
            return false;
        }

        return true;
    }

    public boolean incomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.incomeDetailsHeader()))
        {
            error = "Failed to wait for the 'income details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.creditLifeIncome()))
        {
            error = "Failed to wait for the 'credit life Income'.";
            return false;
        }
        String credit = getData("Amount");
        String creditAmount = getData("Income4");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.creditLifeIncome(), credit))
        {
            error = "Failed to select for the 'credit life Income' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.creditLifeIncomeAmount()))
        {
            error = "Failed to wait for the 'credit life Income amount' .";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.creditLifeIncomeAmount(), creditAmount))
        {
            error = "Failed to enter the 'credit life Income amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.maintenancePlan()))
        {
            error = "Failed to wait for the 'maintenance Plan'.";
            return false;
        }
        String mainttenance = getData("Amount1");
        String mainttenanceAmount = getData("Income4");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maintenancePlan(), mainttenance))
        {
            error = "Failed to select for the 'maintenancePlan'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.maintenancePlanAmount()))
        {
            error = "Failed to wait for the 'maintenance Plan amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.maintenancePlanAmount(), mainttenanceAmount))
        {
            error = "Failed to enter for the 'maintenance Plan Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.paintGlass()))
        {
            error = "Failed to wait for the 'paint glass'.";
            return false;
        }
        String paintGlass = getData("Amount2");
        String paintGlassAmount = getData("Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.paintGlass(), paintGlass))
        {
            error = "Failed to select for the 'paint glass' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.paintGlassAmount()))
        {
            error = "Failed to wait for the 'paint glass amount' .";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.paintGlassAmount(), paintGlassAmount))
        {
            error = "Failed to enter  the 'paint glass amount' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.adminFee()))
        {
            error = "Failed to wait for the 'admin Fee'.";
            return false;
        }
        String adminFee = getData("Income1");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.adminFee(), adminFee))
        {
            error = "Failed to enter  the 'admin Fee' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.creditShortFall()))
        {
            error = "Failed to wait for the 'credit Short Fall'.";
            return false;
        }
        String creditShortFall = getData("Amount");
        String creditShortFallAmount = getData("Income6");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.creditShortFall(), creditShortFall))
        {
            error = "Failed to select for the 'credit Short Fall'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.creditShortFallAmount()))
        {
            error = "Failed to wait for the 'credit Short Fall Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.creditShortFallAmount(), creditShortFallAmount))
        {
            error = "Failed to enter for the 'credit Short Fall Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.bodyInsurance()))
        {
            error = "Failed to wait for the 'Capture DOC' header.";
            return false;
        }
        String bodyInsurance = getData("Amount");
        String bodyInsuranceAmount = getData("Income8");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bodyInsurance(), bodyInsurance))
        {
            error = "Failed to select for the 'body Insurance'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.bodyInsuranceAmount()))
        {
            error = "Failed to wait for the 'body Insurance Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.bodyInsuranceAmount(), bodyInsuranceAmount))
        {
            error = "Failed to enter for the 'body Insurance Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tyreRim()))
        {
            error = "Failed to wait for the 'tyre Rim'.";
            return false;
        }
        String tyreRim = getData("Amount1");
        String tyreRimAmount = getData("Income7");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.tyreRim(), tyreRim))
        {
            error = "Failed to select for the 'tyre Rim'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tyreRimAmount()))
        {
            error = "Failed to wait for the 'tyre Rim Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.tyreRimAmount(), tyreRimAmount))
        {
            error = "Failed to enter for the 'tyre Rim Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.warrantyIncome()))
        {
            error = "Failed to wait for the 'warranty Income'.";
            return false;
        }
        String warrantyIncome = getData("Amount");
        String warrantyIncomeAmount = getData("Income6");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.warrantyIncome(), warrantyIncome))
        {
            error = "Failed to select for the 'warranty Income'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.warrantyIncomeAmount()))
        {
            error = "Failed to wait for the 'warranty Income Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.tyreRimAmount(), warrantyIncomeAmount))
        {
            error = "Failed to enter for the 'tyre Rim Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.trackingIncome()))
        {
            error = "Failed to wait for the 'tracking Income'.";
            return false;
        }
        String trackingIncome = getData("Amount");
        String trackingIncomeAmount = getData("Income4");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.trackingIncome(), trackingIncome))
        {
            error = "Failed to select for the 'tracking Income'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.trackingIncomeAmount()))
        {
            error = "Failed to wait for the 'tracking Income' .";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.trackingIncomeAmount(), trackingIncomeAmount))
        {
            error = "Failed to enter for the 'tracking Income'.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.inflationProctection()))
        {
            error = "Failed to wait for the 'inflation Proctection'.";
            return false;
        }
        String inflationProctection = getData("Amount");
        String inflationProctectionAmount = getData("Income2");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.inflationProctection(), inflationProctection))
        {
            error = "Failed to select for the 'inflation Proctection'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.inflationProctectionAmount()))
        {
            error = "Failed to wait for the 'Capture DOC' header.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.inflationProctectionAmount(), inflationProctectionAmount))
        {
            error = "Failed to enter for the 'inflation Proctection Amount'.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.depositCover()))
        {
            error = "Failed to wait for the 'deposit Cover'.";
            return false;
        }
        String depositCover = getData("Amount");
        String depositCoverAmount = getData("Income8");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.depositCover(), depositCover))
        {
            error = "Failed to select for the 'deposit Cover'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.depositCoverAmount()))
        {
            error = "Failed to wait for the 'deposit Cover Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.depositCoverAmount(), depositCoverAmount))
        {
            error = "Failed to enter for the 'deposit Cover Amount' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.roadCover()))
        {
            error = "Failed to wait for the 'road Cover' .";
            return false;
        }
        String roadCover = getData("Amount");
        String roadCoverAmount = getData("Income5");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.roadCover(), roadCover))
        {
            error = "Failed to select for the 'road Cover' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.roadCoverAmount()))
        {
            error = "Failed to wait for the 'road Cover Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.roadCoverAmount(), roadCoverAmount))
        {
            error = "Failed to enter for the 'road Cover Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.otherIncomeAmount()))
        {
            error = "Failed to wait for the 'other Income Amount'.";
            return false;
        }
        String otherIncomeAmount = getData("Income7");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.otherIncomeAmount(), otherIncomeAmount))
        {
            error = "Failed to enter for the 'other Income Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.compInsBroker()))
        {
            error = "Failed to wait for the 'comp Ins Broker'.";
            return false;
        }
        String compInsBroker = getData("Income");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.compInsBroker(), compInsBroker))
        {
            error = "Failed to enter for the 'comp Ins Broker'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.compInsIncome()))
        {
            error = "Failed to wait for the 'comp  Ins Income'.";
            return false;
        }
        String compInsIncome = getData("Amount");
        String compInsIncomeAmount = getData("Income3");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.compInsIncome(), compInsIncome))
        {
            error = "Failed to select for the 'comp Ins Income'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.compInsIncomeAmount()))
        {
            error = "Failed to wait for the 'comp Ins Income Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.compInsIncomeAmount(), compInsIncomeAmount))
        {
            error = "Failed to enter for the 'comp Ins Income Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.firstGrossAmount()))
        {
            error = "Failed to wait for the 'first Gross Amount'.";
            return false;
        }
        String firstGrossAmount = getData("Income9");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.firstGrossAmount(), firstGrossAmount))
        {
            error = "Failed to enter for the 'first Gross Amount'.";
            return false;
        }

        return true;
    }

    public boolean expenseDetails()
    {
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.expenseDetailsHeader()))
        {
            error = "Failed to scroll to the 'expense details' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealerAdjustmentAmount()))
        {
            error = "Failed to wait for the 'dealer Adjustment Amount' .";
            return false;
        }
        String dealerAdjustmentAmount = getData("Income7");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.dealerAdjustmentAmount(), dealerAdjustmentAmount))
        {
            error = "Failed to enter for the 'dealer Adjustment Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.igfInvoiceFee()))
        {
            error = "Failed to wait for the 'IGF Invoice Fee'.";
            return false;
        }
        String igfInvoiceFee = getData("Income7");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.igfInvoiceFee(), igfInvoiceFee))
        {
            error = "Failed to enter for the 'IGF Invoice Fee' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.bankInvoiceFee()))
        {
            error = "Failed to wait for the 'bank Invoice Fee'.";
            return false;
        }
        String bankInvoiceFee = getData("Income8");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.bankInvoiceFee(), bankInvoiceFee))
        {
            error = "Failed to enter for the 'bank Invoice Fee' header.";
            return false;
        }
        String totalExpenses = SeleniumDriverInstance.retrieveTextByRobot(LightStonePageObjects.totalExpenseExclVat());
        String totalExpensesVat = SeleniumDriverInstance.retrieveTextByRobot(LightStonePageObjects.totalExpenseExclVat());
        double amount1 = Double.parseDouble(dealerAdjustmentAmount);
        double amount2 = Double.parseDouble(igfInvoiceFee);
        double amount3 = Double.parseDouble(bankInvoiceFee);
        double total = amount1 + amount2 + amount3;
        double currentAmount = Double.parseDouble(totalExpenses);
        double currentVat = Double.parseDouble(totalExpensesVat);

        validateTotalExpense(currentAmount, total);
        validateVAT(currentAmount,currentVat);

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.createDOCButton()))
        {
            error = "Failed to wait for the 'create DOC' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.createDOCButton()))
        {
            error = "Failed to click for the 'create DOC' button.";
            return false;
        }

        return true;
    }

    public boolean validateTotalExpense(double currentAmount, double total)
    {
        if (currentAmount != total)
        {
            error = "The total value is not the correct";
            return false;

        }
        return true;
    }

    public boolean validateVAT(double exclVat,double exceptedVat)
    {
         double inclVat = (exclVat * 0.15) + exclVat;
         
         if(inclVat!= exceptedVat)
         {
             error = "Failed the VAT validation";
             return false;
         }

        return true;
    }

}
