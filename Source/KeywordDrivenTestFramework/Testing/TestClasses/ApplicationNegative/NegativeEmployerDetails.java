/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Employer Negative",
        createNewBrowserInstance = false
)
public class NegativeEmployerDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String readOnly = getData("Read Only");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;

    public NegativeEmployerDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!employerDetails())
        {
            return narrator.testFailed("Failed to perform the negative test "+error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean employerDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.employerDetailsTab()))
        {
            error = "Failed to scroll to 'employer details' Tab.";
            return false;
        }
        if (!isAfterMarket)
        {
            List<String> readOnlyField = Arrays.asList(readOnly.split(","));
            for (String readField : readOnlyField)
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.readOnlyInputField(readField)))
                {
                    error = "Failed to validate '" + readField + "' is a read-only field .";
                    return false;
                }
                narrator.stepPassed("Successfully validated " + readField + " field is not manually editable");

            }
        }
        String employerName = getData("Employer Name");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerName()))
        {
            error = "Failed to clear 'employer name'text field.";
            return false;
        }
        if (!employerName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerName(), employerName))
            {
                error = "Failed to enter 'employer name' on the text field.";
                return false;
            }
        }
        narrator.stepPassed("Employer Name: " + employerName);
        String occupation = getData("Occupation");
        if (!occupation.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.occupationDropDown(), getData("Occupation")))
            {
                error = "Failed to select 'occupation' from the list.";
                return false;
            }
        }
        narrator.stepPassed("Occupation : " + occupation);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerIndustryTypeDropDown(), getData("Employer Industry Type")))
        {
            error = "Failed to select 'employer industry type' from the list.";
            return false;
        }
        narrator.stepPassed("Employer Industry Type :" + " " + getData("Employer Industry Type"));

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerType(), getData("Employment Type")))
        {
            error = "Failed to select 'employer type'from the list.";
            return false;
        }
        narrator.stepPassed("Employment Type :" + " " + getData("Employment Type"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerLevel(), getData("Employment Level")))
        {
            error = "Failed to select 'employer level'.";
            return false;
        }
        narrator.stepPassed("Employer Level :" + " " + getData("Employment Level"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerClientType(), getData("Employer Client Type")))
        {
            error = "Failed to select 'employer client type' from the list.";
            return false;
        }
        narrator.stepPassed("Employer Client Type :" + " " + getData("Employer Client Type"));

        if (!isAfterMarket)
        {
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerAddress()))
            {
                error = "Failed to clear 'employer address' into the text field.";
                return false;
            }
            if (!getData("Employer Address1").isEmpty())
            {
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerAddress(), getData("Employer Address1")))
                {
                    error = "Failed to enter 'employer address' into the text field.";
                    return false;
                }
            }
            narrator.stepPassed("Employer Address :" + " " + getData("Employer Address1"));

            String periodYear = getData("Period(Year)");
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerPeriodYear()))
            {
                error = "Failed to enter 'period(year)' into the text field.";
                return false;
            }
            if (!periodYear.isEmpty())
            {
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPeriodYear(), periodYear))
                {
                    error = "Failed to enter 'period(year)' into the text field.";
                    return false;
                }
            }
            narrator.stepPassed("Period Employed YY: " + periodYear);

            String periodMonth = getData("Period(Month)");
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerPeriodMonth()))
            {
                error = "Failed to enter 'period(month)'from the list.";
                return false;
            }
            if (!periodMonth.isEmpty())
            {
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPeriodMonth(), periodMonth))
                {
                    error = "Failed to enter 'period(month)'from the list.";
                    return false;
                }
            }

            narrator.stepPassed("Period Employed MM: " + periodMonth);
        }

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.employerDetailsTab()))
            {
                error = "Failed to scroll to 'employer details' Tab.";
                return false;
            }
           if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");


        }

        return true;
    }

}
