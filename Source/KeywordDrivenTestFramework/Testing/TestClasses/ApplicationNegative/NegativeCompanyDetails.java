/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Company Negative",
        createNewBrowserInstance = false
)
public class NegativeCompanyDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeCompanyDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!companyDetails())
        {

            return narrator.testFailed("Failed to perform the negative test for -"+error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean companyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.companyDetailsHeader()))
        {
            error = "Failed to wait for 'Company Details' header";
            return false;

        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyTransaction()))
        {
            error = "Failed to wait for the'Company transaction' checkBox to appear ";
            return false;

        }
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.companyTransaction()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.companyTransaction()))
            {
                error = "Failed to click the 'Company transaction' checkBox";
                return false;

            }
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyName()))
        {
            error = "Failed to wait for the'Company name' field to appear ";
            return false;

        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyName()))
        {
            error = "Failed to clear the 'Company name' ";
            return false;

        }
        String companyName = getData("Company Name");
        if (!companyName.isEmpty())
        {

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyName(), companyName))
            {
                error = "Failed to enter the 'Company name' ";
                return false;

            }
        }
        narrator.stepPassed("Company Name: " + companyName);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vatNumber()))
        {
            error = "Failed to wait the 'VAT Number' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.vatNumber()))
        {
            error = "Failed to clear the 'VAT Number'";
            return false;

        }
        String vatNumber = getData("VAT Number");
        if (!vatNumber.isEmpty())
        {

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vatNumber(), vatNumber))
            {
                error = "Failed to enter the 'VAT Number'";
                return false;

            }
        }
        narrator.stepPassed("VAT Number: " + getData("VAT Number"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.businessRegistrationNumber()))
        {
            error = "Failed to wait for 'Registration Number' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessRegistrationNumber()))
        {
            error = "Failed to clear the 'Registration Number'";
            return false;

        }
        String registrationNumber = getData("Registration Number");
        if (!registrationNumber.isEmpty())
        {

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegistrationNumber(), getData("Registration Number")))
            {
                error = "Failed to enter the 'Registration Number'";
                return false;

            }
        }
        narrator.stepPassed("Registration Number: " + getData("Registration Number"));

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.companyDetailsHeader()))
            {
                error = "Failed to scroll to the 'Company details' container .";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");


        }

        return true;
    }

}
