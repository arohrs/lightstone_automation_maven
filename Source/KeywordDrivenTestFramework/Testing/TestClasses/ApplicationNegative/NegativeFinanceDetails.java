/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Finance Negative",
        createNewBrowserInstance = false
)

public class NegativeFinanceDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeFinanceDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!financeDetails())
        {
            return narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean financeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financeDetailsTab()))
        {
            error = "Failed to scroll to the 'Finance details' container .";
            return false;
        }
        String rateIndicator = getData("Rate Indicator");
        if (!rateIndicator.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.rateIndicatorDropDown(), rateIndicator))
            {
                error = "Failed to select 'rate indicator' from the list.";
                return false;
            }
        }
        narrator.stepPassed("Rate Indicator : " + rateIndicator);
        String residualPercentage = getData("Residual");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.residualBalloonPercentage()))
        {
            error = "Failed to clear Residual/Balloon field";
        }
        if (!residualPercentage.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.residualBalloonPercentage(), residualPercentage))
            {
                error = "Failed to clear Residual/Balloon field";
            }
        }
        narrator.stepPassed("Residual/Balloon % : " + residualPercentage);
        SeleniumDriverInstance.pressKey(Keys.TAB);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.depositTrade()))
        {
            error = "Failed to clear Cash Deposit field";
        }

        String cashDeposit = getData("Deposit In");
        if (!cashDeposit.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.depositTrade(), cashDeposit))
            {
                error = "Failed to enter the 'Cash Deposit ' amount into the text field - " + cashDeposit;
                return false;
            }
        }
        narrator.stepPassed("Cash deposit amount : " + cashDeposit);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financeDetailsTab()))
            {
                error = "Failed to scroll to the 'Finance details' container .";
                return false;
            }

            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }
        return true;
    }
}
