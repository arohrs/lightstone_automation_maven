/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Residential Negative",
        createNewBrowserInstance = false
)
public class NegativeResidentialDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String readOnly = getData("Read Only");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeResidentialDetails()
    {
    }

    public TestResult executeTest()
    {

        if (!residentialDetails())
        {

            return narrator.testFailed("Failed to perform the negative test -" + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean residentialDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.residentialDetailsTab()))
        {
            error = "Failed to wait for the 'residential address' text field to appear.";
            return false;
        }
        List<String> readOnlyField = Arrays.asList(readOnly.split(","));
        for (String readField : readOnlyField)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.readOnlyInputField(readField)))
            {
                error = "Failed to validate '" + readField + "' is a read-only field .";
                return false;
            }
            narrator.stepPassed("Successfully validated " + readField + " field is not manually editable");

        }
        String address1 = getData("Residential Address");
        if (!address1.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.address1(), address1))
            {
                error = "Failed to enter 'residential address' on the text field.";
                return false;
            }
        }
        narrator.stepPassed("Residential Address: " + address1);

        String periodYear = getData("Period Year");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.periodYear()))
        {
            error = "Failed to clear 'period(year)' text field.";
            return false;
        }

        if (!periodYear.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.periodYear(), periodYear))
            {
                error = "Failed to enter 'period(year)' into the text field.";
                return false;
            }
        }
        narrator.stepPassed("Period at Current Address YY: " + periodYear);

        String periodMonth = getData("Period Month");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.periodMonth()))
        {
            error = "Failed to clear 'period(month)' text field.";
            return false;
        }
        if (!periodMonth.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.periodMonth(), periodMonth))
            {
                error = "Failed to enter 'period(month)' into the text field.";
                return false;
            }
        }

        narrator.stepPassed("Period at Current Address MM: " + periodMonth);

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.residentialDetailsTab()))
            {
                error = "Failed to wait for the 'residential address' text field to appear.";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
