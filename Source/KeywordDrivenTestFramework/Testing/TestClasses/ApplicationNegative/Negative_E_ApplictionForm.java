/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Application Negative",
        createNewBrowserInstance = false
)
public class Negative_E_ApplictionForm extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String submit = getData("Submit");
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;

    public Negative_E_ApplictionForm()
    {
    }

    public TestResult executeTest()
    {
        if (!applicationForm())
        {
            return narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test ");
    }

    public boolean applicationForm()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.applicationDropDown())))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'Application' Menu");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.newApplication())))
        {
            error = "Failed to select 'new application' from the list of applications.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.applicationFormTab()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        narrator.stepPassed("Successfully clicked on the 'New Application' from the list");

        /*Entering customer details on the new application*/
        String firstName = getData("Firstname");
        if (!firstName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationFirstname(), firstName))
            {
                error = "Failed to enter 'firstname' into the firstname text field.";
                return false;
            }

        }
        narrator.stepPassed("First Name: " + " " + firstName);
        String surName = getData("Surname");
        if (!surName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationSurname(), surName))
            {
                error = "Failed to enter 'Surname' into the surname text field.";
                return false;
            }

        }
        narrator.stepPassed("Surname: " + " " + surName);
        String idNumber = getData("ID");
        if (!idNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationIdNumber(), idNumber))
            {
                error = "Failed to enter 'id' number into the id text field.";
                return false;
            }

        }
        narrator.stepPassed("ID Number: " + " " + idNumber);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.idNumberLabel()))
        {
            error = "Failed to click the 'id nnumber label.'";
            return false;
        }
        if (!SeleniumDriverInstance.dismissAlert())
        { //cancells an alert
            error = "Failed to handle an alert.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.applicationVerifiedDropDown()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.applicationVerifiedDropDown(), getData("Verified")))
        {
            error = "Failed to select Yes/NO to check if or not an application is verified.";
            return false;
        }
        narrator.stepPassed("ID Verified: " + " " + getData("Verified"));
        if (!isAfterMarket)
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.clickAbsaBank()))
            {
                error = "Failed to wait for the 'absa bank' check box to appear";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickAbsaBank())))
            {
                error = "Failed to click the 'absa bank' check-box.";
                return false;
            }
            narrator.stepPassed("Clicked ABSA BANK checkBox");
        }
        if (isAfterMarket)
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.afterMarket()))
            {
                error = "Failed to wait for the 'After market' check box to appear";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.afterMarket())))
            {
                error = "Failed to click the 'After market' check-box.";
                return false;
            }
            narrator.stepPassed("Clicked After Market checkBox");

        }
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicationFormTab()))
            {
                error = "Failed to wait for the 'application form tab' to appear.";
                return false;
            }
            if (getData("Verified").equalsIgnoreCase("No"))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to find the red highlighted " + field + " field.";
                    return false;
                }
                narrator.stepPassed("Successfully validated the " + field + " drop-down ");

            }
            if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
            {
                error = "Failed to find the red highlighted " + field + " field.";
                return false;
            }
            pause(500);
            narrator.stepPassedWithScreenShot("Successfully caught the red highlighted " + field + " field");
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
            {
                error = "Failed to find the red highlighted " + field + " field.";
                return false;
            }

            if (!SeleniumDriverInstance.closeCurrentTab(LightStonePageObjects.getPageHandle()))
            {
                error = "Failed to close the current window";
                return false;

            }
            narrator.stepPassed("Moved back to the boadroom page");
        }
        return true;
    }
}
