/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Applicant Negative",
        createNewBrowserInstance = false
)
public class NegativeConsentDetails extends BaseClass
{
     String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");


    public NegativeConsentDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!consentDetails())
        {

            return narrator.testFailed("Failed to perform the negative test "+error);
        }
        return narrator.finalizeTest("Successfully performed the negative test case");
    }

    public boolean consentDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicationDetailsTab()))
        {
            error ="Failed to scroll to the consent details tab";
            return false;
        }
       
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.shareInformation(), getData("Consent1")))
        {
            error = "Failed to select 'Yes/No' to indicate the  consent.";
            return false;
        }
        narrator.stepPassed("Consent to your information being shared: "+getData("Consent1"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sharingInformationToAlpheraGroup(), getData("Consent2")))
        {
            error = "Failed to select 'Yes/No' to indicate the  consent.";
            return false;
        }
        narrator.stepPassed("consent that you forward my personal information :"+getData("Consent2"));
       if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicationDetailsTab()))
            {
                error = "Failed to scroll to the 'banking details' table.";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
