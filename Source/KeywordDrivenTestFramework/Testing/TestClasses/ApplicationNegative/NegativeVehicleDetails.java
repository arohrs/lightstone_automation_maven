/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.Application.E_ApplicationDetails;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Vehicle Negative",
        createNewBrowserInstance = false
)
public class NegativeVehicleDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String readOnly = getData("Read Only");
    String notEditable = getData("Not editable");
    String field = getData("Field");
    String submit = getData("Submit");
    boolean isNegative = (!testData.getData("IsNegative").isEmpty()) ? Boolean.parseBoolean(testData.getData("IsNegative").toLowerCase()) : false;

    public NegativeVehicleDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!vehicleDetails())
        {
            return narrator.testFailed("Failed to perform  the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test ");
    }

    public boolean vehicleDetails()
    {

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsTab()))
        {
            error = "Failed to scroll to the 'Vehicle details' container .";
            return false;
        }
        List<String> readOnlyField = Arrays.asList(readOnly.split(","));
        for (String readField : readOnlyField)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.readOnlyInputField(readField)))
            {
                error = "Failed to validate '" + readField + "' is a read-only field .";
                return false;
            }

        }
        narrator.stepPassed("Successfully validated " + notEditable + " fields are not manually editable");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.purchasePriceTextfield()))
        {
            error = "Failed to wait for 'Purchase Price' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.purchasePriceTextfield()))
        {
            error = "Failed to wait for 'Purchase Price' to appear";
            return false;
        }
        if (!getData("Purchase Price").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.purchasePriceTextfield(), getData("Purchase Price")))
            {
                error = "Failed to enter the 'purchase price' into the text field";
                return false;
            }
             narrator.stepPassed("Purchase Price: " + getData("Purchase Price"));
        }
          narrator.stepPassed("Successfully validated Purchase Price field only accepts numbers");
     

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsTab()))
            {
                error = "Failed to scroll to the 'Vehicle details' container .";
                return false;
            }
            if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
            {
                error = "Failed to scroll to the 'Vehicle details' container .";
                return false;
            }
            pause(500);
            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field ");
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
            {
                error = "Failed to catch the '" + field + "' is Highlighted in red .";
                return false;
            }

        }

        return true;
    }

}
