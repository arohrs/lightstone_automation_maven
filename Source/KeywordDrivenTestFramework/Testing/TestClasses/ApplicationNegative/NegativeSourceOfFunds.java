/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Source of Fund Negative",
        createNewBrowserInstance = false
)
public class NegativeSourceOfFunds extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeSourceOfFunds()
    {
    }

    public TestResult executeTest()
    {
        if (!sourceOfFunds())
        {
            return narrator.testFailed("Failed to perform the negative test -" + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean sourceOfFunds()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.sourceOfFundsTab()))
        {
            error = "Failed to scroll to the 'Source of Funds' container .";
            return false;
        }
//        if (!field.isEmpty())
//        {
//            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.allowanceCheckBox()))
//            {
//                error = "Failed to scroll to the 'delivery method' list.";
//                return false;
//            }
//        }
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.sourceOfFundsTab()))
            {
                error = "Failed to scroll to the 'Source of Funds' container .";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.winningsCheckBox()))
            {
                error = "The '" + field + "' field is not highlighted in red .";
                return false;
            }
            narrator.stepPassed("Successfully validated the 'Winnings ' checkBox is disabled");
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.beeIncomeCheckBox()))
            {
                error = "The '" + field + "' field is not highlighted in red .";
                return false;
            }
            narrator.stepPassed("Successfully validated the 'BEE Income' checkBox is disabled");
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.allowanceCheckBox()))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.allowanceCheckBox()))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }

            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
