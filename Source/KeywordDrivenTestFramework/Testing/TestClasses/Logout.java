/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author nkelechi
 * @Internal Review sbachan 2018-09-14
 */
@KeywordAnnotation(
        Keyword = "Logout",
        createNewBrowserInstance = false
         
)

public class Logout extends BaseClass
{

    String error = "";

    public Logout()
    {
    }

    public TestResult executeTest()
    {
        if (!logout())
        {
            return narrator.testFailed("Failed to log out - " + error);
        }
        return narrator.finalizeTest("Successfully Logged out of SIGNIO.");
    }

    public boolean logout()
    {
        // Waiting for Logout button
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.logoutButtonXpath()))
        {
            error = "Failed to wait for logout button";
            return false;
        }
        pause(500);

        // Clicking on Logout button
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.logoutButtonXpath()))
        {
            error = "Failed to click on logout button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.username()))
        {
            error = "Failed to validate Logout was successful";
            return false;
        }

        return true;
    }
}
