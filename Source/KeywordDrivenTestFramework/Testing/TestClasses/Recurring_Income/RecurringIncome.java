/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Recurring_Income;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Recurring Income",
        createNewBrowserInstance = false
)

public class RecurringIncome extends BaseClass
{

    String error = "";
    String fiList = "F&I";
    String supplyList = "Supplier";
    String department = "Department";
    String saleList = "Type of Sale";
    String vapList = "VAP Category";

    public RecurringIncome()
    {
    }

    public TestResult executeTest()
    {

        if (!recurringIncome())
        {
            return narrator.testFailed("Failed to navigate into Recurring Income - " + error);
        }
        if (!addRecurringIncome())
        {
            return narrator.testFailed("Failed to add new Recurring Income - " + error);
        }

        return narrator.finalizeTest("Successfully validated the Recurring Income window ");
    }

    public boolean recurringIncome()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        //Navigate to DOC
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.mySignioDropDown())))
        {
            error = "Failed to wait for the 'My Signio drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.mySignioDropDown())))
        {
            error = "Failed to click  the 'My Signio drop down' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on 'MY SIGNIO'");
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.myMerchant())))
        {
            error = "Failed to wait for the 'My Merchant drop down' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.myMerchant())))
        {
            error = "Failed to click 'My Merchant' from the MY Signio dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on  'MY MERCHANT'");
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.changeMerchantHeader())))
        {
            error = "Failed to wait for the 'chandge merchant' header.";
            return false;
        }
        String merchant = getData("Merchant");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.currentMerchant(merchant))))
        {
            error = "Failed to click 'the selected merchant'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.selectInstitution())))
        {
            error = "Failed to wait for the 'Select institution' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.selectInstitution())))
        {
            error = "Failed to click 'Select institution' button.";
            return false;
        }
        narrator.stepPassed("Successfully changed merchant to: " + merchant);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.epsonMotorsLabel())))
        {
            error = "Failed to wait for the 'EPSON MOTORS' label on the top right corner.";
            return false;
        }

        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the current merchant is: " + merchant);

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to click 'DOC' dropdown button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on 'DOC' dropdown");
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.recurringIncome())))
        {
            error = "Failed to wait for the 'Recurring Income' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.recurringIncome())))
        {
            error = "Failed to click  the 'Recurring Income' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.recurringIncomeHeader()))
        {
            error = "Failed to wait for the 'Recurring Income page' to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated into Recurring Income");

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
        String currentMonth = dateFormat.format(date);

        String monthHeader = SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.monthHeader());
        if (!currentMonth.equals(monthHeader))
        {
            narrator.stepFailed("Recurring Income did not land on the current month");

        }
        narrator.stepPassed("Successfully validated Recurring Income page is in the current month: "+currentMonth);

        return true;
    }

    public boolean addRecurringIncome()
    {

        List<String> recurringIncomeFI_IDs_Original = SeleniumDriverInstance.retrieveMultipleAttributeByXpath(LightStonePageObjects.recurringIncomeFI(), "id");

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.addRecurringIncome()))
        {
            error = "Failed to wait for the 'Add ' button to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.addRecurringIncome()))
        {
            error = "Failed to click  the 'Add'button to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Add button");

        int timeout = 30;
        int counter = 0;
        boolean elementFound = false;
        int numOfRecurringIncome_Before = recurringIncomeFI_IDs_Original.size() + 1;

        while (!elementFound && counter < timeout)
        {
            if (SeleniumDriverInstance.retrieveNumberOfElementsByXpath(LightStonePageObjects.recurringIncomeFI()) == numOfRecurringIncome_Before)
            {
                elementFound = true;
            }
            pause(500);
            counter++;
        }

        List<String> recurringIncomeFI_IDs_New = SeleniumDriverInstance.retrieveMultipleAttributeByXpath(LightStonePageObjects.recurringIncomeFI(), "id");
        String newRecurringIncomeFI_ID = "";
        for (String currentID : recurringIncomeFI_IDs_New)
        {
            if (!recurringIncomeFI_IDs_Original.stream().anyMatch(s -> s.equalsIgnoreCase(currentID)))
            {
                newRecurringIncomeFI_ID = currentID;
            }
        }

        LightStonePageObjects.setRecurringIncomeFI_BaseRow(newRecurringIncomeFI_ID);

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fnI(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'F&I' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.fnI(newRecurringIncomeFI_ID)))
        {
            error = "Failed to select for the 'F&I' to appear.";
            return false;
        }

        List<String> FnIList = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.fnIDropdown(newRecurringIncomeFI_ID));
        validateDropdown(FnIList, fiList);

        String fi = getData("F&I");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fnI(newRecurringIncomeFI_ID), fi))
        {
            error = "Failed to select for the 'F&I' to appear.";
            return false;
        }
        narrator.stepPassed("F&I : " + fi);
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.supplier(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'supplier' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.supplier(newRecurringIncomeFI_ID),""))
        {
            error = "Failed to select for the 'supplier' to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the Supplier dropdown");
        List<String> supplierList = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.supplierDropdown(newRecurringIncomeFI_ID));
        validateDropdown(supplierList, supplyList);
        String supplier = getData("Supplier");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.supplier(newRecurringIncomeFI_ID), supplier))
        {
            error = "Failed to select for a 'supplier' from the list.";
            return false;
        }
        narrator.stepPassed("Supplier: " + supplier);
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.department(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'department' to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.department(newRecurringIncomeFI_ID)))
        {
            error = "Failed to select for the 'department' to appear.";
            return false;
        }
        List<String> departmentList = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.departmentDropdown(newRecurringIncomeFI_ID));
        validateDropdown(departmentList, department);

        String department = getData("Department");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.department(newRecurringIncomeFI_ID), department))
        {
            error = "Failed to select for a 'department' from the list.";
            return false;
        }
        narrator.stepPassed("Department: " + department);
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.typeSale(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'Type of sale' to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.typeSale(newRecurringIncomeFI_ID)))
        {
            error = "Failed to select for the 'Type of sale' to appear.";
            return false;
        }
        List<String> typeSaleList = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.departmentDropdown(newRecurringIncomeFI_ID));
        validateDropdown(typeSaleList, saleList);

        String typeOfSale = getData("Type of Sale");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeSale(newRecurringIncomeFI_ID), typeOfSale))
        {
            error = "Failed to select for a 'Type of Sale' from the list.";
            return false;
        }
        narrator.stepPassed("Type Of Sale: " + typeOfSale);
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.vapCategory(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'Vap category' to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.vapCategory(newRecurringIncomeFI_ID)))
        {
            error = "Failed to select for the 'Vap category' to appear.";
            return false;
        }
        List<String> vapCategoryList = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.vapCategoryDropdown(newRecurringIncomeFI_ID));
        validateDropdown(vapCategoryList, vapList);

        String vapCategory = getData("VAP Category");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vapCategory(newRecurringIncomeFI_ID), vapCategory))
        {
            error = "Failed to select for a 'VAP Category' from the list.";
            return false;
        }
        narrator.stepPassed("Vap Category: " + vapCategory);
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.vapIncome(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'VAP Category' to appear.";
            return false;
        }
        String vapIncome = getData("VAP Income");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.vapIncome(newRecurringIncomeFI_ID), vapIncome))
        {
            error = "Failed to enter  the 'VAP Income'.";
            return false;
        }
        narrator.stepPassed("Vap Income: " + vapIncome);
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.compIns_Income(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'comp Ins Income' to appear.";
            return false;
        }
        String compInsIncome = getData("Comp Ins Income");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.compIns_Income(newRecurringIncomeFI_ID), compInsIncome))
        {
            error = "Failed to enter for the 'comp Ins Income' to appear.";
            return false;
        }
        narrator.stepPassed("Comp Ins Income: " + compInsIncome);
        List<String> vapIncomeValues = SeleniumDriverInstance.retrieveMultipleAttributeByXpath(LightStonePageObjects.vapIncomeList(),"value");
        double vapIncomeSum =0;
        for(int i =0;i<vapIncomeValues.size();i++)
        {
         vapIncomeSum += Double.parseDouble(vapIncomeValues.get(i));
        }
        List<String> compsIncomeValues = SeleniumDriverInstance.retrieveMultipleAttributeByXpath(LightStonePageObjects.compInsIncomeList(),"value");
        double compIncomeSum =0;
        for(int i =0;i<compsIncomeValues.size();i++)
        {
         compIncomeSum += Double.parseDouble(compsIncomeValues.get(i));
        }
       
        String totalExpensesInclVat = SeleniumDriverInstance.retrieveTextByRobot(LightStonePageObjects.recurrinIncomeTotInclVat());
        double totExclVat = vapIncomeSum + compIncomeSum;
        double totInclVat = Double.parseDouble(totalExpensesInclVat);

        validateVAT(totExclVat, totInclVat);
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.save_RecurringIncome()))
        {
            error = "Failed to wait for the 'save' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.save_RecurringIncome()))
        {
            error = "Failed to enter for the 'save' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.recurringIncomeHeader()))
        {
            error = "Failed to wait for the 'Recurring Income' Header.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'save' button");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.removeRecurringIncome(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'remove' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.removeRecurringIncome(newRecurringIncomeFI_ID)))
        {
            error = "Failed to enter for the 'remove' button to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'remove' button");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.save_RecurringIncome()))
        {
            error = "Failed to wait for the 'save' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.save_RecurringIncome()))
        {
            error = "Failed to click for the 'save' button to appear.";
            return false;
        }
        narrator.stepPassed("Successfully clicked on the 'save' button");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.prevMonthRecurringIncome()))
        {
            error = "Failed to wait for the 'prev Month' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.prevMonthRecurringIncome()))
        {
            error = "Failed to click for the 'prev Month' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.recurringIncomeHeader()))
        {
            error = "Failed to wait for the 'Recurring Income' Header to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'previous month' button");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.nextMonthRecurringIncome()))
        {
            error = "Failed to wait for the 'Next Month' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.nextMonthRecurringIncome()))
        {
            error = "Failed to click for the 'Next Month' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.recurringIncomeHeader()))
        {
            error = "Failed to wait for the 'Recurring Income' Header to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'next month' button");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.prePopulateRecurringIncome()))
        {
            error = "Failed to wait for the 'Recurring Income' button to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.prePopulateRecurringIncome()))
        {
            error = "Failed to click for the 'Next Month' button.";
            return false;
        }
        if(!SeleniumDriverInstance.dismissAlert())
        {
            error ="Failed to dismiss the alert Pop Up";
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'Pre-populate' button");
        SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());
        narrator.stepPassed("Successfully moved back to the boadroom Page");

        return true;
    }

     public boolean validateDropdown(List<String> listOfElement, String field)
    {
        
        for (String textDescription : listOfElement)
        {
            if (textDescription.isEmpty())
            {//to be changed
                narrator.stepWarning("WARNING! One field in the " + field + " list is empty!");

            }

        }
        narrator.stepPassed("Successfully checked the " + field + " list!");
        return true;

    }

    public boolean validateVAT(double exclVat, double exceptedAmount)
    {
        double inclVat = (exclVat * 0.15) + exclVat;

        if (inclVat != exceptedAmount)
        {
            narrator.stepFailed("Failed the VAT validation");
            return false;
        }
        narrator.stepPassed("Successfully validated the total Recurring Income VAT");

        return true;
    }

}
