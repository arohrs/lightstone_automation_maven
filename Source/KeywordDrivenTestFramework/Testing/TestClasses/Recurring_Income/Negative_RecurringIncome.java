/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Recurring_Income;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Negative Recurring Income",
        createNewBrowserInstance = false
)

public class Negative_RecurringIncome extends BaseClass
{

    String error = "";

    public Negative_RecurringIncome()
    {
    }

    public TestResult executeTest()
    {

        if (!recurringIncome())
        {
            return narrator.testFailed("Failed to navigate into Recurring Income - " + error);
        }
        if (!addNegativeRecurringIncome())
        {
            return narrator.testFailed("Failed to add new Recurring Income - " + error);
        }

        return narrator.finalizeTest("Successfully navigated into Recurring Income window ");
    }

    public boolean validateDropdown(List<String> listOfElement, String field)
    {
        for (String textDescription : listOfElement)
        {
            if (textDescription.isEmpty())
            {//to be changed
                narrator.stepWarning("WARNING!one/many field in the "+field+" list is empty!");

            }

        }
        return true;

    }

    public boolean validateVAT(double exclVat, double exceptedAmount)
    {
        double inclVat = (exclVat * 0.15) + exclVat;

        if (inclVat != exceptedAmount)
        {
            narrator.stepFailed("Failed the VAT validation");
            return false;
        }

        return true;
    }

    public boolean recurringIncome()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        /*Creating a new application*/
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.mySignioDropDown())))
        {
            error = "Failed to wait for the 'My Signio drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions((LightStonePageObjects.mySignioDropDown())))
        {
            error = "Failed to click  the 'My Signio drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.myMerchant())))
        {
            error = "Failed to wait for the 'My Merchant drop down' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.myMerchant())))
        {
            error = "Failed to click 'My Merchant' from the MY Signio dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.changeMerchantHeader())))
        {
            error = "Failed to wait for the 'chandge merchant' header.";
            return false;
        }
        String merchant = getData("Merchant");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.currentMerchant(merchant))))
        {
            error = "Failed to click 'the selected merchant'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.selectInstitution())))
        {
            error = "Failed to wait for the 'Select institution' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.selectInstitution())))
        {
            error = "Failed to click 'Select institution' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.epsonMotorsLabel())))
        {
            error = "Failed to wait for the 'EPSON MOTORS' label on the top right corner.";
            return false;
        }

        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully checked the current merchant: " + merchant);

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to click 'DOC' dropdown button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.recurringIncome())))
        {
            error = "Failed to wait for the 'Recurring Income' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.recurringIncome())))
        {
            error = "Failed to click  the 'Recurring Income' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.recurringIncomeHeader()))
        {
            error = "Failed to wait for the 'Recurring Income page' to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated into Recurring Income");

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
        String currentMonth = dateFormat.format(date);

        String monthHeader = SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.monthHeader());
        if (!currentMonth.equals(monthHeader))
        {
            narrator.stepFailed("Recurring Income did not land on the current month");

        }

        return true;
    }

    public boolean addNegativeRecurringIncome()
    {

        List<String> recurringIncomeFI_IDs_Original = SeleniumDriverInstance.retrieveMultipleAttributeByXpath(LightStonePageObjects.recurringIncomeFI(), "id");

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.addRecurringIncome()))
        {
            error = "Failed to wait for the 'Add ' button to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.addRecurringIncome()))
        {
            error = "Failed to wait for the 'Add'button to appear.";
            return false;    
        }

        int timeout = 30;
        int counter = 0;
        boolean elementFound = false;
        int numOfRecurringIncome_Before = recurringIncomeFI_IDs_Original.size() + 1;

        while (!elementFound && counter < timeout)
        {
            if (SeleniumDriverInstance.retrieveNumberOfElementsByXpath(LightStonePageObjects.recurringIncomeFI()) == numOfRecurringIncome_Before)
            {
                elementFound = true;
            }
            pause(1000);
            counter++;
        }

        List<String> recurringIncomeFI_IDs_New = SeleniumDriverInstance.retrieveMultipleAttributeByXpath(LightStonePageObjects.recurringIncomeFI(), "id");
        String newRecurringIncomeFI_ID = "";
        for (String currentID : recurringIncomeFI_IDs_New)
        {
            if (!recurringIncomeFI_IDs_Original.stream().anyMatch(s -> s.equalsIgnoreCase(currentID)))
            {
                newRecurringIncomeFI_ID = currentID;
            }
        }

        LightStonePageObjects.setRecurringIncomeFI_BaseRow(newRecurringIncomeFI_ID);

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fnI(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'F&I' to appear.";
            return false;
        }

        String fi = getData("F&I");

        if (!fi.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fnI(newRecurringIncomeFI_ID), fi))
            {
                error = "Failed to select for the 'F&I' to appear.";
                return false;
            }

        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.supplier(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'supplier' to appear.";
            return false;
        }
        
        String supplier = getData("Supplier");
        if (!supplier.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.supplier(newRecurringIncomeFI_ID), supplier))
            {
                error = "Failed to select for a 'supplier' from the list.";
                return false;
            }

        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.typeSale(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'Type of sale' to appear.";
            return false;
        }

        String typeOfSale = getData("Type of Sale");
        if (!typeOfSale.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeSale(newRecurringIncomeFI_ID), typeOfSale))
            {
                error = "Failed to select for a 'Type of Sale' from the list.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.vapCategory(newRecurringIncomeFI_ID)))
        {
            error = "Failed to wait for the 'Vap category' to appear.";
            return false;
        }

        String vapCategory = getData("VAP Category");
        if (!vapCategory.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vapCategory(newRecurringIncomeFI_ID), vapCategory))
            {
                error = "Failed to select for a 'VAP Category' from the list.";
                return false;
            }

        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.save_RecurringIncome()))
        {
            error = "Failed to wait for the 'save' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.save_RecurringIncome()))
        {
            error = "Failed to enter for the 'save' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.error_RecurringIncome(),3))
        {
            error = "Test failed empty field was not highlighted in red";
            narrator.stepFailedWithScreenShot(error);
            SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully checked an empty mandatory field is highlighted in red ");

        SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());

        return true;
    }

}
