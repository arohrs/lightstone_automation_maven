/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Epson-DOC",
        createNewBrowserInstance = false
)

public class DOC_Epson extends BaseClass
{

    String error = "";
    String merchant = getData("Merchant");
    String myMerchantValidation = getData("My Merchant");
    String myProfileValidation = getData("My Profile");
    String signioValidation = getData("SIGNIO");
    String epsonMotorsValidation = getData("EPSON MOTORS");
    String myPreferencesValidation = getData("My Preferences");
    String monthlyDocValidation = getData("Monthly DOC");

    String docMonthValidation = getData("DOC Month");

    public DOC_Epson()
    {
    }

    public TestResult executeTest()
    {
        if (!navigateDOC())
        {
            return narrator.testFailed("Failed to navigate into  DOC - " + error);
        }
        if (!monthlyDoc())
        {
            return narrator.testFailed("Failed to navigate into  monthly DOC - " + error);
        }
        if (!recurringIncome())
        {
            return narrator.testFailed("Failed navigate into Recurring Income - " + error);
        }
        if (!manageSalesPersons())
        {
            return narrator.testFailed("Failed navigate into Manage SalesPersons - " + error);
        }
        return narrator.finalizeTest("Successfully navigated into all DOC  ");
    }

    public boolean navigateDOC()
    {

        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        /*Creating a new application*/
        narrator.stepPassedWithScreenShot("Successfully landed on the home page.");

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.mySignioDropDown())))
        {
            error = "Failed to wait for the 'My Signio drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.mySignioDropDown())))
        {
            error = "Failed to click  the 'My Signio drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.myMerchant())))
        {
            error = "Failed to wait for the 'My Merchant drop down' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully showing MY SIGNIO drop down elements.");
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.myPreferenceValidation(), myPreferencesValidation))
        {
            error = "Failed to validate that 'My Preferences' is present by text.";
            return false;
        }
        narrator.stepPassed("Successfully validated that :" + myPreferencesValidation + " is present.");
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.myProfile(), myProfileValidation))
        {
            error = "Failed to validate that 'My Profile' is present by text.";
            return false;
        }
        narrator.stepPassed("Successfully validated that :" + myProfileValidation + " is present.");

        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.myMerchant(), myMerchantValidation))
        {
            error = "Failed to validate that 'My Merchant' is present by text.";
            return false;
        }
        narrator.stepPassed("Successfully validated that :" + myMerchantValidation + " is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.myMerchant())))
        {
            error = "Failed to click 'My Merchant' from the MY Signio dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.changeMerchantHeader())))
        {
            error = "Failed to wait for the 'change merchant' header.";
            return false;
        }
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.signioValidation(), signioValidation))
        {
            error = "Failed to validate that the 'My Signio' header is present.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully landed on my merchant page.");
        narrator.stepPassed("Successfully validated that :" + signioValidation + " header is Present.");

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.currentMerchant(merchant))))
        {
            error = "Failed to click 'the selected merchant'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.selectInstitution())))
        {
            error = "Failed to wait for the 'Select institution' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully checked the current merchant: " + merchant);

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.selectInstitution())))
        {
            error = "Failed to click 'Select institution' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.epsonMotorsLabel())))
        {
            error = "Failed to wait for the 'EPSON MOTORS' label on the top right corner.";
            return false;
        }
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.epsonMototsValidation(), epsonMotorsValidation))
        {
            error = "Failed to validate that 'EPSON MOTORS' header is present.";
            return false;
        }
        narrator.stepPassed("Successfully validated that the header changed from : " + signioValidation + " to " + epsonMotorsValidation + ".");

        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated back to the home page.");
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    public boolean monthlyDoc()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to click 'DOC' dropdown button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.monthlyDoc())))
        {
            error = "Failed to wait for the 'Monthly DOC ' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.monthlyDoc(), monthlyDocValidation))
        {
            error = "Failed to validate that 'Monthly DOC ' is present by text.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully showing Doc drop down elements.");
        narrator.stepPassed("Successfully validated that: " + monthlyDocValidation + " is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.monthlyDoc())))
        {
            error = "Failed to click 'Monthly DOC' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to wait for the 'Monthly DOC page' to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.docCurrentMonthValidation(), docMonthValidation))
        {
            error = "Failed to validate that 'doc current month' is present by text.";
            return false;
        }
        narrator.stepPassedWithScreenShot(
                "Successfully landed on DOC Monthly page.");

        narrator.stepPassed("Successfully validated that: " + docMonthValidation + " is present.");

        pause(500);
        if (!SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle()))
        {
            error = "Failed to close the monthly doc tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully closed the monthly doc tab and landed back to the home page.");

        return true;
    }

    public boolean recurringIncome()
    {
        String recurringIncome = getData("Recurring Income");
        String recurringIncomevalidation = getData("Recurring Income Amounts");
        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to click 'DOC' dropdown button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.recurringIncome())))
        {
            error = "Failed to wait for the 'Recurring Income' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.recurringIncome(), recurringIncome))
        {
            error = "Failed to validate that 'Monthly DOC ' is present by text.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully showing Doc drop down elements.");
        narrator.stepPassed("Successfully validated that: " + recurringIncome + " is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.recurringIncome())))
        {
            error = "Failed to click  the 'Recurring Income' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.recurringIncomeHeader()))
        {
            error = "Failed to wait for the 'Recurring Income page' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.recurringIncomeHeader(), recurringIncomevalidation))
        {
            error = "Failed to validate that 'recurring income' is present by text.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully landed on recurring Income page.");
        narrator.stepPassed("Successfully validated that: " + recurringIncomevalidation + " is present.");

        pause(500);

        if (!SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle()))
        {
            error = "Failed to close the recurring income tab.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully closed the recurring tab and landed back to the home page.");
        return true;
    }

    public boolean manageSalesPersons()
    {
        String manageSalesPerson = getData("Manage Salesperson");
        String manageSalespersonValidaton = getData("Salespersons");

        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to click 'DOC' dropdown button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.manageSalesPersons())))
        {
            error = "Failed to wait for the 'Manage SalesPersons' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.manageSalesPersons(), manageSalesPerson))
        {
            error = "Failed to validate that 'manage salesperson' is present by text.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully showing DOC drop down elements.");
        narrator.stepPassed("Successfully validated that: " + manageSalesPerson + " is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.manageSalesPersons())))
        {
            error = "Failed to click 'Manage SalesPersons'from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.salesPersonsHeader()))
        {
            error = "Failed to wait for the 'Manage SalesPersons page' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.salesPersonsHeader(), manageSalespersonValidaton))
        {
            error = "Failed to validate that 'manage saleperson' is present by text.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully landed on manage salesperson page.");
        narrator.stepPassed("Successfully validated that: " + manageSalespersonValidaton + " is present.");

        pause(500);
        if (!SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle()))
        {
            error = "Failed to close the management salesperson tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.docDropDown())))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        narrator.stepPassed("Successfully closed the management salesperson tab and landed back to the home page.");

        return true;
    }

}
