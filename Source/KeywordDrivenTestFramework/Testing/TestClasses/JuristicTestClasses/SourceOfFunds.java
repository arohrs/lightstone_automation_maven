/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Source Of Funds",
        createNewBrowserInstance = false
)
public class SourceOfFunds extends BaseClass
{
    String error="";
    String commission = getData("Commission");
    public SourceOfFunds()
    {
    }
    
    public TestResult executeTest()
    {
        if(!sourceOfFunds())
        {
        
            narrator.testFailed("Failed to select the source of funds -"+error);
        }
    
    
        return narrator.finalizeTest("Successfully selected the source of funds");
    }
    
    public boolean sourceOfFunds()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.sourceOfFundsTab()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.developmentFunding()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Clicked on the Development Funding");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.commissionCheckBox()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.commissionAmount()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.commissionAmount(), commission))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Commission : "+commission);
    
        return true;
    }
    
}
