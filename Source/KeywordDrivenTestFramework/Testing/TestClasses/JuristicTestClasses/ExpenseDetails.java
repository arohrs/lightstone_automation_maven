/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Expense details",
        createNewBrowserInstance = false
)
public class ExpenseDetails extends BaseClass
{

    String error = "";
    String bond = getData("Bond");
    String amount = getData("Payment");
    String payDay = getData("Day");

    public ExpenseDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!expenseDetails())
        {

            narrator.testFailed("Failed to enter the expenses details -" + error);
        }
        if (!otherIncomeDetails())
        {
            narrator.testFailed("Failed to enter ");
        }

        return narrator.finalizeTest("Successfully entered the expenses details");
    }

    public boolean expenseDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryExpenseDetails()))
        {
            error = "Failed to scroll  the 'Expenses Details ' container.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryBondPayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryBondPayment(), bond))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorSpouseBondPayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorSpouseBondPayment(), bond))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Bond Payment : " + bond);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryRent()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryRent(), bond))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseRent()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseRent(), bond))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Rent : " + bond);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryRatesAndWater()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryRatesAndWater(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseRatesAndWater()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseRatesAndWater(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Rates, Water & Electricity : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryVehicleInstalment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryVehicleInstalment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseVehicleInstalment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseVehicleInstalment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Vehicle Instalments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryPersonalLoan()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryPersonalLoan(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpousePersonalLoan()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpousePersonalLoan(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Personal Loan Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryCreditCardRepayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryCreditCardRepayment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseCreditCardRepayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseCreditCardRepayment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Credit Card Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryFurnitureAccount()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryFurnitureAccount(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseFurnitureAccount()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseFurnitureAccount(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Furniture Accounts : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryClothingAccount()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryClothingAccount(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseClothingAccount()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseClothingAccount(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Clothing Accounts : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryOverdraftRepayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryOverdraftRepayment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseOverdraftRepayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseOverdraftRepayment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Overdraft Repayments : " + amount);
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryInsurancePayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Filled in details so far");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryInsurancePayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryInsurancePayment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseInsurancePayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseInsurancePayment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Policy/Insurance Payments : " + amount);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryTelephonePayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryTelephonePayment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseTelephonePayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseTelephonePayment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Telephone Payments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryTransportCost()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryTransportCost(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseTransportCost()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseTransportCost(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Transport Costs : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryFoodEntertainment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryFoodEntertainment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseFoodEntertainment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseFoodEntertainment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Food and Entertainment : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryEducationCost()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEducationCost(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseEducationCost()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseEducationCost(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Education Costs : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryMaintenance()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryMaintenance(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseMaintenance()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseMaintenance(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Maintenance : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryHouseholdExpenses()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryHouseholdExpenses(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseHouseholdExpenses()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseHouseholdExpenses(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Household Expenses : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryOtherExpenses()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryOtherExpenses(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseOtherExpenses()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseOtherExpenses(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Other Expenses : " + amount);
        narrator.stepPassedWithScreenShot("Successfully enter all the expenses details");

        return true;
    }

    public boolean otherIncomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryOtherIncomeTab()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorypayDay(), payDay))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Pay Day : " + payDay);
        narrator.stepPassedWithScreenShot("Successfully entered the other income details ");
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryExpenseDetails()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        return true;
    }
}
