/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Income details",
        createNewBrowserInstance = false
)
public class IncomeDetails extends BaseClass
{

    String error = "";
    
    public IncomeDetails()
    {
        
    }

    public TestResult executeTest()
    {
        if (!incomeDetails())
        {
            narrator.testFailed("Failed to enter the income details -" + error);
        }
        
        return narrator.finalizeTest("Successfully entered the income details");
    }
    
    public boolean incomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryIncomeDetailsTab()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        String sourceOfIncome = getData("Source of Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySourceofIncomeDetails(), sourceOfIncome))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Source of income : " + sourceOfIncome);
        String grossNet = getData("Gross Net");
        String income = getData("Income");
        String netTakeHome = getData("Net");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryGrossRenumeration(), grossNet))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseGrossRenumeration(), grossNet))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Gross Remuneration : " + grossNet);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryMonthlyCommission(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseMonthlyCommission(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Monthly Commission : " + income);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryCarAllowance(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseCarAllowance(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Car allowance : " + income);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryOverTime(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Overtime : " + income);        
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryReimbursement(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Reimbursement : " + income);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryNetTakeHome(), netTakeHome))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseNetTakeHome(), netTakeHome))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Net Take-home Pay : " + netTakeHome);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryOtherIncome(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseOtherIncome(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        
        narrator.stepPassed("Other Income : " + income);
        
        return true;
    }
}
