/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Financial Details",
        createNewBrowserInstance = false
)
public class FinancialDetails extends BaseClass
{

    String error = "";

    public FinancialDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!financialDetails())
        {

            narrator.testFailed("Failed to enter the banking details -" + error);
        }
        if (!accountToSettle())
        {

            narrator.testFailed("Failed to enter the account to be settled details -" + error);
        }
        if (!balanceSheet())
        {
            narrator.testFailed("Failed to enter the Balance sheet details -" + error);

        }

        return narrator.finalizeTest("Successfully entered the company's financial details");

    }

    public boolean financialDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financialDetailsTab()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        String valid = getData("Valid Bank");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.validAccountNumber(), valid))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Do you have a valid bank account number : "+valid);
        String bankName = getData("Bank Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyBankName(), bankName))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Bank Name : "+bankName);
        String branchName = getData("Branch Name");
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBankBranchName()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
      
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankBranchName(), branchName))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Branch Name : "+branchName);
        String branchCode = getData("Branch Code");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBankBranchCode()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankBranchCode(), branchCode))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Branch Code : "+branchCode);
        String accType = getData("Acc Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyBankAccountType(), accType))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Account Type : "+accType);
        String accNumber = getData("Acc Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankAccountNumber(), accNumber))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Account Number : "+accNumber);
        String accName = getData("Acc holder Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankAccountName(), accName))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Account Holder's Name : "+accName);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBankOverDraftLimit()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        String overDraftLimit = getData("OverDraft Limit");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankOverDraftLimit(), overDraftLimit))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Overdraft Limit : "+overDraftLimit);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBankOverDrafBalance()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        String overDraftBalance = getData("OverDraft Balance");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankOverDrafBalance(), overDraftBalance))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Overdraft Balance : "+overDraftBalance);

        return true;
    }

    public boolean accountToSettle()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addAccountToBeSettled()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Clicked on the 'Add Account To Be Settle'");
        String bankName = getData("Company");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.addCompanyAccount(),bankName))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Company : "+bankName);
        String accNumber = getData("Account Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.addCompanyAccountNumber(),accNumber))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Account Number : "+accNumber);
        String monthlyInstalment = getData("Monthly Instalment");
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.addCompanyAccountMonthlyInstalment(),monthlyInstalment))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
         narrator.stepPassed("Monthly Instalment : "+monthlyInstalment);
         String balance = getData("Outstanding Balance");
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.addCompanyAccountBalance(),balance))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
          narrator.stepPassed("Outstanding Balance : "+balance);
          String open =getData("Account Open");
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.addCompanyAccountOpen(),open))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
          narrator.stepPassed("Account Open : "+open);
          String intent = getData("Settlement Intent");
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.addCompanyAccountSettlementIntent(),intent))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Are you going to settle this account : "+intent);
        narrator.stepPassedWithScreenShot("Successfully added the account to be settled");
         if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.removeCompanyAccount()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }  
        narrator.stepPassed("Removed the account to be settled");
        
        
        return true;
    }

    public boolean balanceSheet()
    {
        String year = getData("Year");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBalanceSheetYear(),year))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Year : "+year);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBalanceSheetAssetAmount()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        String assetAmount = getData("Asset Amount");
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBalanceSheetAssetAmount(),assetAmount))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
         narrator.stepPassed("Total Asset Amount : "+assetAmount);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBalanceSheetTurnover()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
         String turnOver = getData("Tunover");
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBalanceSheetTurnover(),turnOver))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Turnover : "+turnOver);
        
        
        return true;
    }

}
