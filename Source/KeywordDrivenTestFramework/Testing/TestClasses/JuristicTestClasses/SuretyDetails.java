/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Surety",
        createNewBrowserInstance = false
)
public class SuretyDetails extends BaseClass
{

    String error = "";
    GeographicalDetails address = new GeographicalDetails();

    public SuretyDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!suretyDetails())
        {
            narrator.testFailed("");
        }

        return narrator.finalizeTest("");
    }

    public boolean suretyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatorySuretyTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        String employmentType = getData("Employment Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyEmploymentType(), employmentType))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Employment Type : "+employmentType);
        String periodEmployed = getData("Period");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedYear(), periodEmployed))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedMonth(), periodEmployed))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Period Currently Employed (YY/MM) : "+periodEmployed+"/"+periodEmployed);
        String residentialStatus = getData("Residential Status");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyResidentialStatus(), residentialStatus))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Residential Status : "+residentialStatus);
        String spouseFirstName = getData("Spouse FirstName");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseFirstName(), spouseFirstName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse First Name : "+spouseFirstName);
        String spouseSurName = getData("Spouse Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseSurname(), spouseSurName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse Surname : "+spouseSurName);
        String kinFirstName = getData("Next of Kin Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinFirstname(), kinFirstName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of Kin First Name : "+kinFirstName);
        String kinSurname = getData("Next of Kin Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinSurname(), kinSurname))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Next of Kin Surname : "+kinSurname);
          String relation = getData("Relation");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinRelation(), relation))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Applicant relation with next of kin : "+relation);
          String telephone = getData("Telephone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinTelephoneNumber(), telephone))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
         narrator.stepPassed("Next of kin telephone number : " +telephone);
         String addressLine = getData("Address");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine1(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Work Address Line 1: "+addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine2(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
         narrator.stepPassed("Work Address Line 2: "+addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyWorkAddressLookup()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        address.addressLookup();
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinWorkAddressLine1(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Address Line 1: "+addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinWorkAddressLine2(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Address Line 2: "+addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyNextofKinWorkAddressLookup()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        address.addressLookup();
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatorySuretyTab()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        return true;
    }
}
