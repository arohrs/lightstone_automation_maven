/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Dealer Extras",
        createNewBrowserInstance = false
)
public class DealerExtras extends BaseClass
{
    String error="";
    String description = getData("Description");
    String amount = getData("Amount");
    
    public DealerExtras()
    {
    }
    
    public TestResult executeTest()
    {
        if(!dealerExtra())
        {
            narrator.testFailed("Failed to enter the dealer extras details -"+error);
        }
    
    
    
        return narrator.finalizeTest("Successfully entered the dealer extras details");
    }
    
    public boolean dealerExtra()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionDealerExtraTab()))
        {
            error = "Failed to select the 'Dealer Extras ' container.";
            return false;
        }
         if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.transactionAddNewExtra()))
        {
            error = "Failed to click  the 'Add New Extras' button.";
            return false;
        }
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionExtraDescription(),description))
        {
            error = "Failed to select  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Description : "+description);
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionExtraAmount(),amount))
        {
            error = "Failed to enter  the 'Amount ' in the text field.";
            return false;
        }
          narrator.stepPassed("Description : "+amount);
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.transactionExtraTotalAmount()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }   
          narrator.stepPassed("Successfully validated the Total Extra Amount field is not manually edittable");
    
        return true;
    }
    
    
    
    
    
}
