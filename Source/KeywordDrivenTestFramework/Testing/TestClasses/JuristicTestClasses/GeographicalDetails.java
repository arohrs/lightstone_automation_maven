/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Geographical details",
        createNewBrowserInstance = false
)
public class GeographicalDetails extends BaseClass
{

    String error = "";
    String address = getData("Address");

    public GeographicalDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!addressDetails())
        {
            narrator.testFailed("Failed to enter the address details -" + error);
        }
        if (!propertyDetails())
        {

            narrator.testFailed("Failed to enter the property details -" + error);
        }

        return narrator.finalizeTest("Successfully entered the geographical details");
    }

    public boolean addressDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.geographicalTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessPhysicalAddress1(), address))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Physical Address 1: " + address);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessPhysicalAddress2(), address))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Physical Address 2: " + address);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.businessPhysicalAddressSuburbButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessPhysicalAddressSuburbButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        addressLookup();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.physicalAddressPopulateButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.physicalAddressPopulateButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessOperatingAddress1(), address))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Operating Address 1: " + address);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessOperatingAddress2(), address))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Operating Address 2: " + address);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.businessOperatingAddressSuburbButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessOperatingAddressSuburbButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        addressLookup();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.operatingAddressPopulateButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.operatingAddressPopulateButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegisteredAddress1(), address))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Registered Address 1: " + address);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegisteredAddress2(), address))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Registered Address 2: " + address);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.businessRegisteredAddressSuburbButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessRegisteredAddressSuburbButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        addressLookup();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.registeredAddressPopulateButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.registeredAddressPopulateButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.geographicalTab())))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered the address details");

        return true;
    }

    public boolean propertyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.propertyDetailsTab()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessPropertyAddress1(), address))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Property Address 1: " + address);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessPropertyAddress2(), address))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Property Address 2: " + address);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.businessPropertyAddressSuburbButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessPropertyAddressSuburbButton()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        addressLookup();
        String propertyOwned = getData("Property owned");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.businessPropertyOwned(), propertyOwned))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Property Owned by Business : " + propertyOwned);
        String typeOfOwnership = getData("Type of ownership");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeOfOwnership(), typeOfOwnership))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Type of Ownership : " + typeOfOwnership);
        String standNumber = getData("Stand number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyStandNumber(), standNumber))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Property Stand Number : " + standNumber);
        String bondName = getData("Bond Holder Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.bondHolderName(), bondName))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Bond Holder's Name : " + bondName);
        String bondBankName = getData("Bond Holder Bank Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bondHolderBankName(), bondBankName))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Bond Holder's Bank Name : " + bondBankName);
        String purchaseDate = getData("Purchase Date");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyPurchaseDate(), purchaseDate))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Purchase Date : " + purchaseDate);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.propertyRentAmountLabel()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        String rentAmount = getData("Rent Amount");

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.propertyRentAmount()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyRentAmount(), rentAmount))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Rent Amount : " + rentAmount);
        String bondAmount = getData("Bond Amount");

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.propertyOriginalBondAmount()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyOriginalBondAmount(), bondAmount))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Original Bond Amount : " + bondAmount);
        String bondRepayment = getData("Bond Repayment");

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.propertyBondRepayment()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyBondRepayment(), bondRepayment))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Bond Repayment : " + bondRepayment);
        String bondBalance = getData("Bond Balance");

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.propertyBondBalance()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyBondBalance(), bondBalance))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Bond Balance : " + bondBalance);
        String bondOutstanding = getData("Outstanding");

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.propertyBondOutstanding()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyBondOutstanding(), bondOutstanding))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Flexi Access Bond Total Outstanding : " + bondOutstanding);
        String propertyValue = getData("Property Value");

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.propertyValue()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyValue(), propertyValue))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Current Property Value : " + propertyValue);

        return true;
    }

    public boolean addressLookup()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchSuburb()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.searchSuburb(), getData("Search Suburb")))
        {
            error = "Failed to enter 'suburb' into the search text field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchButton()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchButton()))
        {
            error = "Failed to click on the 'sububrb' search button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectEmployerSuburbDropDown()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectSuburbDropDown(), getData("Select Suburb")))
        {
            error = "Failed to select 'suburb'.";
            return false;
        }
        narrator.stepPassed("Suburb:" + " " + getData("Select Suburb"));

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectCodeDropDown()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectCodeDropDown(), getData("Select Code")))
        {
            error = "Failed to select the 'code' from the list.";
            return false;
        }
        narrator.stepPassed("Code:" + " " + getData("Select Code"));
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectOK()))
        {
            error = "Failed to click the 'OK' button.";
            return false;
        }

        return true;
    }

}
