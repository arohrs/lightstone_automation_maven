/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic MFC Consent",
        createNewBrowserInstance = false
)
public class MfcConsentDetails extends BaseClass
{

    String error = "";
    String consent = getData("Consent");

    public MfcConsentDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!mfcConsentDetails())
        {
            narrator.testFailed("Failed to enter the MFC Consent details -" + error);
        }

        return narrator.finalizeTest("Successfully entered the MFC Consent details");
    }

    public boolean mfcConsentDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatoryMFCConsentTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryInfoSharing(), consent))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryItcConsent(), consent))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMarketingConsent(), consent))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        return true;

    }

}
