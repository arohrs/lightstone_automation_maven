/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Consent",
        createNewBrowserInstance = false
)
public class ConsentDetails extends BaseClass
{
   String error="";
   String consent = getData("Consent");
   public ConsentDetails()
   {
   }
    public TestResult executeTest()
    {
        if(!consentDetails())
        {
            narrator.testFailed("Failed to update the consent details -"+error);
        }
    
        return narrator.finalizeTest("Successfully updated the company's consent details");
    }
    
    public boolean consentDetails()
    {
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.consentTab()))
        {
            error = "Failed to scroll to the 'Consent' container.";
            return false;
        }
        
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankStatement(),consent))
        {
            error = "Failed to select the 'Bank Statement Consent' from the list.";
            return false;
        } 
        narrator.stepPassed("Bank Statement Consent : "+consent);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.infoSharingConsent(),consent))
        {
            error = "Failed to select the 'Information sharing Consent' from the list.";
            return false;
        } 
         narrator.stepPassed("Information sharing Consent : "+consent);  
           if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.applicantConsent(),consent))
        {
            error = "Failed to select the 'Consent to obtain information' from the list.";
            return false;
        }  
        narrator.stepPassed("Consent to obtain information : "+consent);
           
        return true;
    }
    
}
