/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Application details",
        createNewBrowserInstance = false
)
public class E_ApplicationForm extends BaseClass
{

    String error = "";

    public E_ApplicationForm()
    {
    }

    public TestResult executeTest()
    {
        if (!eApplicationform())
        {
            return narrator.testFailed("Failed to enter the company's details on the E-Application form");
        }

        return narrator.finalizeTest("Successfully entered the company's details on the E-Application form");
    }

    public boolean eApplicationform()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.applicationDropDown())))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.juristicApplication())))
        {
            error = "Failed to wait for  the ' Juristic application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.juristicApplication())))
        {
            error = "Failed to click the ' Juristic application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.applicationFormTab())))
        {
            error = "Failed to wait for the 'E-application form' tab to appear.";
            return false;
        }
          narrator.stepPassedWithScreenShot("Successfully clicked on the 'Juristic Application' from the list");
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.companyName())))
        {
            error = "Failed to wait for the 'E-application form' tab to appear.";
            return false;
        }
        String companyName = getData("Company Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyName(), companyName))
        {
            error = "Failed to enter the 'company name'.";
            return false;
        }
        narrator.stepPassed("Company name : " + companyName);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.tradingName())))
        {
            error = "Failed to wait for the 'trading name' to appear.";
            return false;
        }
        String tradingName = getData("Trading Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.tradingName(), tradingName))
        {
            error = "Failed to enter the 'trading name'.";
            return false;
        }
        narrator.stepPassed("Trading name : " + tradingName);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.establishedDate())))
        {
            error = "Failed to wait for the 'established date' to appear.";
            return false;
        }
        String establishedDate = getData("Established date");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.establishedDate()))
        {
            error = "Failed to click the 'established date'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.establishedDate(), establishedDate))
        {
            error = "Failed to enter the 'established date'.";
            return false;
        }
        narrator.stepPassed("Company established date : " + establishedDate);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.companyYearEnd())))
        {
            error = "Failed to wait for the 'Year end' to appear.";
            return false;
        }
        String yearEnd = getData("Year end");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyYearEnd(), yearEnd))
        {
            error = "Failed to select the 'Company year end ' from the list.";
            return false;
        }
        narrator.stepPassed("Company year end : " + yearEnd);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.companyType())))
        {
            error = "Failed to wait for the 'Company type' to appear.";
            return false;
        }
        String companyType = getData("Company Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyType(), companyType))
        {
            error = "Failed to select the 'company type' from the list.";
            return false;
        }
        narrator.stepPassed("Company type : " + companyType);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.businessRegistrationNumber())))
        {
            error = "Failed to wait for the 'trading name' to appear.";
            return false;
        }
        String registrationNumber = getData("Registration Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegistrationNumber(), registrationNumber))
        {
            error = "Failed to enter the 'Registration number' in the text field.";
            return false;
        }
        narrator.stepPassed("Registration number : " + registrationNumber);

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.vatIndicator())))
        {
            error = "Failed to wait for the 'VAT Indicator' to appear.";
            return false;
        }
        String vatIndicator = getData("Vat Indicator");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vatIndicator(), vatIndicator))
        {
            error = "Failed to select  the 'Vat Indicator' from the list .";
            return false;
        }
        narrator.stepPassed("Vat Indicator : " + vatIndicator);
        if (!SeleniumDriverInstance.dismissAlert())
        { //cancells an alert
            error = "Failed to handle an alert.";
            return false;
        }
        if (!vatIndicator.equalsIgnoreCase("No"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.businessVatNumber())))
            {
                error = "Failed to wait for the 'VAT Number' to appear.";
                return false;
            }
            String vatNumber = getData("Vat Number");

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessVatNumber(), vatNumber))
            {
                error = "Failed to enter the 'Vat Number'.";
                return false;
            }
            narrator.stepPassed("Vat Number : " + vatNumber);
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickAbsaBank())))
        {
            error = "Failed to click the 'absa bank' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked ABSA BANK checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickMfc())))
        {
            error = "Failed to click the 'mfc' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked MFC checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickStandardBank())))
        {
            error = "Failed to click the 'standard bank' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked STANDARD BANK checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickWesbank())))
        {
            error = "Failed to click the 'wesbank' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked WESBANK checkBox");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaDealerCodeDropDown(), getData("ABSA")))
        {
            error = "Failed to select 'absa dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected ABSA - Dealer Code: " + " " + getData("ABSA"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mfcDealerCodeDropDown(), getData("MFC")))
        {
            error = "Failed to select the 'mfc dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected MFC - Dealer Code :" + " " + getData("MFC"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.standardBankDealerCodeDropDown(), getData("Standard Bank")))
        {
            error = "Failed to select the 'standard dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected STANDARD BANK - Dealer Code: " + " " + getData("STANDARD BANK"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.wesBankDealerCodeDropDown(), getData("Wesbank")))
        {
            error = "Failed to select the 'wesbank dealer code from the list.";
            return false;
        }
        narrator.stepPassed("Selected WESBANK - Dealer Code: " + " " + getData("WESBANK"));
        if (!SeleniumDriverInstance.waitForOneElementLeftByXpath(LightStonePageObjects.branchDropDown()))
        {
            error = "Failed to wait for the 'branch Code' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.branchDropDown(), getData("Branch Code")))
        {
            error = "Failed to select the 'branch Code' from the list.";
            return false;
        }
        narrator.stepPassed("WESBANK - Branch Code: " + " " + getData("Branch Code"));

        return true;
    }
}
