/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Finance details",
        createNewBrowserInstance = false
)
public class FinanceDetails extends BaseClass
{

    String error = "";
    String financeType = getData("Finance Type");
    String periodContract = getData("Period contract");
    String paymentFrequency = getData("Payment Frequency");
    String paymentMethod = getData("Payment Method");
    String rateType = getData("Rate Type");
    String interestRate = getData("Interest Rate");
    String price = getData("Purchase Price");
    String retailPrice = getData("Retail Price");
    String initiationFees = getData("Initiation Fees");
    String residual = getData("Residual percent");
    String deposit = getData("Cash Deposit");
    String packageNumber = getData("Package Number");
    String schemeCode = getData("Code");
    String dealType = getData("Deal Type");
    String breakMonth = getData("Break Month");
    String statementMethod = getData("Statement Method");
    

    public FinanceDetails()
    {
    }

    public TestResult executeTest()
    {
        if(!financeDetails())
        {
        
            narrator.testFailed("Failed to enter the finance details -"+error);
        }

        return narrator.finalizeTest("Successfully entered the finance details");
    }

    public boolean financeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionFinanceDetailsTab()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionFinanceType(), financeType))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Finance Type : "+financeType);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPeriodofContact(), periodContract))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Period of Contract : "+periodContract);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPaymentFrequency(), paymentFrequency))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Payment Frequency : "+paymentFrequency);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPaymentMethod(), paymentMethod))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Payment Method : "+paymentMethod);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionRateType(), rateType))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Rate Type : "+rateType);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionInterestRate()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionInterestRate(), interestRate))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Interest Rate : "+interestRate);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionPurchasePrice()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionPurchasePrice(), price))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Cash / Purchase price : "+price);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionRetailPrice()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionRetailPrice(), retailPrice))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Retail Price : "+retailPrice);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionInitiationFees(), initiationFees))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Finance Initiation Fees : "+initiationFees);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionBallonPercent(), residual))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Balloon/Residual Percent : "+residual);
            if (!SeleniumDriverInstance.pressKeyWithRobot("tab"))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        String residualAmount = SeleniumDriverInstance.retrieveTextByRobot(LightStonePageObjects.transactionBallonAmount());
        
        narrator.stepPassed("Balloon/Residual Amount : "+residualAmount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionCashDeposit()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionCashDeposit(), deposit))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Cash Deposit Amount : "+deposit);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionTradeInDeposit()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionTradeInDeposit(), deposit))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Trade in Deposit Amount : "+deposit);
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPackageIndicator(), initiationFees))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Package Indicator : "+initiationFees);
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionPackageNumber(), packageNumber))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Package Number : "+packageNumber);
           if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionSchemeCode(), schemeCode))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Scheme Code : "+schemeCode);
           if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionDealType(), dealType))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Deal Type : "+dealType);
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionBreakMonth(), breakMonth))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Take a Break Month : "+breakMonth);
              if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionStatementMethod(), statementMethod))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Preferred Statement Method? : "+statementMethod);
        
        
        
        

        return true;
    }

}
