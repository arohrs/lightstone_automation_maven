/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Submit Application",
        createNewBrowserInstance = false
)
public class SubmitApplication extends BaseClass
{

    String error = "";

    public SubmitApplication()
    {
    }

    public TestResult executeTest()
    {

        if (!submitApplication())
        {
            return narrator.testFailed("Failed to Submit Application -" + error);
        }
        return narrator.finalizeTest("Successfully Submitted the application.");
    }

    public boolean submitApplication()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.submitApplicationButton()))
        {
            error = "Failed to wait for the 'submit application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
        {
            error = "Failed to wait for the 'submit application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
        {
            error = "Failed to wait for the 'submit application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath((LightStonePageObjects.applicationFormTab())))
        {
            error = "Failed to wait for the 'E-application form' tab to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Sumbmitted the application");
        SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());
         
       

        return true;
    }

}
