/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic F&I details",
        createNewBrowserInstance = false
)
public class FIDealerDetails extends BaseClass
{
    String error="";
    String officail =getData("Official");
    String notification = getData("Notification");
    
    public FIDealerDetails()
    {
    }
     public TestResult executeTest()
     {
         if(!FIdetails())
         {
             narrator.testFailed("Failed to enter the F&I details -"+error);
         }
     
     
         return narrator.finalizeTest("Successfully entered the F&I details");
     }
    
    public boolean FIdetails()
    {   
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.fIDealerTab()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.registeredOffical(),officail))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
         narrator.stepPassed("Accredited/Registered Official : "+officail);
           if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.notificationMethod(),notification))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
         narrator.stepPassed("Notification Method : "+notification); 
    
        return true;
    }
    
    
}
