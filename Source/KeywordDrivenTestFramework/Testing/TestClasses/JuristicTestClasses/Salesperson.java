/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Salesperson",
        createNewBrowserInstance = false
)
public class Salesperson extends BaseClass
{
    String error ="";
    public Salesperson()
    {
    }
    
    public TestResult executeTest()
    {
        if(!salesPerson())
        {
        
            narrator.testFailed("Failed to enter the salesperson details -"+error);
        }
    
    
        return narrator.finalizeTest("Successfully entered the salesperson details");
    }
    public boolean salesPerson ()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.salesPersonTab()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        String person = getData("Salesperson");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.salesPerson(),person))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Salesperson : "+person);
         if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewSalesPersonButton()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
         narrator.stepPassed("Clicked on the 'Add New Salesperon' button");
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.salesPersonFirstname()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
         
         String firstName = getData("Name");
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.salesPersonFirstname(),firstName))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Firstname : "+firstName); 
        String surName = getData("Surname");
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.salesPersonSurname(),surName))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Surname : "+surName);
        String idType = getData("ID type");
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.salesPersonIdType(),idType))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("ID Type : "+idType); 
        String idNumber = getData("ID number");
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.salesPersonIdNumber(),idNumber))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("ID Number : "+idNumber); 
        String title = getData("Title");
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.salesPersonTitle(),title))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Title : "+title); 
         if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addSalesPerson()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully entered the new Salesperson details");
          if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        } 
    
        return true;
    }
    
}
