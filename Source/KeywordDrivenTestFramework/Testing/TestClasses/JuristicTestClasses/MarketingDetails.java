/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Marketing details",
        createNewBrowserInstance = false
)
public class MarketingDetails extends BaseClass
{

    String error = "";
    String answer = getData("Answer");
    String marketingMethod = getData("Method");

    public TestResult executeTest()
    {
        if (!marketingDetails())
        {
            narrator.testFailed("Failed to enter the marketing details -"+error);
        }

        return narrator.finalizeTest("Successfully entered the marketing details");

    }

    public boolean marketingDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.marketingTab()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.telemarketingCampaign(),answer))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Include in Telemarketing Campaign : "+answer);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.includeMarketingList(),answer))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Include in Marketing List : "+answer);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.marketingMethod(),marketingMethod))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Marketing Communication Method : "+marketingMethod);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.massDistribution(),answer))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Include in Mass Distribution : "+answer);  
           if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.throughReconnect(),answer))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Through Reconnect : "+answer);    
             if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.comprehensiveInsurance(),answer))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
          narrator.stepPassed("Comprehensive Insurance Quote : "+answer);     
          
        
        return true;

    }
}
