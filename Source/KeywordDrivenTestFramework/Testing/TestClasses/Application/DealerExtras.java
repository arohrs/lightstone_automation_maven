/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Dealer Extras",
        createNewBrowserInstance = false
         
)

public class DealerExtras extends BaseClass
{

    String error = "";
    int numberOfDealerExtras = 3;

    public DealerExtras()
    {
    }

    public TestResult executeTest()
    {
        if (!dealerExtrasDetails())
        {
            return narrator.testFailed("Failed to enter customers details on the 'Dealer Extras' Form - " + error);
        }
        return narrator.finalizeTest("Successfully entered customers details on the 'Dealer Extras' Form.");
    }

    public boolean dealerExtrasDetails()
    {

        /*Entering dealer extras details*/
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.dealerExtrasTab()))
        {
            error = "Failed to wait for the 'dealer extras' table to appear.";
            return false;
        }
        for (int i = 0; i < numberOfDealerExtras; i++)
        {
            if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.addNewExtra())))
            {
                error = "Failed to click 'add new extra' to add more dealer extras.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.description1DropDown(), getData("Description1")))
        {
            error = "Failed to select 'description' from the list.";
            return false;
        }
        String otherExtra = getData("Amount1");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.amount1(), otherExtra))
        {
            error = "Failed to enter the 'amount' into the text field.";
            return false;
        }
        narrator.stepPassed(getData("Description1") + " : " + " " + otherExtra);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.description2DropDown(), getData("Description2")))
        {
            error = "Failed to select 'description' from the list.";
            return false;
        }
        String licenceFee = getData("Amount2");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.amount2(),licenceFee))
        {
            error = "Failed to enter the 'amount' into the text field.";
            return false;
        }
        narrator.stepPassed(getData("Description2") + " : " + " " + licenceFee);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.description3DrowDown(), getData("Description3")))
        {
            error = "Failed to select 'description' from the list.";
            return false;
        }
        String radio = getData("Amount3");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.amount3(),radio))
        {
            error = "Failed to enter the 'amount' into the text field.";
            return false;
        }
        narrator.stepPassed(getData("Description3") + " : " + " " + radio);

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.total()))
        {
            error = "Failed to click on 'total' text field.";
            return false;
        }
        String total = SeleniumDriverInstance.retrieveTextByRobot(LightStonePageObjects.total());
        double amount1 = Double.parseDouble(otherExtra);
        double amount2 = Double.parseDouble(licenceFee);
        double amount3 = Double.parseDouble(radio);
        double expectedTotal = amount1 + amount2 + amount3;
        double currentTotal = Double.parseDouble(total);
        
        return validateTotalExtras(currentTotal,expectedTotal);
    }
    
    public boolean validateTotalExtras(double currentValue, double expectedValue)
    {
        if (currentValue != expectedValue)
        {
            error = "The total value is not the correct";
            return false;
        }
        narrator.stepPassed("Successfully validated the Total extras " + "<b>Current value<b/>: " + String.format("%.2f", currentValue) + " <b>The expected</b>: " + String.format("%.2f", expectedValue));
        
        return true;
    }
}
