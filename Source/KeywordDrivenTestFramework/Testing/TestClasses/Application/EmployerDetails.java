/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Employer Details",
        createNewBrowserInstance = false
)

public class EmployerDetails extends BaseClass
{

    String error = "";
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;

    public EmployerDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!employerDetails())
        {
            return narrator.testFailed("Failed to enter customers 'employer details' on the Form - " + error);
        }
        if (!isAfterMarket)
        {
            if (!employerAddressInformation())
            {
                return narrator.testFailed("Failed to enter customers 'employer address information' on the Form - " + error);
            }
        }
        return narrator.finalizeTest("Successfully entered customers 'employer details' on the Form.");
    }

    public boolean employerDetails()
    {
        /*Entering customers employer details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.employerDetailsTab()))
        {
            error = "Failed to scroll to 'employer name' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.employerDetailsTab()))
        {
            error = "Failed to wait for the 'employer name' text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerName(), getData("Employer Name")))
        {
            error = "Failed to enter 'employer name' on the text field.";
            return false;
        }
        narrator.stepPassed("Employer Name:" + " " + getData("Employer Name"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.occupationDropDown(), getData("Occupation")))
        {
            error = "Failed to select 'occupation' from the list.";
            return false;
        }
        narrator.stepPassed("Occupation :" + " " + getData("Occupation"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerIndustryTypeDropDown(), getData("Employer Industry Type")))
        {
            error = "Failed to select 'employer industry type' from the list.";
            return false;
        }
        narrator.stepPassed("Employer Industry Type :" + " " + getData("Employer Industry Type"));
        checkIfAvailable();
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerType(), getData("Employment Type")))
        {
            error = "Failed to select 'employer type'from the list.";
            return false;
        }
        narrator.stepPassed("Employment Type :" + " " + getData("Employment Type"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerLevel(), getData("Employment Level")))
        {
            error = "Failed to select 'employer level'.";
            return false;
        }
        narrator.stepPassed("Employer Level :" + " " + getData("Employment Level"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerClientType(), getData("Employer Client Type")))
        {
            error = "Failed to select 'employer client type' from the list.";
            return false;
        }
        narrator.stepPassed("Employer Client Type :" + " " + getData("Employer Client Type"));
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.employerDetailsTab()))
        {
            error = "Failed to scroll to 'employer name' text field.";
            return false;
        }
        return true;
    }

    public boolean employerAddressInformation()
    {

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerAddress(), getData("Employer Address1")))
        {
            error = "Failed to enter 'employer address' into the text field.";
            return false;
        }
        narrator.stepPassed("Employer Address :" + " " + getData("Employer Address1"));
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.employerSuburbButton()))
        {
            error = "Failed to click 'suburb' look up button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerSuburbSearch(), getData("Search Suburb2")))
        {
            error = "Failed to enter 'suburb' into the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.employerSearchButton()))
        {
            error = "Failed to click 'suburb' search button.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectEmployerSuburbDropDown(), getData("Employer Suburb")))
        {
            error = "Failed to select 'suburb'from the list.";
            return false;
        }
        narrator.stepPassed("Employer Suburb:" + " " + getData("Employer Suburb"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerSuburbCodeDropDown(), getData("Postal Code")))
        {
            error = "Failed to select 'postal code'from the list.";
            return false;
        }
        narrator.stepPassed("Employer Postal Code:" + " " + getData("Postal Code"));
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectOK()))
        {
            error = "Failed to click the 'OK' button.";
            return false;
        }

        String periodYear = getData("Period(Year)");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPeriodYear(), periodYear))
        {
            error = "Failed to enter 'period(year)' into the text field.";
            return false;
        }

        String periodMonth = getData("Period(Month)");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPeriodMonth(), periodMonth))
        {
            error = "Failed to enter 'period(month)'from the list.";
            return false;
        }

        narrator.stepPassed("Period Employeed YY: " + periodYear);
        narrator.stepPassed("Period Employeed MM: " + periodMonth);
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.employerDetailsTab()))
        {
            error = "Failed to scroll to 'employer name' text field.";
            return false;
        }
        return true;
    }

    public boolean checkIfAvailable()
    {
        List<String> employementTypeList_TestPack = Arrays.asList(testData.getData("Employement Type List").split(","));
        List<String> employementList_WebPage = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.employerTypeList());

        for (String currentEmploymentType_TestPack : employementTypeList_TestPack)
        {
            if (!employementList_WebPage.stream().anyMatch(s -> currentEmploymentType_TestPack.equalsIgnoreCase(s)))
            {
                error = "Employement Type not part of the list";
                return false;
            }
        }

        for (int i = 1; i < employementList_WebPage.size(); i++)
        {

            String employement_type = employementList_WebPage.get(i);

            if (employement_type.equalsIgnoreCase("Permanent"))
            {

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerType(), employement_type))
                {
                    error = "";
                    return false;
                }
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerLevel(), getData("Employment Level")))
                {
                    error = "";
                    return false;
                }

            } else if (employement_type.equalsIgnoreCase("Contractual"))
            {

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerType(), employement_type))
                {
                    error = "";
                    return false;
                }
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerLevel(), getData("Employment Level")))
                {
                    error = "";
                    return false;
                }

            } else if (employement_type.equalsIgnoreCase("Part time"))
            {

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerType(), employement_type))
                {
                    error = "";
                    return false;
                }
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerLevel(), getData("Employment Level")))
                {
                    error = "";
                    return false;
                }

            } 
            else{
                
            if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerType(),employement_type))
            {
                error = "";
                return false;
            }
            }

        }
       

        return true;
    }
}
