/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Submit_Application",
        createNewBrowserInstance = false
)

public class SubmitApplication extends BaseClass
{

    String error = "";

    public SubmitApplication()
    {
    }

    public TestResult executeTest()
    {

        if (!submitApplication())
        {
            return narrator.testFailed("Failed to Submit Application -" + error);
        }
        return narrator.finalizeTest("Successfully Submitted Application.");
    }

    public boolean submitApplication()
    {
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
        {
            error = "Failed to wait for the 'submit application' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.submitApplication()))
        {
            error = "Failed to scroll to 'submit application' button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.alertHandlerUsingJavaScript())
        {
            error = "Failed to handle the alert window";
            return false;
        }        
        
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
        {
            error = "Failed to click the 'submit application' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.success()))
        {
            error = "Failed to wait for the 'OK' button to confirm submission.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Submit Application' button ");

        if (!SeleniumDriverInstance.updateElementAttributeByXpath(LightStonePageObjects.maskSweetOverlay(), "style", "display: none;"))
        {
            error = "Failed to update Mask Value - none";
            return false;
        }

        String applicationSuccessMessage = SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.applicationSuccessMessage());
        
        narrator.stepPassed("Application Success Message : "+applicationSuccessMessage);
        
        if (!SeleniumDriverInstance.updateElementAttributeByXpath(LightStonePageObjects.maskSweetOverlay(), "style", "display: block;"))
        {
            error = "Failed to update Mask Value - block";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.success()))
        {
            error = "Failed to wait the 'OK' button to confirm submission.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.success()))
        {
            error = "Failed to click the 'OK' button to confirm submission.";
            return false;
        }
          if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.applicationFormTab())))
        {
            error = "Failed to wait for the 'application form tab' to appear";
            return false;
        }
     
        SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.applicationDropDown())))
        {
            error = "Failed to wait  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Successfully switched back to the Inbox  Tab");

        return true;
    }
}
