/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "SA TAXI Insurance",
        createNewBrowserInstance = false
         
)
public class SA_Taxi_InsuranceVAPS extends BaseClass
{

    String error = "";

    public SA_Taxi_InsuranceVAPS()
    {
    }

    public TestResult executeTest()
    {
        if (!saTaxiInsurance())
        {

            return narrator.testFailed("Failed to enter the SA TAXI Insurance's details " + error);
        }
        return narrator.finalizeTest("Successfully entered SA TAXI Insurance's details.");
    }

    public boolean saTaxiInsurance()
    {
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.saTaxiInsuranceForm()))
        {
            error = "Failed to scroll to 'SA TAXI Insurance VAPS 'form.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.insuranceCompany()))
        {
            error = "Failed to wait for 'insurance Company' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.insuranceCompany(), getData("Insurance company")))
        {
            error = "Failed to select the 'insurance Company'in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.insuranceBroker()))
        {
            error = "Failed to wait for 'insurance Broker' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.insuranceBroker(), getData("Insurance broker")))
        {
            error = "Failed to select the 'insurance Broker'in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.insuranceBrokerCode()))
        {
            error = "Failed to wait for 'insurance Broker code' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.insuranceBrokerCode(), getData("Insurance broker code")))
        {
            error = "Failed to enter the 'insurance Broker code'in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.insuranceAmount()))
        {
            error = "Failed to wait for 'insurance Amount' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.insuranceAmount(), getData("Insurance amount")))
        {
            error = "Failed to enter the 'insurance Amount'in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.insuranceAmount()))
        {
            error = "Failed to wait for 'insurance cover' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.insuranceCover()))
        {
            error = "Failed to click the 'insurance cover'in the text field.";
            return false;
        }

        return true;
    }

}
