/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Company details",
        createNewBrowserInstance = false
)
public class CompanyDetails extends BaseClass
{

    String error = "";

    public CompanyDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!companyDetails())
        {
            return narrator.testFailed("Failed to update Company's details container. - " + error);
        }

        return narrator.finalizeTest("Successfully updated the Company's details.");
    }

    public boolean companyDetails()
    {
        //Company details
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.companyDetailsHeader()))
        {
            error = "Failed to wait for 'Company Details' header";
            return false;

        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyTransaction()))
        {
            error = "Failed to wait for the'Company transaction' checkBox to appear ";
            return false;

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.companyTransaction()))
        {
            error = "Failed to click the 'Company transaction' checkBox";
            return false;

        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyName()))
        {
            error = "Failed to wait for the'Company name' field to appear ";
            return false;

        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyName(), getData("Company Name")))
        {
            error = "Failed to enter the 'Company name' ";
            return false;

        }
        narrator.stepPassed("Company Name: " + getData("Company Name"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vatNumber()))
        {
            error = "Failed to wait the 'VAT Number' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vatNumber(), getData("VAT Number")))
        {
            error = "Failed to enter the 'VAT Number'";
            return false;

        }
        narrator.stepPassed("VAT Number: " + getData("VAT Number"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.businessRegistrationNumber()))
        {
            error = "Failed to wait for 'Registration Number' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegistrationNumber(), getData("Registration Number")))
        {
            error = "Failed to enter the 'Registration Number'";
            return false;

        }
        narrator.stepPassed("Registration Number: " + getData("Registration Number"));

        return true;
    }

}
