/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Wesbank Vaps",
        createNewBrowserInstance = false
        
)
public class WesbankVAPS extends BaseClass
{
    String error = "";
    public WesbankVAPS()
    {}
    
     public TestResult executeTest()
     {
          if (!wesbank_VAPS())
        {
            return narrator.testFailed("Failed to enter the Wesbank VAPS detail. - " + error);
        }
     
     
         return narrator.finalizeTest("Successfully updated the Wesbank VAPS detail.");
     }
     
     
     public boolean wesbank_VAPS()
     {
         if(!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.wesbankVaps()))
         {
              error = "Failed to Scroll to 'Wesbank VAPS' Header";
              return false;
         }
         if(!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.addNewWesbankVaps()))
         {
             error = "Failed to wait for 'add new vap' button";
             return false;
             
         }
          if(!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewWesbankVaps()))
         {
             error = "Failed to click 'add new vap' button";
             return false;
             
         }
          if(!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.valueAddedModal()))
         {
             error = "Failed to wait for 'add new vap' modal";
             return false;
             
         }
           if(!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.wesbankVapOwner()))
         {
             error = "Failed to wait for 'Vap Owner' to appear";
             return false;
             
         }
            if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.wesbankVapOwner(),getData("Owner")))
         {
             error = "Failed to select for 'Vap Owner' from the list";
             return false;
             
         }
         narrator.stepPassed("Owner: "+getData("Owner")); 
             if(!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.wesbankVapCategory()))
         {
             error = "Failed to wait for 'Vap category' to appear";
             return false;
             
         }
            if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.wesbankVapCategory(),getData("Category")))
         {
             error = "Failed to select the 'Vap category' from the list";
             return false;
             
         }
         narrator.stepPassed("Category : "+getData("Category"));  
             if(!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.wesbankVapAmount()))
         {
             error = "Failed to wait for 'Vap amount' to appear";
             return false;
             
         }
            if(!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.wesbankVapAmount(),getData("Amount")))
         {
             error = "Failed to enter for 'Vap amount' in the field";
             return false;
             
         }
         narrator.stepPassed("Amount : "+getData("Amount"));  
             if(!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vapOkButton()))
         {
             error = "Failed to wait for 'vap ok ' button";
             return false;
             
         }
              if(!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.vapOkButton()))
         {
             error = "Failed to click  'vap ok' button";
             return false;
             
         }    
         
         return true;
     }
    
}
