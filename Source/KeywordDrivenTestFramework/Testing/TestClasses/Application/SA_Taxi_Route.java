/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "SA TAXI Route",
        createNewBrowserInstance = false
)
public class SA_Taxi_Route extends BaseClass
{
    
    String error = "";

    public SA_Taxi_Route()
    {

    }

    public TestResult executeTest()
    {
        if (!saTaxiRoute())
        {

            return narrator.testFailed("Failed to enter the SA TAXI Route's details " + error);
        }
        return narrator.finalizeTest("Successfully entered SA TAXI Route's details.");
    }

    public boolean saTaxiRoute()
    {
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.saTaxiRouteForm()))
        {
            error = "Failed to scroll to 'SA TAXI ROUTE 'form.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.taxiAssociation()))
        {
            error = "Failed to wait for 'taxi association' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.taxiAssociation(), getData("Taxi association")))
        {
            error = "Failed to enter the 'taxi association'in the text field.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.taxiAssociationName(getData("Taxi association"))))
        {
            error ="Failed";
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.routeFrom()))
        {
            error = "Failed to wait for'Route from/to date' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.routeFrom(), getData("Route from/to")))
        {
            error = "Failed to enter the 'Route from/to'in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.driverWages()))
        {
            error = "Failed to wait for the 'Driver wages' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.driverWages(), getData("Driver wages")))
        {
            error = "Failed to enter the 'Driver wages'in the text field.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.numberOfPassengers()))
        {
            error = "Failed to wait for the 'number of passenger'  text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.numberOfPassengers(), getData("Passengers/trip")))
        {
            error = "Failed to enter the 'number of passenger'in the text field.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.passengerFare()))
        {
            error = "Failed to wait for 'Fare per passengers' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.passengerFare(), getData("Taxi Fare")))
        {
            error = "Failed to enter the 'Fare per passengers'in the text field.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.daysWorked()))
        {
            error = "Failed to wait for 'commencement date' in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.daysWorked(), getData("Days worked")))
        {
            error = "Failed to enter the 'Instalment date'in the text field.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.taxiArea()))
        {
            error = "Failed to enter the 'commencement date' in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.taxiArea(), getData("Area")))
        {
            error = "Failed to enter the 'Instalment date'in the text field.";
            return false;
        }
          if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tripKM()))
        {
            error = "Failed to enter the 'commencement date' in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.tripKM(), getData("KM/trip")))
        {
            error = "Failed to enter the 'Instalment date'in the text field.";
            return false;
        }
          if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tripsPerDay()))
        {
            error = "Failed to enter the 'commencement date' in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.tripsPerDay(), getData("Trip/day")))
        {
            error = "Failed to enter the 'Instalment date'in the text field.";
            return false;
        }
           if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.taxiNumberOfSeats()))
        {
            error = "Failed to enter the 'commencement date' in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.taxiNumberOfSeats(), getData("Number of seat")))
        {
            error = "Failed to enter the 'Instalment date'in the text field.";
            return false;
        }
        

        return true;
    }
}
