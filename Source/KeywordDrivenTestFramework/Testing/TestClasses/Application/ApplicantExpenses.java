/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Applicant Expenses",
        createNewBrowserInstance = false
         
)

public class ApplicantExpenses extends BaseClass
{

    String error = "";

    public ApplicantExpenses()
    {
    }

    public TestResult executeTest()
    {
        if (!applicantExpenses())
        {
            return narrator.testFailed("Failed to enter 'applicant expenses' on the Form." + " " + error);
        }
        return narrator.finalizeTest("Successfully entered 'applicant expenses' on the Form.");
    }

    public boolean applicantExpenses()
    {
        /*Entering customers expenses*/
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicantExpensesTab()))
        {
            error = "Failed to wait for the 'rent' text field to appear.";
            return false;
        }
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.rent())) {
            error = "Failed to clear Rent field";
        }
         String rent = getData("Rent");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.rent(),rent))
        {
            error = "Failed to enter customers 'rent' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Rent:" + " " + rent);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.waterElectricity())) {
            error = "Failed to clear Rent field";
        }
         String waterElectricity = getData("Amount");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.waterElectricity(),waterElectricity))
        {
            error = "Failed to enter customers 'water electricity' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Rates,Water & Electricity:" + " " + waterElectricity);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.vehicleInstallment())) {
            error = "Failed to clear Rent field";
        }
         String vehicleInstallment = getData("Amount");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.waterElectricity(),vehicleInstallment))
        {
            error = "Failed to enter customers 'vehicle installment' amount on the text field.";
            return false;
        }
        narrator.stepPassed("vehicle Installment:" + " " + vehicleInstallment);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.loanRepayment())) {
            error = "Failed to clear Rent field";
        }
         String loanRepayment = getData("Amount");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.loanRepayment(),loanRepayment))
        {
            error = "Failed to enter customers 'loan repayment' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Personal Loan Repayment:" + " " + loanRepayment);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.furnitureAccount())) {
            error = "Failed to clear Rent field";
        }
         String furnitureAccount = getData("Amount");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.loanRepayment(),furnitureAccount))
        {
            error = "Failed to enter customers 'furniture Account' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Furniture Account:" + " " + furnitureAccount);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.overDraftRepayment())) {
            error = "Failed to clear Rent field";
        }
         String overDraftRepayment = getData("Amount");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.overDraftRepayment(),overDraftRepayment))
        {
            error = "Failed to enter customers 'furniture Account' amount on the text field.";
            return false;
        }
        narrator.stepPassed("over draft Repayment:" + " " + overDraftRepayment);
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.telephonePayments()))
        {
            error = "Failed to scroll to the 'applicant income' details form.";
            return false;
        }
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.telephonePayments())) {
            error = "Failed to clear Telephone Payments field";
        }
         String telephonePayment = getData("Telephone Payment");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.telephonePayments(), telephonePayment))
        {
            error = "Failed to enter customers 'telephone payment' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Telephone Payment :" + " " + telephonePayment);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.clothingAccounts())) {
            error = "Failed to clear clothing account field";
        }
          String clothingAccount = getData("Clothing Accounts");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clothingAccounts(),clothingAccount))
        {
            error = "Failed to enter customers 'clothing account' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Clothing Accounts :" + " " + clothingAccount);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.educationCost())) {
            error = "Failed to clear Education costs field";
        }
          String educationCost = getData("Education Costs");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.educationCost(), educationCost))
        {
            error = "Failed to enter customers 'education costs' on the text field.";
            return false;
        }

        narrator.stepPassed("Education Costs :" + " " + educationCost);

        narrator.stepPassedWithScreenShot("Applicant Expenses Data Entered so far");

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.foodAndEntertainmentLabel()))
        {
            error = "Failed to wait for the total expenses text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.customerTotalExpenses()))
        {
            error = "Failed to click on total expenses text field.";
            return false;
        }
        
        String total = SeleniumDriverInstance.retrieveTextByRobot(LightStonePageObjects.customerTotalExpenses());
        double amount1 = Double.parseDouble(rent);
        double amount2 = Double.parseDouble(telephonePayment);
        double amount3 = Double.parseDouble(clothingAccount);
        double amount4 = Double.parseDouble(educationCost);
        double amount5 = Double.parseDouble(waterElectricity);
        double amount6 = Double.parseDouble(furnitureAccount);
        double amount7 = Double.parseDouble(loanRepayment);
        double amount8 = Double.parseDouble(overDraftRepayment);
        double amount9 = Double.parseDouble(vehicleInstallment);
        double expectedTotal = amount1 + amount2 + amount3 + amount4 + amount5 + amount6 + amount7 + amount8 + amount9;
        double currentTotal = Double.parseDouble(total);
        
        validateTotalExpenses(currentTotal,expectedTotal);
        
//        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicantExpensesTab()))
//        {
//            error = "Failed to wait for the 'rent' text field to appear.";
//            return false;
//        }
        return true;
    }
    
    
    public boolean validateTotalExpenses(double currentValue, double expectedValue)
    {
        if (currentValue != expectedValue)
        {
            error = "The total value is not the correct";
            return false;
        }
        narrator.stepPassed("Successfully validated the Total expenses " + "<b>Current value<b/>: " + String.format("%.2f", currentValue) + " <b>The expected</b>: " + String.format("%.2f", expectedValue));
        
        return true;
    }
}
