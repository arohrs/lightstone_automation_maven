/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Information",
        createNewBrowserInstance = false
        
)

public class Information extends BaseClass
{

    String error = "";

    public Information()
    {
    }

    public TestResult executeTest()
    {

        if (!applicationDetails())
        {
            return narrator.testFailed("F & I Information Details-" + error);
        }
        return narrator.finalizeTest("Successfully entered F & I Information on the Form.");
    }

    public boolean applicationDetails()
    {
        /*updating Ff & i information details*/
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.proofOfResidence()))
        {
            error = "Failed to scroll to the Proof of Residence field.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.registeredOfficialName(), getData("Official Name")))
        {
            error = "Failed to select the official name from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.alpheraFI()))
        {
            error = "Failed to wait for the 'submit application' button.";
            return false;
        }
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.alpheraFI(), getData("Aplhera Outlet")))
        {
            error = "Failed to select the 'Alphera outlet ID' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
        {
            error = "Failed to wait for the 'submit application' button.";
            return false;
        }

        narrator.stepPassed("Registered Officials Name : " + getData("Official Name"));

        return true;
    }
}
