/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Payment History",
        createNewBrowserInstance = false
         
)

public class PaymentHistory extends BaseClass
{

    String error = "";

    public PaymentHistory()
    {
    }

    public TestResult executeTest()
    {
        if (!paymentHistory())
        {
            return narrator.testFailed("Failed to enter customers 'residential details' on the Form ." + error);
        }
        return narrator.finalizeTest("Successfully entered customers 'residential details' on the Form.");
    }

    public boolean paymentHistory()
    {
        /*Entering customers payment history*/
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.paymentHistoryTab()))
        {
            error = "Failed to wait for the debt review text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.debtReviewDropDown(), getData("Debt Review")))
        {
            error = "Failed to select 'Yes/No' to indicate if or not under debt review.";
            return false;
        }
        narrator.stepPassed("Under Debt Review:" + " " + getData("Debt Review"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.debtCounsellingDropDown(), getData("Debt Counselling")))
        {
            error = "Failed to select 'Yes/No' to indicate if or not under debt counseling.";
            return false;
        }
        narrator.stepPassed("Under Debt Counselling:" + " " + getData("Debt Counselling"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.curatorBonusDropDown(), getData("Curator Bonus")))
        {
            error = "Failed to select 'Yes/No' to indicate if or not receives curator bonus.";
            return false;
        }
        narrator.stepPassed("Received Curator Bonus: " + getData("Curator Bonus"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.adminOrderDropDown(), getData("Admin Order")))
        {
            error = "Failed to select 'Yes/No' to indicate if or not under admin order.";
            return false;
        }
        narrator.stepPassed("Under Adminstration Order:" + " " + getData("Admin Order"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.judgementDropDown(), getData("Judgement")))
        {
            error = "Failed to select 'Yes/No' to indicate if or not under judgement.";
            return false;
        }
        narrator.stepPassed("Judgement Against You:" + " " + getData("Judgement"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.creditBureauDropDown(), getData("Credit Bureau")))
        {
            error = "Failed to select 'Yes/No' to indicate if or not under credit bureau.";
            return false;
        }
        narrator.stepPassed("Dispute with a Credit Bureau:" + " " + getData("Credit Bureau"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sequastrationOrderDropDown(), getData("Sequestration Order")))
        {
            error = "Failed to select 'Yes/No' to indicate if or not under sequestration order.";
            return false;
        }
        narrator.stepPassed("Sequestration Order:" + " " + getData("Sequestration Order"));
        return true;
    }
}
