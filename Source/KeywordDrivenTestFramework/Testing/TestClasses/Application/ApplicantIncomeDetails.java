/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Income Details",
        createNewBrowserInstance = false
)

public class ApplicantIncomeDetails extends BaseClass
{

    String error = "";

    public ApplicantIncomeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!incomeDetails())
        {
            return narrator.testFailed("Failed to enter customers 'income details' on the Form." + " " + error);
        }
        return narrator.finalizeTest("Successfully entered customers 'income details' on the Form.");
    }

    public boolean incomeDetails()
    {
        /*Entering customers income details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.incomeDetailsTab()))
        {
            error = "Failed to scroll to the 'applicant income' details form.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.incomeDetailsTab()))
        {
            error = "Failed to wait for the 'gross renumeration' text field to appear.";
            return false;
        }
        String sourceIncome = getData("Source of Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sourceOfIncome(), sourceIncome))
        {
            error = "Failed to wait for the 'gross renumeration' text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.grossRenumeration()))
        {
            error = "Failed to clear Gross in field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.grossRenumeration(), getData("Gross")))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Gross Renumeration:" + " " + getData("Gross"));
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.carAllowance()))
        {
            error = "Failed to clear Gross in field";
        }
        String carAllowance = getData("Car allowance");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.carAllowance(), carAllowance))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Car Allowance: " + carAllowance);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.overTime()))
        {
            error = "Failed to clear Gross in field";
        }
        String overTime = getData("Over Time");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.overTime(), overTime))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Over Time: " + overTime);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.netIncome()))
        {
            error = "Failed to clear Net Income in field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.netIncome(), getData("Net")))
        {
            error = "Failed to enter customers 'net income' on the text field.";
            return false;
        }
        narrator.stepPassed("Net Income :" + " " + getData("Net"));
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.rentalIncome()))
        {
            error = "Failed to clear Gross in field";
        }
        String rentalIncome = getData("Rental Income");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.rentalIncome(), rentalIncome))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Rental income: " + rentalIncome);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.maintenanceIncome()))
        {
            error = "Failed to clear Gross in field";
        }
        String maintenanceIncome = getData("Maintenance Income");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.maintenanceIncome(), maintenanceIncome))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Maintenance Income: " + maintenanceIncome);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherIncome()))
        {
            error = "Failed to clear Gross in field";
        }
        String otherIncome = getData("Other");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherIncome(), otherIncome))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Other Income: " + otherIncome);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.tmI()))
        {
            error = "Failed to click on 'total expenses' text field.";
            return false;
        }
        String otherDescription = getData("Description");
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherIncomeDescription(), otherDescription))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Other income description: " +otherDescription);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseGrossRenumeration()))
        {
            error = "Failed to clear Gross in field";
        }
        String spouseGrossIncome = getData("Gross");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseGrossRenumeration(), spouseGrossIncome))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Rental income: " + spouseGrossIncome);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseCarAllowance()))
        {
            error = "Failed to clear Gross in field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseCarAllowance(), carAllowance))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Spouse car allowance: " + carAllowance);
        String spouseNetIncome= getData("Net");
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseNetTakeHome()))
        {
            error = "Failed to clear Gross in field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseNetTakeHome(), spouseNetIncome))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Spouse Net take-home Income: " + spouseNetIncome);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseRentalIncome()))
        {
            error = "Failed to clear Gross in field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseRentalIncome(), rentalIncome))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Spouse rental income: " + rentalIncome);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseMaintenance()))
        {
            error = "Failed to clear Gross in field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseMaintenance(), maintenanceIncome))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Spouse maintenance Income: " + maintenanceIncome);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseOtherIncome()))
        {
            error = "Failed to clear Gross in field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseOtherIncome(), otherIncome))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Spouse other Income: " + otherIncome);
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.incomeDetailsTab()))
        {
            error = "Failed to scroll to the 'applicant income' details form.";
            return false;
        }

        return true;
    }
}
