/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Banking Details",
        createNewBrowserInstance = false
)

public class BankingDetails extends BaseClass
{

    String error = "";
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;
    boolean isSaTaxi = (!testData.getData("isSaTaxi").isEmpty()) ? Boolean.parseBoolean(testData.getData("isSaTaxi").toLowerCase()) : false;

    public BankingDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!paymentHistory())
        {
            return narrator.testFailed("Failed to enter customers 'banking details' on the Form - " + error);
        }
        if (!isAfterMarket)
        {
            if (!bankDetails())
            {
                return narrator.testFailed("Failed to enter customers 'banking details' on the Form - " + error);
            }

        }
        return narrator.finalizeTest("Successfully entered customers 'banking details' on the Form.");
    }

    public boolean paymentHistory()
    {
        /*Entering customers banking details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.bankingDetailsTab()))
        {
            error = "Failed to scroll to the 'banking details' table.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.bankingDetailsTab()))
        {
            error = "Failed to wait for the 'account holder' text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountHolder(), getData("Account Holder")))
        {
            error = "Failed to enter 'account holder' details into the text field.";
            return false;
        }
        narrator.stepPassed("Account Holder  :" + " " + getData("Account Holder"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankNameDropDown(), getData("Bank Name")))
        {
            error = "Failed to select the 'bank name' from the list.";
            return false;
        }
        narrator.stepPassed("Bank Name :" + " " + getData("Bank Name"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankNameDropDown(), getData("Bank Name")))
        {
            error = "Failed to select the 'bank name' from the list.";
            return false;
        }

        narrator.stepPassed("Bank Branch  :" + " " + getData("Bank Branch"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.validAccountNumber(), getData("Valid Account")))
        {
            error = "Failed to select 'Yes/No' to confirm if account is valid.";
            return false;
        }
        narrator.stepPassed("Valid Account  :" + " " + getData("Valid Account"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountNumber(), getData("Account Number")))
        {
            error = "Failed to enter an 'account number' into the text field.";
            return false;
        }
        narrator.stepPassed(" Account Number :" + " " + getData("Account Number"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.accountType(), getData("Account Type")))
        {
            error = "Failed to select 'account type' from the list.";
            return false;
        }
        narrator.stepPassed("Account Type :" + " " + getData("Account Type"));

        narrator.stepPassed(" Branch Code:" + " " + getData("Branch Code"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.paymentMethod(), getData("Payment Method")))
        {
            error = "Failed to select 'payment method' from the list.";
            return false;
        }
        narrator.stepPassed("Payment Method  :" + " " + getData("Payment Method"));

        return true;
    }

    public boolean bankDetails()
    {
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.settleExisting(), getData("Settle Instalment")))
        {
            error = "Failed to select 'Yes/No' to indicate early settlement.";
            return false;
        }
        narrator.stepPassed("Settle Instalment  :" + " " + getData("Settle Instalment"));
        if (getData("Settle Instalment").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.accountNumberToSettle()))
            {
                error = "Failed to wait for the 'account No 1 to settle' to appear.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountNumberToSettle(), getData("Account To Settle")))
            {
                error = "Failed to enter the 'account No 1 to settle' details.";
                return false;
            }
            narrator.stepPassed("Account No.1 to settle  :" + " " + getData("Account To Settle"));
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.settlementAmount(), getData("Amount to settle")))
            {
                error = "Failed to enter the 'account No 1 to settle' details.";
                return false;
            }
            narrator.stepPassed("Settlement amount 1  :" + " " + getData("Amount to settle"));
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankNameAccToSettle(), getData("Bank Name")))
            {
                error = "Failed to select 'Yes/No' to indicate early settlement.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.monthlyInstalmentAmount(), getData("Amount to settle")))
            {
                error = "Failed to enter the 'account No 1 to settle' details.";
                return false;
            }
            narrator.stepPassed("Monthly instalment  :" + " " + getData("Amount to settle"));

        }
        if (isSaTaxi)
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankAccountStatus(), getData("Account Status")))
            {
                error = "Failed to select 'Yes/No' to indicate Bank account status.";
                return false;
            }
            narrator.stepPassed("Account Status  :" + " " + getData("Account Status"));
        }
        
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.bankingDetailsTab()))
        {
            error = "Failed to scroll to the 'banking details' table.";
            return false;
        }

        return true;
    }

}
