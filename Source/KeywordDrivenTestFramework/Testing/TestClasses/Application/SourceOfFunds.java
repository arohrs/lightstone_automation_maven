/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Source Of Funds",
        createNewBrowserInstance = false
)

public class SourceOfFunds extends BaseClass
{

    String error = "";

    public SourceOfFunds()
    {
    }

    public TestResult executeTest()
    {
        if (!sourceOfFunds())
        {
            return narrator.testFailed("Failed to select customes 'source of funds' on the Form." + " " + error);
        }
        return narrator.finalizeTest("Successfully entered customers 'source of funds' on the Form.");
    }

    public boolean sourceOfFunds()
    {
        /*Entering customers source of funds*/
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.sourceOfFundsTab()))
        {
            error = "Failed to scroll to the 'pension' check-box.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.sourceOfFundsTab()))
        {
            error = "Failed to wait for the 'pension' check-box to appear.";
            return false;
        }
        switch (getData("Form"))
        {
            case "Application":

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.pension()))
                {
                    error = "Failed to wait for the 'pension' check-box.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.pension()))
                {
                    error = "Failed to click on the 'pension' check-box.";
                    return false;
                }
                if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.pensionAmount()))
                {
                    error = "Failed to clear pension amount field";
                }

                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.pensionAmount(), getData("Pension Amount")))
                {
                    error = "Failed to enter customers 'pension amount' on the text field.";
                    return false;
                }
                narrator.stepPassed("Pension Amount:" + " " + getData("Pension Amount"));
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.beeIncome()))
                {
                    error = "Failed to wait for the 'bee' check-box.";
                    return false;
                }

                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.beeIncome()))
                {
                    error = "Failed to click on the 'bee' check-box.";
                    return false;
                }
                if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.beeAmount()))
                {
                    error = "Failed to clear bee amount field";
                }
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.beeAmount(), getData("BEE Amount")))
                {
                    error = "Failed to enter customers 'bee amount' on the text field.";
                    return false;
                }
                narrator.stepPassed("BEE Amount :" + " " + getData("BEE Amount"));

                break;
            case "Re-Application":

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.pension()))
                {
                    error = "Failed to wait for the 'pension' check-box.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.pension()))
                {
                    error = "Failed to click on the 'pension' check-box.";
                    return false;
                }
                narrator.stepPassed("Ticked : Pension");
                break;

        }

        return true;
    }
}
