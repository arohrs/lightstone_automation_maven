/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Finance Details",
        createNewBrowserInstance = false
)

public class FinanceDetails extends BaseClass
{

    String error = "";
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;

    public FinanceDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!financeDetails())
        {
            return narrator.testFailed("Failed to enter customers details on the 'Finance Details' Form. - " + error);
        }
        return narrator.finalizeTest("Successfully entered customers details on the 'Finance Details' Form.");
    }

    public boolean financeDetails()
    {

        /*Entering finance details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financeDetailsTab()))
        {
            error = "Failed to wait for the 'rate indicator' list to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.agreement(), getData("Agreement")))
        {
            error = "Failed to select 'agreement type' from the list.";
            return false;
        }
        narrator.stepPassed("Agreement :" + " " + getData("Agreement"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.repaymentPeriod(), getData("Repayment period")))
        {
            error = "Failed to select 'Repayment periode' from the list.";
            return false;
        }
        narrator.stepPassed("Repayment period :" + " " + getData("Repayment period"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.paymentFrequency(), getData("Payment Frequency")))
        {
            error = "Failed to select 'Payment Frequency' from the list.";
            return false;
        }
        narrator.stepPassed("Payment Frequency :" + " " + getData("Payment Frequency"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.interestRate(), getData("Interest rate")))
        {
            error = "Failed to enter the'Interest rate' in the text field.";
            return false;
        }
        narrator.stepPassed("Interest rate :" + " " + getData("Interest rate"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.rateIndicatorDropDown(), getData("Rate Indicator")))
        {
            error = "Failed to select 'rate indicator' from the list.";
            return false;
        }
        narrator.stepPassed("Rate Indicator :" + " " + getData("Rate Indicator"));
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.residualBalloonValue()))
        {
            error = "Failed to clear Residual/Balloon Value field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.residualBalloonValue(), getData("Residual Value")))
        {
            error = "Failed to enter the 'residual value' into the text field.";
            return false;
        }
        narrator.stepPassed("Residual Value :" + getData("Residual Value"));
        String residualPercentage = SeleniumDriverInstance.retrieveTextByRobot(LightStonePageObjects.residualBalloonPercentage());
//        if (!residualPercentage.equals(getData("Residual Percentage")))
//        {
//            error = "Failed to validate the 'residual percentage'.";
//            return false;
//        }
        narrator.stepPassed("Residual Percentage :" + residualPercentage);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.dealTypeWesbank(), getData("Deal Type Wesbank")))
        {
            error = "Failed to select 'description' from the deal type wesbank list.";
            return false;
        }
        narrator.stepPassed("Deal Type Westbank :" + " " + getData("Deal Type Wesbank"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.financeInitiationFees(), getData("Finance Initiation Fees")))
        {
            error = "Failed to select 'description' from the initiation fees list.";
            return false;
        }
        narrator.stepPassed("Finance Initiation Fees :" + " " + getData("Finance Initiation Fees"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.schemeCodeWesbank()))
        {
            error = "Failed to wait for the 'scheme code wesbank' into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.schemeCodeWesbank(), getData("Scheme Code")))
        {
            error = "Failed to enter the 'residual percentage' into the text field.";
            return false;
        }
        narrator.stepPassed("Scheme code WesBank: " + getData("Scheme Code"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.schemeCodeMFC()))
        {
            error = "Failed to wait for the 'scheme code MFC' into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.schemeCodeMFC(), getData("Scheme Code")))
        {
            error = "Failed to enter the 'residual percentage' into the text field.";
            return false;
        }
        narrator.stepPassed("Scheme code MFC Fox: " + getData("Scheme Code"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.packageIndicator(), getData("Package Indicator")))
        {
            error = "Failed to wait for the 'Package Indicator' into the text field.";
            return false;
        }
        narrator.stepPassed("Package indicator: " + getData("Package Indicator"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.packageNumber()))
        {
            error = "Failed to wait for the 'Package number' into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.packageNumber(), getData("Package Number")))
        {
            error = "Failed to enter the 'Package number' into the text field.";
            return false;
        }
        narrator.stepPassed("Package number: " + getData("Package Number"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.payInAdvance(), getData("Pay in Advance")))
        {
            error = "Failed to select the'Yes/No' from the list.";
            return false;
        }
        narrator.stepPassed("Pay in Advance: " + getData("Pay in Advance"));

        if (!isAfterMarket)
        {
            String shockAbsorber = getData("Shock-Absorber");
            if (!shockAbsorber.isEmpty())
            {
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.shockAbsorber(), shockAbsorber))
                {
                    error = "Failed to select the '" + shockAbsorber + "' from the list.";
                    return false;
                }
                narrator.stepPassed("Financial Shock-Absorber: " + shockAbsorber);
            }
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fordSchemeDeal(), getData("Ford Scheme")))
            {
                error = "Failed to select the'Yes/No' from the list.";
                return false;
            }
            narrator.stepPassed("Ford scheme: " + getData("Ford Scheme"));
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.depositTrade()))
        {
            error = "Failed to clear Deposit/Trade field";
        }

        String depositIn = getData("Deposit In");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.depositTrade(), depositIn))
        {
            error = "Failed to enter the 'deposit/trade amount' into the text field - " + depositIn;
            return false;
        }

        //Clicking cash deposit label
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.cashDepositLabel()))
        {
            error = "Failed to click cash deposit label";
            return false;
        }

        narrator.stepPassed("Cash Deposit:" + " " + getData("Deposit In"));
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.tradeInDeposit()))
        {
            error = "Failed to clear Trade In field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.tradeInDeposit(), depositIn))
        {
            error = "Failed to enter the 'Trade In deposit amount' into the text field - " + depositIn;
            return false;
        }
        narrator.stepPassed("Trade In Deposit:" + " " + getData("Deposit In"));
        SeleniumDriverInstance.pressKey(Keys.TAB);

        if (isAfterMarket)
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.depositSource(), getData("Deposit source")))
            {
                error = "Failed to select the'Bank' from the list.";
                return false;
            }
            narrator.stepPassed("Deposit source: " + getData("Deposit source"));

        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financeDetailsTab()))
        {
            error = "Failed to wait for the 'rate indicator' list to appear.";
            return false;
        }

        return true;
    }
}
