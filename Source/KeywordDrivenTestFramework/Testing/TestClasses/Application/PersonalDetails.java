/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Personal Details",
        createNewBrowserInstance = false
)

public class PersonalDetails extends BaseClass
{

    String error = "";

    public PersonalDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!personalDetails())
        {
            return narrator.testFailed("Failed to enter customers 'personal details' on the Form - " + error);
        }
        return narrator.finalizeTest("Successfully entered customers 'personal details' on the Form.");
    }

    public boolean personalDetails()
    {

        /*Entering Dealer personal details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.personalDetailsTab()))
        {
            error = "Failed to scroll to 'race' drop down selection";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.personalDetailsTab()))
        {
            error = "Failed to wait for the 'payout date' of first registration text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.raceDropDown(), getData("Race")))
        {
            error = "Failed to select 'race' from the drop down list.";
            return false;
        }
        narrator.stepPassed("Race :" + " " + getData("Race"));

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.titleDropDown(), getData("Title")))
        {
            error = "Failed to select a 'title' from the list.";
            return false;
        }
        narrator.stepPassed("Title :" + " " + getData("Title"));

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maritalStatusDropDown(), getData("Marital Status")))
        {
            error = "Failed to select 'marital status' from the list.";
            return false;
        }
        narrator.stepPassed("Marital Status :" + " " + getData("Marital Status"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mobileNumber(), getData("Mobile Number")))
        {
            error = "Failed to enter 'mobile number' from the drop down list.";
            return false;
        }
        narrator.stepPassed("Mobile Number :" + " " + getData("Mobile Number"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mobileTypeDropDown(), getData("Mobile Number Type")))
        {
            error = "Failed to select 'mobile number type' from the list.";
            return false;
        }
        narrator.stepPassed("Mobile Number Type:" + " " + getData("Mobile Number Type"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.phoneTypeDropDown(), getData("Phone Number Type")))
        {
            error = "Failed to select 'phone number type' from the drop down list.";
            return false;
        }
        narrator.stepPassed("Phone Number Type:" + " " + getData("Phone Number Type"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.workNumber(), getData("Work Number")))
        {
            error = "Failed to enter 'work phone number' from the list.";
            return false;
        }
        narrator.stepPassed("Work Number :" + " " + getData("Work Number"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.phoneNumber(), getData("Phone Number")))
        {
            error = "Failed to enter 'Home phone number ' from the list.";
            return false;
        }
        narrator.stepPassed("Phone number :" + " " + getData("Phone Number"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.emailAdress(), getData("Email Address")))
        {
            error = "Failed to enter 'email adress' in the text field.";
            return false;
        }
        narrator.stepPassed("Email Address :" + " " + getData("Email Address"));

        switch (getData("Branch"))
        {
            case "Wesbank":

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfBirthDropDown(), getData("Country Of Birth")))
                {
                    error = "Failed to select 'country of birth' from the list.";
                    return false;
                }
                narrator.stepPassed("Country of birth:" + " " + getData("Country Of Birth"));
                break;

            case "Other"://Signio Application and POC Script

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfBirthDropDown(), getData("Country Of Birth")))
                {
                    error = "Failed to select 'country of birth' from the list.";
                    return false;
                }
                narrator.stepPassed("Country of birth:" + " " + getData("Country Of Birth"));
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfResidentialAdressDropDown(), getData("Country Of Residential")))
                {
                    error = "Failed to select country of 'residential address' from the list.";
                    return false;
                }
                narrator.stepPassed("Country of Residential Adress:" + " " + getData("Country Of Residential"));

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.taxObligations(), getData("Tax Obligations")))
                {
                    error = "Failed to select 'tax obligation status' from  list.";
                    return false;
                }
                narrator.stepPassed("Tax Obligations:" + " " + getData("Tax Obligations"));

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.multipleNationalitiesDropDown(), getData("Multiple Nationalities")))
                {
                    error = "Failed to select 'multiple nationality status' from the list.";
                    return false;
                }
                narrator.stepPassed("Multiple Nationalities:" + " " + getData("Multiple Nationalities"));
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.wasSACitizenDropDown(), getData("South African Citizenship")))
                {
                    error = "Failed to select and indicate if the customer was an 'sa citizen' or not from list.";
                    return false;
                }
                narrator.stepPassed("Were you ever a South African:" + " " + getData("South African Citizenship"));
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfCitizenship(), getData("Country Of Citizenship")))
                {
                    error = "Failed to select 'country of citizenship' from the list.";
                    return false;
                }
                narrator.stepPassed("Country of Citizenship:" + " " + getData("Country Of Citizenship"));
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.clientType(), getData("Client Type")))
                {
                    error = "Failed to select 'client type' from the list.";
                    return false;
                }
                narrator.stepPassed("Client Type : " + " " + getData("Client Type"));
                break;

            case "After Market":
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfBirthDropDown(), getData("Country Of Birth")))
                {
                    error = "Failed to select 'country of birth' from the list.";
                    return false;
                }
                narrator.stepPassed("Country of birth:" + " " + getData("Country Of Birth"));
                break;

        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.personalDetailsTab()))
        {
            error = "Failed to scroll to 'race' drop down selection";
            return false;
        }

        return true;
    }

}
