/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Wesbank Re-Application",
        createNewBrowserInstance = false
       
)

public class WesbankReApplication extends BaseClass
{

    String error = "";

    public WesbankReApplication()
    {
    }

    public TestResult executeTest()
    {
        if (!re_Application())
        {
            return narrator.testFailed("Failed to enter customer's details on the 'Wesbank re-application' Form - " + error);
        }
        return narrator.finalizeTest("Successfully searched for 'Wesbank re-application' Form.");
    }

    public boolean re_Application()
    {

        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        /*Creating a new application*/
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.applicationDropDown())))
        {
            error = "Failed to wait for  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.applicationDropDown())))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.wesbankReApplication())))
        {
            error = "Failed to wait for  the 'wesbank re-application' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.wesbankReApplication())))
        {
            error = "Failed to select 'Wesbank re-application' from the list of applications.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbank_Re_ApplicationHeader()))
        {
            error = "Failed to wait for the 'Wesbank re-application header' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.clientIdNumber()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }

        /*Entering the re-application details to search*/
        String customerId = getData("ID");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientIdNumber(), customerId))
        {
            error = "Failed to enter 'firstname' into the firstname text field.";
            return false;
        }
        narrator.stepPassed("Customer ID Number: " + " " + customerId);
        String firstName = getData("Firstname");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fiFirstName()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.fiFirstName(), firstName))
        {
            error = "Failed to enter 'firstname' into the firstname text field.";
            return false;
        }
        narrator.stepPassed("First Name: " + " " + firstName);
        String surName = getData("Surname");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fiSurName()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.fiSurName(), surName))
        {
            error = "Failed to enter 'Surname' into the surname text field.";
            return false;
        }
        narrator.stepPassed("Surname: " + " " + surName);
        String contactNumber = getData("Contact Number");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fiContactNumber()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.fiContactNumber(), contactNumber))
        {
            error = "Failed to enter 'Surname' into the surname text field.";
            return false;
        }
        narrator.stepPassed("Contact number: " + " " + contactNumber);
        String faxNumber = getData("Fax");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fiFaxNumber()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.fiFaxNumber(), faxNumber))
        {
            error = "Failed to enter 'Surname' into the surname text field.";
            return false;
        }
        narrator.stepPassed("Fax number: " + " " + faxNumber);
        String email = getData("Email Address");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fiEmailAddress()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.fiEmailAddress(), email))
        {
            error = "Failed to enter 'Surname' into the surname text field.";
            return false;
        }
        narrator.stepPassed("Email Address: " + " " + email);
        String IDNumber = getData("SA ID Number");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fiIdNumber()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.fiIdNumber(), IDNumber))
        {
            error = "Failed to enter 'Surname' into the surname text field.";
            return false;
        }
        narrator.stepPassed("SA ID Number: " + " " + IDNumber);
        String notification = getData("Notification Criteria");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fiNotificationCriteria()))
        {
            error = "Failed to wait for the 'Notification criteria' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fiNotificationCriteria(), notification))
        {
            error = "Failed to select the 'Notification criteria' from the list.";
            return false;
        }
        narrator.stepPassed("Notification criteria: " + " " + notification);
        String userType = getData("User Type");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fiUserType()))
        {
            error = "Failed to wait for the 'User type' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fiUserType(), userType))
        {
            error = "Failed to select the 'User type' from the list.";
            return false;
        }
        narrator.stepPassed("User Type: " + " " + userType);
        String branchCode = getData("Branch Code");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fiBranchCode()))
        {
            error = "Failed to wait for the 'branch Code' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fiBranchCode(), branchCode))
        {
            error = "Failed to select the 'branch Code' from the list.";
            return false;
        }
        narrator.stepPassed("Branch Code: " + " " + branchCode);

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.fiSearchButton()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Completed Wesbank Re-Application Search Criteria");

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.fiSearchButton())))
        {
            error = "Failed to click the 'absa bank' check-box.";
            return false;
        }
        narrator.stepPassed("Successfully clicked the 'search' button");
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),5))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.applicationFormTab()))
        {
            error = "Failed to wait for the 'E-application header' to appear.";
            return false;
        }
        narrator.stepPassed("Successfully Switched back to E-Application Tab");
        return true;
    }

}
