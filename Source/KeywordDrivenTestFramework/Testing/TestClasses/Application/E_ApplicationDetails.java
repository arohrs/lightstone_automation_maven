/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Application Details",
        createNewBrowserInstance = false
)

public class E_ApplicationDetails extends BaseClass
{

    String error = "";
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;

    public E_ApplicationDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!newApplicationDetails())
        {
            return narrator.testFailed("Failed to enter customer's details on the 'e-application' Form - " + error);
        }
        if (!isAfterMarket)
        {
            if (!applicationForm())
            {
                return narrator.testFailed("Failed to enter customer's details on the 'e-application' Form - " + error);
            }
        }
        return narrator.finalizeTest("Successfully entered customer's details on the 'e-application' Form.");
    }

    public boolean newApplicationDetails()
    {

        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        /*Creating a new application*/
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.applicationDropDown())))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'Application' Menu");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.newApplication())))
        {
            error = "Failed to select 'new application' from the list of applications.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.applicationFormTab()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        narrator.stepPassed("Successfully clicked on the 'New Application' from the list");

        /*Entering customer details on the new application*/
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationFirstname(), getData("Firstname")))
        {
            error = "Failed to enter 'firstname' into the firstname text field.";
            return false;
        }
        narrator.stepPassed("First Name: " + " " + getData("Firstname"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationSurname(), getData("Surname")))
        {
            error = "Failed to enter 'Surname' into the surname text field.";
            return false;
        }
        narrator.stepPassed("Surname: " + " " + getData("Surname"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationMiddleName(), getData("Middle name")))
        {
            error = "Failed to enter 'Middle name' into the Middle name text field.";
            return false;
        }
        narrator.stepPassed("Middle name: " + " " + getData("Middle name"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationIdNumber(), getData("ID")))
        {
            error = "Failed to enter 'id' number into the id text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.idNumberLabel()))
        {
            error = "Failed to click the 'id nnumber label.'";
            return false;
        }
        if (!SeleniumDriverInstance.dismissAlert())
        { //cancells an alert
            error = "Failed to handle an alert.";
            return false;
        }
        narrator.stepPassed("ID Number: " + " " + getData("ID"));
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.applicationVerifiedDropDown()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.applicationVerifiedDropDown(), getData("Verified")))
        {
            error = "Failed to select Yes/NO to check if or not an application is verified.";
            return false;
        }
        narrator.stepPassed("ID Verified: " + " " + getData("Verified"));
        if (isAfterMarket)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.afterMarket()))
            {
                error = "Failed to wait for 'After Market' to appear";
                return false;

            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.afterMarket()))
            {
                error = "Failed to click the 'After Market' checkBox";
            }
            narrator.stepPassed("Clicked After Market checkBox");

        }

        return true;
    }

    public boolean applicationForm()
    {

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.clickAbsaBank()))
        {
            error = "Failed to wait for the 'absa bank' check box to appear";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickAbsaBank())))
        {
            error = "Failed to click the 'absa bank' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked ABSA BANK checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickMfc())))
        {
            error = "Failed to click the 'mfc' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked MFC checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickStandardBank())))
        {
            error = "Failed to click the 'standard bank' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked STANDARD BANK checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickWesbank())))
        {
            error = "Failed to click the 'wesbank' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked WESBANK checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickAlpheraFinance())))
        {
            error = "Failed to click the 'Alphera Finance' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked ALPHERA FINANCE checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickAriva())))
        {
            error = "Failed to click the 'Ariva' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked ARIVA checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickCapitecBank())))
        {
            error = "Failed to click the 'Capitec bank' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked CAPITEC BANK checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickSaTaxiFinance())))
        {
            error = "Failed to click the 'SA Taxi Finance' check-box.";
            return false;
        }
        narrator.stepPassed("Clicked SA TAXI FINANCE checkBox");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.indicationTypeDropDown()))
        {
            error = "Failed to wait for the 'indication type' check box to appear";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.indicationTypeDropDown(), getData("Indication Type")))
        {
            error = "Failed to select the 'indication type' from the list.";
            return false;
        }
        narrator.stepPassed("Indication Type: " + getData("Indication Type"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.driversLicenseDropDown(), getData("License")))
        {
            error = "Failed to select 'Yes/No' to check if or not the license is verified";
            return false;
        }
        narrator.stepPassed("Drivers License Verified: " + " " + getData("License"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaDealerCodeDropDown(), getData("ABSA")))
        {
            error = "Failed to select 'absa dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected ABSA - Dealer Code: " + " " + getData("ABSA"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.alpheraDealerCode(), getData("ALPHERA")))
        {
            error = "Failed to select 'Aplphera Finance dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected Aplphera Finance - Dealer Code: " + " " + getData("ALPHERA"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.arivaDealerCode(), getData("ARIVA")))
        {
            error = "Failed to select 'ARIVA dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected ARIVA - Dealer Code: " + " " + getData("ARIVA"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.capitecDealerCode(), getData("CAPITEC")))
        {
            error = "Failed to select 'CAPITEC BANK dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected CAPITEC BANK - Dealer Code: " + " " + getData("CAPITEC"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mfcDealerCodeDropDown(), getData("MFC")))
        {
            error = "Failed to select the 'mfc dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected MFC - Dealer Code :" + " " + getData("MFC"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.saTaxiDealerCode(), getData("SA TAXI")))
        {
            error = "Failed to select 'SA TAXI dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected SA TAXI - Dealer Code: " + " " + getData("SA TAXI"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.standardBankDealerCodeDropDown(), getData("STANDARD BANK")))
        {
            error = "Failed to select the 'standard dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected STANDARD BANK - Dealer Code: " + " " + getData("STANDARD BANK"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.wesBankDealerCodeDropDown(), getData("WESBANK")))
        {
            error = "Failed to select the 'wesbank dealer code from the list.";
            return false;
        }
        narrator.stepPassed("Selected WESBANK - Dealer Code: " + " " + getData("WESBANK"));
//        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.branchDropDown()))
//        {
//            error = "Failed to wait for the 'branch Code' from the list.";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForOneElementLeftByXpath(LightStonePageObjects.branchDropDown()))
        {
            error = "Failed to wait for the 'branch Code' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.branchDropDown(), getData("Branch Code")))
        {
            error = "Failed to select the 'branch Code' from the list.";
            return false;
        }
        narrator.stepPassed("WESBANK - Branch Code: " + " " + getData("Branch Code"));
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.shortTermInsurers()))
        {
            error = "Failed to wait for the 'branch Code' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.motorInsuranceLead(),5))
        {
            error = "F  ailed to wait for the 'branch Code' from the list.";
            return false;
        }
        narrator.stepPassed("Successfully validated the'Motor insurance Leads' is checked and disabled");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.shortTermInsurers()))
        {
            error = "Failed to wait for the 'branch Code' from the list.";
            return false;
        }
        String termInsurer = getData("Term Insurers");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.shortTermInsurers(), termInsurer))
        {
            error = "Failed to wait for the '" + termInsurer + "' from the list.";
            return false;
        }
        narrator.stepPassed("Short term Insurers: " + termInsurer);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.budgetDealerQuote()))
        {
            error = "Failed to click the 'budget dealer quote'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Budget Dealer Quote checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.budgetLink()))
        {
            error = "Failed to click the 'budget link'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Budget Link checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.compTesting()))
        {
            error = "Failed to click the 'comp testing'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Comp Testing checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.kingPrice()))
        {
            error = "Failed to click the 'King price'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked King Price checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.miWay()))
        {
            error = "Failed to click the 'Miway'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked MiWay checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.standardBankInsuranceLeads()))
        {
            error = "Failed to click the 'Standard bank Insurance leads'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked StandardBank Insurance Leads checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.teleSure()))
        {
            error = "Failed to click the 'telesure'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Telesure checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.iPlatForm()))
        {
            error = "Failed to click the 'iPlatform'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked iPlatform checkBox");
      
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clickTrackingApproved()))
        {
            error = "Failed to wait for the 'tracking Approved'checkBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickTrackingApproved()))
        {
            error = "Failed to click the 'tracking Approved'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Tracking Approved checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickBeame()))
        {
            error = "Failed to click the 'Beame'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Beame checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.cTrack()))
        {
            error = "Failed to click the 'C-track'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked C-Track checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.carTrack()))
        {
            error = "Failed to click the 'Cartrack'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Car Track checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.matrix()))
        {
            error = "Failed to click the 'Matrix'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Matrix checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.netstar()))
        {
            error = "Failed to click the 'Netstar'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Netstar checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.tracker()))
        {
            error = "Failed to click the 'Tracker'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Tracker checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.trackingInfinity()))
        {
            error = "Failed to click the 'Tracking Infinity'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Tracking infinity checkBox");
        String beamePlan = getData("Beame");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.beamePlan(), beamePlan))
        {
            error = "Failed to select the '" + beamePlan + "'from the list.";
            return false;
        }
        narrator.stepPassed("Beame Plan: " + beamePlan);
        String cTrack = getData("C-Track");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.cTrackPlan(), cTrack))
        {
            error = "Failed to select the '" + cTrack + "'from the list.";
            return false;
        }
        narrator.stepPassed("C-Track Plan: " + cTrack);
        String carTrack = getData("CarTrack");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.carTrackPlan(), carTrack))
        {
            error = "Failed to select the '" + carTrack + "'from the list.";
            return false;
        }
        narrator.stepPassed("CarTrack Plan: " + carTrack);
        String netStar = getData("NetStar");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.netStarPlan(), netStar))
        {
            error = "Failed to select the '" + netStar + "'from the list.";
            return false;
        }
        narrator.stepPassed("NetStar Plan: " + netStar);
        String tracker = getData("Tracker");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.trackerPlan(), tracker))
        {
            error = "Failed to select the '" + tracker + "'from the list.";
            return false;
        }
        narrator.stepPassed("Tracker Plan: " + tracker);
        String tracking = getData("Tracking");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.trackingInfinityPlan(), tracking))
        {
            error = "Failed to select the '" + tracking + "'from the list.";
            return false;
        }
        narrator.stepPassed("Tracking Infinity Plan: " + tracking);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.callCenterLeads()))
        {
            error = "Failed to click the 'call canter leads'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked Call center Lead checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.ccTest()))
        {
            error = "Failed to click the 'cc test'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked CC Test checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.keyFind()))
        {
            error = "Failed to click the 'Keyfind'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked KeyFind checkBox");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.zestLifeTest()))
        {
            error = "Failed to click the 'zestlife test'checkBox.";
            return false;
        }
        narrator.stepPassed("Clicked ZestLife checkBox");
          if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.shortTermInsurers()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully filled in the arrange for fitment form");
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicationFormTab()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }

        return true;
    }

}
