/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "SecureX Details",
        createNewBrowserInstance = false
)

public class SecureX extends BaseClass
{

    String error = "";

    public SecureX()
    {
    }

    public TestResult executeTest()
    {

        if (!secureX())
        {
            return narrator.testFailed("Failed to update 'secure x details'." + " " + error);
        }
        return narrator.finalizeTest("Successfully updated 'secure x details' on the Form.");
    }

    public boolean secureX()
    {
        /*updating secure x details*/
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.secureXTab()))
        {
            error = "Failed to scroll to the 'bank statement consent' field.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.bankStatement()))
        {
            error = "Failed to wait for the 'bank statement consent' field to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankStatement(), getData("Bank Statement")))
        {
            error = "Failed to select 'Yes/No' to bank statement consent.";
            return false;
        }
        narrator.stepPassed("Bank Statement Consent : " + getData("Bank Statement"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.spouseBankStatement(), getData("Spouse consent")))
        {
            error = "Failed to select 'Yes/No' spouse statement consent.";
            return false;
        }
        narrator.stepPassed("Spouse consent : " + getData("Spouse consent"));

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.incomeVerification(), getData("Income Verification")))
        {
            error = "Failed to select 'Yes/No' share income verification consent.";
            return false;
        }
        narrator.stepPassed("Income Verification Consent : " + getData("Income Verification"));

        // Select Yes/No to proof of residence consent
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.proofOfResidence(), testData.getData("Proof of residence")))
        {
            error = "Failed to Select Yes/No to proof of residence consent";
            return false;
        }

        narrator.stepPassed("Proof of residence : " + getData("Proof of residence"));
        SeleniumDriverInstance.pause(2000);
        return true;
    }
}
