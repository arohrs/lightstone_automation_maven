/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Statement",
        createNewBrowserInstance = false
)

public class Statement extends BaseClass
{

    String error = "";

    public Statement()
    {
    }

    public TestResult executeTest()
    {

        if (!statement())
        {
            return narrator.testFailed("Failed to choose the 'statement method' of delivery." + " " + error);
        }
        return narrator.finalizeTest("Successfully chose the 'statement' of delivery method.");
    }

    public boolean statement()
    {
        /*Choose statement delivery method*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.statementTab()))
        {
            error = "Failed to scroll to the 'delivery method' list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.statementTab()))
        {
            error = "Failed to wait for the 'delivery method' list to appear.";
            return false;
        }
        validateDeliveryMethod();
//        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.statementDeliveryMethod(), getData("Statement Delivery")))
//        {
//            error = "Failed to select 'statement delivery method' from the list.";
//            return false;
//        }
        narrator.stepPassed("Statement Delivery Method : " + getData("Statement Delivery"));
        return true;
    }

    public boolean validateDeliveryMethod()
    {
        List<String> deliveryMethod_TestPack = Arrays.asList(testData.getData("Delivery_List").split(","));
        List<String> deliveryMethod_WebPage = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.statementDeliveryMethodList());
        String preferredMethod = getData("Delivery Method");
 
        
        for (String deliveryMethod : deliveryMethod_TestPack)
        {
            if (!deliveryMethod_WebPage.stream().anyMatch(s -> deliveryMethod.equalsIgnoreCase(s)))
            {
                error = "Statement delivery method not found in the lsit";

            }

        }

        for (String statement : deliveryMethod_WebPage)

        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.statementDeliveryMethod(), statement))
            {
                error = "Failed to select '" + statement + "'from the list";

            }
            narrator.stepPassed("Statement Delivery Method: "+statement);
            if (statement.equalsIgnoreCase("Electronic Statement"))
            {
                String firstEmail = getData("Email");
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.preferredEmailAddress(), firstEmail))
                {
                    error = "Failed to enter the '" + firstEmail + "'detail in the text field";

                }
                narrator.stepPassed("Preferred Email: "+firstEmail);
                 String secondEmail = getData("Email2");
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.alternativeEmailAddress(), secondEmail))
                {
                    error = "Failed to enter the '" + secondEmail + "'detail in the text field";

                }
                 narrator.stepPassed("Alternative Email: "+secondEmail);
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.preferredDeliveryMethod(), preferredMethod))
                {
                    error = "Failed to select the '" + preferredMethod + "'from the list";

                }
                narrator.stepPassed("Preferred delivery Method: "+preferredMethod);

            } else {
            
             if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.preferredDeliveryMethod(), preferredMethod))
                {
                    error = "Failed to enter the '" + preferredMethod + "'detail in the text field";

                }
                narrator.stepPassed("Preferred delivery Method: "+preferredMethod);
            
            
            }
            
        }
        
        

        return true;
    }
}
