/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Application;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Bee Status",
        createNewBrowserInstance = false
        
)
public class BeeStatus extends BaseClass
{
    String error = "";
    public BeeStatus()
    {}
    
     public TestResult executeTest()
     {
          if (!bee_Status())
        {
            return narrator.testFailed("Failed to enter the BEE Act Status detail. - " + error);
        }
     
     
         return narrator.finalizeTest("Successfully updated the BEE Act Status detail.");
     }
     
     
     public boolean bee_Status()
     {
         if(!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.beeActStatusHeader()))
         {
              error = "Failed to Scroll to 'BEE Act Status' Header";
              return false;
         }
         if(!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.beeActStatus()))
         {
             error = "Failed to wait for 'BEE Act' dropdown";
             return false;
             
         }
         if(!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.beeActStatus(), getData("BEE Act")))
         {
             error ="Failed to select 'Yes/No BEE Act Status' from the list  ";
         }
     
         narrator.stepPassed("BEE Act Status: "+getData("BEE Act"));
         return true;
     }
    
}
