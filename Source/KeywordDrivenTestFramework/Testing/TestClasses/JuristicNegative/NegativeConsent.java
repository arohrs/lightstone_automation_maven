/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Consent Negative",
        createNewBrowserInstance = false
)
public class NegativeConsent extends BaseClass
{

    String error = "";
    String consent1 = getData("Consent1");
    String consent2 = getData("Consent2");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeConsent()
    {
    }

    public TestResult executeTest()
    {
        if (!consentDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully perform the negative test");
    }

    public boolean consentDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.consentTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankStatement(), consent1))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Bank Statement Consent : " + consent1);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.infoSharingConsent(), consent2))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Information sharing Consent : " + consent2);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.consentTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
            {
                error = "Failed to hover over the '" + field + "'field .";
                return false;
            }
            pause(500);
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
            {
                error = "The '" + field + "' field is not highlighted in red .";
                return false;
            }

            narrator.stepPassed("Successfully found the red highlighted " + field + " field");

        }

        return true;

    }
}
