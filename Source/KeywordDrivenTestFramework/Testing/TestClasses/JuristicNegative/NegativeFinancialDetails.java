/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Financial Negative",
        createNewBrowserInstance = false
)
public class NegativeFinancialDetails extends BaseClass
{

    String error = "";
    String bankName = getData("Bank Name");
    String branchName = getData("Branch Name");
    String branchCode = getData("Branch Code");
    String accType = getData("Acc Type");
    String accNumber = getData("Acc Number");
    String accName = getData("Acc holder Name");
    String year = getData("Year");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeFinancialDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!financialDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully perform the negative test");
    }

    public boolean financialDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financialDetailsTab()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyBankName(), bankName))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Bank Name : " + bankName);
        if (!bankName.equalsIgnoreCase("Not Selected"))
        {
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBankBranchName()))
            {
                error = "Failed to scroll to the 'Source of funds' tab.";
                return false;
            }
            if (!branchName.isEmpty())
            {
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankBranchName(), branchName))
                {
                    error = "Failed to scroll to the 'Source of funds' tab.";
                    return false;
                }
            }
            narrator.stepPassed("Branch Name : " + branchName);
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBankBranchCode()))
            {
                error = "Failed to scroll to the 'Source of funds' tab.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankBranchCode(), branchCode))
            {
                error = "Failed to scroll to the 'Source of funds' tab.";
                return false;
            }
            narrator.stepPassed("Branch Code : " + branchCode);
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyBankAccountType(), accType))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Account Type : " + accType);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBankAccountNumber()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankAccountNumber(), accNumber))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Account Number : " + accNumber);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBankAccountName()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBankAccountName(), accName))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Account Holder's Name : " + accName);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyBalanceSheetYear()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyBalanceSheetYear(), year))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Year : " + year);

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financialDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }
}
