/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Finance Negative",
        createNewBrowserInstance = false
)
public class NegativeFinanceDetails extends BaseClass
{

    String error = "";
    String financeType = getData("Finance Type");
    String periodContract = getData("Period contract");
    String paymentMethod = getData("Payment Method");
    String price = getData("Purchase Price");
    String deposit = getData("Cash Deposit");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeFinanceDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!financeDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test -" + error);

    }

    public boolean financeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.transactionFinanceDetailsTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionFinanceType(), financeType))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Finance Type : " + financeType);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPeriodofContact(), periodContract))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Period of Contract : " + periodContract);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPaymentMethod(), paymentMethod))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Payment Method : " + paymentMethod);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionPurchasePrice()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        if (!price.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionPurchasePrice(), price))
            {
                error = "Failed to wait for the'Vehicle lookup' to appear.";
                return false;
            }
        }
        narrator.stepPassed("Cash / Purchase price : " + price);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionCashDeposit()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        if (!deposit.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionCashDeposit(), deposit))
            {
                error = "Failed to wait for the'Vehicle lookup' to appear.";
                return false;
            }
        }
        narrator.stepPassed("Cash Deposit Amount : " + deposit);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionFinanceDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' is Highlighted in red .";
                    return false;
                }
            } else if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
            {
                error = "Failed to catch the '" + field + "' is Highlighted in red .";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field");

        }

        return true;
    }

}
