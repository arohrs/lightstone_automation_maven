/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses.GeographicalDetails;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Geographical Negative",
        createNewBrowserInstance = false
)
public class NegativeGeographicalDetails extends BaseClass
{

    String error = "";
    String address1 = getData("Address1");
    String address2 = getData("Address2");
    String address3 = getData("Address3");
    String searchPhysicalAddress = getData("Physical");
    String searchOperatingAddress = getData("Operating");
    String searchRegisteredAddress = getData("Registered");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");
    GeographicalDetails suburbSearch = new GeographicalDetails();

    public NegativeGeographicalDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!geographicalDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean geographicalDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.geographicalTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessPhysicalAddress1()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessPhysicalAddress1(), address1))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Physical Address 1: " + address1);
        if (searchPhysicalAddress.equalsIgnoreCase("Yes")&&!address1.isEmpty())
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessPhysicalAddressSuburbButton()))
            {
                error = "Failed to click  the 'application' button.";
                return false;
            }
            suburbSearch.addressLookup();
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.physicalAddressPopulateButton()))
            {
                error = "Failed to click  the 'application' button.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessOperatingAddress1()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessOperatingAddress1(), address2))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Operating Address 1: " + address2);
        if (searchPhysicalAddress.equalsIgnoreCase("Yes")&&!address2.isEmpty())
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessOperatingAddressSuburbButton()))
            {
                error = "Failed to click  the 'application' button.";
                return false;
            }
            suburbSearch.addressLookup();
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.operatingAddressPopulateButton()))
            {
                error = "Failed to click  the 'application' button.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessRegisteredAddress1()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegisteredAddress1(), address3))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassed("Registered Address 1: " + address3);
        if (searchPhysicalAddress.equalsIgnoreCase("Yes")&& !address3.isEmpty())
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessRegisteredAddressSuburbButton()))
            {
                error = "Failed to click  the 'application' button.";
                return false;
            }
            suburbSearch.addressLookup();
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.registeredAddressPopulateButton()))
            {
                error = "Failed to click  the 'application' button.";
                return false;
            }
        }
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.geographicalTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");


        }

        return true;
    }

}
