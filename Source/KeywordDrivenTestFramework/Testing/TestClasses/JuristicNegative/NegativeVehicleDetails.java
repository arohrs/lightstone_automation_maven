/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Vehicle Negative",
        createNewBrowserInstance = false
)
public class NegativeVehicleDetails extends BaseClass
{

    String error = "";
    String articleCondition = getData("Article condition");
    String km = getData("Kilometers");
    String redField = getData("redField");
    String readOnly = getData("Read Only");
    String notEditable = getData("Not editable");
    String fieldType = getData("Field Type");
    String field = getData("Field");
    String submit = getData("Submit");

    public NegativeVehicleDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!vehicleDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test -" + error);

    }

    public boolean vehicleDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionVehicleDetailsTab()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        List<String> readOnlyField = Arrays.asList(readOnly.split(","));
        for (String readField : readOnlyField)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.readOnlyInputField(readField)))
            {
                error = "Failed to validate '" + readField + "' is a read-only field .";
                return false;
            }

        }
        narrator.stepPassed("Successfully validated " + notEditable + " fields are not manually editable");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionArticleCondition(), articleCondition))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassed("Article New/Used : " + articleCondition);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionActualKilometers()))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        }
        if (!km.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionActualKilometers(), km))
            {
                error = "Failed to click on the 'OK' button";
                return false;
            }
        }
        narrator.stepPassed("Actual Kilometers : " + km);

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionVehicleDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "Failed to catch the '" + field + "' is Highlighted in red .";
                    return false;
                }
            } else if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
            {
                error = "The'" + field + "' field is highlighted in red .";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field");

        }

        return true;
    }

}
