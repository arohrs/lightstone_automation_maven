/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Premium Negative",
        createNewBrowserInstance = false
)
public class NegativeABSAPremiumCollection extends BaseClass
{

    String error = "";
    String premiumCollection = getData("Dealer Collect Premium");
    String premiumVendor = getData("Premium Vendor");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeABSAPremiumCollection()
    {
    }

    public TestResult executeTest()
    {
        if (!absaCollections())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test -" + error);

    }

    public boolean absaCollections()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.absaPremiumCollectionTab()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaPremiumCollectionDropDown(), premiumCollection))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        narrator.stepPassed("Can the Dealer collect premium : " + premiumCollection);
        if (premiumCollection.equalsIgnoreCase("No"))
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaPremiumVendorDropDown(), premiumVendor))
            {
                error = "Failed to scroll to the 'Source of funds' tab.";
                return false;
            }
            narrator.stepPassed("Premium vendor : " + premiumVendor);
        }
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.absaPremiumCollectionTab()))
            {
                error = "Failed to scroll to the 'Source of funds' tab.";
                return false;
            }
            if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
            {
                error = "Failed to hover over the '" + field + "'field .";
                return false;
            }
            pause(500);
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
            {
                error = "The '" + field + "' field is not highlighted in red .";
                return false;
            }

            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
