/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Income Negative",
        createNewBrowserInstance = false
)
public class NegativeIncomeDetails extends BaseClass
{

    String error = "";
    String grossNet = getData("Gross Net");
    String netTakeHome = getData("Net");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeIncomeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!incomeDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean incomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryIncomeDetailsTab()))
        {
            error = "Failed to scroll to 'Personal detail' container";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryGrossRenumeration()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!grossNet.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryGrossRenumeration(), grossNet))
            {
                error = "Failed to scroll  the 'geograpical 'tab.";
                return false;
            }
        }
        narrator.stepPassed("Gross Remuneration : " + grossNet);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryNetTakeHome()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!netTakeHome.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryNetTakeHome(), netTakeHome))
            {
                error = "Failed to scroll  the 'geograpical 'tab.";
                return false;
            }
        }
        narrator.stepPassed("Net Take-home Pay : " + netTakeHome);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryIncomeDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' is not highlighted in red .";
                    return false;
                }
            } else if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
            {
                error = "The '" + field + "' is not highlighted in red .";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field");
        }

        return true;
    }
}
