/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Statement Negative",
        createNewBrowserInstance = false
)
public class NegativeStatementDetails extends BaseClass
{

    String error = "";
    String statement = getData("Statement Type");
    String email = getData("Email");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeStatementDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!statementDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test -" + error);

    }

    public boolean statementDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.statementTab()))
        {
            error = "Failed to scroll to 'Personal detail' container";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionStatementType(), statement))
        {
            error = "Failed to scroll to the 'delivery method' list.";
            return false;
        }
        narrator.stepPassed("Statement Type : " + statement);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionPreferredEmail()))
        {
            error = "Failed to scroll to the 'delivery method' list.";
            return false;
        }
        if(!email.isEmpty()){
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionPreferredEmail(), email))
        {
            error = "Failed to scroll to the 'delivery method' list.";
            return false;
        }}
        narrator.stepPassed("Preferred Email Address : " + email);

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.statementTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' is Highlighted in red .";
                    return false;
                }
            } else if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
            {
                error = "Failed to catch the '" + field + "' is Highlighted in red .";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field");

        }

        return true;

    }

}
