/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Company Negative",
        createNewBrowserInstance = false
)
public class NegativeCompanyDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");
   

    public TestResult executeTest()
    {
        if(!companyDetails())
        {
            narrator.testFailed("Failed to perform company details container negative test -"+error);
        }
        
        return narrator.finalizeTest("Successfully performed the company details container negative test");
    }

    public boolean companyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.companyDetailsTab())))
        {
            error = "Failed to scroll to the 'application' button.";
            return false;
        }
        String isSolevent = getData("isCompany solvent");
        if (!isSolevent.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isCompanySolvent(), isSolevent))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("is the Company solvent : " + isSolevent);
        String mortgageLoan = getData("isCredit secured");
        if (!mortgageLoan.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isCreditSecure(), mortgageLoan))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("Is the Credit secured by a Mortgage loan : " + mortgageLoan);
        String cellNumber = getData("Cell number");
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyCellNumber()))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        if (!cellNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyCellNumber(), cellNumber))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("Cell Number : " + cellNumber);
        String email = getData("Email");
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyEmail()))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        if (!email.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyEmail(), email))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("E-Mail : " + email);
        String industrySector = getData("Sector of Industry");
        if (!industrySector.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sectorOfIndusrty(), industrySector))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("Sector of Industry : " + industrySector);
        String contactDesignation = getData("Designation");
        if (!contactDesignation.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.businessContactDesignation(), contactDesignation))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("Business Contact Designation : " + contactDesignation);
        String contactName = getData("Contact Name");
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessContactName()))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        if (!contactName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessContactName(), contactName))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("Business Contact Name : " + contactName);
        String preferredLanguage = getData("Preferred Language");
        if (!preferredLanguage.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyPreferredLanguage(), preferredLanguage))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("Preferred Language: " + preferredLanguage);
        String sourceIncome = getData("Source of Income");
        if (!sourceIncome.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companySourceOfIncome(), sourceIncome))
            {
                error = "Failed to scroll to the 'application' button.";
                return false;
            }
        }
        narrator.stepPassed("Source of Income: " + sourceIncome);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.companyDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
             if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field ");
           
        }

        return true;
    }

}
