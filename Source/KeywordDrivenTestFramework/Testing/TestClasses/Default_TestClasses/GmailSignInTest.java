/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Default_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SikuliDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Default_PageObjects.GmailPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Sign In To Gmail Account",
        createNewBrowserInstance = true
)
public class GmailSignInTest extends BaseClass
{

    String error = "";

    public GmailSignInTest()
    {

    }

    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the GMail URL
        if (!NavigateToGmailSignInPage())
        {
            return narrator.testFailed("Failed to navigate to Gmail");
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignInToGmailAccount())
        {
            return narrator.testFailed("Failed to Sign into Gmail");
        }

        return narrator.finalizeTest("Successfully Navigated and signed into Gmail");
    }

    public boolean NavigateToGmailSignInPage()
    {

        if (!SeleniumDriverInstance.navigateTo(GmailPageObject.GmailURL()))
        {
            error = "Failed to navigate to gmail.";
            return false;
        }

        return true;
    }

    public boolean SignInToGmailAccount()
    {

        String emailAddress = testData.getData("Email Address");
        String password = testData.getData("Password");

        if (!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.emailTextBoxXpath(), emailAddress))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }

        testData.extractParameter("Username Entered", emailAddress, "Pass");

        if (!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.newNextButtonXpath()))
        {
            error = "Failed to click next button.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.newPasswordTextBoxXpath(), password))
        {
            error = "Failed to enter password into password field.";
            return false;
        }

        testData.extractParameter("Password Entered", "[redacted]", "Warning");

        if (!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.newNextButtonXpath()))
        {
            error = "Failed to click sign in button.";
            return false;
        }

        // Must ensure that the compose button appears!
        if (!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.composeButtonXpath()))
        {
            error = "Failed to load the compose button";
            return false;
        }

        return true;

    }

}
