/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author sbachan
 */
public class LightstoneScriptingUtility extends BaseClass
{
    String error;
    public boolean maskHandling_Login()  
    {
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.mask_InboxScreen(), 5))
        {
            if (!SeleniumDriverInstance.waitForElementNoLongerPresentByXpath(LightStonePageObjects.mask_InboxScreen(),120))
            {
                error = "Failed to Handle Mask";
                Narrator.logError(error);
                return false;
            }
        }
        return true;
    }

    /**
     * take a string and capitalize the first character
     * @param input Text to capitalize
     * @return capitalized text
     */
    public String ucFirst(String input)
    {
        return input.toLowerCase().substring(0, 1).toUpperCase() + input.toLowerCase().substring(1);
    }
    
}
