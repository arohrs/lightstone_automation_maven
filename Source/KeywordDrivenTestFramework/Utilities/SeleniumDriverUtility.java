/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.DataRow;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.RetrievedTestValues;
import KeywordDrivenTestFramework.Reporting.Narrator;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author fnell
 * @editir jjoubert
 */
// Contains logic for handling accessor methods and driver calls.
public class SeleniumDriverUtility extends BaseClass
{

    public WebDriver Driver;
    private Enums.BrowserType browserType;
    File fileIEDriver;
    File fileChromeDriver;
    File filePhantomJsDriver;
    File fileEdgeDriver;
    public Boolean _isDriverRunning = false;
    public RetrievedTestValues retrievedTestValues;
    public String DriverExceptionDetail = "";
//    TestEntity testData;
    public Object document;
    public String mainWindowsHandle;

    EventFiringWebDriver eventDriver;
    public static final String DIR_DOWNLOADS = System.getProperty("user.home") + "//Downloads";

    public SeleniumDriverUtility(Enums.BrowserType selectedBrowser)
    {
        retrievedTestValues = new RetrievedTestValues();

        _isDriverRunning = false;
        browserType = selectedBrowser;

        fileIEDriver = new File("IEDriverServer.exe");
        System.setProperty("webdriver.ie.driver", fileIEDriver.getAbsolutePath());

        fileChromeDriver = new File("chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());

        fileEdgeDriver = new File("MicrosoftWebDriver.exe");
        System.setProperty("webdriver.edge.driver", fileEdgeDriver.getAbsolutePath());

        filePhantomJsDriver = new File("phantomjs.exe");
        System.setProperty("phantomjs.binary.path", filePhantomJsDriver.getAbsolutePath());

    }

    public boolean isDriverRunning()
    {
        return _isDriverRunning;
    }

    public void startDriver()
    {
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");

        switch (browserType)
        {
            case EDGE:
                Driver = new EdgeDriver();
                _isDriverRunning = true;
                break;
            case IE:
                Driver = new InternetExplorerDriver();
                _isDriverRunning = true;
                break;
            case FireFox:
                Driver = new FirefoxDriver();
                _isDriverRunning = true;
                Driver.manage().window().maximize();
                // <editor-fold defaultstate="collapsed" desc="Firefox Help">
/*
                 NOTE: Firefox doesn't allow implicit waits anymore.
                 Current gecko Driver: v0.14.0
                 Suggested Firefox: v52.9
                 Download from: https://ftp.mozilla.org/pub/firefox/releases/ or https://ftp.mozilla.org/pub/firefox/releases/52.9.0esr/win32/en-ZA/
                 */
// </editor-fold>
                break;
            case Chrome:
                Driver = new ChromeDriver();
                _isDriverRunning = true;
                break;

            case mobileChrome:
                AppiumDriverInstance = new AppiumDriverUtility();
                Driver = AppiumDriverInstance.Driver;
                _isDriverRunning = true;
                break;
            case Safari: ;
                break;
        }

        if (browserType != Enums.BrowserType.mobileChrome && browserType != Enums.BrowserType.FireFox)
        {
            retrievedTestValues = new RetrievedTestValues();
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            Driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
            Driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
            Driver.manage().window().maximize();
        }

    }

    public boolean navigateTo(String pageUrl)
    {
        try
        {
            Driver.navigate().to(pageUrl);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" navigating to URL  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean closeTab(String originalHandle)
    {
        try
        {

            Driver.getWindowHandles().stream().filter(handle -> !handle.equals(originalHandle)).forEach((handle)
                    ->
            {
                Driver.switchTo().window(handle);
                Driver.close();
            });

            Driver.switchTo().window(originalHandle);

            Narrator.logDebug("Close all tabs open expected " + originalHandle);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to switch to Close tabs expected '" + originalHandle + "'- " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean closeCurrentTab(String archiveTabHandle)
    {
        try
        {
            String DealFileTabHandle = Driver.getWindowHandle();
            Driver.switchTo().window(DealFileTabHandle);
            Driver.close();

            //switchToTabOrWindow();
            Driver.switchTo().window(archiveTabHandle);

            Narrator.logDebug("Successfully closed tab ");
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to close expected tab - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public String getWindowHandle()
    {
        try
        {
            return Driver.getWindowHandle();
        } catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve Window handle - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return null;
        }
    }

    public ArrayList<String> retrieveMultipleTextByXpath(String elementXpath)
    {//xpath should be direct to elements you want. 
        try
        {
            this.waitForElementPresentByXpath(elementXpath);

            List<WebElement> ElementData;
            ArrayList<String> textData = new ArrayList<>();

            ElementData = SeleniumDriverInstance.Driver.findElements(By.xpath(elementXpath));
            for (int i = 0; i < ElementData.size(); i++)
            {
                textData.add(ElementData.get(i).getText());
            }
            return textData;
        } catch (Exception e)
        {
            Narrator.logError("Failed to retrieve multiple text by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return null;
        }
    }

    public List<WebElement> retrieveWebElementByXpath(String elementXpath)
    {//xpath should be direct to elements you want. 
        try
        {
            this.waitForElementByXpath(elementXpath);

            return SeleniumDriverInstance.Driver.findElements(By.xpath(elementXpath));

        } catch (Exception e)
        {
            Narrator.logError("Failed to retrieve multiple webElement from xpath '" + elementXpath + "+' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return null;
        }
    }

    public ArrayList<String> retrieveMultipleAttributeByXpath(String elementXpath, String attribute)
    {//xpath should be direct to elements you want. 
        try
        {
            this.waitForElementByXpath(elementXpath);

            List<WebElement> ElementData;
            ArrayList<String> textData = new ArrayList<>();

            ElementData = SeleniumDriverInstance.Driver.findElements(By.xpath(elementXpath));
            for (int i = 0; i < ElementData.size(); i++)
            {
                textData.add(ElementData.get(i).getAttribute(attribute));
            }
            return textData;
        } catch (Exception e)
        {
            Narrator.logError("Failed to retrieve multiple " + attribute + " by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return null;
        }
    }

    public int retrieveNumberOfElementsByXpath(String elementXpath)
    {//xpath should be direct to elements you want to Count. 
        try
        {
            this.waitForElementPresentByXpath(elementXpath);

            List<WebElement> ElementData;
            ElementData = Driver.findElements(By.xpath(elementXpath));

            return ElementData.size();
        } catch (Exception e)
        {
            Narrator.logError("Failed to retrieve number of Elements for Xpath " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return 0;
        }
    }

    public boolean clickElementbyXpath(String elementXpath)
    {
        try
        {

            waitForElementByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
         //   wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            elementToClick.click();
            Narrator.logDebug("Clicked element by Xpath : " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean roboClickLatestElementbyXpath(String elementXpath)
    {
        try
        {
            List<WebElement> elementList = SeleniumDriverInstance.Driver.findElements(By.xpath(elementXpath));
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
        //    wait.until(ExpectedConditions.elementToBeClickable(elementList.get(elementList.size() - 1)));
            WebElement elementToClick = ((elementList.get(elementList.size() - 1)));
            elementToClick.click();
            Narrator.logDebug("Clicked element by Xpath : " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean roboWaitForLatestElementbyXpath(String elementXpath)
    {
        try
        {
            List<WebElement> elementList = SeleniumDriverInstance.Driver.findElements(By.xpath(elementXpath));
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
         //   wait.until(ExpectedConditions.elementToBeClickable(elementList.get(elementList.size() - 1)));
            //WebElement elementToClick = ((elementList.get(elementList.size()-1)));
            //elementToClick.click();
            Narrator.logDebug("Waited for element by Xpath : " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickWebElementUsingActions(WebElement elementToClick)
    {
        try
        {

            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
      //      wait.until(ExpectedConditions.visibilityOf(elementToClick));
        //    wait.until(ExpectedConditions.elementToBeClickable(elementToClick));

            Actions action = new Actions(Driver);
            action.moveToElement(elementToClick);
            action.click(elementToClick);
            action.perform();
            Narrator.logDebug(" Clicked WebElement : " + elementToClick.toString());

            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to click on WebElement - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyXpathUsingActions(String elementXpath)
    {
        try
        {
            waitForElementByXpath(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
         //   wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            Actions action = new Actions(Driver);
            action.moveToElement(elementToClick);
            action.click(elementToClick);
            action.perform();
            Narrator.logDebug(" Clicked element by Xpath : " + elementXpath);

            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to click on element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean doubleClickElementbyXpath(String elementLinkText)
    {
        try
        {
            //waitForElementByLinkText(elementLinkText);
            Actions act = new Actions(Driver);
            WebElement elementToClick = Driver.findElement(By.xpath(elementLinkText));

            act.doubleClick(elementToClick).perform();
            Narrator.logDebug(" Double-clicked element by Xpath : " + elementLinkText);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" failed to double click element by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyXpathUsingJavascript(String elementXpath)
    {
        try
        {
            this.waitForElementPresentByXpath(elementXpath);
            WebElement element = Driver.findElement(By.xpath(elementXpath));
            JavascriptExecutor executor = (JavascriptExecutor) Driver;
            executor.executeScript("arguments[0].click();", element);
//            executor.executeScript("var thisElement = document.evaluate(\"" + elementXpath + "\",document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;thisElement.click();");
            //            // <editor-fold defaultstate="collapsed" desc="JavaScript Code">
////var timelined = document.evaluate("//a[text()='Dashboard']",document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;timelined.click();
//// </editor-fold>
            Thread.sleep(2500);
            return true;
        } catch (Exception e)
        {
            System.out.println("Failed to click Element by Xpath using Javascript " + e.getMessage());
            return false;
        }
    }

    //Use MousePos.exe App to get Co-ordinates for co ordinate Related Methods
    public boolean ClickElementByXpathRobot(String elementXpath, int x, int y)//Use MousePos.exe App to get Co-ordinates
    {
        try
        {
            //Checking if element is on page
            this.waitForElementByXpath(elementXpath);
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));

            //Retreiving element coordinates
            Point coordinates = elementToClick.getLocation();
            Robot robot = new Robot();

            //Moving to Coordinate of element
            robot.mouseMove(coordinates.getX(), coordinates.getY()); //Does not Send Pointer to correct location
            robot.mouseMove(x, y);

            //Clicking Mouse
            pause(1500);
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            pauseInSeconds(1);

            //Moving mouse off for next click
            robot.mouseMove(x + 5, y + 5);

            Narrator.logInfo("[Info]Clicked element Successfully - " + elementXpath);
            return true;

        } catch (Exception e)
        {
            Narrator.logError("[Error] Failed to Click element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                  //  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    if (!isCucumberMobileTesting)
                    {
                    //    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    }
                   // if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        Narrator.logDebug(" Found element by Xpath : " + elementXpath);
                        break;
                    }
                } catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }
            if (waitCount == ApplicationConfig.WaitTimeout())
            {
                Narrator.logError("Reached TimeOut whilst waiting for element by Xpath '" + elementXpath + "'");

                return elementFound;
            }

        } catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementPresentByXpath(String elementXpath)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                 //   wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                   // if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        Narrator.logDebug(" Found element by Xpath : " + elementXpath);
                        break;
                    }
                } catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        } catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementPresentByXpath(String elementXpath, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                   // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    //if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        Narrator.logDebug(" Found element by Xpath : " + elementXpath);
                        break;
                    }
                } catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        } catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpath(String elementXpath, int timeout)
    {
        boolean elementFound = false;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                 //   wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                   // wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    //if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null)
                    {
                        elementFound = true;
                        Narrator.logDebug(" Found element by Xpath : " + elementXpath);
                        break;
                    }
                } catch (Exception e)
                {
                    elementFound = false;
                }
                //Thread.sleep(500);
                waitCount++;
            }

        } catch (Exception e)
        {
            Narrator.logError(" waiting for element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementNoLongerPresentByXpath(String elementXpath)
    {
        boolean elementNoLongerFound = false;
        try
        {
            int waitCount = 0;
            while (!elementNoLongerFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                  //  if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {//If wait passes, element is still present. 
                        elementNoLongerFound = false;
                    }
                } catch (Exception e)
                {//If Exception is thrown, element is not present anymore.
                    elementNoLongerFound = true;
                }
                waitCount++;
                pause(1000);//Pause required due to the nature of this method.
            }
        } catch (Exception e)
        {
            Narrator.logError(" Failed to wait for element to no longer be present by Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementNoLongerFound;
    }

    public boolean waitForElementNoLongerPresentByXpath(String elementXpath, int timeout)
    {
        boolean elementNoLongerFound = false;
        try
        {
            int waitCount = 0;
            while (!elementNoLongerFound && waitCount < timeout)
            {
                try
                {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                  //  if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null)
                    {//If wait passes, element is still present. 
                        elementNoLongerFound = false;
                    }
                } catch (Exception e)
                {//If Exception is thrown, element is not present anymore.
                    elementNoLongerFound = true;
                }
                waitCount++;
                pause(1000);//Pause required due to the nature of this method.
            }
        } catch (Exception e)
        {
            Narrator.logError(" Failed to wait for element to no longer be present by Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementNoLongerFound;
    }

    public boolean enterTextByXpath(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(textToEnter);
            Narrator.logDebug("Entered Text of: " + textToEnter + " to: " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    //implement still...
    public boolean roboEnterLastTextByXpath(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
//            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(textToEnter);
            Narrator.logDebug("Entered Text of: " + textToEnter + " to: " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByXpathUsingActions(String elementXpath, String textToEnter)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToTypeIn = Driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();
            Narrator.logDebug("Entered Text of: " + textToEnter + " to: " + elementXpath);

            return true;
        } catch (Exception e)
        {
            Narrator.logError(" entering text - " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectFromDropDownListUsingXpath(String elementXpath, String valueToSelect)
    {
        try
        {
            waitForElementByXpath(elementXpath);

            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            WebElement formxpath = Driver.findElement(By.xpath(elementXpath));
            Actions action = new Actions(Driver);
            action.moveToElement(formxpath);
            action.click(formxpath).perform();
            dropDownList.selectByVisibleText(valueToSelect);
            Narrator.logDebug("Selected Text of: " + valueToSelect + " from: " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" selecting from dropdownlist by text using Id  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean alertHandler()
    {
        try
        {
            Narrator.logDebug("Attempting to click OK in alert pop-up");
            // Get a handle to the open alert, prompt or confirmation
            Alert alert = Driver.switchTo().alert();
            // Get the text of the alert or prompt
            alert.getText();
            // And acknowledge the alert (equivalent to clicking "OK")
            alert.accept();
            Narrator.logDebug("Ok Clicked successfully...proceeding");
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" clicking OK in alert pop-up - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
     public String alertHandlerGetMessage()
    {
        try
        {
            Narrator.logDebug("Attempting to retrieve alert text pop-up");
            // Get a handle to the open alert, prompt or confirmation
            Alert alert = Driver.switchTo().alert();
            // Get the text of the alert or prompt
            Narrator.logDebug("retrieving alert message");
            return alert.getText();
        } catch (Exception e)
        {
            Narrator.logError(" clicking OK in alert pop-up - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return "";
        }
    }

    public String retrieveTextByXpath(String elementXpath)
    {
        String retrievedText = "";
        try
        {
            this.waitForElementPresentByXpath(elementXpath);
            WebElement elementToRead = Driver.findElement(By.xpath(elementXpath));
            retrievedText = elementToRead.getText();
            Narrator.logDebug("Text: " + retrievedText + " retrieved successfully from element - " + elementXpath);
            return retrievedText;

        } catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve text from element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveAttributeByXpath(String elementXpath, String Attribute)
    {
        String retrievedAttribute = "";
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToRead = Driver.findElement(By.xpath(elementXpath));
            pause(1000);
            retrievedAttribute = elementToRead.getAttribute(Attribute);
            Narrator.logDebug("Attribute retrieved successfully from element - " + elementXpath);
            return retrievedAttribute;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to retrieve attribute from element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedAttribute;
        }
    }

    public String retrieveTextByRobot(String elementXpath)
    {//untested.
        String retreivedText = "";
        try
        {
            waitForElementByXpath(elementXpath);//check that the element is clickable.
            clickElementbyXpath(elementXpath);
            pause(1000);
            pressKeyWithRobot("selectall");
            pause(500);
            pressKeyWithRobot("copy");
            //Getting text from Clipboard
            retreivedText = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
            return retreivedText;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to retreive text by Robot - " + e.getMessage());
            return retreivedText;
        }
    }

    public boolean pressKeyWithRobot(String key)
    {
        try
        {
            Robot robot = new Robot();
            pause(1500);

            switch (key.toLowerCase())
            {
                case "tab":
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                    break;
                case "enter":
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    break;
                case "closePage":
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_F4);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyRelease(KeyEvent.VK_F4);
                    break;
                case "copy":
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_C);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyRelease(KeyEvent.VK_C);
                    break;
                case "cut":
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_X);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyRelease(KeyEvent.VK_X);
                    break;
                case "paste":
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_V);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyRelease(KeyEvent.VK_V);
                    break;
                case "maximise":
                    robot.keyPress(KeyEvent.VK_WINDOWS);
                    robot.keyPress(KeyEvent.VK_UP);
                    robot.keyRelease(KeyEvent.VK_WINDOWS);
                    robot.keyRelease(KeyEvent.VK_UP);
                    break;
                case "escape":
                    robot.keyPress(KeyEvent.VK_ESCAPE);
                    robot.keyRelease(KeyEvent.VK_ESCAPE);
                case "selectall":
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_A);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyRelease(KeyEvent.VK_A);
                    break;
                default:
                    throw new AssertionError();
            }

            pauseInSeconds(1);
            Narrator.logDebug("Pressed Key Successfully - " + key);
            return true;

        } catch (Exception e)
        {
            Narrator.logError("[Error] Failed to Click element Key - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void pressKey(Keys keyToPress)
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(keyToPress);
            action.perform();
        } catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - " + keyToPress);
        }
    }

    public Boolean ValidateByAttribute(String elementXpath, String Attribute, String testData)
    {
        try
        {
            String attribute = retrieveAttributeByXpath(elementXpath, Attribute);
            String data = testData;

            if (!attribute.equals(data))
            {
                Narrator.logError("Failed to validate " + attribute + ", against " + data + ".");
                return false;
            }
            Narrator.logDebug("Validated by attribute value: " + attribute + ", successfully.");

            return true;

        } catch (Exception e)
        {
            Narrator.logError("Failed to Validate attribute against TestData - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public Boolean ValidateByText(String elementXpath, String testData)
    {
        try
        {
            String text = retrieveTextByXpath(elementXpath);
            String data = testData;

            if (!text.equals(data))
            {
                Narrator.logError("Failed to validate " + text + ", against " + data + ".");
                return false;
            }
            Narrator.logDebug("Validated by text value: " + text + ", successfully.");

            return true;

        } catch (Exception e)
        {
            Narrator.logError("Failed to Validate Text against TestData - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public boolean switchToFrameByXpath(String frameXpath)
    {
        int waitCount = 0;
        try
        {
            while (waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    Driver.switchTo().frame(frameXpath);
                    Narrator.logDebug("Switched to frame " + frameXpath);
                    return true;
                } catch (Exception e)
                {
                    //Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        } catch (Exception e)
        {
            Narrator.logError(" switching to frame by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContent()
    {
        try
        {
            Driver.switchTo().defaultContent();
            Narrator.logDebug("Switched to default content");
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" switching to default content  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public LinkedList<DataColumn> captureTableInformation(String TableXpath)
    {
        DataRow newRow = new DataRow();

        try
        {
            //Finds the Table
            List<WebElement> Header = Driver.findElements(By.xpath(TableXpath + "//th"));
            List<String> Headers = new ArrayList<>();

            for (int i = 0; i < Header.size(); i++)
            {
                String Head = retrieveTextByXpath(TableXpath + "//th[" + (i + 1) + "]");
                Headers.add(Head);
            }

            List<WebElement> Rows = Driver.findElements(By.xpath(TableXpath + "//tr"));

            for (WebElement Row : Rows)
            {
                List<WebElement> Cells = Row.findElements(By.xpath(TableXpath + "//td"));

                for (int i = 0; i < Cells.size(); i++)
                {

                    DataColumn newColumn = new DataColumn("", "", Enums.ResultStatus.UNCERTAIN);

                    newColumn.columnHeader = Headers.get((i % (Headers.size())));
                    newColumn.columnValue = Cells.get(i).getText();
                    newColumn.resultStatus = Enums.ResultStatus.UNCERTAIN;

                    newRow.DataColumns.add(newColumn);
                }

                return newRow.DataColumns;
            }

        } catch (Exception e)
        {
            System.err.println("Error Creating Table" + e);
            this.DriverExceptionDetail = "Error Creating Table";

        }
        return newRow.DataColumns;
    }

    public boolean SaveCookiesToFile(String filepath)
    {
        Set<Cookie> cookieSetReturn;

        Set<Cookie> cookies = Driver.manage().getCookies();

        String path = filepath + "\\Cookies.ser";
        try
        {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(cookies);
            out.close();
            fileOut.close();
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Failed to Add Cookies, error - " + e.getMessage());
            return false;
        }
    }

    public boolean AddCookiesFromFile(String filepath)
    {
        Set<Cookie> cookieSetReturn;

        try
        {
            //Reads the Cookie File
            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            cookieSetReturn = (Set<Cookie>) in.readObject();
            in.close();
            fileIn.close();

            for (Cookie getcookies : cookieSetReturn)
            {
                Driver.manage().addCookie(getcookies);

            }
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Failed to Add Cookies, error - " + e.getMessage());
            return false;
        }
    }

    public void takeScreenShot(String screenShotDescription, boolean isError)
    {
        String imageFilePathString = "";

        if (!isCucumberTesting)
        {
            if (testCaseId == null)
            {
                //return;
            }
        }

        try
        {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            imageFilePathBuilder.append(this.reportDirectory + "\\");

            relativeScreenShotPath = testCaseId + "\\";

            if (isError)
            {
                relativeScreenShotPath += "FAILED_";
            } else
            {
                relativeScreenShotPath += "PASSED_";
            }

            relativeScreenShotPath += screenShotDescription + ".png";

            imageFilePathBuilder.append(relativeScreenShotPath);

            //imageFilePathBuilder.append(this.generateDateTimeString() + ".png");
            imageFilePathString = imageFilePathBuilder.toString();

            setScreenshotPath(imageFilePathString);

            File screenShot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));
        } catch (Exception e)
        {
            Narrator.logError(" could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    public void shutDown()
    {
        retrievedTestValues = null;
        try
        {

            Driver.quit();

        } catch (Exception e)
        {
            Narrator.logError(" shutting down driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        _isDriverRunning = false;
    }

    public void copyKeys()
    {
        try
        {
            Actions action = new Actions(Driver);
            action.sendKeys(Keys.CONTROL, "c");
            action.perform();
        } catch (Exception e)
        {
            this.DriverExceptionDetail = e.getMessage();
            Narrator.logError(" Failed to send keypress to element - Contoll + C");

        }
    }

    public boolean downloadImgUsingURL(String imageURL, String destinationFile)
    {
        try
        {
            URL url = new URL(imageURL);
            System.setProperty("jsse.enableSNIExtension", "false");
            InputStream fis = url.openStream();
            OutputStream fos = new FileOutputStream(destinationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = fis.read(b)) != -1)
            {
                fos.write(b, 0, length);
            }

            fis.close();
            fos.close();
            return true;
        } catch (Exception e)
        {
            narrator.logError("Failed to download Image, error - " + e.getMessage());
            return false;
        }
    }

    public boolean switchToTabOrWindow()
    {
        try
        {
            String winHandleBefore = SeleniumDriverInstance.Driver.getWindowHandle();
            for (String winHandle : SeleniumDriverInstance.Driver.getWindowHandles())
            {
                SeleniumDriverInstance.Driver.switchTo().window(winHandle);
            }
        } catch (Exception ex)
        {
            Narrator.logError("Could not switch to new tab" + ex.getMessage());
            this.DriverExceptionDetail = ex.getMessage();
            return false;
        }
        Narrator.logDebug("Switched to window " + Driver.getTitle());
        return true;
    }

    public boolean switchToWindow(WebDriver driver, String title)
    {
        mainWindowsHandle = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles(); // Gets all the available windows
        for (String handle : handles)
        {
            driver.switchTo().window(handle); // switching back to each window in loop
            if (driver.getTitle().equals(title)) // Compare title and if title matches stop loop and return true
            {
                Narrator.logDebug("Switched to window " + Driver.getTitle());
                return true; // We switched to window, so stop the loop and come out of funcation with positive response
            }
        }
        driver.switchTo().window(mainWindowsHandle); // Switch back to original window handle
        return false; // Return false as failed to find window with given title.
    }

    public static String getCurrentDate()
    {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public boolean scrollToElement(String elementXpath)
    {
        try
        {
            WebElement element = Driver.findElement(By.xpath(elementXpath));
            ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", element);
            Narrator.logDebug("Scrolled to element - " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Error scrolling to element - " + elementXpath + " - " + e.getStackTrace());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public boolean alertHandlerUsingJavaScript()
    {//https://stackoverflow.com/questions/39398226/how-to-handle-alert-pop-up-with-javascript-selenium-webdriver
        try
        {
            Narrator.logDebug("Attempting to click OK in alert pop-up Using JavaScript");

            JavascriptExecutor executor = (JavascriptExecutor) Driver;
            executor.executeScript("window.confirm = function() { return true;}");

            Narrator.logDebug("Ok Clicked successfully...proceeding");
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" clicking OK in alert pop-up - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean dismissAlert()
    {

        {
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    Alert alert = Driver.switchTo().alert();
                    alert.dismiss();
                    break;
                } catch (NoAlertPresentException e)
                {
                    SeleniumDriverInstance.pause(1000);
                }
            }
        }
        return true;
    }

    public boolean downloadLinkText(String linkText)
    {
        try
        {
            WebElement downloadCNALink = SeleniumDriverInstance.Driver.findElements(By.linkText(linkText)).get(0);
            JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
            js.executeScript("arguments[0].setAttribute(arguments[1],arguments[2])", downloadCNALink, "download", "");
            js.executeScript("arguments[0].setAttribute(arguments[1],arguments[2])", downloadCNALink, "target", "_blank");
            SeleniumDriverInstance.clickWebElementUsingActions(downloadCNALink);
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Failed to click element by link text '" + linkText + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public boolean clearTextByXpath(String elementXpath)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            WebElement elementToClear = Driver.findElement(By.xpath(elementXpath));
            elementToClear.sendKeys(Keys.chord(Keys.CONTROL, "a"), "");
            elementToClear.sendKeys(Keys.chord(Keys.DELETE));
            elementToClear.clear();
            Narrator.logDebug("Cleared Text Successfully: " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to Clear Text- " + elementXpath + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean isSelected(String elementXpath)
    {
        try
        {
            this.waitForElementByXpath(elementXpath);
            //this.waitUntilElementEnabledByID(elementXpath);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
           // wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement checkBox = Driver.findElement(By.xpath(elementXpath));
            if (checkBox.isSelected())
            {
                return true;
            } else
            {
                return false;
            }

        } catch (Exception e)
        {
            Narrator.logError(" selecting checkbox byId - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public static String getTimeForwardInDays(int daysForward, String dateFormat)
    {
        try
        {
            Calendar now = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat(dateFormat);//e.g."kk:mm"
            now.add(Calendar.DATE, daysForward);
            return df.format(now.getTime());

        } catch (Exception e)
        {
            //Narrator.log error here
            return null;
        }
    }

    public boolean updateElementAttributeByXpath(String elementXpath, String attributeName, String attributeValue)
    {//https://stackoverflow.com/questions/8473024/selenium-can-i-set-any-of-the-attribute-value-of-a-webelement-in-selenium
        try
        {
            waitForElementPresentByXpath(elementXpath);
            //convert xpath to WebElement
            WebElement element = Driver.findElement(By.xpath(elementXpath));
            //Extract ElementID
            String elementID = element.getAttribute("id");
            //Extract ElementClass
            String elementClass = element.getAttribute("class");
            //Create JavaScript Executor Object
            JavascriptExecutor executor = (JavascriptExecutor) Driver;
            //Try to enter with ID first. 
            if (!elementID.isEmpty())
            {
                executor.executeScript("document.getElementById('" + elementID + "').setAttribute('" + attributeName + "', '" + attributeValue + "')");
                //js.executeScript("document.getElementById('//id of element').setAttribute('attr', '10')");
            }//If no ID, try Class
            else if (!elementClass.isEmpty())
            {
                executor.executeScript("document.getElementsByClassName('" + elementClass + "')[0].setAttribute('" + attributeName + "', '" + attributeValue + "')");
            }//If neither, Try without them
            else
            {
                executor.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);", element, attributeName, attributeValue);
            }
            pause(2500);
            return true;
        } catch (Exception e)
        {
            Narrator.logError(" Failed to Update Element Attribute by Javascript - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean isFileInFolder(String filepath, String fileName)
    {
        boolean fileFound = false;
        try
        {
//retreiving list of all files in folder
            ArrayList<File> fileList = new ArrayList<>(Arrays.asList(new File(filepath).listFiles()));
            System.out.println("No. of files found in " + filepath + " : " + fileList.size());
//Iterating through list 
// fileFound = fileList.stream().anyMatch(s -> fileName.toLowerCase().contains(s.getName().toLowerCase()));
            fileFound = fileList.stream().anyMatch(s -> s.getName().toLowerCase().contains(fileName.toLowerCase()));

//<editor-fold defaultstate="collapsed" desc="Commented out For Loop Version">
// for (File file : fileList)
// {
// String currentFileName = file.getName();
// //to check whether our build is present
// if (fileNameWithExtension.toLowerCase().contains(currentFileName.toLowerCase()))
// {
// out.println("!!File Found!! : " + currentFileName);
// fileFound = true;
// break;
// }
// }
//</editor-fold>
        } catch (Exception e)
        {
            fileFound = false;
            narrator.logError("Failed To check if File is in Folder - " + e.getMessage());
        }
        return fileFound;
    }

    public boolean moveFileToCurrentDirectory(String fileName)
    {
        try
        {
            String filePathFrom = SeleniumDriverInstance.DIR_DOWNLOADS;
            String filePathTo = reportDirectory;

            List<File> downloadsFileList = new ArrayList<>(Arrays.asList(new File(filePathFrom).listFiles()));
            File fileToMove = downloadsFileList.stream().filter(s -> s.getName().toLowerCase().contains(fileName.toLowerCase())).collect(Collectors.toList()).get(0);
            
            
            if (!fileToMove.renameTo(new File(filePathTo + "\\" + fileToMove.getName())))
            {
                Narrator.logError("Failed to move file '" + fileName);
                return false;
            }

            if (!isFileInFolder(filePathTo, fileName))
            {
                Narrator.logError("Failed to validate File has been moved '" + fileName);
                return false;
            }

// //<editor-fold defaultstate="collapsed" desc="//old method">
////File folder = new File(System.getProperty("user.home") + "\\Downloads");
// File[] listOfFiles = folder.listFiles();
// File source = new File(System.getProperty("user.home") + "\\Downloads\\" + fileName);
// if (!source.renameTo(new File(reportDirectory + "\\" + source.getName())))
// {
// Narrator.logError("Failed to move file '" + fileName);
// return false;
// } else
// {
// System.out.println("Successfully moved file");
// }
//</editor-fold>
            pause(2000);
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Failed to move file " + e.getMessage());
            return false;
        }
    }

    public boolean deleteAllFilesInFolder(String filePath, String fileName)
    {
        try
        {
//deleting all files in downloads folder contianing the word 'fixtures' 
// //<editor-fold defaultstate="collapsed" desc="For Loop Version">
//breakdown of DL FileList
// List<File> fileList;//create OBJ of fileList
// File[] dlArray = new File(SeleniumDriverInstance.DIR_DOWNLOADS).listFiles();//Stored Array of files in DL folder
// ArrayList<File> dlList = new ArrayList<>(Arrays.asList(dlArray));//converted Array to ArrayList
// fileList = dlList;//Stored ArrayList into List
//
// List<File> downloadsFileList = new ArrayList<>(Arrays.asList(new File(SeleniumDriverInstance.DIR_DOWNLOADS).listFiles()));
//
// //For Loop Version
// for (int i = 0; i < downloadsFileList.size(); i++)
// {
// File currentFile = downloadsFileList.get(i);
// if (currentFile.getName().toLowerCase().contains("fixtures"))
// {
// fixturesFileList.add(currentFile);
// }
// }
//
// //For-Each / Enhanced For Loop Version
// for (File currentFile : downloadsFileList)
// {
// if (currentFile.getName().toLowerCase().contains("fixtures"))
// {
// if (!currentFile.delete())
// {
// narrator.logError("Failed to delete fixture file - " + currentFile.getName());
// return false;
// }
// }
// }
//</editor-fold>

            List<File> downloadsFileList = new ArrayList<>(Arrays.asList(new File(SeleniumDriverInstance.DIR_DOWNLOADS).listFiles()));
            downloadsFileList.stream().filter(s -> s.getName().toLowerCase().contains(fileName.toLowerCase())).forEach(s -> s.delete());
            pause(1000);
            if (isFileInFolder(filePath, fileName))
            {
                narrator.logError("Failed to Delete All files in Folder - " + fileName);
                return false;
            }

            narrator.logDebug("Successfully Deleted All Files in Folder - " + fileName);
            return true;
        } catch (Exception e)
        {
            narrator.logError("Failed to Delete All files in Folder - " + fileName);
            return false;
        }
    }

    public boolean waitForOneElementLeftByXpath(String elementXpath)
    {   //Method made to handle SaleElementException
        //http://darrellgrainger.blogspot.co.za/2012/06/staleelementexception.html 
        boolean elementFound = false;
        List<WebElement> elementData;
        try
        {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout())
            {
                try
                {
                    int elementsLeft = Driver.findElements(By.xpath(elementXpath)).size();
                    if (elementsLeft == 1)
                    {
                        elementFound = true;
                        Narrator.logDebug(" Found ONE element by Xpath : " + elementXpath);
                        break;
                    }
                } catch (Exception e)
                {
                    elementFound = false;
                }
                Thread.sleep(1000);
                waitCount++;
            }

        } catch (Exception e)
        {
            Narrator.logError(" Failed to wait for One element by Xpath '" + elementXpath + "' - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean hoverOverElementByXpath(String elementXpath)
    {
        try
        {
            this.waitForElementPresentByXpath(elementXpath);

            Actions hoverTo = new Actions(Driver);
            hoverTo.moveToElement(Driver.findElement(By.xpath(elementXpath)));
            hoverTo.perform();

            Narrator.logInfo("Hovered over element - " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Failed to hover over element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
   public boolean dragDropElementByXpathToPixel(String elementXpath, int x, int y)//x and y: be positive and negative 
    {//http://www.software-testing-tutorials-automation.com/2014/10/selenium-webdriver-drag-and-drop.html
        try
        {
            this.waitForElementByXpath(elementXpath);

            WebElement dragElementFrom = Driver.findElement(By.xpath(elementXpath));
            Actions dragDrop = new Actions(Driver);
            dragDrop.dragAndDropBy(dragElementFrom, x, y).build().perform();

            Narrator.logInfo("Draged and Dropped element - " + elementXpath);
            return true;
        } catch (Exception e)
        {
            Narrator.logError("Failed to Drag and Drop element Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
    
  public String ReadReportFile(String fileppath)
    {
        String pdfContent = "";
        try
        {
            File file = new File(fileppath);

            PDDocument pdfDoc = PDDocument.load(file);
            PDFTextStripper pdfStripper = new PDFTextStripper();

            pdfContent = pdfStripper.getText(pdfDoc);

            pdfDoc.close();

        }
        catch (Exception | Error e)
        {
            return null;
        }

        return pdfContent;
    }
    
    public boolean moveFileToReportDirectory(String filePath, String newFilePath)
    {
        File filename = new File(filePath);
        try
        {
            if (!filename.renameTo(new File(newFilePath)))
            {
                Narrator.logError("Failed to move file '" + filename.getName() + "'");
                return false;

            }
            else
            {
                System.out.println("Successfully moved file");
            }
            pause(1000);
            return true;
        }
        catch (Exception e)
        {
            Narrator.logError("Failed to move file: " + e.getMessage());
            return false;
        }
    }
    

}
