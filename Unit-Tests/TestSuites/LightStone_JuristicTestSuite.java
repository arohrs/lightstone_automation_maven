/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.requiresBrowser;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import static TestSuites.LightStone_POCTestSuite.instance;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author pkankolongo
 */
public class LightStone_JuristicTestSuite extends BaseClass
{
    static TestMarshall instance;
    public static Enums.DeviceConfig test;
    
    public LightStone_JuristicTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
        
    
    }
    @Test
    public void executeLightStoneJuristic() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStoneJuristic Application  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-79 JuristicApplication.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
    
    }
    
    @Test
    public void executeLightStoneJuristicNegative() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStoneJuristic Application (Negative)  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-80 JuristicApplication_Negative.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
    
    }
    
    
    
}
