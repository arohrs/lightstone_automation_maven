
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author nkelechi
 */
public class Lightstone_LoginTestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_LoginTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void loginTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Login Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\LightstoneRegressionFolder\\001 LIG-34 Lightstone Login.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}

