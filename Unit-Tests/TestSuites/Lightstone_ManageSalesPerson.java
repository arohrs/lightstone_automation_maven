
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
// * @author nkelechi
 */
public class Lightstone_ManageSalesPerson extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_ManageSalesPerson()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void ManageSalepersonTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Manage Salesperson Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-52 ManageSalesPerson.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}

