
package TestSuites.Default_TestSuite;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class GMAILTestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public GMAILTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;
        //Comment lines below for Standard GMAIL test
//        TestMarshall.currentDevice = Enums.Device.ZebraMC40;
//        TestMarshall.currentDeviceConfig = Enums.DeviceConfig.Doddle_Facelift_MC40;
//        TestMarshall.currentPlatform = Enums.MobilePlatform.Android;
        //*******************************************
    }

    @Test
    public void GMAILTEST() throws FileNotFoundException
    {
        Narrator.logDebug("GMAIL - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\GMAIL TEST PACK.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void MobileTest() throws FileNotFoundException
    {
        
        Narrator.logDebug("GMAIL - Scenario 1  - Test Pack");
        currentDevice = Enums.Device.Emulator;
        currentDeviceConfig = Enums.DeviceConfig.Test;
        
        instance = new TestMarshall("TestPacks\\GMAIL TEST PACK.xlsx", Enums.BrowserType.mobileChrome);
        instance.runKeywordDrivenTests();
    }

    

}

