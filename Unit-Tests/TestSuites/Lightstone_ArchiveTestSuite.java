
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
// * @author nkelechi
 */
public class Lightstone_ArchiveTestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_ArchiveTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void ArchiveTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Archive Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\Archive Test Pack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void LIG_45_ArchiveTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Archive Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-45 Archive_TestPack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}

