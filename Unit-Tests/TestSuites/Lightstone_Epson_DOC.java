package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class Lightstone_Epson_DOC extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_Epson_DOC()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void executeLightStone_DOC() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-48 DOC.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void executeLightStone_MonthlyDOC() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-49 MonthlyDOC.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void executeLightStone_RecurringIncome() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("RecurringIncome  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-50 RecurringIncome.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
     @Test
    public void executeLightStone_RecurringIncomeIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("RecurringIncome  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-50 RecurringIncome.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
     @Test
    public void executeLightStone_NegativeRecurringIncome() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Negative RecurringIncome  - Test Pack");
        instance = new TestMarshall("TestPacks\\Negative_RecurringIncome.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
