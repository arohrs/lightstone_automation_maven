package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class Lightstone_InboxApp_TestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_InboxApp_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void inboxTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Inbox Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\INBOX TEST PACK.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void executeLightStone_SignioApplication() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-57 SignioApp-New Application_Negative.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void executeLightStone_WesbankReApplication() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Wesbank Re-Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-43 Wesbank Re-Application.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void executeLightStone_DVTApplication() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-54 Complete All Fields Application.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
      @Test
    public void executeLightStone_DVTApplicationIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-54 Complete All Fields Application.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
     @Test
    public void executeLightStone_AfterMarket() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-55 AfterMarket.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void executeLightStone_AfterMarketIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-55 AfterMarket.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
    
}
