
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
// * @author nkelechi
 */
public class Lightstone_TestSuite4PodiumAbsa extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_TestSuite4PodiumAbsa()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void ArchiveTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Archive Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\Podium Test Pack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}

