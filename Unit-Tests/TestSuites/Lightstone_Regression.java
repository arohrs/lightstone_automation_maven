package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Reporting.TestReportEmailerUtility;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class Lightstone_Regression extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_Regression()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
        TestReportEmailerUtility.setEmailRecipients("sbachan@dvt.co.za;tknoetzen@dvt.co.za;pkankologo@dvt.co.za;");
    }

    @Test
    public void executeLightStoneRegression() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\LightstoneRegressionFolder", Enums.BrowserType.Chrome, "true");
        instance.runKeywordDrivenTests();
    }
    
}
