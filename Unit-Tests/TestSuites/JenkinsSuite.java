/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.TestReportEmailerUtility;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import static java.lang.System.out;
import java.util.Properties;
import org.junit.Test;

/**
 *
 * @author fnell
 */
public class JenkinsSuite 
{
 static TestMarshall instance;
    
    @Test
    public void executeLightStone() throws FileNotFoundException, IOException
    { 
        System.out.println("Starting Jenkins Test Suite");
        Properties props = System.getProperties();

        String browser = props.getProperty("Browser");
        String testpack = props.getProperty("TestPack");
        String zipToRemote = props.getProperty("ZipToRemote");
        zipToRemote = (zipToRemote != null) ? zipToRemote.toLowerCase() : zipToRemote;
        String environment = props.getProperty("Environment");
        String regressionEmailList = props.getProperty("RegressionEmailList");

//        browser = "Chrome";
//        testpack = "TempFolder";
//        environment = "LightStone";
//        zipToRemote = "true";
//        regressionEmailList = "sbachan@dvt.co.za";
        
        out.println(props.stringPropertyNames());
        out.println("[INFO] Executing tests: Browser - " + browser + ", Environment - " + environment + ", Test Pack - " + testpack + ", ZipToRemote = " + zipToRemote + ", regressionEmailList" + regressionEmailList);
        
        ApplicationConfig appConfig = new ApplicationConfig();
        
        instance = new TestMarshall("TestPacks\\" + testpack, appConfig.resolveBrowserType(browser), zipToRemote);
        
        TestMarshall.currentEnvironment = Enums.resolveTestEnvironment(environment);
        TestReportEmailerUtility.setEmailRecipients(regressionEmailList);
        out.println("[INFO] Executing tests: Browser - " + browser + ", Environment - " + environment + ", Test Pack - " + testpack);
//        instance.getKeywordTestClass("Bulk run");
        instance.runKeywordDrivenTests();
    }   
}
