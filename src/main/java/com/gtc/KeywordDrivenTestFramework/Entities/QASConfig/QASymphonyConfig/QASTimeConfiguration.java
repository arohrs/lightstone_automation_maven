/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyConfig;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author ZGlenn
 */
public class QASTimeConfiguration {
    
    private String time;

    public String getUnixTime() {
        long myTime = System.currentTimeMillis();
        long milli = myTime % 1000;
        long seconds = myTime / 1000 % 60;
        long min = myTime / (60 * 1000) % 60;
        long hours = myTime / (60 * 60 * 1000) % 24;

        time = getHours(hours) + ":" + getMins(min) + ":" + getSeconds(seconds) + "." + getMilliSeconds(milli) + "Z";
        return time;
    }

    public String getDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now).concat("T");
    }

    public String getQASDateFormat() {
        return getDate() + getUnixTime();
    }

    public String getHours(long hours) {
        if (hours > 9) {
            return String.valueOf(hours);
        } else {
            return "0" + String.valueOf(hours);
        }
    }

    public String getMins(long min) {
        if (min > 9) {
            return String.valueOf(min);
        } else {
            return "0" + String.valueOf(min);
        }
    }

    public String getSeconds(long sec) {
        if (sec > 9) {
            return String.valueOf(sec);
        } else {
            return "0" + String.valueOf(sec);
        }

    }

    public String getMilliSeconds(long milli) {
        if (milli < 10) {
            return "00" + String.valueOf(milli);
        } else if (milli < 100) {
            return "0" + String.valueOf(milli);
        } else {
            return String.valueOf(milli);
        }
    }
}