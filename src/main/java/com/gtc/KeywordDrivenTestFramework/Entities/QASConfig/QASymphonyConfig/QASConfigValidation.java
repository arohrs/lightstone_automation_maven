/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyConfig;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.validator.routines.UrlValidator;

/**
 *
 * @author ZGlenn
 */
public class QASConfigValidation {

    private final String WEBHOOK_KEY = "Webhook";
    private final String TEST_CYCLE_ID_KEY = "TestCycleID";
    private final String PROJECT_ID_KEY = "ProjectID";
    private final String MODULE_NAME_KEY = "ModuleName";
    private final String KEY_NOT_FOUND = "NONE";
    private InputStream propertiesInputStream;
    private Properties qasProp;
    private QASInfo info;
    private String temp;

    public QASConfigValidation(String QAPropFilePath) {
        try {
            propertiesInputStream = new FileInputStream(QAPropFilePath);
            qasProp = new Properties();
            qasProp.load(propertiesInputStream);
            info = new QASInfo();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(QASConfigValidation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(QASConfigValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean validateWebhook() {
        temp = qasProp.getProperty(WEBHOOK_KEY, KEY_NOT_FOUND);
        if (temp.equals(KEY_NOT_FOUND)) {
            return false;
        } else {
            if (validateWebhook(temp)) {
                info.setWebhook(temp);
                return true;
            } else {
                return false;
            }

        }

    }

    private boolean validateTCID() {
        temp = qasProp.getProperty(TEST_CYCLE_ID_KEY, KEY_NOT_FOUND);
        if (temp.equals(KEY_NOT_FOUND)) {
            return false;
        } else {
            if (validateTestCycleID(temp)) {
                info.setTestCycleID(temp);
                return true;
            } else {
                return false;
            }
        }
    }

    private boolean validatePID() {
        temp = qasProp.getProperty(PROJECT_ID_KEY, KEY_NOT_FOUND);
        if (temp.equals(KEY_NOT_FOUND)) {
            return false;
        } else {
            if (validateProjectID(temp)) {
                info.setProjectID(temp);
                return true;
            } else {
                return false;
            }

        }
    }

    public boolean validateModulName() {
        temp = qasProp.getProperty(MODULE_NAME_KEY, KEY_NOT_FOUND);
        if (temp.equals(KEY_NOT_FOUND)) {
            return false;
        } else {
            if (validatemn(temp)) {
                info.setModuleName(temp);
                return true;
            } else {
                return false;
            }
        }

    }

    private boolean validateTestCycleID(String numbers) {
        return numbers.matches("[0-9]+");
    }

    /*
    Validates pid with regects and logs errors
     */
    private boolean validateProjectID(String numbers) {
        return numbers.matches("[0-9]+");
    }

    private boolean validateWebhook(String webhook) {
        String[] customSchemes = {"////", "https"};
        UrlValidator customValidator = new UrlValidator(customSchemes);
        return customValidator.isValid(webhook);
    }

    private boolean validatemn(String moduleName) {
        return !(moduleName == null || moduleName.equals("null") || moduleName.length() == 0);
    }
    //******//

    public boolean validateALlConfig() {
        boolean webhook = validateWebhook();
        boolean tcid = validateTCID();
        boolean pid = validatePID();
        boolean md = validateModulName();
        return webhook && tcid && pid && md;
    }

    public QASInfo getInfo() {
        return info;
    }

}