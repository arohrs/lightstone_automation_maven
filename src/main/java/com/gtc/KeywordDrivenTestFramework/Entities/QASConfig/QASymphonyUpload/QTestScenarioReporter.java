/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyUpload;

/*
    Ferdinand && ZGlenn
*/


import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;

public class QTestScenarioReporter {

    HttpClient httpClient;

    public QTestScenarioReporter () {
        httpClient = HttpClientBuilder.create().build();
    }

 
   
    public void PushJSONReportToJira(File reportFile, long testCycleID, long projectID , String module_name, String webhook,String startDate, String endDate)
    {
        try
        {
//            HttpPost request = new HttpPost("https://pulse.qas-labs.com/api/v1/webhooks/WmJcKoDnBfNitqcZC/zE7zh6RhmgwYKCb49/c015f130-17d3-4858-9309-06580b11d957");
            HttpPost request = new HttpPost(webhook);
            ObjectMapper mapper = new ObjectMapper();
         
            // add request heade
            request.addHeader("Content-Type", "application/json");
            request.addHeader("Accept-Type", "application/json");
            execution qtTestExec = new execution();
            
            qtTestExec.projectid = projectID;
            qtTestExec.test_cycle = testCycleID;
            qtTestExec.module_name = module_name;
            //Current TIme plus 2
            //qtTestExec.start_date = "\"2018-06-13T12:19:31.999Z\"";
            qtTestExec.start_date = "\""+startDate+"\"";
           // qtTestExec.end_date = "\"2018-06-13T12:23:31.999Z\"";
            qtTestExec.end_date = "\""+endDate+"\"";
            String jsonTxt = new String(Files.readAllBytes(reportFile.toPath()));
  
            JSONArray array = new JSONArray(jsonTxt); 
           
            
            String jsonResultsString = array.toString();
    
            qtTestExec.result = jsonResultsString;
            
            String requestBody = mapper.writeValueAsString(qtTestExec);
            
            requestBody = requestBody.replace("test_cycle", "test-cycle");
            
            System.out.println(requestBody);  
   
            request.setEntity(new StringEntity(requestBody));  
               
            HttpResponse response = httpClient.execute(request);  
            
            if(response.getStatusLine().getStatusCode() != HttpStatus.OK_200)
            {
                System.out.println("[ERROR] QTest API - Response code - " + response.getStatusLine());
               
            }
            else
               System.out.println("[INFO] QTest Request sent - " + EntityUtils.toString(response.getEntity())) ;
                
        }
        catch(IOException | ParseException | JSONException ex)
        {
            System.out.println("[ERROR] QTest API - Response code - " + ex.getMessage());
        }
    }
}

