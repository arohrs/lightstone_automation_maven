/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyUpload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRawValue;
import java.io.Serializable;

/**
 *
 * @author Ferdinand && ZGlenn
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class execution implements Serializable
{

    public long test_cycle;
    public long projectid;
    
     @JsonRawValue
     public String module_name;
     
     @JsonRawValue
     public String start_date;
     
     @JsonRawValue
     public String end_date;
     
    @JsonRawValue
    public String result;
    
    
    public execution()
    {
           }

}
