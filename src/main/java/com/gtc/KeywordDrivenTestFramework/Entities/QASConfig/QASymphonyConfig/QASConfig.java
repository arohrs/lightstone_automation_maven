/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyConfig;

import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import java.io.File;

/**
 *
 * @author ZGlenn
 */
public class QASConfig {

    private final String PATH_TO_QAS;
    private final File QASConfigFile;
    private QASConfigValidation validate;

    public QASConfig(String PathToQASConfig) {
        PATH_TO_QAS = PathToQASConfig;
        QASConfigFile = new File(PATH_TO_QAS);
        if(!validateQASFile()){
            System.exit(1);
        }
    }
    
    public QASConfig(Enums.QAS QASConfiguration)
    {
        PATH_TO_QAS = "";
        QASConfigFile = null;
    }
    
    public QASConfigValidation getConfigValidation() {
        return validate;
    }
    

    private boolean validateQASFile() {
        if (!QASConfigFile.exists()) {
            QASConfigFile.mkdirs();
            return false;
        } else {
            validate = new QASConfigValidation(PATH_TO_QAS);
            boolean checkAll = validate.validateALlConfig();
            if (checkAll) {
                return true;
            }
        }
        return false;
    }
}
