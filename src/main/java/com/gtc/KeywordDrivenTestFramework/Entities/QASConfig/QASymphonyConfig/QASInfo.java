/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyConfig;

import com.gtc.KeywordDrivenTestFramework.Entities.Enums;


/**
 *
 * @author ZGlenn
 */
public class QASInfo {
    private String Webhook;
    private int TestCycleID;
    private int  ProjectID;
    private QASTimeConfiguration qasTime;
    private String moduleName;

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public QASInfo() {
    }
    
    public QASInfo(Enums.QAS QASConfiguration)
    {
        this.Webhook = QASConfiguration.Webhook;
        this.TestCycleID = QASConfiguration.TestCycle;
        this.ProjectID = QASConfiguration.ProjectID;
        this.moduleName = QASConfiguration.ModuleName;
    }

    public String getWebhook() {
        return Webhook;
    }

    public void setWebhook(String Webhook) {
        this.Webhook = Webhook;
    }

    public int getTestCycleID() {
        return TestCycleID;
    }

    public void setTestCycleID(String TestCycleID) {
        this.TestCycleID = Integer.valueOf(TestCycleID);
    }

    public int getProjectID() {
        return ProjectID;
    }

    public void setProjectID(String ProjectID) {
        this.ProjectID = Integer.valueOf(ProjectID);
    }
    public String getDateTime(){
        qasTime = new QASTimeConfiguration();
        return qasTime.getQASDateFormat();
    }
    
    
}
