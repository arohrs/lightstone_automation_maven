/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyUpload;


import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyConfig.QASConfig;
import com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyConfig.QASInfo;
import java.io.File;

/**
 *
 *  zglenn
 */
public class RunQTestUpload {

    private final QASConfig qas;
    private final QASInfo info;
    private String startTime;
    private String endTime;

    public RunQTestUpload(String pathToQAS) {
        
        qas = new QASConfig(pathToQAS);
        info = qas.getConfigValidation().getInfo();
    }
    
    public RunQTestUpload(Enums.QAS QASConfiguration) {
        
        qas = new QASConfig(QASConfiguration);
        info = qas.getConfigValidation().getInfo();
    }

    public void setStartTime() {
        startTime = info.getDateTime();
    }

    public void setEndTime() {
        endTime = info.getDateTime();
    }

    public void run() {
        QTestScenarioReporter qtestReporter = new QTestScenarioReporter();
        qtestReporter.PushJSONReportToJira(new File("target\\cucumber.json"), info.getTestCycleID(), info.getProjectID(), "\"" + info.getModuleName() + "\"", info.getWebhook(), startTime, endTime);
    }
     public void tdd_run(File Payload) {
        QTestScenarioReporter qtestReporter = new QTestScenarioReporter();
        qtestReporter.PushJSONReportToJira(Payload, info.getTestCycleID(), info.getProjectID(), "\"" + info.getModuleName() + "\"", info.getWebhook(), startTime, endTime);
    }
}
