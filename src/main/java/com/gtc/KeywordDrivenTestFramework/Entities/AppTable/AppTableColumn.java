/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Entities.AppTable;

import org.openqa.selenium.WebElement;

/**
 *
 * @author sbeck
 */
public class AppTableColumn
{
    public String columnName;
    public String cellText;
    public WebElement element;
        
    
    public AppTableColumn(String column, String text, WebElement webElement)
    {
        this.columnName = column;
        this.cellText = text;
        this.element = webElement;
    }
}
