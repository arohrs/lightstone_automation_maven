/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Default_Folder;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.DataRow;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import static com.gtc.KeywordDrivenTestFramework.Entities.Enums.ResultStatus.FAIL;
import static com.gtc.KeywordDrivenTestFramework.Entities.Enums.ResultStatus.PASS;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Reporting.ReportGenerator;
import com.gtc.KeywordDrivenTestFramework.Utilities.DataBaseUtility;
import com.gtc.KeywordDrivenTestFramework.Utilities.DataTableUtility;
import static java.lang.System.err;
import java.util.LinkedList;

/**
 *
 * @author Ferdinand
 */
@KeywordAnnotation(
        Keyword = "ETL Data Compare",
        createNewBrowserInstance = false
)
public class ETLDataCompare extends BaseClass {

    String error = "";


    LinkedList<DataRow> sourceData, destinationData;

    public ETLDataCompare() 
    {

    }

    public TestResult executeTest() 
    {
        if (!ExtractDataSources()) 
        {
            return narrator.testFailed("Failed extract data sets from Source and Destination queries");
        }
        
        if (!ValidateETLDataSets()) 
        {
            return narrator.testFailed("Failed validate data sets from Source and Destination queries");
        }

        return narrator.finalizeTest("Successfully validated ETL data sets");
    }

    public boolean ExtractDataSources() 
    {
        try
            
        {
            DataBaseUtility dbUtil = new DataBaseUtility();
            sourceData = new LinkedList<>();
            destinationData = new LinkedList<>();

            String sourceSQLStatement = narrator.getData("Source SQL Query");
            String destinationSQLStatement = narrator.getData("Destination SQL Query");
            
            BaseClass.currentDatabase = Enums.Database.SourceData;

            sourceData = dbUtil.resultSetToArrayList(dbUtil.RunQuery(sourceSQLStatement));

            BaseClass.currentDatabase = Enums.Database.DestinationData;

            dbUtil = new DataBaseUtility();

            destinationData = dbUtil.resultSetToArrayList(dbUtil.RunQuery(destinationSQLStatement));
            
            narrator.stepPassed("Successfully extracted Source and Destination data sets");
            
            BaseClass.testData.extractParameter("Source Data Rows", String.valueOf(sourceData.size()) , "PASS");
            BaseClass.testData.extractParameter("Destination Data Rows", String.valueOf(destinationData.size()) , "PASS");
        
            return true;
        }
        catch(Exception ex)
        {
            
            return false;
        }
                    
        
    }
    
    
    public boolean ValidateETLDataSets() 
    {
        try
            
        {
            
            int failCount = 0;
            boolean hasFailed = false;
            LinkedList<DataRow> failedSource = new LinkedList<DataRow>();
            LinkedList<DataRow> failedDestination = new LinkedList<DataRow>();
            DataTableUtility dtUtil = new DataTableUtility();
            ///Check M/F = 1/2 rule
            for(DataRow sourceRow : sourceData)
            {
                String userId,userEmail,userGender, userGenderCode;
                userEmail = sourceRow.getColumnValue("USER_EMAIL");
                userId = sourceRow.getColumnValue("USER_ID");
                DataRow destinationRow = dtUtil.getSpecificRowByColumnValue(destinationData, "USER_EMAIL", userEmail);

                if(destinationRow == null)
                {
                    sourceRow.getColumn("USER_EMAIL").resultStatus = FAIL;
                    sourceRow.getColumn("USER_EMAIL").columnValue += " | Destination Data record not found";
                    BaseClass.testData.extractParameter("[Source Id No - " + userId + " | " + userEmail + "]", "Not found in Destination Table" , "FAIL");
                    err.println("[Source Id No - "+userEmail+"] Not found in Destination Table");
                    hasFailed = true;
                    failedSource.add(sourceRow);
                    failCount++;
                    continue;
                }

                destinationRow.getColumn("USER_EMAIL").resultStatus = PASS;
                sourceRow.getColumn("USER_EMAIL").resultStatus = PASS;
                
                userGender = sourceRow.getColumnValue("USER_GENDER");
                userGenderCode = destinationRow.getColumnValue("USER_GENDER_CODE");
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                // COMPARE USER_GENDER M/F TO USER_GENDER_CODE 1/2
                if((userGender.equals("F") && userGenderCode.equals("1")) || (userGender.equals("M") && userGenderCode.equals("2")))
                {
                    destinationRow.getColumn("USER_GENDER_CODE").resultStatus = PASS;
                    System.out.println("[Source Id No - " + userId + " | " + userEmail + "] Gender " + userGender + " Matched to Gender Code " + userGenderCode + " In destination table");

                }
                else
                {
                    hasFailed = true;
                    destinationRow.getColumn("USER_GENDER_CODE").resultStatus = FAIL;
                    destinationRow.getColumn("USER_GENDER_CODE").columnValue = "Gender " + userGender + " Failed to Match to Gender Code " + userGenderCode;
                    BaseClass.testData.extractParameter("[Source Id No - " + userId + " | " + userEmail + "]", "Gender " + userGender + " Failed to Match to Gender Code " + userGenderCode + " In destination table" , "FAIL");
                    System.err.println("[Source Id No - " + userId + " | " + userEmail + "] Gender " + userGender + " Failed to Match to Gender Code " + userGenderCode + " In destination table");
                    failedDestination.add(destinationRow);
                    failCount++;
                }   

            } 
            
            
            
            
            
            
            
            
            
            BaseClass.testData.extractParameter("Total Number of Failures", String.valueOf(failCount) + " Total Failures", "FAIL");
                    
            
            
            
            ////Write Excel Outputs
            ReportGenerator excelWriter = new ReportGenerator();
            excelWriter.GenerateExcelDataSheetReport(destinationData, reportDirectory + "\\ETLData.xlsx", "DestinationData");
            excelWriter.GenerateExcelDataSheetReport(sourceData, reportDirectory + "\\ETLData.xlsx", "SourceData");
            excelWriter.GenerateExcelDataSheetReport(failedDestination, reportDirectory + "\\ETLData.xlsx", "DestinationFailures");
            excelWriter.GenerateExcelDataSheetReport(failedSource, reportDirectory + "\\ETLData.xlsx", "SourceFailures");
        
            return !hasFailed;
        }
        catch(Exception ex)
        {
            
            return false;
        }
                    
        
    }

}
