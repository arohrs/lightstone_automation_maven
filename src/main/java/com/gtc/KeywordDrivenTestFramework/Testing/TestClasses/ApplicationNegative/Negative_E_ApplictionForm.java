/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.testData;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Application Negative",
        createNewBrowserInstance = false
)
public class Negative_E_ApplictionForm extends BaseClass
{

    String error = "";
    String dob = getData("DOB");
    String redField = getData("redField");
    String field = getData("Field");
    String submit = getData("Submit");
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;
    Pattern pattern;

    public Negative_E_ApplictionForm()
    {
    }

    public TestResult executeTest()
    {
        if (!applicationForm())
        {
            return narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test ");
    }

    public boolean applicationForm()
    {
        /*Entering customer details on the new application*/
        String firstName = getData("Firstname");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.applicationFirstname()))
        {
            error = "Failed to clear 'firstname'text field.";
            return false;
        }
        if (!firstName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationFirstname(), firstName))
            {
                error = "Failed to enter 'firstname' into the firstname text field.";
                return false;
            }

        }
        narrator.stepPassed("First Name: " + " " + firstName);
        String surName = getData("Surname");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.applicationSurname()))
        {
            error = "Failed to clear 'Surname'text field.";
            return false;
        }
        if (!surName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationSurname(), surName))
            {
                error = "Failed to enter 'Surname' into the surname text field.";
                return false;
            }

        }
        narrator.stepPassed("Surname: " + " " + surName);
        String idNumber = getData("ID");
          if (!idNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationIdNumber(), idNumber))
            {
                error = "Failed to enter 'id' number into the id text field.";
                return false;
            }

        }
        narrator.stepPassed("ID Number: " + " " + idNumber);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.idNumberLabel()))
        {
            error = "Failed to click the 'id nnumber label.'";
            return false;
        }
        if (!SeleniumDriverInstance.dismissAlert())
        { //cancells an alert
            error = "Failed to handle an alert.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.applicationVerifiedDropDown()))
        {
            error = "Failed to wait for the 'ID verified field' to appear.";
            return false;
        }
        validateIdVerified();
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.applicationVerifiedDropDown(), getData("Verified")))
        {
            error = "Failed to select Yes/NO to check if or not an application is verified.";
            return false;
        }
        narrator.stepPassed("ID Verified: " + " " + getData("Verified"));
        if (!isAfterMarket)
        {

            if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.clickAbsaBank()))
            {
                error = "Failed to wait for the 'absa bank' check box to appear";
                return false;
            }
            if (!SeleniumDriverInstance.isSelected((LightStonePageObjects.clickAbsaBank())))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickAbsaBank())))
                {
                    error = "Failed to click the 'absa bank' check-box.";
                    return false;
                }
            }

            narrator.stepPassed("Clicked ABSA BANK checkBox");
        }
        if (isAfterMarket)
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.afterMarket()))
            {
                error = "Failed to wait for the 'After market' check box to appear";
                return false;
            }
            if (!SeleniumDriverInstance.isSelected((LightStonePageObjects.afterMarket())))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.afterMarket())))
                {
                    error = "Failed to click the 'After Market' check-box.";
                    return false;
                }
            }
            narrator.stepPassed("Clicked After Market checkBox");

        }
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicationFormTab()))
            {
                error = "Failed to wait for the 'application form tab' to appear.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
            {
                error = "Failed to find the red highlighted " + field + " field.";
                return false;
            }
            if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
            {
                error = "Failed to find the red highlighted " + field + " field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field");
            pause(500);
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.applicationIdNumber()))
            {
                error = "Failed to clear ID Number textfield.";
                return false;

            }
        }
        return true;
    }

    public boolean validateIdVerified()
    {
        List<String> idVerifiedOption_TestPack = Arrays.asList(testData.getData("OptionList").split(","));
        List<String> idVerifiedOption_WebPage = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.applicationVerifiedDropDownOption());

        for (String option : idVerifiedOption_TestPack)
        {
            if (!idVerifiedOption_WebPage.stream().anyMatch(s -> option.equalsIgnoreCase(s)))
            {
                error = "Statement delivery method not found in the lsit";

            }

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.applicationVerifiedDropDown()))
        {
            error = "Failed to click ID verified drop down button";
        }
        narrator.stepPassedWithScreenShot("Successfully validated the 'ID Verified' field");
        return true;
    }
}
