/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.ArrayList;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Doc-Expense Details",
        createNewBrowserInstance = false
)

public class MonthlyDOC_ExpenseDetails extends BaseClass
{

    String error = "";
    ArrayList<String> details;
    String rowDetails = "";
    String monthlyDocValidation = getData("Monthly DOC");

    public MonthlyDOC_ExpenseDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!incomeDetails())
        {
            return narrator.testFailed("Failed to capture Income Details- " + error);
        }
        if (!expenseDetails())
        {
            return narrator.testFailed("Failed to capture Expense Details - " + error);
        }
        if (!updateAndMerge())
        {
            return narrator.testFailed("Failed to change client Status - " + error);
        }
        return narrator.finalizeTest("Successfully entered customer's details on the 'e-application' Form.");
    }

    public boolean incomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.incomeDetailsHeader()))
        {
            error = "Failed to wait for the 'income details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.creditLifeIncome()))
        {
            error = "Failed to wait for the 'credit life Income'.";
            return false;
        }
        String credit = getData("Amount");
        String creditAmount = getData("Income4");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.creditLifeIncome(), credit))
        {
            error = "Failed to select for the 'credit life Income' .";
            return false;
        }
        narrator.stepPassed("Selected credit Life Income: " + credit);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.creditLifeIncomeAmount()))
        {
            error = "Failed to wait for the 'credit life Income amount' .";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.creditLifeIncomeAmount(), creditAmount))
        {
            error = "Failed to enter the 'credit life Income amount'.";
            return false;
        }
        narrator.stepPassed("Entered credit Life Income: " + creditAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.maintenancePlan()))
        {
            error = "Failed to wait for the 'maintenance Plan'.";
            return false;
        }
        String mainttenance = getData("Amount1");
        String mainttenanceAmount = getData("Income4");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maintenancePlan(), mainttenance))
        {
            error = "Failed to select for the 'maintenancePlan'.";
            return false;
        }
        narrator.stepPassed("Selected Maintenance: " + mainttenance);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.maintenancePlanAmount()))
        {
            error = "Failed to wait for the 'maintenance Plan amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.maintenancePlanAmount(), mainttenanceAmount))
        {
            error = "Failed to enter for the 'maintenance Plan Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Maintenance: " + mainttenanceAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.paintGlass()))
        {
            error = "Failed to wait for the 'paint glass'.";
            return false;
        }
        String paintGlass = getData("Amount2");
        String paintGlassAmount = getData("Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.paintGlass(), paintGlass))
        {
            error = "Failed to select for the 'paint glass' .";
            return false;
        }
        narrator.stepPassed("Selected Paint Glass Amount: " + paintGlass);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.paintGlassAmount()))
        {
            error = "Failed to wait for the 'paint glass amount' .";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.paintGlassAmount(), paintGlassAmount))
        {
            error = "Failed to enter  the 'paint glass amount' .";
            return false;
        }
        narrator.stepPassed("Entered Paint Glass Amount: " + paintGlassAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.adminFee()))
        {
            error = "Failed to wait for the 'admin Fee'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.adminFee()))
        {
            error = "Failed to clear 'Admin Fee' text field.";
            return false;
        }
        String adminFee = getData("Income1");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.adminFee(), adminFee))
        {
            error = "Failed to enter  the 'admin Fee' .";
            return false;
        }
        narrator.stepPassed("Entered: " + adminFee + " Admin Fee Income textfield");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.creditShortFall()))
        {
            error = "Failed to wait for the 'credit Short Fall'.";
            return false;
        }
        String creditShortFall = getData("Amount");
        String creditShortFallAmount = getData("Income6");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.creditShortFall(), creditShortFall))
        {
            error = "Failed to select for the 'credit Short Fall'.";
            return false;
        }
        narrator.stepPassed("Selected Credit Short Fall: " + creditShortFall);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.creditShortFallAmount()))
        {
            error = "Failed to wait for the 'credit Short Fall Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.creditShortFallAmount(), creditShortFallAmount))
        {
            error = "Failed to enter for the 'credit Short Fall Amount'.";
            return false;
        }
        narrator.stepPassed("Entered: " + creditShortFallAmount + " Credit Shortfall Income textfield");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.bodyInsurance()))
        {
            error = "Failed to wait for the 'Body Insurance Income' .";
            return false;
        }
        String bodyInsurance = getData("Amount");
        String bodyInsuranceAmount = getData("Income8");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bodyInsurance(), bodyInsurance))
        {
            error = "Failed to select for the 'body Insurance'.";
            return false;
        }
        narrator.stepPassed("Selected Body Insurance Income: " + bodyInsurance);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.bodyInsuranceAmount()))
        {
            error = "Failed to wait for the 'body Insurance Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.bodyInsuranceAmount(), bodyInsuranceAmount))
        {
            error = "Failed to enter for the 'body Insurance Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Body Insurance Income amount: " + bodyInsurance);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tyreRim()))
        {
            error = "Failed to wait for the 'tyre Rim'.";
            return false;
        }
        String tyreRim = getData("Amount1");
        String tyreRimAmount = getData("Income7");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.tyreRim(), tyreRim)) //Tyre and Rim
        {
            error = "Failed to select for the 'tyre Rim'.";
            return false;
        }
        narrator.stepPassed("Selected Tyre and Rim: " + tyreRim);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tyreRimAmount()))
        {
            error = "Failed to wait for the 'tyre Rim Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.tyreRimAmount(), tyreRimAmount))
        {
            error = "Failed to enter for the 'tyre Rim Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Tyre and Rim amount: " + tyreRimAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.warrantyIncome()))
        {
            error = "Failed to wait for the 'warranty Income'.";
            return false;
        }
        String warrantyIncome = getData("Amount");
        String warrantyIncomeAmount = getData("Income6");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.warrantyIncome(), warrantyIncome))
        {
            error = "Failed to select for the 'warranty Income'.";
            return false;
        }
        narrator.stepPassed("Selected Warranty Income: " + warrantyIncome);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.warrantyIncomeAmount()))
        {
            error = "Failed to wait for the 'warranty Income Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.warrantyIncomeAmount(), warrantyIncomeAmount))
        {
            error = "Failed to enter for the 'tyre Rim Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Warranty Income amount: " + warrantyIncomeAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.trackingIncome()))
        {
            error = "Failed to wait for the 'tracking Income'.";
            return false;
        }
        String trackingIncome = getData("Amount");
        String trackingIncomeAmount = getData("Income4");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.trackingIncome(), trackingIncome))
        {
            error = "Failed to select for the 'tracking Income'.";
            return false;
        }
        narrator.stepPassed("Selected Tracking Income: " + trackingIncome);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.trackingIncomeAmount()))
        {
            error = "Failed to wait for the 'tracking Income' .";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.trackingIncomeAmount(), trackingIncomeAmount))
        {
            error = "Failed to enter for the 'tracking Income'.";
            return false;
        }
        narrator.stepPassed("Entered Tracking Income: " + trackingIncomeAmount);

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.inflationProctection()))
        {
            error = "Failed to wait for the 'inflation Proctection'.";
            return false;
        }
        String inflationProctection = getData("Amount");
        String inflationProctectionAmount = getData("Income2");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.inflationProctection(), inflationProctection))
        {
            error = "Failed to select for the 'inflation Proctection'.";
            return false;
        }
        narrator.stepPassed("Selected Inflation Protection Income: " + inflationProctection);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.inflationProctectionAmount()))
        {
            error = "Failed to wait for the 'Capture DOC' header.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.inflationProctectionAmount(), inflationProctectionAmount))
        {
            error = "Failed to enter for the 'inflation Proctection Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Inflation Protection Income amount: " + inflationProctection);

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.depositCover()))
        {
            error = "Failed to wait for the 'deposit Cover'.";
            return false;
        }
        String depositCover = getData("Amount");
        String depositCoverAmount = getData("Income8");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.depositCover(), depositCover))
        {
            error = "Failed to select for the 'deposit Cover'.";
            return false;
        }
        narrator.stepPassed("Selected Deposit Cover Income: " + depositCover);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.depositCoverAmount()))
        {
            error = "Failed to wait for the 'deposit Cover Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.depositCoverAmount(), depositCoverAmount))
        {
            error = "Failed to enter for the 'deposit Cover Amount' .";
            return false;
        }
        narrator.stepPassed("Entered Deposit Cover Income amount: " + depositCoverAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.roadCover()))
        {
            error = "Failed to wait for the 'road Cover' .";
            return false;
        }
        String roadCover = getData("Amount");
        String roadCoverAmount = getData("Income5");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.roadCover(), roadCover))
        {
            error = "Failed to select for the 'road Cover' .";
            return false;
        }
        narrator.stepPassed("Selected Road Cover Income: " + roadCover);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.roadCoverAmount()))
        {
            error = "Failed to wait for the 'road Cover Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.roadCoverAmount(), roadCoverAmount))
        {
            error = "Failed to enter for the 'road Cover Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Road Cover Income amount: " + roadCoverAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.otherIncomeAmount()))
        {
            error = "Failed to wait for the 'other Income Amount'.";
            return false;
        }
        String otherIncomeAmount = getData("Income7");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.otherIncomeAmount(), otherIncomeAmount))
        {
            error = "Failed to enter for the 'other Income Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Other Income amount: " + otherIncomeAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.compInsBroker()))
        {
            error = "Failed to wait for the 'comp Ins Broker'.";
            return false;
        }
        String compInsBroker = getData("Income");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.compInsBroker(), compInsBroker))
        {
            error = "Failed to enter for the 'comp Ins Broker'.";
            return false;
        }
        narrator.stepPassed("Entered Comp Ins Broker amount: " + compInsBroker);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.compInsIncome()))
        {
            error = "Failed to wait for the 'comp  Ins Income'.";
            return false;
        }
        String compInsIncome = getData("Amount");
        String compInsIncomeAmount = getData("Income3");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.compInsIncome(), compInsIncome))
        {
            error = "Failed to select for the 'comp Ins Income'.";
            return false;
        }
        narrator.stepPassed("Selected Comp Ins Income: " + compInsIncome);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.compInsIncomeAmount()))
        {
            error = "Failed to wait for the 'comp Ins Income Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.compInsIncomeAmount(), compInsIncomeAmount))
        {
            error = "Failed to enter for the 'comp Ins Income Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Comp Ins Income amount: " + compInsIncomeAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.firstGrossAmount()))
        {
            error = "Failed to wait for the 'first Gross Amount'.";
            return false;
        }
        String firstGrossAmount = getData("Income9");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.firstGrossAmount(), firstGrossAmount))
        {
            error = "Failed to enter for the 'first Gross Amount'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Entered First Gross Income amount: " + firstGrossAmount);

        return true;
    }

    public boolean expenseDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.expenseDetailsHeader()))
        {
            error = "Failed to scroll to the 'expense details' .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealerAdjustmentAmount()))
        {
            error = "Failed to wait for the 'dealer Adjustment Amount' .";
            return false;
        }
        String dealerAdjustmentAmount = getData("Income7");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.dealerAdjustmentAmount(), dealerAdjustmentAmount))
        {
            error = "Failed to enter for the 'dealer Adjustment Amount'.";
            return false;
        }
        narrator.stepPassed("Entered Dealer Adjustment amount: " + dealerAdjustmentAmount);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.igfInvoiceFee()))
        {
            error = "Failed to wait for the 'IGF Invoice Fee'.";
            return false;
        }
        String igfInvoiceFee = getData("Income7");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.igfInvoiceFee(), igfInvoiceFee))
        {
            error = "Failed to enter for the 'IGF Invoice Fee' .";
            return false;
        }
        narrator.stepPassed("Entered IGF Invoice Fee amount: " + igfInvoiceFee);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.bankInvoiceFee()))
        {
            error = "Failed to wait for the 'bank Invoice Fee'.";
            return false;
        }
        String bankInvoiceFee = getData("Income8");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.bankInvoiceFee(), bankInvoiceFee))
        {
            error = "Failed to enter for the 'bank Invoice Fee' header.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.totalExpenseExclVat()))
        {
            error = "Failed to click 'Total Expense label'";
            return false;
        }
        narrator.stepPassedWithScreenShot("Entered Bank Invoice Fee amount: " + bankInvoiceFee);
        String totalExpenses = SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.totalExpenseExclVat());
        String totalExpensesVat = SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.totalExpenseInclVat());
        double amount1 = Double.parseDouble(dealerAdjustmentAmount);
        double amount2 = Double.parseDouble(igfInvoiceFee);
        double amount3 = Double.parseDouble(bankInvoiceFee);
        double total = amount1 + amount2 + amount3;
        double currentAmount = Double.parseDouble(totalExpenses);
        double currentVat = Double.parseDouble(totalExpensesVat);
        if (!validateTotalExpense(currentAmount, total))
        {
            error = "The total value is not the correct value";
            return false;
        }
        narrator.stepPassedWithScreenShot("Validated Total Expense, excluding VAT");
        if (!validateVAT(currentAmount, currentVat))
        {
            error = "Failed to run VAT validation";
            return false;
        }
        narrator.stepPassedWithScreenShot("Validated Total Expense including VAT");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.createDOCButton()))
        {
            error = "Failed to wait for the 'create DOC' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.createDOCButton()))
        {
            error = "Failed to click for the 'create DOC' button.";
            return false;
        }
//        if (!SeleniumDriverInstance.alertHandler())
//        {
//            error = "Failed to Handle alert";
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Succesfully added new document");

        return true;
    }

    public boolean updateAndMerge()
    {

        String idNumber = getData("ID Number");
        int indexIdNumber = Integer.parseInt(getData("IndexIdNumber"));

        if (!searchCriteria(LightStonePageObjects.searchClientIDNumber(), idNumber, indexIdNumber))
        {
            error = "Failed to search using 'ID Number'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully added new document");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to wait for result";
            return false;

        }
        if (!lookupStatus(LightStonePageObjects.inCompleteDealStatus()))
        {
            error = "Failed to lookup result";
            return false;

        }
        narrator.stepPassedWithScreenShot("Validated deal status for added client is Orange");
        ArrayList<String> count = new ArrayList<>(SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.rowCount()));
        Search:
        for (int i = 0; i < count.size(); i++)
        {
            if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.monthTableRow(i) + LightStonePageObjects.inCompleteDealStatus(), 2))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.monthTableRow(i) + LightStonePageObjects.inCompleteDealStatus()))
                {
                    error = "Failed to click on result";
                    return false;

                }
                LightStonePageObjects.setrowDetails(i);
                break Search;

            }
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.docDetailHeader_zero()))
        {
            error = "Failed to wait for Doc Details";
            return false;

        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.possibleMergeDeal(), 3))
//        {
//            error = "Failed to wait for Possible Merge Header";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.mergeBtn()))
//        {
//            error = "Failed to wait for Merge Button";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.mergeBtn()))
//        {
//            error = "Failed to click Merge Button";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.mergeBtn()))
//        {
//            error = "Failed to click Merge Button";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.loadMask()))
//        {
//            error = "Failed to wait for load mask";
//            return false;
//        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.completeDealCheckBox()))
        {
            error = "Failed to wait for Doc Details";
            return false;

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.completeDealCheckBox()))
        {
            error = "Failed to wait for Doc Details";
            return false;

        }
        narrator.stepPassedWithScreenShot("Clicked 'Complete Deal' checkbox");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.updateBtn()))
        {
            error = "Failed to wait for Update Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.updateBtn()))
        {
            error = "Failed to click Update Button";
            return false;

        }
        narrator.stepPassedWithScreenShot("Clicked 'Update' Button");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for result";
            return false;

        }
        if (!lookupStatus(LightStonePageObjects.completeDealStatus()))
        {
            error = "Failed to lookup Status";
            return false;

        }
        narrator.stepPassedWithScreenShot("Validated that client status changed to Green");
        Search:
        for (int i = 0; i < count.size(); i++)
        {
            if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.monthTableRow(i) + LightStonePageObjects.completeDealStatus(), 2))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.monthTableRow(i) + LightStonePageObjects.completeDealStatus()))
                {
                    error = "Failed to click on result";
                    return false;

                }
                LightStonePageObjects.setrowDetails(i);
                break Search;

            }
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.reversalcheckBox()))
        {
            error = "Failed to wait for Reversal checkbox";
            return false;

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.reversalcheckBox()))
        {
            error = "Failed to click Reversal checkbox";
            return false;

        }
        if (!SeleniumDriverInstance.alertHandler())
        {
            error = "Failed to handle alert";
            return false;

        }
        narrator.stepPassedWithScreenShot("Clicked 'Reversal' checkbox");
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.completeDealCheckBox()))
        {
            error = "Failed to wait for Doc Details";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.completeDealCheckBox()))
        {
            error = "Failed to wait for Doc Details";
            return false;
        }
        narrator.stepPassedWithScreenShot("Unchecked 'Complete Deal' checkbox");
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.updateBtn()))
        {
            error = "Failed to scroll to Update Button";
            return false;

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.updateBtn()))
        {
            error = "Failed to click Update Button";
            return false;

        }
        narrator.stepPassedWithScreenShot("Clicked 'Update' Button");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for result";
            return false;

        }
        if (!lookupStatus(LightStonePageObjects.reversalStat()))
        {
            error = "Failed to wait for Deal Status";
            return false;

        }
        narrator.stepPassedWithScreenShot("Validated that client status changed to Pink");

        count = new ArrayList<>(SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.rowCount()));
        Search:
        for (int i = 0; i < count.size(); i++)
        {
            if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.monthTableRow(i) + LightStonePageObjects.reversalStat(), 2))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.monthTableRow(i) + LightStonePageObjects.reversalStat()))
                {
                    error = "Failed to click on result";
                    return false;

                }
                LightStonePageObjects.setrowDetails(i);
                break Search;

            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.docDetailHeader_zero()))
        {
            error = "Failed to wait for Doc Details";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.reversalcheckBox()))
        {
            error = "Failed to wait for Reversal checkbox";
            return false;

        }
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.reversalcheckBox()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.reversalcheckBox()))
            {
                error = "Failed to click Reversal checkbox";
                return false;

            }
            if (!SeleniumDriverInstance.alertHandler())
            {
                error = "Failed to handle alert";
                return false;

            }
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.completeDealCheckBox()))
        {
            error = "Failed to wait for Doc Details";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.completeDealCheckBox()))
        {
            error = "Failed to wait for Doc Details";
            return false;
        }
        narrator.stepPassedWithScreenShot("Checked 'Complete Deal' checkbox");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.updateBtn()))
        {
            error = "Failed to wait for Update Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.updateBtn()))
        {
            error = "Failed to click Update Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked 'Update' Button");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for result";
            return false;

        }
        if (!lookupStatus(LightStonePageObjects.reversalWithCompleteStat()))
        {
            error = "Failed to lookup status";
            return false;

        }
        narrator.stepPassedWithScreenShot("Validated that client status has changed to Red");
        return true;
    }

    public boolean validateTotalExpense(double currentAmount, double total)
    {
        if (currentAmount != total)
        {
            return false;
        }
        return true;
    }

    public boolean validateVAT(double exclVat, double exceptedVat)
    {
        double inclVat = (exclVat * 0.15) + exclVat;
        if (inclVat != exceptedVat)
        {
            return false;
        }
        return true;
    }

    public boolean searchCriteria(String xPath, String criteria, int indexCriteria)
    {
        MonthlyDOC month = new MonthlyDOC();
        if (!SeleniumDriverInstance.waitForElementByXpath(xPath))
        {
            error = "Failed to wait for the 'Client Name' Textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(xPath, criteria))
        {
            error = "Failed to enter 'Client Name' in the Textfield.";
            return false;
        }
        narrator.stepPassed("Succesfully entered text: " + criteria + " into textfield.");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to wait for the 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        narrator.stepPassed("Clicked Search button");

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for the 'searched DOC' current Month.";
            return false;
        }
        if (!month.validateDetails(criteria, indexCriteria))
        {
            error = "Failed to validate results.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Succesfully searched: " + criteria);

        narrator.stepPassed("Clicked 'Clear' button");

        return true;
    }

    public boolean lookupStatus(String conXpath)
    {
        ArrayList<String> count = new ArrayList<>(SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.rowCount()));
        Search:
        for (int i = 0; i < count.size(); i++)
        {
            if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.monthTableRow(i), 2)) //If True, check if the deal status is incomplete
            {
                if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.monthTableRow(i) + conXpath, 2))
                {
                    break Search;
                }
            }
        }
        return true;
    }

}
