/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;


/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Income Expenses",
        createNewBrowserInstance = false
)
public class TotalIncomeExpenses extends BaseClass
{

    String error = "";

    public TotalIncomeExpenses()
    {
    }

    public TestResult executeTest()
    {
        if (!totalIncomeExpenses())
        {
            return narrator.testFailed("Failed to validate the total income and expenses container -" + error);
        }
        return narrator.finalizeTest("Successfully validated the total income and expenses container");
    }

    public boolean totalIncomeExpenses()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.totalIncomeExpensesTab()))
        {
            error = "Failed to scroll to the 'Total Income and Expenses'";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.totalIncome()))
        {
            error = "Failed to wait for the 'Total Income' to appear";
            return false;
        }
        narrator.stepPassed("Validated the 'total income' field is not editable");

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.totalExpenses()))
        {
            error = "Failed to wait for the 'total expenses' to appear";
            return false;
        }
        narrator.stepPassed("Validated the 'total expenses' field is not editable");

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.totaldisposableIncome()))
        {
            error = "Failed to wait for the 'disposable income' to appear";
            return false;
        }
        narrator.stepPassed("Validated the 'disposable income' field is not editable");

        return true;
    }

}
