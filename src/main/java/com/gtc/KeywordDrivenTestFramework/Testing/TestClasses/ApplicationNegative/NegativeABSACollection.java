/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "ABSA Premium Negative",
        createNewBrowserInstance = false
)
public class NegativeABSACollection extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String submit = getData("Submit");

    public NegativeABSACollection()
    {
    }

    public TestResult executeTest()
    {
        if (!absaCollectionDetails())
        {
            return narrator.testFailed("Failed to perform the ABSA Premium Collection negative test- "+error);
        }

        return narrator.finalizeTest("Successfully performed the ABSA Premium Collection negative test ");
    }

    public boolean absaCollectionDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.absaPremiumCollectionTab()))
        {
            error = "Failed to scroll to the 'Absa Premium Collection' container .";
            return false;
        }
        String absaCollection ="";
        if (!absaCollection.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaPremiumCollectionDropDown(),absaCollection))
            {
                error = "Failed to select '"+absaCollection+"' from the list.";
                return false;
            }
        }
        narrator.stepPassed("Dealer Collect Premium : "+absaCollection);

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.absaPremiumCollectionTab()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
             if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField),5))
            {
                error = "The '" + field + "' field is was highlighted in red .";
                return false;
            }
               narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field ");
            if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
            {
                error = "Failed to hover over the '" + field + "' is Highlighted in red .";
                return false;
            }
            pause(500);
           
           
           
        }

        return true;
    }

}
