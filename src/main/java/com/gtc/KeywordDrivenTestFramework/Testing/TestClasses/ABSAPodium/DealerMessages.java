/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;


/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Dealer Messages",
        createNewBrowserInstance = false
)
public class DealerMessages extends BaseClass
{

    String error = "";
    String note = getData("Note");

    public DealerMessages()
    {
    }

    public TestResult executeTest()
    {
//        if (!dealerMessages())
//        {
//            return narrator.testFailed("Failed to add a note -" + error);
//        }
        return narrator.finalizeTest("Successfully added a note to the dealer");
    }

    public boolean dealerMessages()
    {
        
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.dealerMessagesTab()))
        {
            error = "Failed to scroll the 'Dealer Message' container";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addBPCNote()))
        {
            error = "Failed to click the 'Add BPC Note' button";
            return false;
        }
     

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.addBPCNoteContainer()))
        {
            error = "Failed to scroll to the 'Note' container";
            return false;
        }
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.bpcTextarea(),note))
        {
            error = "Failed to enter the 'note message'";
            return false;
        }
         narrator.stepPassed("Note : "+note);
          if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.bpcCancelButton()))
        {
            error = "Failed to click the 'cancel' button";
            return false;
        }
         if(!SeleniumDriverInstance.alertHandler())
         {
            error = "Failed to close the alert pop up"; 
         }
        

        return true;
    }

}
