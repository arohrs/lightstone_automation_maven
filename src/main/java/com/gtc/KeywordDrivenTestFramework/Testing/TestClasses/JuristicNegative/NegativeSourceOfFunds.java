/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Source of Funds Negative",
        createNewBrowserInstance = false
)
public class NegativeSourceOfFunds extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeSourceOfFunds()
    {
    }

    public TestResult executeTest()
    {
        if (!sourceOfFunds())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully perform the negative test");
    }

    public boolean sourceOfFunds()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.sourceOfFundsTab())))
        {
            error = "Failed to scroll  the 'Source Of Funds 'tab.";
            return false;
        }
        if (submit.equalsIgnoreCase("No"))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.developmentFunding()))
            {
                error = "Failed to click Development Funding checkbox.";
                return false;
            }
            narrator.stepPassed("Clicked on the Development Funding");

        }
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.sourceOfFundsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

                narrator.stepPassed("No source of funds checkBox has been selected");
            }

            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }
}
