/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Doc-Client Details",
        createNewBrowserInstance = false
)

public class MonthlyDOC_CaptureDetails extends BaseClass
{

    String error = "";
    String rowDetails = "";
    String monthlyDocValidation = getData("Monthly DOC");
  

    public MonthlyDOC_CaptureDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!captureClientDetails())
        {
            return narrator.testFailed("Failed capture Client Details- " + error);
        }

        return narrator.finalizeTest("Successfully entered customer's details on the 'e-application' Form.");
    }
   public boolean captureClientDetails()
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.addNew()))
        {
            error = "Failed to wait for the 'Add New' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNew()))
        {
            error = "Failed to wait for the 'Client Details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.captureDocHeader()))
        {
            error = "Failed to wait for the 'Capture DOC' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientDetailsHeader()))
        {
            error = "Failed to wait for the 'Client Details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.validRSAID()))
        {
            error = "Failed to wait for the 'Valid RSA ID' checkBox.";
            return false;
        }

        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.validRSAID()))
        {
            error = "Failed to check if the 'Valid RSA ID' is selected.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Valid RSA checkbox is selected");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientName()))
        {
            error = "Failed to wait for the 'Client Name'.";
            return false;
        }
        String clientName = getData("Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientName(), clientName))
        {
            error = "Failed to enter  the 'client name'.";
            return false;
        }
        narrator.stepPassed("Entered client name succesfully: "+clientName);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientSurname()))
        {
            error = "Failed to wait for the 'client surname'.";
            return false;
        }
        String clientSurname = getData("Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientSurname(), clientSurname))
        {
            error = "Failed to enter  the 'client surname'.";
            return false;
        }
        narrator.stepPassed("Entered client surname succesfully: "+clientSurname);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientID()))
        {
            error = "Failed to wait for the 'client ID' .";
            return false;
        }
        String clientID = getData("ID Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientID(), clientID))
        {
            error = "Failed to enter  the ' ID'.";
            return false;
        }
        narrator.stepPassed("Entered client identification succesfully: "+clientID);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientEmail()))
        {
            error = "Failed to wait for the ' email' .";
            return false;
        }
        String email = getData("Email");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientEmail(), email))
        {
            error = "Failed to enter  the ' email'.";
            return false;
        }
        narrator.stepPassed("Entered email address succesfully: "+email);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientCellPhone()))
        {
            error = "Failed to wait for the 'Cell Phone' .";
            return false;
        }
        String cellPhone = getData("Cell Phone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientCellPhone(), cellPhone))
        {
            error = "Failed to enter  the 'Cell Phone'.";
            return false;
        }
        narrator.stepPassed("Entered client cell phone succesfully: "+cellPhone);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientworkPhone()))
        {
            error = "Failed to wait for the 'work phone'.";
            return false;
        }
        String workPhone = getData("Work Phone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clientworkPhone(), workPhone))
        {
            error = "Failed to enter  the 'work phone'.";
            return false;
        }
        narrator.stepPassed("Entered client work phone succesfully: "+workPhone);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.isCompany()))
        {
            error = "Failed to wait for the 'is Company' checkBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.isCompany()))
        {
            error = "Failed to click 'is Company' checkBox.";              //Fix Xpath for is Company
            return false;
        }
        narrator.stepPassedWithScreenShot("Selected 'is Company' checkbox ");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.validateCompany()))
        {
            error = "Failed to wait for 'Comany Name' TextField.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.validateReg()))
        {
            error = "Failed to wait for 'Registration Number' TextField.";
            return false;
        }
        String registrationNum =getData("Registration");
        if (!validate(LightStonePageObjects.getLabelReg(),registrationNum ))
        {
            error = "Failed to validate Registration Label";
            return false;
        }
           String companyName =getData("Company");
        if (!validate(LightStonePageObjects.getLabelCompany(), getData("Company")))
        {
            error = "Failed to validate Company Label";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.isCompany()))
        {
            error = "Failed to click for the 'is Company' checkBox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Unselected 'is Company' checkbox ");
        return true;
    }

    public boolean validate(String xPath, String text)
    {
        if (!SeleniumDriverInstance.ValidateByText(xPath, text))
        {
            error = "Failed to validate: " + text + "against: " + SeleniumDriverInstance.retrieveTextByXpath(xPath);
            return false;
        }
        narrator.stepPassedWithScreenShot("Sucessfully Validated: " + text);

        return true;

    }

}
