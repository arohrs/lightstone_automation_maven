/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses.GeographicalDetails;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Surety Negative",
        createNewBrowserInstance = false
)
public class NegativeSurety extends BaseClass
{

    String error = "";
    String suburb = getData("Search Suburb");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");
    GeographicalDetails suburbSearch = new GeographicalDetails();

    public NegativeSurety()
    {
    }

    public TestResult executeTest()
    {
        if (!suretyDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test -" + error);

    }

    public boolean suretyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatorySuretyTab()))
        {
            error = "Failed to scroll to the 'Surety' tab.";
            return false;
        }
        String employmentType = getData("Employment Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyEmploymentType(), employmentType))
        {
            error = "Failed to select surety employment type.";
            return false;
        }
        narrator.stepPassed("Employment Type : " + employmentType);
        String periodEmployed = getData("Period");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.suretyPeriodEmployedYear()))
        {
            error = "Failed to clear 'Perod Currently Employed - Year' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.suretyPeriodEmployedMonth()))
        {
            error = "Failed to clear 'Perod Currently Employed - Month' textfield.";
            return false;
        }
        if (!periodEmployed.isEmpty())
        {

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedYear(), periodEmployed))
            {
                error = "Failed to enter 'Perod Currently Employed - Year'.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedMonth(), periodEmployed))
            {
                error = "Failed to enter 'Perod Currently Employed - Month'.";
                return false;
            }
        }
        narrator.stepPassed("Period Currently Employed (YY/MM) : " + periodEmployed + "/" + periodEmployed);
        String residentialStatus = getData("Residential Status");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyResidentialStatus(), residentialStatus))
        {
            error = "Failed to select residential status";
            return false;
        }
        narrator.stepPassed("Residential Status : " + residentialStatus);
        String addressLine = getData("Address");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.suretyWorkAddressLine1()))
        {
            error = "Failed to clear Work Address textfield.";
            return false;
        }
        if (!addressLine.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine1(), addressLine))
            {
                error = "Failed to enter Work Address1.";
                return false;
            }
        }
        narrator.stepPassed("Work Address Line 1: " + addressLine);
        if (!suburb.isEmpty() && !addressLine.isEmpty())
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyWorkAddressLookup()))
            {
                error = "Failed to click the 'Select Suburb' button.";
                return false;
            }
            suburbSearch.addressLookup();
        }

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatorySuretyTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
