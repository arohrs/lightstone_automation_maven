/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Employer Details",
        createNewBrowserInstance = false
)
public class EmployerDetails extends BaseClass
{

    String error = "";
    String employerName = getData("Employer Name");
    String industryType = getData(" Industry Type");
    String employmentType = getData("Employment Type");
    String occupation = getData("Occupation");
    String clientType = getData("Client Type");
    String sector = getData("Sector");
    String phoneType = getData("Phone Type");
    String phoneNo = getData("Phone No");
    String address = getData("Address");
    String city = getData("City");
    String suburb = getData("Suburb");
    String postalCode = getData("Postal Code");
    String postalAddress = getData("Postal Address");
    String currentPeriod = getData("Current Period");
    String previousPeriod = getData("Previous Period");

    public EmployerDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!employerDetails())
        {
            return narrator.testFailed("Failed to enter the customer's employer details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the customer's employer details");
    }

    public boolean employerDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.employerDetailsContainer()))
        {
            error = "Failed to scroll to the 'Emplyer Details'";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerName()))
        {
            error = "Failed to clear the 'Employer Name'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerName(), employerName))
        {
            error = "Failed to enter the 'Employer Name'";
            return false;
        }
        narrator.stepPassed("Employer Name : " + employerName);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerIndustryTypeDropDown(), industryType))
        {
            error = "Failed to select the 'Employer Industry Type'";
            return false;
        }
        narrator.stepPassed("Employer Industry Type : " + industryType);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerType(), employmentType))
        {
            error = "Failed to select the 'Employment Type'";
            return false;
        }
        narrator.stepPassed("Employment Type : " + employmentType);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.occupationDropDown(), occupation))
        {
            error = "Failed to select the 'Occupation'";
            return false;
        }
        narrator.stepPassed("Occupation : " + occupation);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerClientType(), clientType))
        {
            error = "Failed to select the 'Client Type'";
            return false;
        }
        narrator.stepPassed("Client Type : " + clientType);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerIndustrySector(), sector))
        {
            error = "Failed to select the 'Sector Of Industry'";
            return false;
        }
        narrator.stepPassed("Sector Of Industry : " + sector);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.employerPhoneType(), phoneType))
        {
            error = "Failed to select the 'Employer Phone Type' ";
            return false;
        }
        narrator.stepPassed("Employer Phone Type : " + phoneType);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerPhoneNo()))
        {
            error = "Failed to clear the 'Phone no' ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPhoneNo(), phoneNo))
        {
            error = "Failed to enter the 'Phone no' ";
            return false;
        }
        narrator.stepPassed("Phone no : " + phoneNo);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerAddress()))
        {
            error = "Failed to clear the 'Employer Address Line 1'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerAddress(), address))
        {
            error = "Failed to enter 'Employer Address Line 1'";
            return false;
        }
        narrator.stepPassed("Employer Address Line 1 : " + address);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerSecondAddress()))
        {
            error = "Failed to clear the 'Employer Address Line 2' ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerSecondAddress(), address))
        {
            error = "Failed to enter the 'Employer Address Line 2'";
            return false;
        }
        narrator.stepPassed("Employer Address Line 2 : " + address);
        employerAddressLookup();
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerPeriodYear()))
        {
            error = "Failed to clear the 'YY Period currently Employed' ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPeriodYear(), currentPeriod))
        {
            error = "Failed to enter the 'YY Period currently Employed'";
            return false;
        }
        narrator.stepPassed("Period Currently Employed  (Year): " + currentPeriod);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.employerPeriodMonth()))
        {
            error = "Failed to clear the 'MM Period currently Employed'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.employerPeriodMonth(), currentPeriod))
        {
            error = "Failed to enter the 'MM Period currently Employed' ";
            return false;
        }
        narrator.stepPassed("Period Currently Employed  (Month): " + currentPeriod);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.previousPeriodEmployedYear()))
        {
            error = "Failed to clear the 'YY Period Previously Employed'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.previousPeriodEmployedYear(), previousPeriod))
        {
            error = "Failed to enter the 'YY Period Previously Employed'";
            return false;
        }
        narrator.stepPassed("Period Previously Employed (Year): " + previousPeriod);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.previousPeriodEmployedMonth()))
        {
            error = "Failed to clear the 'MM Period Previously Employed'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.previousPeriodEmployedMonth(), previousPeriod))
        {
            error = "Failed to enter the 'MM Period Previously Employed' ";
            return false;
        }
        narrator.stepPassed("Period Previously Employed (Month): " + previousPeriod);

        return true;
    }

    public boolean employerAddressLookup()
    {
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.employerSuburbButton()))
        {
            error = "Failed to click 'suburb' lookup button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchSuburb(), 5))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.searchSuburb(), city))
        {
            error = "Failed to enter 'suburb' into the search text field.";
            return false;
        }
        narrator.stepPassed("Search Suburb : " + city);

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchButton()))
        {
            error = "Failed to click on the 'sububrb' search button.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectSuburbDropDown(), suburb))
        {
            error = "Failed to select 'suburb'.";
            return false;
        }
        narrator.stepPassed("Select Suburb : " + suburb);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectCodeDropDown(), postalCode))
        {
            error = "Failed to select the 'code' from the list.";
            return false;
        }
        narrator.stepPassed("Select Code : " + postalCode);
        narrator.stepPassedWithScreenShot("Successfully captured the address lookup details");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectOK()))
        {
            error = "Failed to click the 'OK' button.";
            return false;
        }

        return true;
    }

}
