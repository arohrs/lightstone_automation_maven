/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Marketing Consent",
        createNewBrowserInstance = false
)
public class MarketingConsent extends BaseClass
{

    String error = "";
    String option = getData("Option");
    String name = getData("Name");

    public MarketingConsent()
    {
    }

    public TestResult executeTest()
    {
        if (!totalIncomeExpenses())
        {
            return narrator.testFailed("Failed to enter the marketing consent details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the marketing consent details");
    }

    public boolean totalIncomeExpenses()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.marketingConsentTab()))
        {
            error = "Failed to scroll to the 'Marketing Consent'";
            return false;
        }
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.podiumSalePerson()))
        {
            error = "Failed to clear the 'Sales Person'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.podiumSalePerson(),name))
        {
            error = "Failed to enter the 'Sales Person'";
            return false;
        }
        narrator.stepPassed("Sales Person : "+name);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.marketingConsent(),option))
        {
            error = "Failed to select the 'Utilise personal' option";
            return false;
        }
        narrator.stepPassed("Utilise personal information for marketing purposes? : "+option);

        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.creditProviderCheckBox()))
        {
             if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.creditProviderCheckBox())){
            error = "Failed to tick the 'Credit Provider' checkBox";
            return false;
             }
        }
        narrator.stepPassed("I hereby give consent to the Credit Provider : "+option);

        return true;
    }

}
