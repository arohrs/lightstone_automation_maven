/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Applicant Details",
        createNewBrowserInstance = false
)

public class ApplicationDetails extends BaseClass
{

    String error = "";
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;

    public ApplicationDetails()
    {
    }

    public TestResult executeTest()
    {

        if (!consentDetails())
        {
            return narrator.testFailed("Failed to update application details. - " + error);
        }
        if (!isAfterMarket)
        {
            if (!applicationDetails())
            {
                return narrator.testFailed("Failed to update application details. - " + error);
            }
        }

        return narrator.finalizeTest("Successfully updated 'application details' on the Form.");
    }

    public boolean consentDetails()
    {
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.ConsentDetails()))
        {
            error = "Failed to scroll to the 'share info consent' field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.insuranceConsent(), getData("Consent2")))
        {
            error = "Failed to select 'Yes/No' to indicate the  consent.";
            return false;
        }
        narrator.stepPassed("I hereby give consent that you forward my personal information : " + getData("Consent2"));
        if (isAfterMarket)
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.salesPerson(), getData("SalesPerson")))
            {
                error = "Failed to select a 'salesPerson' from the list.";
                return false;
            }
            narrator.stepPassed("SalesPerson : " + getData("SalesPerson"));
        }
        //additional consent
//        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.additionalCredit(), getData("Consent2")))
//        {
//            error = "Failed to select 'Yes/No' to indicate the joint income consent.";
//            return false;
//        }
//        narrator.stepPassed("Do you give express consent for additional credit?  : " + getData("Consent2"));
        return true;
    }

    public boolean applicationDetails()
    {
        /*updating application details*/
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.marketingConsent(), getData("Consent2")))
        {
            error = "Failed to select 'Yes/No' to indicate the first consent.";
            return false;
        }
        narrator.stepPassed("Do you consent your information to be shared : " + getData("Consent2"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.consentToAlphera(), getData("Consent3")))
        {
            error = "Failed to select 'Yes/No' to indicate the Alphera consent.";
            return false;
        }
        narrator.stepPassed("Share details within the Alphera Group Companies : " + getData("Consent3"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.itcConsent(), getData("Consent4")))
        {
            error = "Failed to select 'Yes/No' to indicate the applicant consent.";
            return false;
        }
        narrator.stepPassed("Apllicant consent to the Credit Provider accessing my credit : " + getData("Consent4"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.itcSpouseConsent(), getData("Consent5")))
        {
            error = "Failed to select 'Yes/No' to indicate the spouse consent.";
            return false;
        }
        narrator.stepPassed("Spouse consent to the Credit Provider accessing my credit : " + getData("Consent5"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.jointIncomeConsent(), getData("Consent6")))
        {
            error = "Failed to select 'Yes/No' to indicate the joint income consent.";
            return false;
        }
        narrator.stepPassed("Do you consent to a joint income assessment? : " + getData("Consent6"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.jointIncomeConsent(), getData("Consent1")))
        {
            error = "Failed to select 'Yes/No' to indicate Tracking consent.";
            return false;
        }
        narrator.stepPassed("You hereby provide consent to Altech to assess your information : " + getData("Consent1"));

        pause(500);
        return true;
    }

}
