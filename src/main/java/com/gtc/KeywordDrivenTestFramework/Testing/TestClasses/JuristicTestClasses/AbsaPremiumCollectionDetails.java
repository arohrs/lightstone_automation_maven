/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;


/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Premium Collection",
        createNewBrowserInstance = false
)
public class AbsaPremiumCollectionDetails extends BaseClass
{

    String error = "";

    public AbsaPremiumCollectionDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!premiumCollection())
        {

            narrator.testFailed("Failed to update the Absa Premium Collection details -" + error);
        }
        return narrator.finalizeTest("Successfully updated the Absa Premium Collection details");
    }

    public boolean premiumCollection()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.absaPremiumCollectionTab()))
        {
            error = "Failed to scroll to the 'Absa Premium Collection' tab.";
            return false;
        }
        String premiumCollection = getData("Dealer Collect Premium");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaPremiumCollectionDropDown(), premiumCollection))
        {
            error = "Failed to select Premium Collection";
            return false;
        }
        narrator.stepPassed("Can the Dealer collect premium : "+premiumCollection);
        String premiumVendor = getData("Premium Vendor");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaPremiumVendorDropDown(), premiumVendor))
        {
            error = "Failed to select Premium Vendor";
            return false;
        }
        narrator.stepPassed("Premium vendor : "+premiumVendor);

        return true;
    }
}
