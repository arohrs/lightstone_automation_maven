/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Personal Details",
        createNewBrowserInstance = false
)
public class PersonalDetails extends BaseClass
{

    String error = "";
    String language = getData("Language");
    String gender = getData("Gender");
    String race = getData("Race");
    String title = getData("Title");
    String cellNumber = getData("Mobile Number");
    String mobileType = getData("Mobile  Type");
    String contactMethod = getData("Contact Method");
    String homePhoneNumber = getData("Home Phone Number");
    String emailAddress = getData("Email Address");
    String countryNationality = getData("Country Nationality");
    String faxNumber = getData("Fax Number");

    public PersonalDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!personalDetails())
        {
            return narrator.testFailed("Failed to enter the personal details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the personal details");
    }

    public boolean personalDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumPersonalDetails()))
        {
            error = "Failed to wait for the 'Podium Details'";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.podiumPreferredLanguage(), language))
        {
            error = "Failed to select the 'Preferred Language'";
            return false;
        }
        narrator.stepPassed("Preferred Language : "+language);
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.podiumGender(), gender))
        {
            error = "Failed to select the 'Gender'";
            return false;
        }
        narrator.stepPassed("Gender : "+gender);
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.titleDropDown(), title))
        {
            error = "Failed to select the 'Title'";
            return false;
        }
        narrator.stepPassed("Title : "+title);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.raceDropDown(),race))
        {
            error = "Failed to select the 'Race/Ethnic Group'";
            return false;
        }
        narrator.stepPassed("Race/Ethnic Group : "+race);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfNationality(),countryNationality))
        {
            error = "Failed to select the 'Country of Nationality'";
            return false;
        }
        narrator.stepPassed("Country of Nationality : "+countryNationality);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyPreferredContactMethod(),contactMethod))
        {

            error = "Failed to select the 'Preferred Contact Method'";
            return false;

        }
         narrator.stepPassed("Preferred Contact Method : "+contactMethod);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.mobileNumber()))
        {
            error = "Failed to clear the 'Mobile Phone'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mobileNumber(), cellNumber))
        {
            error = "Failed to enter the 'Mobile Phone'";
            return false;
        }
        narrator.stepPassed("Mobile Phone: " + cellNumber);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mobileTypeDropDown(), mobileType))
        {
            error = "Failed to select the 'Mobile Type'";
            return false;
        }
        narrator.stepPassed("Mobile Type: " + mobileType);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.phoneNumber(), homePhoneNumber))
        {
            error = "Failed to enter the 'Customer Home Phone'";
            return false;
        }
        narrator.stepPassed("Customer Home Phone : " + homePhoneNumber);
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.emailAdress(), emailAddress))
        {
            error = "Failed to enter the 'Email Address '";
            return false;
        }
        narrator.stepPassed("Email Address : " + emailAddress);
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.podiumFaxNumber(), faxNumber))
        {
            error = "Failed to enter the 'Fax Number'";
            return false;
        }
        narrator.stepPassed("Fax Number : " + faxNumber);

        return true;
    }

}
