/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.LightstoneScriptingInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.currentBrowser;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.testData;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative.NegativeE_NavigateToNewApplication;

/**
 *
 * @author nkelechi
 * @Internal Review sbachan 2018-09-14
 */
@KeywordAnnotation(
        Keyword = "Navigate and Login",
        createNewBrowserInstance = true
)

public class NavigateAndLogin extends BaseClass
{

    String error = "", username, password, successMessage;
    boolean isPositiveLogin = true;

    public NavigateAndLogin()
    {
        username = testData.getDataIgnoreCase("Username").isEmpty() ? Enums.Credentials.Lightstone_Credential.getUsername() : testData.getData("Username");
        password = testData.getData("Password").isEmpty() ? Enums.Credentials.Lightstone_Credential.getPassword() : testData.getData("Password");

        isPositiveLogin = testData.getData("isPositiveLogin").isEmpty() ? isPositiveLogin : Boolean.getBoolean(testData.getData("isPositiveLogin"));
        successMessage = isPositiveLogin ? "Successfully navigated to the 'lightstone' website." : "Successfully validated negative scenario with Username: '" + username + "' and password: '" + password + "'";

        testData.extractParameter("Username", username, "");
        testData.extractParameter("Password", password, "");
    }

    public TestResult executeTest()
    {
        if (!navigateToLightStonePage())
        {
            return narrator.testFailed("Failed to navigate to the 'lightstone' website - " + error);
        }

        return narrator.finalizeTest(successMessage);
    }

    public boolean navigateToLightStonePage()
    {

        /**
         * createNewBrowserInstance variable is set to false and the driver is
         * manually started here.
         */
       // NegativeE_NavigateToNewApplication nestedclick = new NegativeE_NavigateToNewApplication();
        if (!SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.startDriver();
        }

        /*This step will Launch the browser*/
        if (!SeleniumDriverInstance.navigateTo(LightStonePageObjects.lightstoneStagingURL()))
        {
            error = "Failed to launch the browser.";
            return false;
        }

        narrator.stepPassed(currentBrowser.name() + " Browser launched successfully.");

        //Waiting for username
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.username()))
        {
            error = "Failed to wait for username";
            return false;
        }
        narrator.stepPassedWithScreenShot("Navigated to Stage site sucessfully.");

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.username(), username))
        {
            error = "Failed to enter 'username' into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.password(), password))
        {
            error = "Failed to enter 'pasword' into the text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered the user credentials ");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clickLogin()))
        {
            error = "Failed to wait for the 'login' button to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript((LightStonePageObjects.clickLogin())))
        {
            error = "Failed to click the 'login' button.";
            return false;
        }

        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }

        if (isPositiveLogin)
        {//If we want to login successfully, we should find the search criteria.

            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteria()))
            {
                error = "Failed to wait for the table to appear";
                return false;
            }           

        } else
        {//If we want the login to fail, we should find an error message
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.loginErrorMessage()))
            {
                error = "Failed to wait for Login Error Message during Negative Login Test";
                return false;
            }

            String errorMessage = SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.loginErrorMessage());
            if (!errorMessage.contains(testData.getData("Error Message")))
            {
                error = "Failed to validate eror message. Expected '" + testData.getData("Error message") + "' but found '" + errorMessage + "'";
                return false;
            }
            testData.extractParameter("Error Message", testData.getData("Error Message"), "");
        }

        return true;
    }
}
