/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Application Negative",
        createNewBrowserInstance = false
)
public class NegativeE_ApplicationFormDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeE_ApplicationFormDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!eApplicationForm())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean eApplicationForm()
    {

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.companyName()))
        {
            error = "Failed to scroll to Company Name";
            return false;

        }
        //Test

        String companyName = getData("Company Name");
        if (!companyName.isEmpty())
        {
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyName()))
            {
                error = "Failed to clear the 'company name' textfield.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyName(), companyName))
            {
                error = "Failed to enter the 'company name'.";
                return false;
            }
        }
        narrator.stepPassed("Company name : " + companyName);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tradingName()))
        {
            error = "Failed to wait for the 'trading name' to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.establishedDate()))
        {
            error = "Failed to wait for the 'established date' to appear.";
            return false;
        }
        String establishedDate = getData("Established date");
        if (!establishedDate.isEmpty())
        {
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.establishedDate()))
            {
                error = "Failed to clear 'established date'.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.establishedDate()))
            {
                error = "Failed to click the 'established date'.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.establishedDate(), establishedDate))
            {
                error = "Failed to enter the 'established date'.";
                return false;
            }
        }
        narrator.stepPassed("Company established date : " + establishedDate);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyYearEnd()))
        {
            error = "Failed to wait for the 'Company Year End' to appear.";
            return false;
        }
        String yearEnd = getData("Year end");
        if (!yearEnd.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyYearEnd(), yearEnd))
            {
                error = "Failed to enter the 'Year end'.";
                return false;
            }
        }
        narrator.stepPassed("Company year end : " + yearEnd);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyType()))
        {
            error = "Failed to wait for the 'Company type' to appear.";
            return false;
        }
        String companyType = getData("Company Type");
        if (!companyType.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyType(), companyType))
            {
                error = "Failed to enter the 'company type'.";
                return false;
            }
        }
        narrator.stepPassed("Company type : " + companyType);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.businessRegistrationNumber()))
        {
            error = "Failed to wait for the 'Registration number' to appear.";
            return false;
        }
        String registrationNumber = getData("Registration Number");
        if (!companyType.equalsIgnoreCase("Not Selected") && !registrationNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegistrationNumber(), registrationNumber))
            {
                error = "Failed to enter the 'Registration number'.";
                return false;
            }
        }
        narrator.stepPassed("Registration number : " + registrationNumber);

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vatIndicator()))
        {
            error = "Failed to wait for the 'VAT Indicator' to appear.";
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.TAB);
        String vatIndicator = getData("Vat Indicator");
        if (!vatIndicator.isEmpty())
        {
            if (!SeleniumDriverInstance.dismissAlert())
            { //cancells an alert
                error = "Failed to handle an alert.";
                return false;
            }
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vatIndicator(), vatIndicator))
            {
                error = "Failed to enter the 'Vat Indicator'.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.dismissAlert())
        { //cancells an alert
            error = "Failed to handle an alert.";
            return false;
        }

        narrator.stepPassed("Vat Indicator : " + vatIndicator);

        String absaDealer = getData("ABSA");
        if (!absaDealer.isEmpty())
        {
            if (SeleniumDriverInstance.isSelected(LightStonePageObjects.clickMfc()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickMfc()))
                {
                    error = "Failed to click the 'mfc' check-box.";
                    return false;
                }
            }
            if (SeleniumDriverInstance.isSelected(LightStonePageObjects.clickStandardBank()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickStandardBank()))
                {
                    error = "Failed to click the 'standard bank' check-box.";
                    return false;
                }
            }
            if (SeleniumDriverInstance.isSelected(LightStonePageObjects.clickWesbank()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickWesbank()))
                {
                    error = "Failed to click the 'standard bank' check-box.";
                    return false;
                }
            }
            if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.clickAbsaBank()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickAbsaBank()))
                {
                    error = "Failed to click the 'absa bank' check-box.";
                    return false;
                }
            }

        }
        narrator.stepPassed("Ticked 'ABSA BANK' ");
        if (!SeleniumDriverInstance.dismissAlert())
        { //cancells an alert
            error = "Failed to handle an alert.";
            return false;
        }
        String vatNumber = getData("Vat Number");
        if (vatIndicator.equalsIgnoreCase("Yes") && !vatNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickAbsaBank()))
            {
                error = "Failed to click the 'absa bank' check-box.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.businessVatNumber()))
            {
                error = "Failed to wait for the 'Bussiness VAT Number' to appear.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessVatNumber(), vatNumber))
            {
                error = "Failed to enter the 'Vat Number'.";
                return false;
            }

        }
        narrator.stepPassed("Vat Number : " + vatNumber);
        if (!absaDealer.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaDealerCodeDropDown(), getData("ABSA")))
            {
                error = "Failed to select 'absa dealer code' from the list.";
                return false;
            }
            narrator.stepPassed("Selected ABSA - Dealer Code: " + " " + getData("ABSA"));
        }

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicationFormTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field ");
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field ");
                pause(500);

            }
            if (absaDealer.isEmpty())

            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.financialInstitutionAlert(), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully found the Financial Institution alert message ");

                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.alertConfirmButton()))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
              

            }
//            if (!SeleniumDriverInstance.closeCurrentTab(LightStonePageObjects.getPageHandle()))
//            {
//                error = "Failed to close the current window";
//                return false;
//
//            }

        }
        return true;
    }

}
