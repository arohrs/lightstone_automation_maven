<<<<<<< HEAD
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Manage_SalesPerson;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.DOC_Epson;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.ConstructID;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Manage Salesperson",
        createNewBrowserInstance = false
)

public class ManageSalesPerson extends BaseClass
{

    String error = "";
    ConstructID Id = new ConstructID("male",20,60);

    public ManageSalesPerson()
    {
    }

    public TestResult executeTest()
    {
        if (!manageSalesPerson())
        {
            return narrator.testFailed("Failed to create and save the details of the salesperson - " + error);
        }

        return narrator.finalizeTest("Successfully created  and saved the details of the salesperson.");
    }

    public boolean manageSalesPerson()
    {
        
        String name = testData.getData("Name");
        String surname = testData.getData("Surname");
        String identificationType = testData.getData("Identification Type");
       // String idNumber = testData.getData("ID Number");
        String idNumber = Id.getID();
        String emailAddress = testData.getData("Email Address");
        String cellphone = testData.getData("Cell Phone");
        String title = testData.getData("Title");
        String manageSalesPerson = testData.getData("Manage Salesperson");


        /*Entering manage salesperson details*/
        DOC_Epson docNavigation = new DOC_Epson();
        if (!docNavigation.navigateDOC())
        {
            error = "Failed to navigate to DOC.";
            return false;
        }     
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.doc()))
        {
            error = "Failed to wait for the DOC tab to be present.";
            return false;
        }

        //narrator.stepPassedWithScreenShot("Successfully landed on the home page.");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.doc()))
        {
            error = "Failed to click on the DOC drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.manageSalesPersons()))
        {
            error = "Failed to wait for the MANAGE SALESPERSON drop down.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully showing MANAGE SALESPERSON from the drop down elements.");

        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.manageSalesPersons(), manageSalesPerson))
        {
            error = "Failed to validate that 'MANAGE SALESPERSON' is present by text.";
            return false;
        }

        narrator.stepPassed("Successfully validated that: " + manageSalesPerson + " is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.manageSalesPersons()))
        {
            error = "Failed to click MANAGE SALESPERSON.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to the SALESPERSON tab.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.salesPersonsHeader()))
        {
            error = "Failed to wait for the SALESPERSONS header.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully landed on the SALESPERSONS page.");

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to wait for the ADD NEW button.";
            return false;
        }
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to click on ADD NEW button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to click on ADD NEW button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientDetails()))
        {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewSalesPerson()))
            {
                error = "Failed to click on ADD NEW button.";
                return false;
            }
//            error = "Failed to wait for the Client detail's bar to appear.";
//            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked 'Add New' and opened 'Capture Salesperson' window");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.nameOfSalesPerson()))
        {
            error = "Failed to wait for 'Name' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.nameOfSalesPerson(), name))
        {
            error = "Failed to enter " + name + " into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.surnameOfSalesPerson(), surname))
        {
            error = "Failed to enter " + surname + " into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.identificationTypeOfSalesPerson(),identificationType))
        {
            error = "Failed to select the'IDENTIFICATION TYPE' from the list.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.idNumberOfSalesPerson()))
        {
            error = "Failed to wait for 'ID Number'text field to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.idNumberOfSalesPerson(), idNumber))
        {
            error = "Failed to enter " + idNumber + " into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.emailAddressOfSalesPerson(), emailAddress))
        {
            error = "Failed to enter " + emailAddress + "  into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.cellphoneOfSalesPerson(), cellphone))
        {
            error = "Failed to enter " + cellphone + " into the text field.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.titleOfSalesPerson()))
        {
            error = "Failed to wait for the 'Title' field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.titleOfSalesPerson(),title))
        {
            error = "Failed to select the'title' field.";
            return false;
        }
        testData.extractParameter("name : ", name, "");
        testData.extractParameter("Surname : ", surname, "");
        testData.extractParameter("Identification Type : ", identificationType, "");
        testData.extractParameter("ID Number : ", idNumber, "");
        testData.extractParameter("Email Address : ", emailAddress, "");
        testData.extractParameter("Cell Phone : ", cellphone, "");
        testData.extractParameter("Title : ", title, "");

        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.epsonMototorsCheckBox()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.epsonMototorsCheckBox()))
            {
                error = "Failed to select the EPSON MOTORS check box.";
                narrator.stepFailedWithScreenShot(error);
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully validated that the EPSON MOTORS  check box is checked.");
        if (SeleniumDriverInstance.isSelected(LightStonePageObjects.signioPtyLtdCheckBox()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.signioPtyLtdCheckBox()))
            {
                error = "Failed to select the SIGNO (PTY) LTD check box.";
                narrator.stepFailedWithScreenShot(error);
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully validated that the SIGNIO(PTY) LTD check box is un-checked.");

        narrator.stepPassedWithScreenShot("Successfully captured SALESPERSONS :" + name + " " + surname + " credentials.");
        pause(1000);
       
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.saveSalesPerson()))
        {
            error = "Failed to click on save SALESPERSON.";
            return false;
        }
          if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.closeButtonSalesPerson()))
        {
            error = "Failed to click on close button.";
            return false;
        }
        SeleniumDriverInstance.dismissAlert();
        
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.salepersonSavedValidation(idNumber),5))
        {
            error = "Failed to validate Salesperson has been created.";
            narrator.stepFailedWithScreenShot(error);
            return false;
        }

        return true;
    }
}
=======
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Manage_SalesPerson;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.DOC_Epson;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.ConstructID;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Manage Salesperson",
        createNewBrowserInstance = false
)

public class ManageSalesPerson extends BaseClass
{

    String error = "";
    ConstructID Id = new ConstructID("male",20,60);

    public ManageSalesPerson()
    {
    }

    public TestResult executeTest()
    {
        if (!manageSalesPerson())
        {
            return narrator.testFailed("Failed to create and save the details of the salesperson - " + error);
        }

        return narrator.finalizeTest("Successfully created  and saved the details of the salesperson.");
    }

    public boolean manageSalesPerson()
    {
        
        String name = testData.getData("Name");
        String surname = testData.getData("Surname");
        String identificationType = testData.getData("Identification Type");
       // String idNumber = testData.getData("ID Number");
        String idNumber = Id.getID();
        String emailAddress = testData.getData("Email Address");
        String cellphone = testData.getData("Cell Phone");
        String title = testData.getData("Title");
        String manageSalesPerson = testData.getData("Manage Salesperson");


        /*Entering manage salesperson details*/
        DOC_Epson docNavigation = new DOC_Epson();
        if (!docNavigation.navigateDOC())
        {
            error = "Failed to navigate to DOC.";
            return false;
        }     
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.doc()))
        {
            error = "Failed to wait for the DOC tab to be present.";
            return false;
        }

        //narrator.stepPassedWithScreenShot("Successfully landed on the home page.");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.doc()))
        {
            error = "Failed to click on the DOC drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.manageSalesPersons()))
        {
            error = "Failed to wait for the MANAGE SALESPERSON drop down.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully showing MANAGE SALESPERSON from the drop down elements.");

        if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.manageSalesPersons(), manageSalesPerson))
        {
            error = "Failed to validate that 'MANAGE SALESPERSON' is present by text.";
            return false;
        }

        narrator.stepPassed("Successfully validated that: " + manageSalesPerson + " is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.manageSalesPersons()))
        {
            error = "Failed to click MANAGE SALESPERSON.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to the SALESPERSON tab.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.salesPersonsHeader()))
        {
            error = "Failed to wait for the SALESPERSONS header.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully landed on the SALESPERSONS page.");

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to wait for the ADD NEW button.";
            return false;
        }
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to click on ADD NEW button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to click on ADD NEW button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientDetails()))
        {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewSalesPerson()))
            {
                error = "Failed to click on ADD NEW button.";
                return false;
            }
//            error = "Failed to wait for the Client detail's bar to appear.";
//            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked 'Add New' and opened 'Capture Salesperson' window");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.nameOfSalesPerson()))
        {
            error = "Failed to wait for 'Name' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.nameOfSalesPerson(), name))
        {
            error = "Failed to enter " + name + " into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.surnameOfSalesPerson(), surname))
        {
            error = "Failed to enter " + surname + " into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.identificationTypeOfSalesPerson(),identificationType))
        {
            error = "Failed to select the'IDENTIFICATION TYPE' from the list.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.idNumberOfSalesPerson()))
        {
            error = "Failed to wait for 'ID Number'text field to appear.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.idNumberOfSalesPerson(), idNumber))
        {
            error = "Failed to enter " + idNumber + " into the text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.emailAddressOfSalesPerson(), emailAddress))
        {
            error = "Failed to enter " + emailAddress + "  into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.cellphoneOfSalesPerson(), cellphone))
        {
            error = "Failed to enter " + cellphone + " into the text field.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.titleOfSalesPerson()))
        {
            error = "Failed to wait for the 'Title' field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.titleOfSalesPerson(),title))
        {
            error = "Failed to select the'title' field.";
            return false;
        }
        testData.extractParameter("name : ", name, "");
        testData.extractParameter("Surname : ", surname, "");
        testData.extractParameter("Identification Type : ", identificationType, "");
        testData.extractParameter("ID Number : ", idNumber, "");
        testData.extractParameter("Email Address : ", emailAddress, "");
        testData.extractParameter("Cell Phone : ", cellphone, "");
        testData.extractParameter("Title : ", title, "");

        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.epsonMototorsCheckBox()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.epsonMototorsCheckBox()))
            {
                error = "Failed to select the EPSON MOTORS check box.";
                narrator.stepFailedWithScreenShot(error);
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully validated that the EPSON MOTORS  check box is checked.");
        if (SeleniumDriverInstance.isSelected(LightStonePageObjects.signioPtyLtdCheckBox()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.signioPtyLtdCheckBox()))
            {
                error = "Failed to select the SIGNO (PTY) LTD check box.";
                narrator.stepFailedWithScreenShot(error);
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully validated that the SIGNIO(PTY) LTD check box is un-checked.");

        narrator.stepPassedWithScreenShot("Successfully captured SALESPERSONS :" + name + " " + surname + " credentials.");
        pause(1000);
       
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.saveSalesPerson()))
        {
            error = "Failed to click on save SALESPERSON.";
            return false;
        }
          if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.closeButtonSalesPerson()))
        {
            error = "Failed to click on close button.";
            return false;
        }
        SeleniumDriverInstance.dismissAlert();
        
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.salepersonSavedValidation(idNumber),5))
        {
            error = "Failed to validate Salesperson has been created.";
            narrator.stepFailedWithScreenShot(error);
            return false;
        }

        return true;
    }
}
>>>>>>> d07de976829ca921ddd915f7730068c7a5e2d2f4
