/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Applicant Income Details",
        createNewBrowserInstance = false
)
public class ApplicantIncomeDetails extends BaseClass
{

    String error = "";
    String remuneration = getData("Remuneration");
    String net = getData("Net Take-home");
    String rent = getData("Rent Income");
    String commission = getData("Commission");
    String amount = getData("Amount");

    public ApplicantIncomeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!applicantIncomeDetails())
        {
            return narrator.testFailed("Failed to select a source of funds -" + error);
        }
        return narrator.finalizeTest("Successfully selected a source of funds");
    }

    public boolean applicantIncomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicantIncomeDetails()))
        {
            error = "Failed to wait for the 'Podium Details'";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.grossRemuneration()))
        {
            error = "Failed to clear the 'Gross remuneration' field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.grossRemuneration(), remuneration))
        {
            error = "Failed to enter the 'Gross remuneration' field";
            return false;
        }
        narrator.stepPassed("Gross Remuneration : " + remuneration);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.monthlyCommission()))
        {
            error = "Failed to clear the 'monthly commission'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.monthlyCommission(), commission))
        {
            error = "Failed to enter the 'monthly commission'";
            return false;
        }
        narrator.stepPassed("Monthly Commission : " + commission);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.overTime()))
        {
            error = "Failed to clear the 'overtime '";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.overTime(), amount))
        {
            error = "Failed to enter the 'overtime'";
            return false;
        }
        narrator.stepPassed("Overtime : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.carAllowance()))
        {
            error = "Failed to clear the 'car allowance'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.carAllowance(), amount))
        {
            error = "Failed to enter the 'car allowance'";
            return false;
        }
        narrator.stepPassed("Car allowance : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.netIncome()))
        {
            error = "Failed to clear the 'Net Take-Home'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.netIncome(), net))
        {
            error = "Failed to enter the 'Net Take-Home'";
            return false;
        }
        narrator.stepPassed("Net Take-home Pay : " + net);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.rentalIncome()))
        {
            error = "Failed to clear the 'Rent Income'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.rentalIncome(), rent))
        {
            error = "Failed to enter the 'Rent Income'";
            return false;
        }
        narrator.stepPassed("Rent Income : " + rent);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.maintenanceIncomeDetails()))
        {
            error = "Failed to clear  the 'Maintenance'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.maintenanceIncomeDetails(), amount))
        {
            error = "Failed to enter the 'Maintenance'";
            return false;
        }
        narrator.stepPassed("Maintenance : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherIncome()))
        {
            error = "Failed to clear the 'Other Income'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherIncome(), amount))
        {
            error = "Failed to enter the 'Other Income'";
            return false;
        }
        narrator.stepPassed("Other Income : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherIncomeDescription()))
        {
            error = "Failed to clear the 'Other Income Description'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherIncomeDescription(), amount))
        {
            error = "Failed to enter the 'Other Income Description'";
            return false;
        }
        narrator.stepPassed("Other Income Description : " + amount);
        SeleniumDriverInstance.pressKey(Keys.TAB);

        String netHome = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.netIncome(),"value");
        String otherIncome = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.otherIncome(),"value");
        String totalIncome = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.tmI(),"value");
        double netHomeAmount = Double.parseDouble(netHome);
        double otherIncomeAmount = Double.parseDouble(otherIncome);

        double sum = netHomeAmount + otherIncomeAmount;
        double totalAmount = Double.parseDouble(totalIncome);

        if (sum != totalAmount)
        {
            error = "Failed to validate the total monthly Income";
            return false;
        }
        narrator.stepPassed("Successfully validated the total monthly income field");
        return true;
    }

}
