/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Banking Details",
        createNewBrowserInstance = false
)
public class BankingDetails extends BaseClass
{

    String error = "";
    String option = getData("Option");
    String bankName = getData("Bank Name");
    String accountNumber = getData("Account Number");
    String accountType = getData("Account Type");
    String bankBranch = getData("Branch");
    String branchCode = getData("Branch Code");

    public BankingDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!bankingDetails())
        {
            return narrator.testFailed("Failed to enter the customer banking details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the customer banking details");
    }

    public boolean bankingDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumBankingDetails()))
        {
            error = "Failed to scroll to the 'Banking Details' container";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.validAccountNumber(), option))
        {
            error = "Failed to select the 'Valid Bank' option";
            return false;
        }
        narrator.stepPassed("I have a valid bank account number : " + option);
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.accountHolderCheckBox()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.accountHolderCheckBox()))
            {
                error = "Failed to tick the 'account holder' checkBox";
                return false;
            }
        }
        String accountHolder = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.accountHolder(),"value");
        narrator.stepPassed("Account Holder : " + accountHolder);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankNameDropDown(), bankName))
        {
            error = "Failed to select the 'Bank Name' option";
            return false;
        }
        narrator.stepPassed("Bank Name : " + bankName);
        narrator.stepPassed("Bank Branch  : "+bankBranch);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.accountNumber()))
        {
            error = "Failed to clear  the 'account number'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountNumber(), accountNumber))
        {
            error = "Failed to enter the 'account number'";
            return false;
        }
        narrator.stepPassed("Account Number : " + accountNumber);
         SeleniumDriverInstance.pressKey(Keys.TAB);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.branchCode()))
        {
            error = "Failed to wait for the 'Branch Code'";
            return false;
        }
        narrator.stepPassed("Branch Code : " +branchCode);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.accountType(), accountType))
        {
            error = "Failed to select the 'account type' option";
            return false;
        }
        narrator.stepPassed("Account type : " + accountType);

        return true;
    }

}
