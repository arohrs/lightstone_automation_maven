/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "My Profile",
        createNewBrowserInstance = false
)

public class MyProfile extends BaseClass
{

    String error = "";

    public MyProfile()
    {
    }

    public TestResult executeTest()
    {
        if (!newApplicationDetails())
        {
            return narrator.testFailed("Failed to enter customer's details on the 'e-application' Form - " + error);
        }
        return narrator.finalizeTest("Successfully entered customer's details on the 'e-application' Form.");
    }

    public boolean newApplicationDetails()
    {

       LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());
       
       
        /*Creating a new application*/        
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.mySignioDropDown())))
        {
            error = "Failed to click  the 'My Signio drop down' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.myProfileDropDown())))
        {
            error = "Failed to select 'new application' from the list of applications.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.eComplianceHeader()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }

        /*Entering customer details on the new application*/
       
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.profileTab()))
        {
            error = "Failed to click the 'id nnumber label.'";
            return false;
        }
        if (!SeleniumDriverInstance.dismissAlert())
        { //cancells an alert
            error = "Failed to handle an alert.";
            return false;
        }
        narrator.stepPassed("ID Number: " + " " + getData("ID"));
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.applicationVerifiedDropDown()))
        {
            error = "Failed to wait for the 'application form tab' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.applicationVerifiedDropDown(), getData("Verified")))
        {
            error = "Failed to select Yes/NO to check if or not an application is verified.";
            return false;
        }
        narrator.stepPassed("ID Verified: " + " " + getData("Verified"));
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.clickAbsaBank()))
        {
            error = "Failed to wait for the 'absa bank' check box to appear";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickAbsaBank())))
        {
            error = "Failed to click the 'absa bank' check-box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickMfc())))
        {
            error = "Failed to click the 'mfc' check-box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickStandardBank())))
        {
            error = "Failed to click the 'standard bank' check-box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath((LightStonePageObjects.clickWesbank())))
        {
            error = "Failed to click the 'wesbank' check-box.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.indicationTypeDropDown()))
        {
            error = "Failed to wait for the 'indication type' check box to appear";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.indicationTypeDropDown(), getData("Indication Type")))
        {
            error = "Failed to select the 'indication type' from the list.";
            return false;
        }
        narrator.stepPassed("Indication Type: " + getData("Indication Type"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.driversLicenseDropDown(), getData("License")))
        {
            error = "Failed to select 'Yes/No' to check if or not the license is verified";
            return false;
        }
        narrator.stepPassed("Drivers License Verified: " + " " + getData("License"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaDealerCodeDropDown(), getData("ABSA")))
        {
            error = "Failed to select 'absa dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected ABSA - Dealer Code: " + " " + getData("ABSA"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mfcDealerCodeDropDown(), getData("MFC")))
        {
            error = "Failed to select the 'mfc dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected MFC - Dealer Code :" + " " + getData("MFC"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.standardBankDealerCodeDropDown(), getData("STANDARD BANK")))
        {
            error = "Failed to select the 'standard dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected STANDARD BANK - Dealer Code: " + " " + getData("STANDARD BANK"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.wesBankDealerCodeDropDown(), getData("WESBANK")))
        {
            error = "Failed to select the 'wesbank dealer code from the list.";
            return false;
        }
        narrator.stepPassed("Selected WESBANK - Dealer Code: " + " " + getData("WESBANK"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.branchDropDown()))
        {
            error = "Failed to wait for the 'branch Code' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.branchDropDown(), getData("Branch Code")))
        {
            error = "Failed to select the 'branch Code' from the list.";
            return false;
        }
        narrator.stepPassed("WESBANK - Branch Code: " + " " + getData("Branch Code"));
        return true;
    }

  
}
