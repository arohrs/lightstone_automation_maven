/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Marital Details",
        createNewBrowserInstance = false
)
public class MaritalDetails extends BaseClass
{

    String error = "";
    String statusList = getData("Status List");
    String maritalContract = getData("Marital Contract");
    String dateMarried = getData("Date Married");
    String name = getData("Name");
    String middleName = getData("Middle Name");
    String idType = getData("ID Type");
    String idNumber = getData("ID Number");
    String cellNumber = getData("Mobile Number");
    String dob = getData("Dob");

    public MaritalDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!maritalDetails())
        {
            return narrator.testFailed("Failed to enter the marital's details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the marital's details");
    }

    public boolean maritalDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.maritalDetailsTab()))
        {
            error = "Failed to scroll to the 'Marital Details'";
            return false;
        }
        List<String> status = Arrays.asList(statusList.split(","));
        
        //loop through the marital status
        for (String statusList : status)
        {
            if (!statusList.equalsIgnoreCase("Married"))
            {
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maritalStatusDropDown(), statusList))
                {
                    error = "Failed to select the 'Marital Status'";
                    return false;
                }
                pause(500);

            }
            if (statusList.equalsIgnoreCase("Married"))
            {
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maritalStatusDropDown(), statusList))
                {
                    error = "Failed to select the 'Marital Status'";
                    return false;
                }
                narrator.stepPassed("Marital Status : " + statusList);
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maritalContract(), maritalContract))
                {
                    error = "Failed to select the 'Marital Contract'";
                    return false;
                }
                narrator.stepPassed("Marital Contract  : " + maritalContract);
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.marriedDate(), dateMarried))
                {
                    error = "Failed to enter the 'Date Married'";
                    return false;
                }
                narrator.stepPassed("Date Married : " + dateMarried);
                SeleniumDriverInstance.pressKey(Keys.TAB);

            }

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.spouseFirstName()))
        {
            error = "Failed to wait for the 'Spouse First Name'";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseFirstName()))
        {
            error = "Failed to clear the 'Spouse First Name'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseFirstName(), name))
        {
            error = "Failed to enter the 'Spouse First Name'";
            return false;
        }
        narrator.stepPassed("First Name : " + name);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseMiddleName(), middleName))
        {
            error = "Failed to enter the 'Middle Name'";
            return false;
        }
        narrator.stepPassed("Middle Name : " + middleName);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseSurname()))
        {
            error = "Failed to clear the 'Spouse Surname'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseSurname(), name))
        {
            error = "Failed to enter the 'Spouse Surname'";
            return false;
        }
        narrator.stepPassed("Surname : " + name);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.spouseIDType(), idType))
        {
            error = "Failed to select the 'Identification Type'";
            return false;
        }
        narrator.stepPassed("Identification Type : " + idType);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseIDNumber()))
        {
            error = "Failed to clear the 'ID Number'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseIDNumber(), idNumber))
        {
            error = "Failed to enter the 'ID Number'";
            return false;
        }
        narrator.stepPassed("ID Number : " + idNumber);
        SeleniumDriverInstance.pressKey(Keys.TAB);
        pause(500);

        String dobWebsite = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.spouseDOB(),"value");

        if (!dobWebsite.equals(dob))
        {
            narrator.stepWarning("Failed to validate 'Date of birth '");

        }
        narrator.stepPassed("Successfully validated the date of birth :"+dob);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseMobileNumber()))
        {
            error = "Failed to clear the 'Mobile Number'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseMobileNumber(), cellNumber))
        {
            error = "Failed to enter the 'Mobile Number'";
            return false;
        }
        narrator.stepPassed("Mobile Number : " + cellNumber);
        SeleniumDriverInstance.pressKey(Keys.TAB);

        return true;
    }

}
