/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Dealer Extras",
        createNewBrowserInstance = false
)
public class DealerExtras extends BaseClass
{
    String error="";
    String description = getData("Description");
    String amount = getData("Amount");
    
    public DealerExtras()
    {
    }
    
    public TestResult executeTest()
    {
        if(!dealerExtra())
        {
            narrator.testFailed("Failed to enter the dealer extras details -"+error);
        }
    
    
    
        return narrator.finalizeTest("Successfully entered the dealer extras details");
    }
    
    public boolean dealerExtra()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionDealerExtraTab()))
        {
            error = "Failed to select the 'Dealer Extras ' container.";
            return false;
        }
         if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.transactionAddNewExtra()))
        {
            error = "Failed to click  the 'Add New Extras' button.";
            return false;
        }
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionExtraDescription(),description))
        {
            error = "Failed to select 'Extra Description'.";
            return false;
        }
          narrator.stepPassed("Description : "+description);
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionExtraAmount(),amount))
        {
            error = "Failed to enter  the 'Amount ' in the text field.";
            return false;
        }
          narrator.stepPassed("Description : "+amount);
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.transactionExtraTotalAmount()))
        {
            error = "Failed to wait Extra Total Amount textfield.";
            return false;
        }   
          narrator.stepPassed("Successfully validated the Total Extra Amount field is not manually edittable");
    
        return true;
    }
    
    
    
    
    
}
