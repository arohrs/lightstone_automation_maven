/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.ArrayList;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Monthly_DOC",
        createNewBrowserInstance = false
)

public class MonthlyDOC extends BaseClass
{

    String error = "";
    ArrayList<String> details;
    String rowDetails = "";
    String monthlyDocValidation = getData("Monthly DOC");

    public MonthlyDOC()
    {
    }

    public TestResult executeTest()
    {

        if (!monthlyDOC())
        {
            return narrator.testFailed("Failed to search customers on Monthly Doc  - " + error);
        }
        if (!fandISearch())
        {
            return narrator.testFailed("Failed to search customers on Monthly Doc - " + error);
        }
        if (!displayTSpinner())
        {
            return narrator.testFailed("Failed to number of Enteries to be displayed - " + error);
        }

        return narrator.finalizeTest("Successfully Searched customers using all Search Criterias.");
    }

    public boolean monthlyDOC()
    {

        DOC_Epson doc = new DOC_Epson();
        if (!doc.navigateDOC())
        {
            error = "Failed to navigate to Epson motors.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.docDropDown()))
        {
            error = "Failed to click 'DOC' dropdown button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.monthlyDoc()))
        {
            error = "Failed to wait for the 'Monthly DOC ' from the dropdown list.";
            return false;
        }
        if (!monthlyDocValidation.isEmpty())
        {
            if (!SeleniumDriverInstance.ValidateByText(LightStonePageObjects.monthlyDoc(), monthlyDocValidation))
            {
                error = "Failed to validate that 'Monthly DOC ' is present by text.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully showing Doc drop down elements.");
            narrator.stepPassed("Successfully validated that: " + monthlyDocValidation + " is present.");
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.monthlyDoc()))
        {
            error = "Failed to click 'Monthly DOC' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to wait for the 'Monthly DOC page' to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to click Doc Current Month Header";
            return false;

        }
        String number = getData("Display Spinner");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.displaySpinner(), number))
        {
            error = "Failed to select " + number + " from Display Total Spinner";
            return false;
        }
        details = extractDetails();
        String clientName = details.get(1);
        if (!searchCriteria(LightStonePageObjects.searchClientName(), clientName, 1))
        {
            error = "Failed to search according to Search Criteria: " + clientName;
            return false;
        }
        String clientSurname = details.get(2);
        if (!searchCriteria(LightStonePageObjects.searchClientSurname(), clientSurname, 2))
        {
            error = "Failed to search according to Search Criteria: " + clientName;
            return false;
        }
        String clientID = details.get(4);
        if (!searchCriteria(LightStonePageObjects.searchClientIDNumber(), clientID, 4))
        {
            error = "Failed to search using criteria 'Client ID Number'.";
            return false;
        }
        String clientreferenceNum = details.get(5);
        if (!searchCriteria(LightStonePageObjects.referenceNum(), clientreferenceNum, 5))
        {
            error = "Failed to search using criteria 'Client Reference Number'.";
            return false;
        }
        String[] index6 = details.get(6).split(" ");
        String bankName = (index6[0].substring(0, 1) + index6[0].substring(1, index6[0].length()).toLowerCase()) + " "
                + (index6[1].substring(0, 1) + index6[1].substring(1, index6[1].length()).toLowerCase());

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.searchBankName(), bankName))
        {
            error = "Failed to select " + bankName + " from the dropdown list.";
            return false;
        }
        narrator.stepPassed("Succesfully entered: " + bankName);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait to wait for result";
            return false;

        }
        if (!validateDetails(bankName, 6))
        {
            error = "Failed to validate results.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Succesfully searched using Search Criteria: " + bankName);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.previousDocMonth()))
        {
            error = "Failed to scroll to Previous Month Table";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.displayPrev(), 2))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.previousDocMonth()))
            {
                error = "Failed to refreash Prevous Month Table";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.previousDocMonth()))
            {
                error = "Failed to wait for Prevous Month Table";
                return false;
            }
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(LightStonePageObjects.previousDocMonth()))
            {
                error = "Failed to refreash Prevous Month Table";
                return false;
            }
            searcMaskHandling();
            narrator.stepPassed("Double clicked Previous month table");

        } else
        {
            if (!SeleniumDriverInstance.doubleClickElementbyXpath(LightStonePageObjects.previousDocMonth()))
            {
                error = "Failed to refreash Prevous Month Table";
                return false;
            }
            searcMaskHandling();
            narrator.stepPassed("Double clicked Previous month table");
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for results";
            return false;
        }
        narrator.stepPassedWithScreenShot("Refreashed Previous Month Table");
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to scroll to Current Month Table";
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to refreash Current Month Table";
            return false;
        }

        searcMaskHandling();
        narrator.stepPassed("Double clicked Current month table");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for results";
            return false;
        }
        narrator.stepPassedWithScreenShot("Refreashed Current Month Table");

        return true;
    }

    public boolean fandISearch()
    {
        ArrayList<String> dealStatus = new ArrayList<>();
        String incompleteStatus = getData("Status1");
        String completeStatus = getData("Status2");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchDealStatus()))
        {
            error = "Failed to wait for the 'deal status' list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.searchDealStatus(), incompleteStatus))
        {
            error = "Failed to select the'deal status' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to wait for the 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for the 'searched DOC' result .";
            return false;

        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.inCompleteDealStatus()))
        {
            error = "Failed to wait for the 'Incomplete Deal' status .";
            return false;

        }
        narrator.stepPassedWithScreenShot("Displayed results of " + incompleteStatus + " successfully.");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchDealStatus()))
        {
            error = "Failed to wait for the 'deal status' list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.searchDealStatus(), completeStatus))
        {
            error = "Failed to select the'deal status' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to wait for the 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for the 'searched DOC' result .";
            return false;

        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.completeDealStatus()))
        {
            error = "Failed to wait for the 'Complete Deal' status .";
            return false;

        }
        narrator.stepPassedWithScreenShot("Displayed results of " + completeStatus + " successfully.");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }

        String fAndIName = getData("F&I6");

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fAndI()))
        {
            error = "Failed to wait for the 'F&I' list.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fAndI(), fAndIName))
        {
            error = "Failed to select 'F&I' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click  the 'Search' button.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for the 'searched DOC' result .";
            return false;

        }
        narrator.stepPassedWithScreenShot("Displayed results of " + fAndIName + " successfully.");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click  the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fAndI()))
        {
            error = "Failed to wait for the 'F&I' list.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to wait for the 'Current DOC' Header .";
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(LightStonePageObjects.currentMonthDOC()))
        {
            error = "Failed to double click  the 'Current DOC' Header.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click  the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.previousDocMonth()))
        {
            error = "Failed to wait for the 'Previous DOC' Header.";
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(LightStonePageObjects.previousDocMonth()))
        {
            error = "Failed to double click for the 'Previous DOC' Header.";
            return false;
        }
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }
        return true;
    }

    public boolean searcMaskHandling()
    {
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchLoadMask(), 5))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchLoadMask(), 120))
            {
                error = "Failed to handle the search mask";
                return false;
            }

        }
        return true;
    }

    public boolean searchCriteria(String xPath, String criteria, int indexCriteria)
    {
        if (!SeleniumDriverInstance.waitForElementByXpath(xPath))
        {
            error = "Failed to wait for the 'Client Name' Textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(xPath, criteria))
        {
            error = "Failed to enter 'Client Name' in the Textfield.";
            return false;
        }
        narrator.stepPassed("Succesfully entered text: " + criteria + " into textfield.");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to wait for the 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchCriteriaButton()))
        {
            error = "Failed to click for the 'Search' button.";
            return false;
        }
        narrator.stepPassed("Clicked Search button");
        searcMaskHandling();
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resultRow()))
        {
            error = "Failed to wait for the 'searched DOC' current Month.";
            return false;
        }
        if (!validateDetails(criteria, indexCriteria))
        {
            error = "Failed to validate results.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Succesfully searched: " + criteria);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to wait for the 'clear' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clearSearch()))
        {
            error = "Failed to click for the 'clear' button.";
            return false;
        }
        narrator.stepPassed("Clicked 'Clear' button");

        return true;
    }

    public boolean displayTSpinner()
    {

        ArrayList<String> rows = new ArrayList<>();

        for (int i = 5; i < 21; i += 5)
        {
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.displaySpinner()))
            {
                error = "Failed to scroll to display Spinner";
                return false;
            }
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.displaySpinner(), Integer.toString(i)))
            {
                error = "Failed to select " + Integer.toString(i) + " from Display Total Spinner";
                return false;
            }
            searcMaskHandling();
            narrator.stepPassed("Selected: " + i + " on Display Total Spinner");
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.currentMonthDOC()))
            {
                error = "Failed to Current Month Doc";
                return false;
            }
            rows = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.rowCount());
            if (rows.size() != i)
            {
                error = "Enteries displayed does not equal number selected on 'Display Spinner' ";
                return false;

            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.displaySpinner()))
            {
                error = "Failed to scroll to display Spinner";
                return false;
            }
            narrator.stepPassedWithScreenShot("Entries displayed equal number selected in Display Total spinner");
        }
        String number = getData("Display Spin");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.displaySpinner(), number))
        {
            error = "Failed to select " + number + " from Display Total Spinner";
            return false;
        }
        rows = extractDetails();
        String before = rows.get(4);

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.nextLink()))
        {
            error = "Failed to wait for 'Next Link'";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.nextLink()))
        {
            error = "Failed to click 'Next Link'.";
            return false;
        }

        narrator.stepPassed("Clicked 'Next' link.");
        rows = extractDetailsAnyBank();
        String after = rows.get(4);
        if (before.equals(after)) //if true then next link failed
        {
            error = "Failed to click Next Link";
            return false;

        } else //false - nextLink succesful
        {
            narrator.stepPassedWithScreenShot("Succesfully displayed entries on the next page");
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.previousLink()))
        {
            error = "Failed to wait for 'Previous Link'";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.previousLink()))
        {
            error = "Failed to click 'Previous Link'";
            return false;
        }

        return true;
    }

    public ArrayList<String> extractDetails()
    {

        ArrayList<String> rowdetails = new ArrayList<>();
        ArrayList<String> noRows = new ArrayList<>();
        noRows = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.rowCount());
        Search:
        for (int cnt = 0; cnt < noRows.size(); cnt++)
        {
            rowdetails = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.monthTableRow(cnt));
            if (!rowdetails.get(1).isEmpty() && !rowdetails.get(2).isEmpty())
            {
                if (!rowdetails.get(3).isEmpty() && !rowdetails.get(4).isEmpty())
                {
                    if (!rowdetails.get(5).isEmpty() && !rowdetails.get(6).isEmpty() && rowdetails.get(6).equals("ABSA BANK"))
                    {
                        break Search;
                    }
                }
            }

        }
        return rowdetails;
    }

    public ArrayList<String> extractDetailsAnyBank()
    {
        ArrayList<String> rowdetails = new ArrayList<>();
        ArrayList<String> noRows = new ArrayList<>();
        noRows = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.rowCount());
        Search:
        for (int cnt = 0; cnt < noRows.size(); cnt++)
        {
            rowdetails = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.monthTableRow(cnt));
            if (!rowdetails.get(2).equals(details.get(2)))
            {
                break Search;
            }
        }
        return rowdetails;
    }

    public boolean validateDetails(String criteria, int index)
    {
        ArrayList<String> noRows = new ArrayList<>();
        noRows = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.monthTableRow(0));
        if (!noRows.get(index).equalsIgnoreCase(criteria))
        {
            return false;
        }

        return true;
    }
}
