/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Address Details",
        createNewBrowserInstance = false
)
public class AddressDetails extends BaseClass
{

    String error = "";
    String address = getData("Address");
    String city = getData("City");
    String suburb = getData("Suburb");
    String postalCode = getData("Postal Code");
    String postalAddress = getData("Postal Address");
    String period = getData("Period");

    public AddressDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!addressDetails())
        {
            return narrator.testFailed("Failed to enter the customer's address details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the customer's address details");
    }

    public boolean addressDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumPersonalDetails()))
        {
            error = "Failed to scroll to the 'Podium Details' container";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.address1()))
        {
            error = "Failed to clear  the 'Residential Address 1' field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.address1(), address))
        {
            error = "Failed to enter  the 'Residential Address 1'";
            return false;
        }
        narrator.stepPassed("Residential Address 1 : " + address);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.address2()))
        {
            error = "Failed to clear clear  the 'Residential Address 2' field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.address2(), address))
        {
            error = "Failed to enter the 'Residential Address 2'";
            return false;
        }
        narrator.stepPassed("Residential Address 2 : " + address);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.address3()))
        {
            error = "Failed to clear clear  the 'Residential Address 3' field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.address3(), address))
        {
            error = "Failed to enter the 'Residential Address 3'";
            return false;
        }
        narrator.stepPassed("Residential Address 3 : " + address);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.residentialCity()))
        {
            error = "Failed to clear for the 'Podium Details'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.residentialCity(), city))
        {
            error = "Failed to enter for the 'Podium Details'";
            return false;
        }
        narrator.stepPassed("Residential City : " + city);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.residentialSuburb()))
        {
            error = "Failed to clear the 'Residential Suburb'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.residentialSuburb(), suburb))
        {
            error = "Failed to enter the 'Residential Suburb'";
            return false;
        }
        narrator.stepPassed("Residential Suburb : " + suburb);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.residentialPostalCode()))
        {
            error = "Failed to clear the 'Residential Postal Code'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.residentialPostalCode(), postalCode))
        {
            error = "Failed to enter the 'Residential Postal Code'";
            return false;
        }
        narrator.stepPassed("Residential Postal Code  : " + postalCode);
         addressLookUp();
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.postalAdressDropDown(), postalAddress))
        {
            error = "Failed to select the '"+postalAddress+"' from the list ";
            return false;
        }
        narrator.stepPassed("Postal Address : " + postalAddress);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.periodYear()))
        {
            error = "Failed to clear the 'YY period at current address'.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.periodYear(), period))
        {
            error = "Failed to enter the 'YY period at current address' ";
            return false;
        }
        narrator.stepPassed("Current Address (Year): " + period);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.periodMonth()))
        {
            error = "Failed to select the 'MM period at current address' ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.periodMonth(), period))
        {
            error = "Failed to select the 'MM period at current address'";
            return false;
        }
        narrator.stepPassed("Current Address  (Month): " + period);

        return true;
    }
    
    public boolean addressLookUp()
    {
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.suburb()))
        {
            error = "Failed to click 'suburb' lookup button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchSuburb(), 5))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.searchSuburb(), city))
        {
            error = "Failed to enter 'suburb' into the search text field.";
            return false;
        }
        narrator.stepPassed("Search Suburb : " + city);

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchButton()))
        {
            error = "Failed to click on the 'sububrb' search button.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectSuburbDropDown(), suburb))
        {
            error = "Failed to select 'suburb'.";
            return false;
        }
        narrator.stepPassed("Select Suburb : " + suburb);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectCodeDropDown(), postalCode))
        {
            error = "Failed to select the 'code' from the list.";
            return false;
        }
        narrator.stepPassed("Select Code : " + postalCode);
         narrator.stepPassedWithScreenShot("Successfully captured the address lookup details");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectOK()))
        {
            error = "Failed to click the 'OK' button.";
            return false;
        }
    
        return true;
    }

}
