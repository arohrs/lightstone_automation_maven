<<<<<<< HEAD
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateDOB;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.ConstructID;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Signatory Details",
        createNewBrowserInstance = false
)
public class SignatoriesDetails extends BaseClass
{

    String error = "";
    String gender = getData("Gender");
    GeographicalDetails address = new GeographicalDetails();

    public SignatoriesDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!signatoriesDetails())
        {

            narrator.testFailed("Failed to enter the signatory's details -" + error);
        }
        if (!signatoryOtherDetails())
        {
            narrator.testFailed("Failed to enter the signatory's details -" + error);
        }

        return narrator.finalizeTest("Successfully entered the signatory's details");
    }

    public boolean signatoriesDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatoriesTab())))
        {
            error = "Failed to scroll to the 'Signatory' tab.";
            return false;
        }
        String signAsSurety = getData("Surety");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signAsSurety(), signAsSurety))
        {
            error = "Failed to select 'sign as Surety' option.";
            return false;
        }
        narrator.stepPassed("Will this Signatory sign as Surety : " + signAsSurety);
        String firstName = getData("First Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryFirstName(), firstName))
        {
            error = "Failed to enter first Name.";
            return false;
        }
        narrator.stepPassed("First name : " + firstName);
        String surname = getData("Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySurName(), surname))
        {
            error = "Failed to enter surname.";
            return false;
        }
        narrator.stepPassed("Surname :" + surname);
        String contactType = getData("Contact Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactType(), contactType))
        {
            error = "Failed to select contact type.";
            return false;
        }
        narrator.stepPassed("Contact Type : " + contactType);
        String dateOfBirth = "";
        GenerateDOB generateDOB = new GenerateDOB(21, 60);
        String dob = generateDOB.getIdDateOfBirth();
        dateOfBirth = generateDOB.getDateOfBirth();
        String id = "";
        try
        {

            ConstructID randID = new ConstructID();
            id = randID.getSouthAfricanID(gender, dob, "South African");
        } catch (Exception e)
        {
            narrator.logDebug("Failed to generate south african ID - " + e);
        }
        String idType = getData("ID Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryIdType(), idType))
        {
            error = "Failed to select id type.";
            return false;
        }
        narrator.stepPassed("Id Type : " + idType);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryIdNumber(), id))
        {
            error = "Failed to enter ID number.";
            return false;
        }
        narrator.stepPassed("ID Number : " + id);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryDateofBirth(), dateOfBirth))
        {
            error = "Failed to enter Date of Birth.";
            return false;
        }
        narrator.stepPassed("Date of Birth : " + dateOfBirth);
        String subType = getData("Sub Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryClientSubType(), subType))
        {
            error = "Failed to select client Sub Type.";
            return false;
        }
        narrator.stepPassed("Client sub type : " + subType);
        String preferredLanguage = getData("Language");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryPreferredLanguage(), preferredLanguage))
        {
            error = "Failed to select prefered language.";
            return false;
        }
        narrator.stepPassed("Preferred language : " + preferredLanguage);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryGender(), gender))
        {
            error = "Failed to select gender.";
            return false;
        }
        narrator.stepPassed("Gender : " + gender);
        String title = getData("Title");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactTitle(), title))
        {
            error = "Failed to select title.";
            return false;
        }
        narrator.stepPassed("Contact Title :" + title);
        String ethnicity = getData("Ethnicity");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryEthnicity(), ethnicity))
        {
            error = "Failed to select ethnicity.";
            return false;
        }
        narrator.stepPassed("Ethnicity :" + ethnicity);
        String employmentSector = getData("Employment Sector");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryEmploymentSector(), employmentSector))
        {
            error = "Failed to select employment sector.";
            return false;
        }
        narrator.stepPassed("Employment sector : " + employmentSector);
        String clientType = getData("Client Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryClientType(), clientType))
        {
            error = "Failed to select client type.";
            return false;
        }
        narrator.stepPassed("Client type : " + clientType);
        String maritalStatus = getData("Marital Status");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMaritalStatus(), maritalStatus))
        {
            error = "Failed to select marital status.";
            return false;
        }
        narrator.stepPassed("Marital Status : " + maritalStatus);
        String maritalContract = getData("Marital Contract");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMaritalContract(), maritalContract))
        {
            error = "Failed to select marital contract.";
            return false;
        }
        narrator.stepPassed("Marital Contract : " + maritalContract);
        String dateMarried = getData("Date Married");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryDateMarried(), dateMarried))
        {
            error = "Failed to enter date of Marriage.";
            return false;
        }
        narrator.stepPassed("Date Married : " + dateMarried);
        String industryType = getData("Industry Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryIndustryType(), industryType))
        {
            error = "Failed to select signatory Industry Type.";
            return false;
        }
        narrator.stepPassed("Industry Type : " + industryType);
        String residenceTime = getData("Residence");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryResidenceYear(), residenceTime))
        {
            error = "Failed to enter date of Residence - Year";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryResidenceMonth(), residenceTime))
        {
            error = "Failed to enter date of Residence - Month";
            return false;
        }
        narrator.stepPassed("Time at Residence (YY/MM):" + residenceTime + "/" + residenceTime);
        String employerTime = getData("Employer");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerYear(), employerTime))
        {
            error = "Failed to enter date of Employment - Year";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerMonth(), employerTime))
        {
            error = "Failed to enter date of Employment - Month";
            return false;
        }
        narrator.stepPassed("Time at Employer (YY/MM):" + employerTime + "/" + employerTime);
        String taxPayer = getData("Tax Payer");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryRegisteredTaxPayer(), taxPayer))
        {
            error = "Failed to select registered tax payer";
            return false;
        }
        narrator.stepPassed("Registered Tax Payer : " + taxPayer);
        String taxNumber = getData("Tax Number");
        if (taxPayer.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryTaxNumber(), taxNumber))
            {
                error = "Failed to enter tax number";
                return false;
            }
            narrator.stepPassed("Tax Number : " + taxNumber);
        }
        String addressLine = getData("Address");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAddressLine1(), addressLine))
        {
            error = "Failed to enter Physical Address - Address Line1";
            return false;
        }
        narrator.stepPassed("Address Line 1 : " + addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAddressLine2(), addressLine))
        {
            error = "Failed to enter Physical Address - Address Line2";
            return false;
        }
        narrator.stepPassed("Address Line 2 : " + addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.signatoryAddressLookup()))
        {
            error = "Failed to click 'Select Suburb' button";
            return false;
        }
        address.addressLookup();
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.signatoryPopulateAddressButton()))
        {
            error = "Failed to click 'Populate with Physical Address' button";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatoriesTab())))
        {
            error = "Failed to scroll to the 'Signatories' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Filled in details so far");

        return true;
    }

    public boolean signatoryOtherDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryHomephoneLabel()))
        {
            error = "Failed to scroll to 'Home Phone' label.";
            return false;
        }
        String homephone = getData("Telephone number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryHomePhone(), homephone))
        {
            error = "Failed to enter Home Phone number.";
            return false;
        }
        narrator.stepPassed("Home Phone : " + homephone);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryWorkPhone(), homephone))
        {
            error = "Failed to enter Work Phone number.";
            return false;
        }
        narrator.stepPassed("Work Phone : " + homephone);
        String cellPhone = getData("Cell number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryCellPhone(), cellPhone))
        {
            error = "Failed to enter Cell Phone number.";
            return false;
        }
        narrator.stepPassed("Cell Phone : " + cellPhone);
        String email = getData("Email");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmailAddress(), email))
        {
            error = "Failed to enter email address.";
            return false;
        }
        narrator.stepPassed("Email : " + email);
        String ownership = getData("Type of ownership");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryTypeOfOwnership(), ownership))
        {
            error = "Failed to select type of Ownership";
            return false;
        }
        narrator.stepPassed("Type of Ownership : " + ownership);
        String propertyValue = getData("Property Value");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryPropertyCurrentValue()))
        {
            error = "Failed to clear Current Property Value textfield";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryPropertyCurrentValue(), propertyValue))
        {
            error = "Failed to enter Current Property Value textfield.";
            return false;
        }
        narrator.stepPassed("Current Property Value : " + propertyValue);
        String balance = getData("Bond Balance");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryPropertyBondBalance()))
        {
            error = "Failed to clear Bond balance textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryPropertyBondBalance(), balance))
        {
            error = "Failed to enter Bond balance.";
            return false;
        }
        narrator.stepPassed("Bond Balance : " + balance);
        String salutation = getData("Salutation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySalutation(), salutation))
        {
            error = "Failed to select salutation.";
            return false;
        }
        narrator.stepPassed("Salutation : " + salutation);
        String bank = getData("Bank Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryBankName(), bank))
        {
            error = "Failed to select Bank Name";
            return false;
        }
        narrator.stepPassed("Bank Name : " + bank);
        String accType = getData("Acc Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryAccountType(), accType))
        {
            error = "Failed to select account type";
            return false;
        }
        narrator.stepPassed("Account Type : " + accType);
        String accHolderName = getData("Acc holder Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAccHolderName(), accHolderName))
        {
            error = "Failed to enter account Holder name.";
            return false;
        }
        narrator.stepPassed("Account Holder's Name : " + accHolderName);
        String accNumber = getData("Acc Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAccNumber(), accNumber))
        {
            error = "Failed to enter account number";
            return false;
        }
        narrator.stepPassed("Account Number : " + accNumber);
        String branchCode = getData("Branch Code");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryBranchCode(), branchCode))
        {
            error = "Failed to enter Branch Code.";
            return false;
        }
        narrator.stepPassed("Branch Code : " + branchCode);
        String occupation = getData("Occupation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryOccupation(), occupation))
        {
            error = "Failed to select occupation.";
            return false;
        }
        narrator.stepPassed("Occupation : " + occupation);
        String employerName = getData("Employer Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerName(), employerName))
        {
            error = "Failed to enter name of employer.";
            return false;
        }
        narrator.stepPassed("Employer Name : " + employerName);
        String employerPhone = getData("Employer Work Phone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerWorkPhone(), employerPhone))
        {
            error = "Failed to enter employer work phone.";
            return false;
        }
        narrator.stepPassed("Employer Work Phone : " + employerPhone);
        String sourceOfIncome = getData("Source of Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySourceofIncome(), sourceOfIncome))
        {
            error = "Failed to enter source of income.";
            return false;
        }
        narrator.stepPassed("Source of Income : " + sourceOfIncome);
        String monthlyIncome = getData("Monthly Income");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryMonthlyIncome()))
        {
            error = "Failed to clear monthly income textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryMonthlyIncome(), monthlyIncome))
        {
            error = "Failed to enter monthly income";
            return false;
        }
        narrator.stepPassed("Monthly Income : " + monthlyIncome);
        String percentage = getData("Share Percentage");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySharePercentage(), percentage))
        {
            error = "Failed to enter share percentage.";
            return false;
        }
        narrator.stepPassed("Share Percentage : " + percentage);
        String networkType = getData("Network Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryNetworkType(), networkType))
        {
            error = "Failed to enter network type.";
            return false;
        }
        narrator.stepPassed("Network Type : " + networkType);
        String suretyType = getData("Surety Type");
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatorySuretyType()))
        {
            error = "Failed to scroll to surety type";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySuretyType(), suretyType))
        {
            error = "Failed to select surety type.";
            return false;
        }
        narrator.stepPassed("Surety Type :" + suretyType);
        String yearInPosition = getData("Year in Position");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryYearInPosition(), yearInPosition))
        {
            error = "Failed to enter year in position";
            return false;
        }
        narrator.stepPassed("Number of Years in Position : " + yearInPosition);
        String foreignIdentity = getData("Foreign Identity");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryForeignIdentity(), foreignIdentity))
        {
            error = "Failed to select foreign identity.";
            return false;
        }
        narrator.stepPassed("Do you have a foreign identity : " + foreignIdentity);
        String multiNationality = getData("Multiple Nationalities");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMultipleNationalities(), multiNationality))
        {
            error = "Failed to select multiNationality.";
            return false;
        }
        narrator.stepPassed("Do you have Multiple Nationalities : " + multiNationality);
        String nationality = getData("Nationality");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryNationality(), nationality))
        {
            error = "Failed to select signatory Nationality.";
            return false;
        }
        narrator.stepPassed("Nationality : " + nationality);
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryNationality1()))
        {
            error = "Failed to scroll to the Nationality1 textfield.";
            return false;
        }
        String otherNationality = getData("Nationality1");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryNationality1(), otherNationality))
        {
            error = "Failed to select signatory Nationality1.";
            return false;
        }
        narrator.stepPassed("Nationality 1: " + otherNationality);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryNationality2(), otherNationality))
        {
            error = "Failed to select signatory Nationality2.";
            return false;
        }
        narrator.stepPassed("Nationality 2: " + otherNationality);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryCountryofResidence(), otherNationality))
        {
            error = "Failed to select country of Resdience.";
            return false;
        }
        narrator.stepPassed("Country of Residence : " + otherNationality);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryCountryOfBirth(), otherNationality))
        {
            error = "Failed to select country of birth.";
            return false;
        }
        narrator.stepPassed("Country of Birth : " + otherNationality);
        String designation = getData("Designation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactDesignation(), designation))
        {
            error = "Failed to select designation.";
            return false;
        }
        narrator.stepPassed("Contact Designation : " + designation);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployeeNumber(), getData("Emp Number")))
        {
            error = "Failed to enter employee number";
            return false;
        }
        narrator.stepPassed("Employee Number : " + getData("Emp Number"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isUSCitizen(), getData("US Citizen")))
        {
            error = "Failed to select US Citizenship";
            return false;
        }
        narrator.stepPassed("Are you a US citizen : " + getData("US Citizen"));

        return true;
    }

}
=======
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateDOB;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.ConstructID;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Signatory Details",
        createNewBrowserInstance = false
)
public class SignatoriesDetails extends BaseClass
{

    String error = "";
    String gender = getData("Gender");
    GeographicalDetails address = new GeographicalDetails();

    public SignatoriesDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!signatoriesDetails())
        {

            narrator.testFailed("Failed to enter the signatory's details -" + error);
        }
        if (!signatoryOtherDetails())
        {
            narrator.testFailed("Failed to enter the signatory's details -" + error);
        }

        return narrator.finalizeTest("Successfully entered the signatory's details");
    }

    public boolean signatoriesDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatoriesTab())))
        {
            error = "Failed to scroll to the 'Signatory' tab.";
            return false;
        }
        String signAsSurety = getData("Surety");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signAsSurety(), signAsSurety))
        {
            error = "Failed to select 'sign as Surety' option.";
            return false;
        }
        narrator.stepPassed("Will this Signatory sign as Surety : " + signAsSurety);
        String firstName = getData("First Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryFirstName(), firstName))
        {
            error = "Failed to enter first Name.";
            return false;
        }
        narrator.stepPassed("First name : " + firstName);
        String surname = getData("Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySurName(), surname))
        {
            error = "Failed to enter surname.";
            return false;
        }
        narrator.stepPassed("Surname :" + surname);
        String contactType = getData("Contact Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactType(), contactType))
        {
            error = "Failed to select contact type.";
            return false;
        }
        narrator.stepPassed("Contact Type : " + contactType);
        String dateOfBirth = "";
        GenerateDOB generateDOB = new GenerateDOB(21, 60);
        String dob = generateDOB.getIdDateOfBirth();
        dateOfBirth = generateDOB.getDateOfBirth();
        String id = "";
        try
        {

            ConstructID randID = new ConstructID();
            id = randID.getSouthAfricanID(gender, dob, "South African");
        } catch (Exception e)
        {
            narrator.logDebug("Failed to generate south african ID - " + e);
        }
        String idType = getData("ID Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryIdType(), idType))
        {
            error = "Failed to select id type.";
            return false;
        }
        narrator.stepPassed("Id Type : " + idType);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryIdNumber(), id))
        {
            error = "Failed to enter ID number.";
            return false;
        }
        narrator.stepPassed("ID Number : " + id);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryDateofBirth(), dateOfBirth))
        {
            error = "Failed to enter Date of Birth.";
            return false;
        }
        narrator.stepPassed("Date of Birth : " + dateOfBirth);
        String subType = getData("Sub Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryClientSubType(), subType))
        {
            error = "Failed to select client Sub Type.";
            return false;
        }
        narrator.stepPassed("Client sub type : " + subType);
        String preferredLanguage = getData("Language");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryPreferredLanguage(), preferredLanguage))
        {
            error = "Failed to select prefered language.";
            return false;
        }
        narrator.stepPassed("Preferred language : " + preferredLanguage);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryGender(), gender))
        {
            error = "Failed to select gender.";
            return false;
        }
        narrator.stepPassed("Gender : " + gender);
        String title = getData("Title");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactTitle(), title))
        {
            error = "Failed to select title.";
            return false;
        }
        narrator.stepPassed("Contact Title :" + title);
        String ethnicity = getData("Ethnicity");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryEthnicity(), ethnicity))
        {
            error = "Failed to select ethnicity.";
            return false;
        }
        narrator.stepPassed("Ethnicity :" + ethnicity);
        String employmentSector = getData("Employment Sector");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryEmploymentSector(), employmentSector))
        {
            error = "Failed to select employment sector.";
            return false;
        }
        narrator.stepPassed("Employment sector : " + employmentSector);
        String clientType = getData("Client Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryClientType(), clientType))
        {
            error = "Failed to select client type.";
            return false;
        }
        narrator.stepPassed("Client type : " + clientType);
        String maritalStatus = getData("Marital Status");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMaritalStatus(), maritalStatus))
        {
            error = "Failed to select marital status.";
            return false;
        }
        narrator.stepPassed("Marital Status : " + maritalStatus);
        String maritalContract = getData("Marital Contract");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMaritalContract(), maritalContract))
        {
            error = "Failed to select marital contract.";
            return false;
        }
        narrator.stepPassed("Marital Contract : " + maritalContract);
        String dateMarried = getData("Date Married");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryDateMarried(), dateMarried))
        {
            error = "Failed to enter date of Marriage.";
            return false;
        }
        narrator.stepPassed("Date Married : " + dateMarried);
        String industryType = getData("Industry Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryIndustryType(), industryType))
        {
            error = "Failed to select signatory Industry Type.";
            return false;
        }
        narrator.stepPassed("Industry Type : " + industryType);
        String residenceTime = getData("Residence");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryResidenceYear(), residenceTime))
        {
            error = "Failed to enter date of Residence - Year";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryResidenceMonth(), residenceTime))
        {
            error = "Failed to enter date of Residence - Month";
            return false;
        }
        narrator.stepPassed("Time at Residence (YY/MM):" + residenceTime + "/" + residenceTime);
        String employerTime = getData("Employer");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerYear(), employerTime))
        {
            error = "Failed to enter date of Employment - Year";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerMonth(), employerTime))
        {
            error = "Failed to enter date of Employment - Month";
            return false;
        }
        narrator.stepPassed("Time at Employer (YY/MM):" + employerTime + "/" + employerTime);
        String taxPayer = getData("Tax Payer");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryRegisteredTaxPayer(), taxPayer))
        {
            error = "Failed to select registered tax payer";
            return false;
        }
        narrator.stepPassed("Registered Tax Payer : " + taxPayer);
        String taxNumber = getData("Tax Number");
        if (taxPayer.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryTaxNumber(), taxNumber))
            {
                error = "Failed to enter tax number";
                return false;
            }
            narrator.stepPassed("Tax Number : " + taxNumber);
        }
        String addressLine = getData("Address");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAddressLine1(), addressLine))
        {
            error = "Failed to enter Physical Address - Address Line1";
            return false;
        }
        narrator.stepPassed("Address Line 1 : " + addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAddressLine2(), addressLine))
        {
            error = "Failed to enter Physical Address - Address Line2";
            return false;
        }
        narrator.stepPassed("Address Line 2 : " + addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.signatoryAddressLookup()))
        {
            error = "Failed to click 'Select Suburb' button";
            return false;
        }
        address.addressLookup();
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.signatoryPopulateAddressButton()))
        {
            error = "Failed to click 'Populate with Physical Address' button";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatoriesTab())))
        {
            error = "Failed to scroll to the 'Signatories' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Filled in details so far");

        return true;
    }

    public boolean signatoryOtherDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryHomephoneLabel()))
        {
            error = "Failed to scroll to 'Home Phone' label.";
            return false;
        }
        String homephone = getData("Telephone number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryHomePhone(), homephone))
        {
            error = "Failed to enter Home Phone number.";
            return false;
        }
        narrator.stepPassed("Home Phone : " + homephone);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryWorkPhone(), homephone))
        {
            error = "Failed to enter Work Phone number.";
            return false;
        }
        narrator.stepPassed("Work Phone : " + homephone);
        String cellPhone = getData("Cell number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryCellPhone(), cellPhone))
        {
            error = "Failed to enter Cell Phone number.";
            return false;
        }
        narrator.stepPassed("Cell Phone : " + cellPhone);
        String email = getData("Email");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmailAddress(), email))
        {
            error = "Failed to enter email address.";
            return false;
        }
        narrator.stepPassed("Email : " + email);
        String ownership = getData("Type of ownership");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryTypeOfOwnership(), ownership))
        {
            error = "Failed to select type of Ownership";
            return false;
        }
        narrator.stepPassed("Type of Ownership : " + ownership);
        String propertyValue = getData("Property Value");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryPropertyCurrentValue()))
        {
            error = "Failed to clear Current Property Value textfield";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryPropertyCurrentValue(), propertyValue))
        {
            error = "Failed to enter Current Property Value textfield.";
            return false;
        }
        narrator.stepPassed("Current Property Value : " + propertyValue);
        String balance = getData("Bond Balance");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryPropertyBondBalance()))
        {
            error = "Failed to clear Bond balance textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryPropertyBondBalance(), balance))
        {
            error = "Failed to enter Bond balance.";
            return false;
        }
        narrator.stepPassed("Bond Balance : " + balance);
        String salutation = getData("Salutation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySalutation(), salutation))
        {
            error = "Failed to select salutation.";
            return false;
        }
        narrator.stepPassed("Salutation : " + salutation);
        String bank = getData("Bank Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryBankName(), bank))
        {
            error = "Failed to select Bank Name";
            return false;
        }
        narrator.stepPassed("Bank Name : " + bank);
        String accType = getData("Acc Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryAccountType(), accType))
        {
            error = "Failed to select account type";
            return false;
        }
        narrator.stepPassed("Account Type : " + accType);
        String accHolderName = getData("Acc holder Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAccHolderName(), accHolderName))
        {
            error = "Failed to enter account Holder name.";
            return false;
        }
        narrator.stepPassed("Account Holder's Name : " + accHolderName);
        String accNumber = getData("Acc Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAccNumber(), accNumber))
        {
            error = "Failed to enter account number";
            return false;
        }
        narrator.stepPassed("Account Number : " + accNumber);
        String branchCode = getData("Branch Code");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryBranchCode(), branchCode))
        {
            error = "Failed to enter Branch Code.";
            return false;
        }
        narrator.stepPassed("Branch Code : " + branchCode);
        String occupation = getData("Occupation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryOccupation(), occupation))
        {
            error = "Failed to select occupation.";
            return false;
        }
        narrator.stepPassed("Occupation : " + occupation);
        String employerName = getData("Employer Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerName(), employerName))
        {
            error = "Failed to enter name of employer.";
            return false;
        }
        narrator.stepPassed("Employer Name : " + employerName);
        String employerPhone = getData("Employer Work Phone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerWorkPhone(), employerPhone))
        {
            error = "Failed to enter employer work phone.";
            return false;
        }
        narrator.stepPassed("Employer Work Phone : " + employerPhone);
        String sourceOfIncome = getData("Source of Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySourceofIncome(), sourceOfIncome))
        {
            error = "Failed to enter source of income.";
            return false;
        }
        narrator.stepPassed("Source of Income : " + sourceOfIncome);
        String monthlyIncome = getData("Monthly Income");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryMonthlyIncome()))
        {
            error = "Failed to clear monthly income textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryMonthlyIncome(), monthlyIncome))
        {
            error = "Failed to enter monthly income";
            return false;
        }
        narrator.stepPassed("Monthly Income : " + monthlyIncome);
        String percentage = getData("Share Percentage");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySharePercentage(), percentage))
        {
            error = "Failed to enter share percentage.";
            return false;
        }
        narrator.stepPassed("Share Percentage : " + percentage);
        String networkType = getData("Network Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryNetworkType(), networkType))
        {
            error = "Failed to enter network type.";
            return false;
        }
        narrator.stepPassed("Network Type : " + networkType);
        String suretyType = getData("Surety Type");
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatorySuretyType()))
        {
            error = "Failed to scroll to surety type";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySuretyType(), suretyType))
        {
            error = "Failed to select surety type.";
            return false;
        }
        narrator.stepPassed("Surety Type :" + suretyType);
        String yearInPosition = getData("Year in Position");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryYearInPosition(), yearInPosition))
        {
            error = "Failed to enter year in position";
            return false;
        }
        narrator.stepPassed("Number of Years in Position : " + yearInPosition);
        String foreignIdentity = getData("Foreign Identity");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryForeignIdentity(), foreignIdentity))
        {
            error = "Failed to select foreign identity.";
            return false;
        }
        narrator.stepPassed("Do you have a foreign identity : " + foreignIdentity);
        String multiNationality = getData("Multiple Nationalities");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMultipleNationalities(), multiNationality))
        {
            error = "Failed to select multiNationality.";
            return false;
        }
        narrator.stepPassed("Do you have Multiple Nationalities : " + multiNationality);
        String nationality = getData("Nationality");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryNationality(), nationality))
        {
            error = "Failed to select signatory Nationality.";
            return false;
        }
        narrator.stepPassed("Nationality : " + nationality);
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryNationality1()))
        {
            error = "Failed to scroll to the Nationality1 textfield.";
            return false;
        }
        String otherNationality = getData("Nationality1");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryNationality1(), otherNationality))
        {
            error = "Failed to select signatory Nationality1.";
            return false;
        }
        narrator.stepPassed("Nationality 1: " + otherNationality);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryNationality2(), otherNationality))
        {
            error = "Failed to select signatory Nationality2.";
            return false;
        }
        narrator.stepPassed("Nationality 2: " + otherNationality);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryCountryofResidence(), otherNationality))
        {
            error = "Failed to select country of Resdience.";
            return false;
        }
        narrator.stepPassed("Country of Residence : " + otherNationality);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryCountryOfBirth(), otherNationality))
        {
            error = "Failed to select country of birth.";
            return false;
        }
        narrator.stepPassed("Country of Birth : " + otherNationality);
        String designation = getData("Designation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactDesignation(), designation))
        {
            error = "Failed to select designation.";
            return false;
        }
        narrator.stepPassed("Contact Designation : " + designation);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployeeNumber(), getData("Emp Number")))
        {
            error = "Failed to enter employee number";
            return false;
        }
        narrator.stepPassed("Employee Number : " + getData("Emp Number"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isUSCitizen(), getData("US Citizen")))
        {
            error = "Failed to select US Citizenship";
            return false;
        }
        narrator.stepPassed("Are you a US citizen : " + getData("US Citizen"));

        return true;
    }

}
>>>>>>> d07de976829ca921ddd915f7730068c7a5e2d2f4
