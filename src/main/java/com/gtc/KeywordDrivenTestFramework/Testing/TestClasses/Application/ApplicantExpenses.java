/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Applicant Expenses",
        createNewBrowserInstance = false
)

public class ApplicantExpenses extends BaseClass
{

    String error = "";
    String rent = getData("Rent");
    String amount = getData("Amount");

    public ApplicantExpenses()
    {
    }

    public TestResult executeTest()
    {
        if (!applicantExpenses())
        {
            return narrator.testFailed("Failed to enter 'applicant expenses' on the Form -" + error);
        }
        return narrator.finalizeTest("Successfully entered 'applicant expenses' on the Form.");
    }

    public boolean applicantExpenses()
    {
        /*Entering customers expenses*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicantExpensesTab()))
        {
            error = "Failed to scroll to the 'Applicant expenses' container.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.rent()))
        {
            error = "Failed to clear 'Rent' field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.rent(), rent))
        {
            error = "Failed to enter customers 'rent' in the text field.";
            return false;
        }
        narrator.stepPassed("Rent: " + rent);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.waterElectricity()))
        {
            error = "Failed to clear Rent field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.waterElectricity(), amount))
        {
            error = "Failed to enter customers 'water electricity' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Rates,Water & Electricity:" + " " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.vehicleInstallment()))
        {
            error = "Failed to clear Rent field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vehicleInstallment(), amount))
        {
            error = "Failed to enter customers 'vehicle installment' amount on the text field.";
            return false;
        }
        narrator.stepPassed("vehicle Installment:" + " " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.loanRepayment()))
        {
            error = "Failed to clear Rent field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.loanRepayment(), amount))
        {
            error = "Failed to enter customers 'loan repayment' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Personal Loan Repayment:" + " " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.furnitureAccount()))
        {
            error = "Failed to clear Rent field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.loanRepayment(), amount))
        {
            error = "Failed to enter customers 'furniture Account' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Furniture Account:" + " " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.overDraftRepayment()))
        {
            error = "Failed to clear Rent field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.overDraftRepayment(), amount))
        {
            error = "Failed to enter customers 'furniture Account' amount on the text field.";
            return false;
        }
        narrator.stepPassed("over draft Repayment:" + " " + amount);
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.telephonePayments()))
        {
            error = "Failed to scroll to the 'applicant income' details form.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.telephonePayments()))
        {
            error = "Failed to clear Telephone Payments field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.telephonePayments(), amount))
        {
            error = "Failed to enter customers 'telephone payment' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Telephone Payment :" + " " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.clothingAccounts()))
        {
            error = "Failed to clear clothing account field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clothingAccounts(), amount))
        {
            error = "Failed to enter customers 'clothing account' amount on the text field.";
            return false;
        }
        narrator.stepPassed("Clothing Accounts :" + " " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.educationCost()))
        {
            error = "Failed to clear Education costs field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.educationCost(), amount))
        {
            error = "Failed to enter customers 'education costs' on the text field.";
            return false;
        }

        narrator.stepPassed("Education Costs :" + " " + amount);

        narrator.stepPassedWithScreenShot("Applicant Expenses Data Entered so far");

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.foodAndEntertainmentLabel()))
        {
            error = "Failed to wait for the total expenses text field to appear.";
            return false;
        }

        String total = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.customerTotalExpenses(),"value");
        double amount1 = Double.parseDouble(rent);
        double amount2 = Double.parseDouble(amount);
        double expectedTotal = amount1 + (amount2 * 7);
        double currentTotal = Double.parseDouble(total);

        validateTotalExpenses(currentTotal, expectedTotal);

        return true;
    }

    public boolean validateTotalExpenses(double currentValue, double expectedValue)
    {
        if (currentValue != expectedValue)
        {
            error = "The total value is not the correct";
            return false;
        }
        narrator.stepPassed("Successfully validated the Total expenses " + "<b>Current value<b/>: " + String.format("%.2f", currentValue) + " <b>The expected</b>: " + String.format("%.2f", expectedValue));

        return true;
    }
}
