/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "SBSA Internal Details",
        createNewBrowserInstance = false
)

public class SBSAInternalDetails extends BaseClass
{

    String error = "";

    public SBSAInternalDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!sbsaInternalDetails())
        {
            return narrator.testFailed("Failed to enter customers details on the 'sbsa internal details' Form - " + error);
        }
        return narrator.finalizeTest("Successfully entered customers details on the 'sbsa internal details' Form.");
    }

    public boolean sbsaInternalDetails()
    {

        /*Entering SBSA Internal Details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.internalDetailsTab()))
        {
            error = "Failed to scroll to the 'sbsa internal details'.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.incomeDetailsTab()))
        {
            error = "Failed to wait for the 'payout dealer code' field to appear.";
            return false;
        }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.payoutDealerCode()))
        {
            error = "Failed to scroll 'payout dealer code' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.payoutDealerCode(), getData("Payout Dealer Code")))
        {
            error = "Failed to enter 'payout dealer code' into the text field.";
            return false;
        }
        narrator.stepPassed("Payout Dealer Code: " + " " + getData("Payout Dealer Code"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.payoutDealerName(), getData("Payout Dealer Name")))
        {
            error = "Failed to enter the 'payout dealer name' into the text field.";
            return false;
        }
        narrator.stepPassed("Payout Dealer Name: " + " " + getData("Payout Dealer Name"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.marketSegmentDropDown(), getData("Market Segment")))
        {
            error = "Failed to select the 'market segment' from the list.";
            return false;
        }
        narrator.stepPassed("Market Segment : " + " " + getData("Market Segment"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vafBusinessUnit(), getData("VAF Business Unit")))
        {
            error = "Failed to select the 'vaf business unit' from the list.";
            return false;
        }
        narrator.stepPassed("VAF Business Unit : " + " " + getData("VAF Business Unit"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.originCode(), getData("Origin Code")))
        {
            error = "Failed to select the 'origin code' from the list.";
            return false;
        }
        narrator.stepPassed("Origin Code : " + " " + getData("Origin Code"));
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.marketPlan()))
        {
            error = "Failed to wait for the 'Market Plan' field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.marketPlan(), getData("Market Plan")))
        {
            error = "Failed to enter 'Market Plan' into the text field.";
            return false;
        }
        narrator.stepPassed("Market Plan : " + " " + getData("Market Plan"));
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.accountExecutive()))
        {
            error = "Failed to wait for the 'Account Executive' field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountExecutive(), getData("Account Executive")))
        {
            error = "Failed to enter 'Account Executive' into the text field.";
            return false;
        }
        narrator.stepPassed("Account Executive  : " + " " + getData("Account Executive"));
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.bdoNumber()))
        {
            error = "Failed to wait for the 'BDO Number' field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.bdoNumber(), getData("BDO")))
        {
            error = "Failed to enter 'BDO Number' into the text field.";
            return false;
        }
        narrator.stepPassed("BDO Number  : " + " " + getData("BDO"));
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.sbsaBranchIbt()))
        {
            error = "Failed to wait for the 'SBSA Branch IT' field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sbsaBranchIbt(), getData("SBSA Branch")))
        {
            error = "Failed to select the 'SBSA Branch IT' from the list.";
            return false;
        }
        narrator.stepPassed("SBSA Branch IT  : " + " " + getData("SBSA Branch"));

        return true;
    }
}
