/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;


/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Relative Details",
        createNewBrowserInstance = false
)
public class RelativeDetails extends BaseClass
{

    String error = "";
    String firstname = getData("First Name");
    String surname = getData("Surname");
    String relation = getData("Relation");
    String contactMethod = getData("Contact Method");
    String cellPhoneNumber = getData("Number");
    String address = getData("Address");
    String city = getData("City");
    String suburb = getData("Suburb");
    String postalCode = getData("Postal Code");
    

    public RelativeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!relativeDetails())
        {
            return narrator.testFailed("Failed to enter the relative's details -" + error);
        }
        return narrator.finalizeTest("Successfully retrieved the relative's details");
    }

    public boolean relativeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.relativeDetailsContainer()))
        {
            error = "Failed to scroll to the 'Relative Details'";
            return false;
        }
     
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relativeFirstname()))
        {
            error = "Failed to clear the 'First Name'";
            return false;
        }
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeFirstname(),firstname))
        {
            error = "Failed to enter the 'First Name'";
            return false;
        }
        narrator.stepPassed("First Name : " + firstname);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relativeSurname()))
        {
            error = "Failed to clear the 'Surname'";
            return false;
        }
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeSurname(),surname))
        {
            error = "Failed to enter the 'Surname'";
            return false;
        }
        narrator.stepPassed("Surname : " + surname);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.relationDropDown(),relation))
        {
            error = "Failed to select the 'Relation'";
            return false;
        }
        narrator.stepPassed("Relation : " + relation);
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.preferedContactMethodDropDown(),contactMethod))
        {
            error = "Failed to select the 'Preferred Contact Method'";
            return false;
        }
        narrator.stepPassed("Preferred Contact Method : " + contactMethod);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relativePhoneNumber()))
        {
            error = "Failed to clear the 'Contact Number'";
            return false;
        }
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativePhoneNumber(),cellPhoneNumber))
        {
            error = "Failed to enter the 'Contact Number'";
            return false;
        }
        narrator.stepPassed("Contact Number : " + cellPhoneNumber);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relativeAddress()))
        {
            error = "Failed to clear the 'Relative Address 1'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeAddress(),address))
        {
            error = "Failed to enter the 'Relative Address 1'";
            return false;
        }
        narrator.stepPassed("Relative Address 1 : " + address);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relative_Address2()))
        {
            error = "Failed to clear the 'Relative Address 2'";
            return false;
        }
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relative_Address2(),address))
        {
            error = "Failed to enter the 'Relative Address 2'";
            return false;
        }
        narrator.stepPassed("Relative Address 2 : " + address);
        relativeAddressLookup();
       
 

        return true;
    }
    public boolean relativeAddressLookup()
    {
         if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.relativeSuburb()))
        {
            error = "Failed to click 'suburb' lookup button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchSuburb(), 5))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.searchSuburb(), city))
        {
            error = "Failed to enter 'suburb' into the search text field.";
            return false;
        }
        narrator.stepPassed("Search Suburb : " + city);

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchButton()))
        {
            error = "Failed to click on the 'sububrb' search button.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectSuburbDropDown(), suburb))
        {
            error = "Failed to select 'suburb'.";
            return false;
        }
        narrator.stepPassed("Select Suburb : " + suburb);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectCodeDropDown(), postalCode))
        {
            error = "Failed to select the 'code' from the list.";
            return false;
        }
        narrator.stepPassed("Select Code : " + postalCode);
        narrator.stepPassedWithScreenShot("Successfully captured the address lookup details");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectOK()))
        {
            error = "Failed to click the 'OK' button.";
            return false;
        }
    
        return true;
    }

}
