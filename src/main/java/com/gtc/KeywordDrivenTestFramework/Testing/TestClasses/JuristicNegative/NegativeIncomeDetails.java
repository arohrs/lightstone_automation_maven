/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Income Negative",
        createNewBrowserInstance = false
)
public class NegativeIncomeDetails extends BaseClass
{

    String error = "";
    String grossNet = getData("Gross Net");
    String netTakeHome = getData("Net");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeIncomeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!incomeDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean incomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryIncomeDetailsTab()))
        {
            error = "Failed to scroll to 'Personal detail' container";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryGrossRenumeration()))
        {
            error = "Failed to clear  the 'Gross Remuneration 'textfield.";
            return false;
        }
        if (!grossNet.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryGrossRenumeration(), grossNet))
            {
                error = "Failed to enter the 'Gross Remuneration'.";
                return false;
            }
        }
        narrator.stepPassed("Gross Remuneration : " + grossNet);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryNetTakeHome()))
        {
            error = "Failed to clear 'Net Take Home' textfield.";
            return false;
        }
        if (!netTakeHome.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryNetTakeHome(), netTakeHome))
            {
                error = "Failed to enter 'Net Take Home'.";
                return false;
            }
        }
        narrator.stepPassed("Net Take-home Pay : " + netTakeHome);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryIncomeDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' is not highlighted in red .";
                    return false;
                }
                 if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "The '" + field + "' is not highlighted in red .";
                    return false;
                }
            } else if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
            {
                error = "The '" + field + "' is not highlighted in red .";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field");
        }

        return true;
    }
}
