/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Liability Details",
        createNewBrowserInstance = false
)
public class LiabilityDetails extends BaseClass
{
    String error = "";
    public LiabilityDetails()
    {
    }
    public TestResult executeTest()
    {

        if (!liabilitiesDetails())
        {
            return narrator.testFailed("Failed to update the liability detail's form." + " " + error);
        }
        return narrator.finalizeTest("Successfully updated the liability detail's form");
    }
    
    public boolean liabilitiesDetails()
    {
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.liabiltiesDetails()))
        {
            error = "Failed to wait for the 'delivery method' list to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.surety(), getData("Surety")))
        {
            error = "Failed to select 'Yes' from the surety list.";
            return false;
        }
        narrator.stepPassed("Surety : "+getData("Surety"));
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyDescription(), getData("Description")))
        {
            error = "Failed to enter the'surety description' detail in the textfield.";
            return false;
        }
        narrator.stepPassed("Surety description : "+getData("Description"));
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.guarantor(), getData("Guarantor")))
        {
            error = "Failed to select 'Yes' from the Guarantor list.";
            return false;
        }
        narrator.stepPassed("Guarantor : "+getData("Guarantor"));
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.guarantorDescription(), getData("Description")))
        {
            error = "Failed to enter the 'guarantor' detail in the textfield.";
            return false;
        }
        narrator.stepPassed("Guarantor description : "+getData("Description"));
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.coDebtor(), getData("Co-Debtor")))
        {
            error = "Failed to select 'Yes' from the Co-Debtor list.";
            return false;
        }
        narrator.stepPassed("Co-Debtor : "+getData("Co-Debtor"));
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.coDebtorDescription(), getData("Description")))
        {
            error = "Failed to enter the 'co-Debtor' detail in the textfield.";
            return false;
        }
        narrator.stepPassed("Co-Debtor description : "+getData("Description"));
    
    
        return true;
    }
    
    
}
