package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Default_Folder;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestEntity;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.Eribank_PageObject;

@KeywordAnnotation(
        Keyword = "Eribank_Payment",
        createNewBrowserInstance = false
)
public class EriBank_EnterPayment extends BaseClass
{

    public TestEntity testData;
    String error = "";

    public EriBank_EnterPayment()
    {
     

    }

    public TestResult executeTest()
    {
        if (!Payment())
        {
            return narrator.testFailed("Failed to Enter Payment - " + error);
        }
        return narrator.finalizeTest("Process: Payment successful ");
    }

    public boolean Payment()
    {
        String phoneNo = testData.getData("PhoneNumber");
                
        AppiumDriverInstance.pause(2000);
        if (!AppiumDriverInstance.clickElementbyXpath(Eribank_PageObject.PaymentButton()))
        {
            error = "Failed to Click Payments button";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        
        if (!AppiumDriverInstance.enterTextByXpath(Eribank_PageObject.PhoneNo(), phoneNo))
        {
            error = "Failed to enter Phone Number";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        
        if(phoneNo.length()!=10){
            narrator.stepPassed(phoneNo+" is not a valid Phone Number, the length is "+ phoneNo.length()+".");
        }
        narrator.stepPassed(phoneNo+" is a valid Phone Number");
        
        if (!AppiumDriverInstance.enterTextByXpath(Eribank_PageObject.Name(), testData.getData("Name")))
        {
            error = "Failed to enter Name";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        if (!AppiumDriverInstance.enterTextByXpath(Eribank_PageObject.Amount(), testData.getData("Amount")))
        {
            error = "Failed to enter Amount";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        if (!AppiumDriverInstance.clickElementbyXpath(Eribank_PageObject.SelectCountry()))
        {
            error = "Failed to click Country";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        if (!AppiumDriverInstance.clickElementbyXpath(Eribank_PageObject.CountrySelection(testData.getData("Country"))))
        {
            error = "Failed to select Country";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("All Fields have been filled in with the appropriate information.");
        if (!AppiumDriverInstance.clickElementbyXpath(Eribank_PageObject.SendPayments()))
        {
            error = "Failed to click Send Payments button";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        if (!AppiumDriverInstance.clickElementbyXpath(Eribank_PageObject.PaymentYes()))
        {
            error = "Failed to click Yes button";
            return false;
        }
        return true;

    }

}
