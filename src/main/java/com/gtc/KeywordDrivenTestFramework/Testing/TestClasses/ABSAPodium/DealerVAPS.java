/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Dealer VAPS",
        createNewBrowserInstance = false
)
public class DealerVAPS extends BaseClass
{

    String error = "";
    String description = getData("Description");
    String amount = getData("Amount");

    public DealerVAPS()
    {
    }

    public TestResult executeTest()
    {
        if (!dealerVAPS())
        {
            return narrator.testFailed("Failed to enter the dealer VAPS details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the dealer VAPS details");
    }

    public boolean dealerVAPS()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.dealerVAPSTab()))
        {
            error = "Failed to scroll to the 'Dealer VAPS' container";
            return false;
        }
        List extras;
        int dealerVaps = 0;

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealerVapsExtrasList(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.podiumAddDealer()))
            {
                error = "Failed to click on the 'Add new Dealer VAPS' button";
                return false;
            }
            narrator.stepPassed("Clicked on the 'Add New Dealer VAP' button");
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealerVAPSDescription()))
            {

                error = "Failed to wait for the 'description ' field to appear";
                return false;

            }
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.dealerVAPSDescription(), description))
            {
                error = "Failed to select the '" + description + "' option";
                return false;
            }
            narrator.stepPassed("Description: " + description);
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.dealerVAPSAmount(), amount))
            {
                error = "Failed to enter the 'amount'";
                return false;
            }
            narrator.stepPassed("Amount: " + amount);
        } else if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealerVapsExtrasList(), 5))
        {
            extras = SeleniumDriverInstance.retrieveWebElementByXpath(LightStonePageObjects.dealerVapsExtrasList());
            dealerVaps = extras.size();

            if (dealerVaps < 10)
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewDealerVap()))
                {
                    error = "Failed to click on the 'Add new Dealer VAPS' button";
                    return false;
                }
                narrator.stepPassed("Clicked on the 'Add New Dealer VAP' button");
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealerVAPSDescription()))
                {

                    error = "Failed to wait for the 'description' field to appear";
                    return false;

                }
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.dealerVAPSDescription(), description))
                {
                    error = "Failed to select the '" + description + "' option";
                    return false;
                }
                narrator.stepPassed("Description: " + description);
                if (SeleniumDriverInstance.dismissAlert())
                {
                    if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.removeDealerVAPS()))
                    {
                        error = "Failed to click the 'delete' button";
                        return false;
                    }
                }
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.dealerVAPSAmount(), amount))
                {
                    error = "Failed to enter the 'amount'";
                    return false;
                }
                narrator.stepPassed("Amount: " + amount);

            }

        }

        String totalExtra = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.dealerVAPSTotalAmount(),"value");
        narrator.stepPassed("Total Dealer VAPS Amount: " + totalExtra);

        return true;
    }

}
