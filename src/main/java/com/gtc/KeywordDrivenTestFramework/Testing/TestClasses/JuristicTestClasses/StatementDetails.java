/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Statement",
        createNewBrowserInstance = false
)
public class StatementDetails extends BaseClass
{

    String error = "";
    String email = getData("Email");

    public StatementDetails()
    {
    }

    public TestResult executeTest()
    {
        if(!statementDetails())
        {
        
            narrator.testFailed("Failed to enter the statement details -"+error);
        }
        return narrator.finalizeTest("Successfully enter the statement details");
    }

    public boolean statementDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.statementTab()))
        {
            error = "Failed to scroll to the 'Statement' tab.";
            return false;
        }

        List<String> statementType_TestPack = Arrays.asList(testData.getData("Statement_List").split(","));
        List<String> statementType_WebPage = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.statementTypeList());

        for (String statementType : statementType_TestPack)
        {
            if (!statementType_WebPage.stream().anyMatch(s -> statementType.equalsIgnoreCase(s)))
            {
                error = "Statement delivery method not found in the lsit";

            }

        }

        for (String statement : statementType_WebPage)
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionStatementType(), statement))
            {
                error = "Failed to select statement type.";
                return false;
            }
            narrator.stepPassed("Statement Type : "+statement);
            if (statement.equalsIgnoreCase("Electronic Statement"))
            {
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionPreferredEmail(), email))
                {
                    error = "Failed to enter preffered email.";
                    return false;
                }
                narrator.stepPassed("Preferred Email Address : "+email);
            }
            
        }

        return true;
    }

}
