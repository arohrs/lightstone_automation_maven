/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Income Negative",
        createNewBrowserInstance = false
)
public class NegativeApplicantIncomeDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String readOnly = getData("Read Only");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeApplicantIncomeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!applicantIncomeDetails())
        {
            return narrator.testFailed("Failed to perform the negative test " + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean applicantIncomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.incomeDetailsTab()))
        {
            error = "Failed to scroll to the 'applicant income' details form.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.grossRemuneration()))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        if (!getData("Gross").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.grossRemuneration(), getData("Gross")))
            {
                error = "Failed to enter customers 'gross renumeration' on the text field.";
                return false;
            }
        }
        narrator.stepPassed("Gross Renumeration:" + " " + getData("Gross"));
//        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.netIncome()))
//        {
//            error = "Failed to enter customers 'net income' on the text field.";
//            return false;
//        }
//        if (!getData("Net").isEmpty())
//        {
//            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.netIncome(), getData("Net")))
//            {
//                error = "Failed to enter customers 'net income' on the text field.";
//                return false;
//            }
//        }
//        narrator.stepPassed("Net Income :" + " " + getData("Net"));

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.incomeDetailsTab()))
            {
                error = "Failed to scroll to the 'applicant income' details form.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
            {
                error = "The '" + field + "' field is not highlighted in red .";
                return false;
            }

            if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
            {
                error = "Failed to hover over the '" + field + "' field .";
                return false;
            }
             pause(500);
            narrator.stepPassedWithScreenShot("Successfully found the red highlighted <b>" + field + "</b> field ");
           

            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.readOnlyInputField(readOnly)))
            {
                error = "Failed to validate '" + readOnly + "' is a read-only field .";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.readOnlyInputField(readOnly)))
            {
                error = "Failed to scroll to '" + readOnly + "' is a read-only field .";
                return false;
            }
            if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.readOnlyInputField(readOnly)))
            {
                error = "Failed to validate '" + readOnly + "' is a read-only field .";
                return false;
            }
            pause(500);
            narrator.stepPassedWithScreenShot("Successfully validated <b>" + readOnly + "</b> field is not manually editable");

        }
        return true;
    }
}
