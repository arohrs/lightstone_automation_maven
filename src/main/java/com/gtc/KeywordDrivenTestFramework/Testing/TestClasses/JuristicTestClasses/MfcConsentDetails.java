/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic MFC Consent",
        createNewBrowserInstance = false
)
public class MfcConsentDetails extends BaseClass
{

    String error = "";
    String consent = getData("Consent");

    public MfcConsentDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!mfcConsentDetails())
        {
            narrator.testFailed("Failed to enter the MFC Consent details -" + error);
        }

        return narrator.finalizeTest("Successfully entered the MFC Consent details");
    }

    public boolean mfcConsentDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatoryMFCConsentTab())))
        {
            error = "Failed to scroll  the 'MFC Consent' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryInfoSharing(), consent))
        {
            error = "Failed to select response for Info Sharing.";
            return false;
        }
        narrator.stepPassed("Client has consented to a credit or payment profile : "+consent);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryItcConsent(), consent))
        {
            error = "Failed to select response for ITC consent.";
            return false;
        }
        narrator.stepPassed("Client has consented to MFC sharing payment history : "+consent);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMarketingConsent(), consent))
        {
            error = "Failed to select response for Marketing consent.";
            return false;
        }
        narrator.stepPassed("Include in Marketing List : "+consent);

        return true;

    }

}
