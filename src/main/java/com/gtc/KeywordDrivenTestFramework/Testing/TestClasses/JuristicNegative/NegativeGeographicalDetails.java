/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses.GeographicalDetails;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Geographical Negative",
        createNewBrowserInstance = false
)
public class NegativeGeographicalDetails extends BaseClass
{

    String error = "";
    String address1 = getData("Address1");
    String address2 = getData("Address2");
    String address3 = getData("Address3");
    String searchPhysicalAddress = getData("Physical");
    String searchOperatingAddress = getData("Operating");
    String searchRegisteredAddress = getData("Registered");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");
    GeographicalDetails suburbSearch = new GeographicalDetails();

    public NegativeGeographicalDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!geographicalDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean geographicalDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.geographicalTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessPhysicalAddress1()))
        {
            error = "Failed to clear the 'Physical Address1' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessPhysicalAddress1(), address1))
        {
            error = "Failed to enter 'Physical Address1'.";
            return false;
        }
        narrator.stepPassed("<h5> Physical Address </h5>");
        narrator.stepPassed("Physical Address 1: " + address1);
        if (searchPhysicalAddress.equalsIgnoreCase("Yes") && !address1.isEmpty())
        {
            narrator.stepPassed("Physical Address 2: " + address1);
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessPhysicalAddressSuburbButton()))
            {
                error = "Failed to click  the 'application' button.";
                return false;
            }
            suburbSearch.addressLookup();
//            
//            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.physicalAddressPopulateButton()))
//            {
//                error = "Failed to click  the 'application' button.";
//                return false;
//            }

        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.postalAddress1()))
        {
            error = "Failed to clear 'Postal Address1' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.postalAddress1(), address1))
        {
            error = "Failed to enter 'Postal Address1'.";
            return false;
        }
        narrator.stepPassed("<h5> Postal Address </h5>");
        narrator.stepPassed("Postal Address 1: " + address1);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.postalAddress2()))
        {
            error = "Failed to clear 'Postal Address2' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.postalAddress2(), address1))
        {
            error = "Failed to enter 'Postal Address2'.";
            return false;
        }
        if (searchPhysicalAddress.equalsIgnoreCase("Yes"))
        {
            narrator.stepPassed("Postal Address 2: " + address1);
            narrator.stepPassed("Suburb:" + " " + getData("Select Suburb"));
            narrator.stepPassed("Code :" + " " + getData("Select Code"));
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessOperatingAddress1()))
        {
            error = "Failed to clear 'Operating Address1' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessOperatingAddress1(), address2))
        {
            error = "Failed to enter 'Operating Address1'.";
            return false;
        }
        narrator.stepPassed("<h5> Operating Address </h5>");
        narrator.stepPassed("Operating Address 1: " + address2);
        if (searchPhysicalAddress.equalsIgnoreCase("Yes") && !address2.isEmpty())
        {
            narrator.stepPassed("Operating Address 2: " + address2);
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessOperatingAddressSuburbButton()))
            {
                error = "Failed to click the 'Select suburb' button.";
                return false;
            }
            suburbSearch.addressLookup();

            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.operatingAddressPopulateButton()))
            {
                error = "Failed to click  the 'Populate With Address' button.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessRegisteredAddress1()))
        {
            error = "Failed to clear 'Business Registered Address1' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.businessRegisteredAddress1()))
        {
            error = "Failed to scroll to the 'Business Registered Address1' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegisteredAddress1(), address3))
        {
            error = "Failed to enter 'Business Registered Address1'.";
            return false;
        }
        narrator.stepPassed("<h5> Registered Address </h5>");
        narrator.stepPassed("Registered Address 1: " + address3);
        if (searchPhysicalAddress.equalsIgnoreCase("Yes") && !address3.isEmpty())
        {
            narrator.stepPassed("Registered Address 2: " + address3);
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.businessRegisteredAddressSuburbButton()))
            {
                error = "Failed to click the 'Select suburb' button.";
                return false;
            }
            suburbSearch.addressLookup();
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.registeredAddressPopulateButton()))
            {
                error = "Failed to click  the 'Populate With Address' button.";
                return false;
            }
        }
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.geographicalTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
