/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Vehicle details",
        createNewBrowserInstance = false
)

public class VehicleDetails extends BaseClass
{
    String error="";
    String articleType = getData("Article type");
    String articleCondition = getData("Article condition");
    String vehicleUse = getData("Vehicle use");
    String mCode = getData("M&M");
    String km = getData("Kilometers");
    String chassis = getData("Chassis Number");
    String registrationNumber = getData("Registration Number");
    String engineNumber = getData("Engine Number");
    String mileage = getData("Mileage");
    
      public TestResult executeTest()
      {
          if(!transactionsDetails())
          {
          
              narrator.testFailed("Failed to enter the vehilce details -"+error);
          }
      
          return narrator.finalizeTest("Successfully entered the vehilce details");
      }
      
      public boolean transactionsDetails ()
      {
           if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionsTab()))
        {
            error = "Failed to scroll to the 'Transaction' tab";
            return false;
        }
           if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionVehicleDetailsTab()))
        {
            error = "Failed to scroll 'Vehicle Details' tab.";
            return false;
        }   
      
       if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionArticleType(),articleType))
        {
            error = "Failed to select article type.";
            return false;
        }
        narrator.stepPassed("Article Type : "+articleType);
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionArticleCondition(),articleCondition))
        {
            error = "Failed to select artcile condition";
            return false;
        }
         narrator.stepPassed("Article New/Used : "+articleCondition);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionArticleUse(),vehicleUse))
        {
            error = "Failed to select Vehicle Use";
            return false;
        }
          narrator.stepPassed("Vehicle Use : "+vehicleUse);
          if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.transactionSelectVehicleButton()))
        {
            error = "Failed to click on the select vehicle button.";
            return false;
        }
        narrator.stepPassed("Clicked on select vehicle button");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.vehicleLookup()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully opened the vehicle lookup modal"); 
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mnMCode(),getData("M&M")))
        {
            error = "Failed to enter the 'M&M code'data.";
            return false;
        }
         narrator.stepPassed("M&M Code : "+getData("M&M"));
           if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchMMcode()))
        {
            error = "Failed to wait for the 'M&M search' button to apear.";
            return false;
        }
              if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchMMcode()))
        {
            error = "Failed to click the 'M&M search'button.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.manufacturerDropdown(), getData("Manufacturer")))
        {
            error = "Failed to select the 'manufacturer' from the list.";
            return false;
        }
        narrator.stepPassed("Manufacturer: " + getData("Manufacturer"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.yearDropdown(), getData("Year")))
        {
            error = "Failed to select the 'year' from the calender.";
            return false;
        }
        narrator.stepPassed("Year: " + " " + getData("Year"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.modelDropdown(), getData("Model")))
        {
            error = "Failed to select the 'model' from the list";
            return false;
        }
        narrator.stepPassed("Model: " + getData("Model"));
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.ok()))
        {
            error = "Failed to click  the 'OK' button to apear.";
            return false;
        }
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionActualKilometers(),km))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        }
         narrator.stepPassed("Actual Kilometers : "+km);
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionChassisNumber(),chassis))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        }
          narrator.stepPassed("Chassis Number : "+chassis);
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionRegistrationNumber(),registrationNumber))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        }
         narrator.stepPassed("Registration Number : "+registrationNumber);
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionEngineNumber(),engineNumber))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        } 
          narrator.stepPassed("Engine Number : "+engineNumber);
           if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionExpectedMileage(),mileage))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        }  
        narrator.stepPassed("Maximum Annual Mileage : "+mileage);
        String stockNumber = getData("Stock Number");
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vehicleStockNumber(),stockNumber))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        }  
        narrator.stepPassed("Vehicle Job/Stock Number : "+stockNumber);
        String modelCode = getData("Model Code");
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.shortModelCode(),modelCode))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        }  
        narrator.stepPassed("Vehicle Short Model Code : "+modelCode);
      
      
          return true;
      }
    
}
