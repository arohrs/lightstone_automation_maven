/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;


/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Finance details",
        createNewBrowserInstance = false
)
public class FinanceDetails extends BaseClass
{

    String error = "";
    String financeType = getData("Finance Type");
    String periodContract = getData("Period contract");
    String paymentFrequency = getData("Payment Frequency");
    String paymentMethod = getData("Payment Method");
    String rateType = getData("Rate Type");
    String interestRate = getData("Interest Rate");
    String price = getData("Purchase Price");
    String retailPrice = getData("Retail Price");
    String initiationFees = getData("Initiation Fees");
    String residual = getData("Residual percent");
    String deposit = getData("Cash Deposit");
    String packageNumber = getData("Package Number");
    String schemeCode = getData("Code");
    String dealType = getData("Deal Type");
    String breakMonth = getData("Break Month");
    String statementMethod = getData("Statement Method");
    

    public FinanceDetails()
    {
    }

    public TestResult executeTest()
    {
        if(!financeDetails())
        {
        
            narrator.testFailed("Failed to enter the finance details -"+error);
        }

        return narrator.finalizeTest("Successfully entered the finance details");
    }

    public boolean financeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionFinanceDetailsTab()))
        {
            error = "Failed to scroll to the Financial Details Tab";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionFinanceType(), financeType))
        {
            error = "Failed to select finance type.";
            return false;
        }
        narrator.stepPassed("Finance Type : "+financeType);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPeriodofContact(), periodContract))
        {
            error = "Failed to select period of contract.";
            return false;
        }
        narrator.stepPassed("Period of Contract : "+periodContract);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPaymentFrequency(), paymentFrequency))
        {
            error = "Failed to select payment frequency.";
            return false;
        }
        narrator.stepPassed("Payment Frequency : "+paymentFrequency);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPaymentMethod(), paymentMethod))
        {
            error = "Failed to select payment method.";
            return false;
        }
        narrator.stepPassed("Payment Method : "+paymentMethod);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionRateType(), rateType))
        {
            error = "Failed to select rate type.";
            return false;
        }
        narrator.stepPassed("Rate Type : "+rateType);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionInterestRate()))
        {
            error = "Failed to the clear Interest Rate textfield";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionInterestRate(), interestRate))
        {
            error = "Failed to enter interest rate.";
            return false;
        }
        narrator.stepPassed("Interest Rate : "+interestRate);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionPurchasePrice()))
        {
            error = "Failed to clear Purchase Price textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionPurchasePrice(), price))
        {
            error = "Failed to enter Purchase Price";
            return false;
        }
        narrator.stepPassed("Cash / Purchase price : "+price);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionRetailPrice()))
        {
            error = "Failed to clear Retail Price textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionRetailPrice(), retailPrice))
        {
            error = "Failed to enter Retail Price.";
            return false;
        }
        narrator.stepPassed("Retail Price : "+retailPrice);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionInitiationFees(), initiationFees))
        {
            error = "Failed to select Initiation Fee.";
            return false;
        }
        narrator.stepPassed("Finance Initiation Fees : "+initiationFees);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionBallonPercent(), residual))
        {
            error = "Failed to enter Ballon Percentage.";
            return false;
        }
        narrator.stepPassed("Balloon/Residual Percent : "+residual);
            if (!SeleniumDriverInstance.pressKeyWithRobot("tab"))
        {
            error = "Failed to press the 'tab' key.";
            return false;
        }
        String residualAmount = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.transactionBallonAmount(),"value");
        
        narrator.stepPassed("Balloon/Residual Amount : "+residualAmount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionCashDeposit()))
        {
            error = "Failed to clear Cash Deposit textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionCashDeposit(), deposit))
        {
            error = "Failed to enter Cash Deposit.";
            return false;
        }
        narrator.stepPassed("Cash Deposit Amount : "+deposit);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionTradeInDeposit()))
        {
            error = "Failed to clear the 'Trade in Deposit' textfield";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionTradeInDeposit(), deposit))
        {
            error = "Failed to enter Trade in Deposit amount.";
            return false;
        }
        narrator.stepPassed("Trade in Deposit Amount : "+deposit);
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionPackageIndicator(), initiationFees))
        {
            error = "Failed to select Initiation Fee.";
            return false;
        }
        narrator.stepPassed("Package Indicator : "+initiationFees);
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionPackageNumber(), packageNumber))
        {
            error = "Failed to enter package number.";
            return false;
        }
        narrator.stepPassed("Package Number : "+packageNumber);
           if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionSchemeCode(), schemeCode))
        {
            error = "Failed to enter Scheme Code.";
            return false;
        }
        narrator.stepPassed("Scheme Code : "+schemeCode);
           if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionDealType(), dealType))
        {
            error = "Failed to select deal type.";
            return false;
        }
        narrator.stepPassed("Deal Type : "+dealType);
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionBreakMonth(), breakMonth))
        {
            error = "Failed to select break month.";
            return false;
        }
        narrator.stepPassed("Take a Break Month : "+breakMonth);
              if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionStatementMethod(), statementMethod))
        {
            error = "Failed to select statement method.";
            return false;
        }
        narrator.stepPassed("Preferred Statement Method? : "+statementMethod);
        
        
        
        

        return true;
    }

}
