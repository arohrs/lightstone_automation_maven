package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Default_Folder;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestEntity;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.Eribank_PageObject;
import com.gtc.KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;

@KeywordAnnotation(
        Keyword = "Eribank_Logout",
        createNewBrowserInstance = false
)
public class EriBank_Logout extends BaseClass {

    public TestEntity testData;
    String error = "";
    Narrator narrator;

    public EriBank_Logout(TestEntity testData) {
        this.testData = testData;
       
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest() {
        if (!Logout()) {
            return narrator.testFailed("Failed to Logout - " + error);
        }
        return narrator.finalizeTest("Process: Logged on successfully ");
    }

    public boolean Logout() {

        AppiumDriverInstance.pause(2000);
        if (!AppiumDriverInstance.clickElementbyXpath(Eribank_PageObject.Logout()))
        {
            error="Failed to click Logout button";
            return false;
        }
        return true;

    }

}
