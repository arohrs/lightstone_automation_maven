/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Vehicle Details",
        createNewBrowserInstance = false
)
public class VehicleDetails extends BaseClass
{

    String error = "";
    String vehicleUse_List = getData("Use List");
    String fields = getData("Fields");
    String readOnly = getData("Disable Fields");
    String vehicleType = getData("Vehicle Type");
    String vehicleUse = getData("Vehicle Use");
    String mmCode = getData("M&M");
    String manufacture = getData("Manufacturer");
    String year = getData("Year");
    String model = getData("Model");
    String engineSize = getData("Engine Size");
    String seats = getData("Seat");
    String km = getData("Actual Kilometers");
    String chassisNumber = getData("Chassis Number");
    String engineNumber = getData("Engine Number");
    String registrationNumber = getData("Registration Number");
    String serialNumber = getData("Serial Number");
    String price = getData("Price");
    String condition = getData("Condition");

    public VehicleDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!vehicleDetails())
        {
            return narrator.testFailed("Failed to enter the vehicle's details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the vehicle's details");
    }

    public boolean vehicleDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsTabHeader()))
        {
            error = "Failed to scroll to the 'Vehicle Details'";
            return false;
        }
        ArrayList<String> vehicleUseList = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.vehicleUseList());
        List<String> useOfArticle = Arrays.asList(vehicleUse_List.split(","));

        for (String useType : useOfArticle)
        {
            if (!vehicleUseList.stream().anyMatch(s -> useType.equalsIgnoreCase(s)))
            {
                error = "Vehicle use not on the list - " + useType;
            }

        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.useOfArticle(), vehicleUse))
        {
            error = "Failed to select the 'Vehicle Use'";
            return false;
        }
        if (!SeleniumDriverInstance.alertHandler())
        {
            error = "Failed to wait for the 'Podium Details'";
        }

        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click for the 'OK' button";
                return false;
            }
        }
        narrator.stepPassed("Vehicle Use: " + vehicleUse);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.articleType(), vehicleType))
        {
            error = "Failed to select the 'Vehicle Type'";
            return false;
        }
        narrator.stepPassed("Vehicle Type: " + vehicleType);
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click for the 'OK' button";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.purchasePriceTextfield()))
        {
            error = "Failed to clear the 'Cash Price'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.purchasePriceTextfield(), price))
        {
            error = "Failed to enter the 'Cash Price'";
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.TAB);
        if (!SeleniumDriverInstance.alertHandler())
        {
            error = "Failed to wait for the 'Podium Details'";
        }

        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to wait for the 'Podium Details'";
                return false;
            }
        }

        narrator.stepPassed("Cash Price : " + price);
        List<String> disabledField = Arrays.asList(readOnly.split(","));
        List<String> disabledFieldName = Arrays.asList(fields.split(","));

        for (String fieldName : disabledFieldName)
        {
            for (String field : disabledField)

            {
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.readOnlyInputField(field), 5))
                {
                    error = "Field " + field + " is not disabled";
                }

            }
            narrator.stepPassed("Successfully validated " + fieldName + " field is disabled");
        }
        vehicleLookup();

        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.goodDescription()))
        {
            error = "Failed to wait  the 'Good description' to appear";
            return false;
        }
        narrator.stepPassed("Successfully validated the Good description field is disabled");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.motorBikeSize()))
        {
            error = "Failed to enter the 'Engine Size'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.motorBikeSize(), engineSize))
        {
            error = "Failed to enter the 'Engine Size'";
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.TAB);
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click  the 'OK' button";
                return false;
            }
        }

        narrator.stepPassed("Engine Size : " + engineSize);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.numberOfSeat()))
        {
            error = "Failed to enter the ''";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.numberOfSeat(), seats))
        {
            error = "Failed to enter the ''";
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.TAB);
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }

        narrator.stepPassed("Number of Seats : " + seats);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.actualKilometersTextfield()))
        {
            error = "Failed to clear the 'Actual Kilometers'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.actualKilometersTextfield(), km))
        {
            error = "Failed to enter the 'Actual Kilometers'";
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.TAB);
        if (!SeleniumDriverInstance.alertHandler())
        {
            error = "Failed to wait for the 'Podium Details'";
        }
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }
        narrator.stepPassed("Actual Kilometers : " + km);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.chassisNumberTextfield()))
        {
            error = "Failed to clear the 'Chassis Number'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.chassisNumberTextfield(), chassisNumber))
        {
            error = "Failed to enter the 'Chassis Number'";
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.TAB);
        narrator.stepPassed("Chassis Number : " + chassisNumber);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.engineNumber()))
        {
            error = "Failed to clear the 'Engine Number'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.engineNumber(), engineNumber))
        {
            error = "Failed to enter the 'Engine Number'";
            return false;
        }

        narrator.stepPassed("Engine Number : " + engineNumber);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.registrationNumber()))
        {
            error = "Failed to clear  the 'Registration Number'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.registrationNumber(), registrationNumber))
        {
            error = "Failed to enter  the 'Registration Number'";
            return false;
        }

        narrator.stepPassed("Registration Number : " + registrationNumber);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.serialNumber()))
        {
            error = "Failed to clear the 'Serial Number'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.serialNumber(), serialNumber))
        {
            error = "Failed to enter the 'Serial Number'";
            return false;
        }
        narrator.stepPassed("Serial Number : " + serialNumber);

        return true;
    }

    public boolean vehicleLookup()
    {

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.selectVehicleButton()))
        {
            error = "Failed to click on the select vehicle button.";
            return false;
        }
        narrator.stepPassed("Clicked on select vehicle button");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.vehicleLookup()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.selectVehicleButton()))
            {
                error = "Failed to wait for the'Vehicle lookup' to appear.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully opened the vehicle lookup modal");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.carTypedropdown(), condition))
        {
            error = "Failed to select the 'Vehicle condition'";
            return false;
        }
        narrator.stepPassed("Vehicle condition: " + condition);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mnMCode(), getData("M&M")))
        {
            error = "Failed to enter the 'M&M code'data.";
            return false;
        }
        narrator.stepPassed("M&M Code : " + getData("M&M"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchMMcode()))
        {
            error = "Failed to wait for the 'M&M search' button to apear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchMMcode()))
        {
            error = "Failed to click the 'M&M search'button.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.manufacturerDropdown(), getData("Manufacturer")))
        {
            error = "Failed to select the 'manufacturer' from the list.";
            return false;
        }
        narrator.stepPassed("Manufacturer: " + getData("Manufacturer"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.yearDropdown(), getData("Year")))
        {
            error = "Failed to select the 'year' from the calender.";
            return false;
        }
        narrator.stepPassed("Year: " + " " + getData("Year"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.modelDropdown(), getData("Model")))
        {
            error = "Failed to select the 'model' from the list";
            return false;
        }
        narrator.stepPassed("Model: " + getData("Model"));
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.ok()))
        {
            error = "Failed to click  the 'OK' button.";
            return false;
        }
        if (!SeleniumDriverInstance.alertHandler())
        {
            error = "Failed to wait for the 'Podium Details'";
        }
        return true;

    }
}
