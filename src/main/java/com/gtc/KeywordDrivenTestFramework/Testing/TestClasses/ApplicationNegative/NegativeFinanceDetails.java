/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Finance Negative",
        createNewBrowserInstance = false
)

public class NegativeFinanceDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeFinanceDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!financeDetails())
        {
            return narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean financeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financeDetailsTab()))
        {
            error = "Failed to scroll to the 'Finance details' container .";
            return false;
        }
        String rateIndicator = getData("Rate Indicator");
        if (!rateIndicator.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.rateIndicatorDropDown(), rateIndicator))
            {
                error = "Failed to select 'rate indicator' from the list.";
                return false;
            }
        }
        narrator.stepPassed("Rate Indicator : " + rateIndicator);
        String residualPercentage = getData("Residual");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.residualBalloonPercentage()))
        {
            error = "Failed to clear Residual/Balloon field";
        }
        if (!residualPercentage.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.residualBalloonPercentage(), residualPercentage))
            {
                error = "Failed to clear Residual/Balloon field";
            }
        }
        narrator.stepPassed("Residual/Balloon % : " + residualPercentage);
        SeleniumDriverInstance.pressKey(Keys.TAB);
        String cashDeposit = getData("Cash");
        String tradeDeposit = getData("Trade In");
        if (!cashDeposit.isEmpty() && !tradeDeposit.isEmpty())
        {
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.depositTrade()))
            {
                error = "Failed to clear Cash Deposit field";
            }

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.depositTrade(), cashDeposit))
            {
                error = "Failed to enter the 'Cash Deposit ' amount into the text field - " + cashDeposit;
                return false;
            }

            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.tradeInDeposit()))
            {
                error = "Failed to clear Trade in Deposit field";
            }

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.tradeInDeposit(), tradeDeposit))
            {
                error = "Failed to clear Trade in Deposit field";
            }

        }
        narrator.stepPassed("Cash deposit amount : " + cashDeposit);
        narrator.stepPassed("Trade in deposit amount : " + tradeDeposit);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financeDetailsTab()))
            {
                error = "Failed to scroll to the 'Finance details' container .";
                return false;
            }

            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }
        return true;
    }
}
