/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Income Details",
        createNewBrowserInstance = false
)

public class ApplicantIncomeDetails extends BaseClass
{

    String error = "";
    String amount = getData("Amount");
    String sourceIncome = getData("Source of Income");
    String otherDescription = getData("Description");
    String spouseGrossIncome = getData("Gross");
    String grossRemuneration = getData("Gross");
    String netIncome = getData("Net");

    public ApplicantIncomeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!incomeDetails())
        {
            return narrator.testFailed("Failed to enter customers 'income details' on the Form." + " " + error);
        }
        return narrator.finalizeTest("Successfully entered customers 'income details' on the Form.");
    }

    public boolean incomeDetails()
    {
        /*Entering customers income details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.incomeDetailsTab()))
        {
            error = "Failed to scroll to the 'applicant income' details form.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sourceOfIncome(), sourceIncome))
        {
            error = "Failed to select the 'source of income'.";
            return false;
        }
        narrator.stepPassed("Source of Income : " + sourceIncome);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.grossRemuneration()))
        {
            error = "Failed to clear Gross in field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.grossRemuneration(), grossRemuneration))
        {
            error = "Failed to enter customers 'gross renumeration' on the text field.";
            return false;
        }
        narrator.stepPassed("Gross Renumeration:" + " " + grossRemuneration);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.monthlyCommission()))
        {
            error = "Failed to clear the 'Monthly commission'  field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.monthlyCommission(), amount))
        {
            error = "Failed to enter customers 'Monthly commission' on the text field.";
            return false;
        }
        narrator.stepPassed("Monthly Commission: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.carAllowance()))
        {
            error = "Failed to clear the 'Car Allowance'  field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.carAllowance(), amount))
        {
            error = "Failed to enter customers 'Car Allowance' on the text field.";
            return false;
        }
        narrator.stepPassed("Car Allowance: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.overTime()))
        {
            error = "Failed to clear the 'Over Time' field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.overTime(), amount))
        {
            error = "Failed to enter customers 'Over Time' in the text field.";
            return false;
        }
        narrator.stepPassed("Over Time: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.reimbursementIncome()))
        {
            error = "Failed to clear the 'Reimbursement' field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.reimbursementIncome(), amount))
        {
            error = "Failed to enter customers 'Reimbursement' in the text field.";
            return false;
        }
        narrator.stepPassed("Reimbursement : " + amount);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.netIncome()))
        {
            error = "Failed to clear the 'Net Income'field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.netIncome(), netIncome))
        {
            error = "Failed to enter customers 'Net income' in the text field.";
            return false;
        }
        narrator.stepPassed("Net Income :" + " " + getData("Net"));
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.rentalIncome()))
        {
            error = "Failed to clear the 'Rental income'  field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.rentalIncome(), amount))
        {
            error = "Failed to enter customers 'Rental income' in the text field.";
            return false;
        }
        narrator.stepPassed("Rental income: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.maintenanceIncome()))
        {
            error = "Failed to clear 'Maintenance Income' field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.maintenanceIncome(), amount))
        {
            error = "Failed to enter customers 'Maintenance Income' in the text field.";
            return false;
        }
        narrator.stepPassed("Maintenance Income: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherIncome()))
        {
            error = "Failed to clear 'Other Income' field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherIncome(), amount))
        {
            error = "Failed to enter customers 'Other Income' in the text field.";
            return false;
        }
        narrator.stepPassed("Other Income: " + amount);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherIncomeDescription(), otherDescription))
        {
            error = "Failed to enter customers 'Other income description' in the text field.";
            return false;
        }
        narrator.stepPassed("Other income description: " + otherDescription);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseGrossRenumeration()))
        {
            error = "Failed to clear 'Spouse Rental income' field";
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseGrossRenumeration(), spouseGrossIncome))
        {
            error = "Failed to enter customers 'Spouse Rental income' on the text field.";
            return false;
        }
        narrator.stepPassed("Rental income: " + spouseGrossIncome);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseCarAllowance()))
        {
            error = "Failed to clear 'Spouse car allowance' in field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseCarAllowance(), amount))
        {
            error = "Failed to enter customers 'Spouse car allowance' in the text field.";
            return false;
        }
        narrator.stepPassed("Spouse car allowance: " + amount);
        String spouseNetIncome = getData("Net");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseNetTakeHome()))
        {
            error = "Failed to clear 'Spouse Net take-home Income' field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseNetTakeHome(), spouseNetIncome))
        {
            error = "Failed to enter customers 'Spouse Net take-home Income' in the text field.";
            return false;
        }
        narrator.stepPassed("Spouse Net take-home Income: " + spouseNetIncome);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseRentalIncome()))
        {
            error = "Failed to clear 'Spouse rental income' in field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseRentalIncome(), amount))
        {
            error = "Failed to enter customers 'Spouse rental income' in the text field.";
            return false;
        }
        narrator.stepPassed("Spouse rental income: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseMaintenance()))
        {
            error = "Failed to clear 'Spouse maintenance Income'  field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseMaintenance(), amount))
        {
            error = "Failed to enter customers 'Spouse maintenance Income' in the text field.";
            return false;
        }
        narrator.stepPassed("Spouse maintenance Income: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseOtherIncome()))
        {
            error = "Failed to clear 'Spouse other Income' field";
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseOtherIncome(), amount))
        {
            error = "Failed to enter customers 'Spouse other Income' in the text field.";
            return false;
        }
        narrator.stepPassed("Spouse other Income: " + amount);
        String applicantIncome = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.tmI(),"value");
        String spouseIncome = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.spousalTotalMonthlyIncome(),"value");

        narrator.stepPassed("Applicant total monthly income : " + applicantIncome);
        narrator.stepPassed("Spouse total monthly income : " + spouseIncome);
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.incomeDetailsTab()))
        {
            error = "Failed to scroll to the 'applicant income' details form.";
            return false;
        }

        return true;
    }
}
