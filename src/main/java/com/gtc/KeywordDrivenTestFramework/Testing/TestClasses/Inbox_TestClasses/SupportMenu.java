/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Inbox_TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Support Menu",
        createNewBrowserInstance = false
)

public class SupportMenu extends BaseClass
{

    String error = "", expectedFileName, path;

    public TestResult executeTest()
    {
        if (!supportDropDown())
        {
            return narrator.testFailed("Failed to validate the Support Menu -" + error);
        }

        return narrator.finalizeTest("Successfully validated Inbox dashboard page.");
    }

    public boolean supportDropDown()
    {
        // Get Current tab title
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.supportDropdownXpath()))
        {
            error = "Failed to wait for the 'Support' dropdown";
            return false;
        }
        
        narrator.stepPassed("Successfully validated that the 'Support' button is present.");
        // Clicking on 'Support' dropdown
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.supportDropdownXpath()))
        {
            error = "Failed to click on 'Support' dropdown";
            return false;
        }

        // Waiting for 'Help' dropdown item
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpItem()))
        {
            error = "Failed to wait for 'Help' dropdown item";
            return false;
        }

        // Clicking on 'Help' dropdown Item
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.helpItem()))
        {
            error = "Failed to click on 'Help' dropdown Item";
            return false;
        }

        // Waiting for 'Software links' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpPanel("Software links")))
        {
            error = "Failed to wait for 'Software links' panel";
            return false;
        }

        narrator.stepPassed("Successfully validated that the software links are present.");

        // Waiting for 'Vaps' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpPanel("VAPS")))
        {
            error = "Failed to wait for 'Vaps' panel";
            return false;
        }
        narrator.stepPassed("Successfully validated that VAPS is present.");
        // Waiting for 'Test bank doc' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpPanel("test bank doc")))
        {
            error = "Failed to wait for 'Test bank doc' panel";
            return false;
        }
        narrator.stepPassed("Successfully validated that Test Bank Document is present.");

        // Waiting for 'Help' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.helpPanel("Help")))
        {
            error = "Failed to wait for 'Help' panel";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to the 'Help' page.");

        // Downlaod File embedded on the link button
        // Download 'FAQs FOR VAPS.PDF' file
        if (!SeleniumDriverInstance.downloadLinkText("FAQ'S FOR VAPS.PDF"))
        {
            error = "Failed to Download 'FAQs FOR VAPS.PDF' file";
            return false;
        }

        // delay of 5s for download to complete
        pause(5000);

        expectedFileName = "FAQ'S FOR VAPS.PDF";
        // Check if 'epad devices.exe' is in the downloads folder
        // Construct path
        path = System.getProperty("user.home") + "/Downloads/" + expectedFileName;
        if (!new File(path).exists())
        {
            error = "Failed to download 'ePad devices.exe' file.";
            return false;
        }

        // Move Expected File to current report directory
        try
        {
            File downloadFolder = new File(reportDirectory + "/" + testCaseId + "/file");
            downloadFolder.mkdir();
            FileUtils.moveFileToDirectory(new File(path), downloadFolder, false);
        } catch (IOException ex)
        {
            error = "Failed to move '" + expectedFileName + "' file to report folder";
            return false;
        }

        // Clicking on 'Inbox' menu
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.inboxMenu()))
        {
            error = "Failed to click on 'Inbox' menu";
            return false;
        }

        // Waiting for inbox table to load
        if (!LightstoneScriptingInstance.maskHandling_Login())
        {
            error = "Failed to handle mask";
            return false;
        }

        // Waiting for 'Search criteria'
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchCriteria()))
        {
            error = "Failed to wait for 'Search criteria'";
            return false;
        }

        // Clicking on 'Terms and Conditions'
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.termsAndConditions()))
        {
            error = "Failed to click on 'Terms and Conditions'";
            return false;
        }

        // Switch New tab
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to Switch New tab";
            return false;
        }

        // Validate that tab open is from column
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.embeddedTerms()))
        {
            error = "Failed to Validate that tab open is 'user agreement' column";
            return false;
        }
        pause(2500);
        narrator.stepPassedWithScreenShot("Successfully opened the 'Terms and conditions' page.");

        //extracting new tab Title page name to extends report
        testData.extractParameter("Terms and condition page Title", SeleniumDriverInstance.Driver.getTitle(), "");

        // Close the new Tab and switch back to original tab
        SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());

        /**
         * Archives Page not working (19/09/2018)
         */
        // Clicking on 'Archives' menu
//        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.archiveMenu())) {
//            error = "Failed to click on 'Archives' menu";
//            return false;
//        }
//
//        // Switch New tab
//        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
//            error = "Failed to Switch New tab";
//            return false;
//        }
//
//        // Validate that tab open is from column
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.signioArchivePage())) {
//            error = "Failed to Validate that tab open is from 'Leads' column";
//            return false;
//        }
//
//        //extracting new tab Title page name to extends report
//        testData.extractParameter("Signio Archive page Title", SeleniumDriverInstance.Driver.getTitle(), "");
//        narrator.stepPassedWithScreenShot("Successfully opened 'Signio Archive' page.");
//
//        // Close the new Tab and switch back to original tab
//        SeleniumDriverInstance.closeTab(originalHandle);
        return true;
    }

}
