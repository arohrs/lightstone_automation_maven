/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Inbox_TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Compare",
        createNewBrowserInstance = false
)

public class ComparePodium extends BaseClass
{

    String error = "";

    public TestResult executeTest()
    {
        if (!cPodium())
        {
            return narrator.testFailed("Failed to validate the Compare Podium -" + error);
        }

        return narrator.finalizeTest("Successfully validated Inbox dashboard page.");
    }

    public boolean cPodium()
    {
        // Get Current tab title
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());
        // Clicking on 'Compare'
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.suppDocs("C", 18)))
        {
            error = "Failed to wait for the 'Compare' column";
            return false;
        }

        narrator.stepPassed("Successfully validated that the 'COMPARE PODIUM' button is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suppDocs("C", 18)))
        {
            error = "Failed to click on the 'Compare' button.";
            return false;
        }

        // Waiting for 'Compare' modal
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.compareModal(), 3))
        {
            // Clicking on 'Compare' button on 'Compare modal'
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.compareModalCompareButton()))
            {
                error = "Failed to click on 'Compare' button on 'Compare modal'";
                return false;
            }
        }

        // Switch New tab
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to Switch to a new tab.";
            return false;
        }
        // Validate that tab open is from column
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.podiumComparePage()))
        {
            error = "Failed to Validate that tab open is from 'Compare' column";
            return false;
        }

        //extracting new tab Title page name to extends report
        testData.extractParameter("Supporting Document page Title", SeleniumDriverInstance.Driver.getTitle(), "");
        narrator.stepPassedWithScreenShot("Successfully opened the 'Compare' column.");

        // Close the new Tab and switch back to original tab
        if (!SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle()))
        {
            error = "Failed to close the current tab.";
            return false;
        }

        return true;
    }

}
