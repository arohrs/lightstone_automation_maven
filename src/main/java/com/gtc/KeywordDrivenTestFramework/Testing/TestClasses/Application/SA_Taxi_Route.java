/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "SA TAXI Route",
        createNewBrowserInstance = false
)
public class SA_Taxi_Route extends BaseClass
{

    String error = "";

    public SA_Taxi_Route()
    {

    }

    public TestResult executeTest()
    {
        if (!saTaxiRoute())
        {

            return narrator.testFailed("Failed to enter the SA TAXI Route's details " + error);
        }
        return narrator.finalizeTest("Successfully entered SA TAXI Route's details.");
    }

    public boolean saTaxiRoute()
    {
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.saTaxiRouteForm()))
        {
            error = "Failed to scroll to 'SA TAXI ROUTE 'form.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.taxiAssociation()))
        {
            error = "Failed to wait for 'taxi association' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.taxiAssociation(), getData("Taxi association")))
        {
            error = "Failed to enter the 'taxi association'in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.taxiAssociationName(getData("Taxi association"))))
        {
            error = "Failed to click the 'taxi association'";
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.routeFrom()))
        {
            error = "Failed to wait for'Route from/to date' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.routeFrom(), getData("Route from/to")))
        {
            error = "Failed to enter the 'Route from/to'in the text field.";
            return false;
        }
        narrator.stepPassed("Route From/To : " + getData("Route from/to"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.driverWages()))
        {
            error = "Failed to wait for the 'Driver wages' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.driverWages(), getData("Driver wages")))
        {
            error = "Failed to enter the 'Driver wages'in the text field.";
            return false;
        }
        narrator.stepPassed("Drivers Wages Per Month : " + getData("Driver wages"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.numberOfPassengers()))
        {
            error = "Failed to wait for the 'number of passenger'  text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.numberOfPassengers(), getData("Passengers/trip")))
        {
            error = "Failed to enter the 'number of passenger'in the text field.";
            return false;
        }
        narrator.stepPassed("Average No of Passengers per Trip : " + getData("Passengers/trip"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.passengerFare()))
        {
            error = "Failed to wait for 'Fare per passengers' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.passengerFare(), getData("Taxi Fare")))
        {
            error = "Failed to enter the 'Fare per passengers'in the text field.";
            return false;
        }
        narrator.stepPassed("Fare Per Passengers : " + getData("Taxi Fare"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.daysWorked()))
        {
            error = "Failed to wait for ' days worked/month ' field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.daysWorked(), getData("Days worked")))
        {
            error = "Failed to select the 'days worked/month";
            return false;
        }
        narrator.stepPassed("Days worked per month : " + getData("Days worked"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.taxiArea()))
        {
            error = "Failed to wait the 'Area' field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.taxiArea(), getData("Area")))
        {
            error = "Failed to select the 'Area'.";
            return false;
        }
        narrator.stepPassed("Area : " + getData("Area"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tripKM()))
        {
            error = "Failed to wait the 'KM/trip ' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.tripKM(), getData("KM/trip")))
        {
            error = "Failed to enter the 'KM/trip'in the text field.";
            return false;
        }
        narrator.stepPassed("KM Per Trip : " + getData("KM/trip"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.tripsPerDay()))
        {
            error = "Failed to wait the 'No of Trips per Day'  field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.tripsPerDay(), getData("Trip/day")))
        {
            error = "Failed to enter the ' No of Trips per Day'in the text field.";
            return false;
        }
        narrator.stepPassed("No of Trips per Day :" + getData("Trip/day"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.taxiNumberOfSeats()))
        {
            error = "Failed to wait the 'Number of seats'  field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.taxiNumberOfSeats(), getData("Number of seat")))
        {
            error = "Failed to enter the 'Number of seats'in the text field.";
            return false;
        }
        narrator.stepPassed("Number of Licenced Seats : " + getData("Number of seat"));

        return true;
    }
}
