/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Residential Details",
        createNewBrowserInstance = false
)

public class ResidentialDetails extends BaseClass
{

    String error = "";

    public ResidentialDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!residentialDetails())
        {
            return narrator.testFailed("Failed to enter customers 'residential details' on the Form - " + error);
        }
        return narrator.finalizeTest("Successfully entered customers 'residential details' on the Form.");
    }

    public boolean residentialDetails()
    {
        /*Entering customers residential details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.residentialDetailsTab()))
        {
            error = "Failed to wait for the 'residential details'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.address1(), getData("Residential Address")))
        {
            error = "Failed to enter 'residential address' on the text field.";
            return false;
        }
        narrator.stepPassed("Residential Address 1:" + " " + getData("Residential Address"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.address2(), getData("Residential Address2")))
        {
            error = "Failed to enter 'residential address 2' on the text field.";
            return false;
        }
        narrator.stepPassed("Residential Address 2:" + " " + getData("Residential Address2"));
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.suburb()))
        {
            error = "Failed to click 'suburb' lookup button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchSuburb()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.searchSuburb(), getData("Search Suburb")))
        {
            error = "Failed to enter 'suburb' into the search text field.";
            return false;
        }
        narrator.stepPassed("Search suburb : " + getData("Search Suburb"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchButton()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchButton()))
        {
            error = "Failed to click on the 'sububrb' search button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectEmployerSuburbDropDown()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectSuburbDropDown(), getData("Select Suburb")))
        {
            error = "Failed to select 'suburb'.";
            return false;
        }
        narrator.stepPassed("Suburb : " + getData("Select Suburb"));

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectCodeDropDown()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.selectCodeDropDown(), getData("Select Code")))
        {
            error = "Failed to select the 'code' from the list.";
            return false;
        }
        narrator.stepPassed("Code : " + getData("Select Code"));
        narrator.stepPassedWithScreenShot("Successfully captured the address lookup details");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectOK()))
        {
            error = "Failed to click the 'OK' button.";
            return false;
        }
        SeleniumDriverInstance.pause(9000);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.postalAdressDropDown()))
        {
            error = "Failed to wait for the postal address list.";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.postalAdressDropDown(), getData("Postal Address")))
        {
            error = "Failed to select 'postal address' from the list.";
            return false;
        }
        narrator.stepPassed("Postal Address:" + " " + "Same as residential address");
        checkIfAvailable();
        String periodYear = getData("Period Year");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.periodYear(), periodYear))
        {
            error = "Failed to enter 'period(year)' into the text field.";
            return false;
        }

        String periodMonth = getData("Period Month");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.periodMonth(), periodMonth))
        {
            error = "Failed to enter 'period(month)' into the text field.";
            return false;
        }

        narrator.stepPassed("Period at Current Address YY: " + periodYear);
        narrator.stepPassed("Period at Current Address MM: " + periodMonth);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.previousAddressYear(), periodMonth))
        {
            error = "Failed to enter 'period(year)' into the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.previousAddressMonth(), periodYear))
        {
            error = "Failed to enter 'period(year)' into the text field.";
            return false;
        }
        narrator.stepPassed("Period at Previous Address YY: " + periodMonth);
        narrator.stepPassed("Period at Previous Address MM: " + periodYear);

        return true;
    }

    public boolean checkIfAvailable()
    {
        List<String> occupantList_TestPack = Arrays.asList(testData.getData("Occupant_List").split(","));
        List<String> occupantList_WebPage = SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.occupantTypeList());

        for (String currentOccupant_TestPack : occupantList_TestPack)
        {
            if (!occupantList_WebPage.stream().anyMatch(s -> currentOccupant_TestPack.equalsIgnoreCase(s)))
            {
                error = "Occupant not part of the list";
                return false;
            }
        }

        for (String occupant : occupantList_WebPage)
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.occupantType(), occupant))
            {
                error = "Failed to select' " + occupant + " 'from the list";
                return false;
            }
            if (occupant.contains("Owner - Bonded"))
            {
                narrator.stepPassed("Owner/Tenant/Lodger : " + occupant.valueOf("Owner"));
            }
            if (occupant.toLowerCase().contains("owner"))
            {
                //enter data
                //val outstanding bond balance
                if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.bondBalance()))
                {
                    error = "Failed to clear text field";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.bondBalance(), getData("Outstanding Balance")))
                {
                    error = "Failed to enter the 'outstading balance'";
                    return false;
                }

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeOfHouse(), getData("Type of House")))
                {
                    error = "Failed to select the 'Type of house' ";
                    return false;
                }

                if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.propertyValue()))
                {
                    error = "Failed to clear the 'Propert current value'";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.propertyValue(), getData("Current value")))
                {
                    error = "Failed to enter the 'Propert current value'";
                    return false;
                }

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bondedVia(), getData("Bonded Via")))
                {
                    error = "Failed to select the 'Bonded Via'";
                    return false;
                }

            } else
            {

                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeOfHouse(), getData("Type of House")))
                {
                    error = "Failed to select the 'Type of house' ";
                    return false;
                }
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bondedVia(), getData("Bonded Via")))
                {
                    error = "Failed to select the 'Bonded Via'";
                    return false;
                }

            }

        }
        narrator.stepPassed("Outstanding Bond Balance : " + getData("Outstanding Balance"));
        narrator.stepPassed("Type of house : " + getData("Type of House"));
        narrator.stepPassed("Property Current Value : " + getData("Current value"));
        narrator.stepPassed("Bonded via : " + getData("Bonded Via"));

        return true;
    }
}
