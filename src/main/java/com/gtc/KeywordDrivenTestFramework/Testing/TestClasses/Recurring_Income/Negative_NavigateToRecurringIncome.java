/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Recurring_Income;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Navigate Recurring Income",
        createNewBrowserInstance = false
)

public class Negative_NavigateToRecurringIncome extends BaseClass
{

    String error = "";

    public Negative_NavigateToRecurringIncome()
    {
    }

    public TestResult executeTest()
    {

        if (!recurringIncome())
        {
            return narrator.testFailed("Failed to navigate into Recurring Income - " + error);
        }

        return narrator.finalizeTest("Successfully navigated into Recurring Income window ");
    }

    public boolean recurringIncome()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.docDropDown()))
        {
            error = "Failed to wait for the 'DOC drop down' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(LightStonePageObjects.docDropDown()))
        {
            error = "Failed to click 'DOC' dropdown button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.recurringIncome()))
        {
            error = "Failed to wait for the 'Recurring Income' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(LightStonePageObjects.recurringIncome()))
        {
            error = "Failed to click  the 'Recurring Income' from the dropdown list.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new browser window.";
            return false;
        }
        pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.recurringIncomeHeader(), 120))
        {
            error = "Failed to wait for the 'Recurring Income page' to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated into Recurring Income");

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
        String currentMonth = dateFormat.format(date);

        String monthHeader = SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.monthHeader());
        if (!currentMonth.equals(monthHeader))
        {
            narrator.stepFailed("Recurring Income did not land on the current month");

        }
        return true;
    }

}
