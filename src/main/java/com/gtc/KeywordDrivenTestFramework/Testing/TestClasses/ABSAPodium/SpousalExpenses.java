/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Spousal Expenses",
        createNewBrowserInstance = false
)
public class SpousalExpenses extends BaseClass
{

    String error = "";
    String rent = getData("Rent");
    String amount = getData("Amount");

    public SpousalExpenses()
    {
    }

    public TestResult executeTest()
    {
        if (!spousalExpenses())
        {
            return narrator.testFailed("Failed to enter the spousal expenses details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the spousal expenses  details");
    }

    public boolean spousalExpenses()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.spousealExpensesTab()))
        {
            error = "Failed to scroll to the 'Spousal Expenses'";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesRentTotal()))
        {

            error = "Failed to clear the 'Bond Payment/Rent Total'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesRentTotal(), rent))
        {
            error = "Failed to enter the 'Bond Payment/Rent Total'";
            return false;
        }
        narrator.stepPassed("Bond Payment/Rent Total : " + rent);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesRates()))
        {
            error = "Failed to clear the 'Rates, Water & Electricity'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesRates(), amount))
        {
            error = "Failed to enter the 'Rates, Water & Electricity'";
            return false;
        }
        narrator.stepPassed("Rates, Water & Electricity : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesVehicleInstalment()))
        {
            error = "Failed to clear the 'Vehicle Instalments'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesVehicleInstalment(), amount))
        {
            error = "Failed to enter the 'Vehicle Instalments'";
            return false;
        }
        narrator.stepPassed("Vehicle Instalments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesPersonalLoan()))
        {
            error = "Failed to clear the 'Personal Loan Repayments'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesPersonalLoan(), amount))
        {
            error = "Failed to enter the 'Personal Loan Repayments'";
            return false;
        }
        narrator.stepPassed("Personal Loan Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesCreditCar()))
        {
            error = "Failed to clear the 'Credit Card Repayments'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesCreditCar(), amount))
        {
            error = "Failed to enter the 'Credit Card Repayments'";
            return false;
        }
        narrator.stepPassed("Credit Card Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesFurnitureAccounts()))
        {
            error = "Failed to clear the 'Furniture Accounts'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesFurnitureAccounts(), amount))
        {
            error = "Failed to enter the 'Furniture Accounts'";
            return false;
        }
        narrator.stepPassed("Furniture Accounts : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesClothingAccounts()))
        {
            error = "Failed to clear the 'Clothing Accounts'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesClothingAccounts(), amount))
        {
            error = "Failed to enter the 'Clothing Accounts'";
            return false;
        }
        narrator.stepPassed("Clothing Accounts : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesOverdraft()))
        {
            error = "Failed to clear the 'Overdraft Repayments'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesOverdraft(), amount))
        {
            error = "Failed to enter the 'Overdraft Repayments'";
            return false;
        }
        narrator.stepPassed("Overdraft Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesInsurancePayment()))
        {
            error = "Failed to clear the 'Policy/Insurance Payments'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesInsurancePayment(), amount))
        {
            error = "Failed to enter the 'Policy/Insurance Payments'";
            return false;
        }
        narrator.stepPassed("Policy/Insurance Payments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesTelephonePayment()))
        {
            error = "Failed to clear the 'Telephone Payments'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesTelephonePayment(), amount))
        {
            error = "Failed to enter the 'Telephone Payments'";
            return false;
        }
        narrator.stepPassed("Telephone Payments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesTransportCosts()))
        {
            error = "Failed to clear the 'Transport Costs'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesTransportCosts(), amount))
        {
            error = "Failed to enter the 'Transport Costs'";
            return false;
        }
        narrator.stepPassed("Transport Costs : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesFood()))
        {
            error = "Failed to wait for the 'Podium Details'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesFood(), amount))
        {
            error = "Failed to wait for the 'Podium Details'";
            return false;
        }
        narrator.stepPassed("Food and Entertainment : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesEducationCosts()))
        {
            error = "Failed to clear the 'Education Costs'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesEducationCosts(), amount))
        {
            error = "Failed to enter the 'Education Costs'";
            return false;
        }
        narrator.stepPassed("Education Costs : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesMaintenance()))
        {
            error = "Failed to clear the 'Maintenance'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesMaintenance(), amount))
        {
            error = "Failed to enter  the 'Maintenance'";
            return false;
        }
        narrator.stepPassed("Maintenance : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesHousehold()))
        {
            error = "Failed to clear the 'Household Expenses'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesHousehold(), amount))
        {
            error = "Failed to enter the 'Household Expenses'";
            return false;
        }
        narrator.stepPassed("Household Expenses : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesSecurity()))
        {
            error = "Failed to clear the 'Security Payment'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesSecurity(), amount))
        {
            error = "Failed to enter the 'Security Payment'";
            return false;
        }
        narrator.stepPassed("Security Payment : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesOther1()))
        {
            error = "Failed to clear the 'Other Expenses 1 '";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesOther1(), amount))
        {
            error = "Failed to enter the 'Other Expenses 1 '";
            return false;
        }
        narrator.stepPassed("Other Expenses 1 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesOtherDesc1()))
        {
            error = "Failed to clear the 'Other Expenses Description 1'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesOtherDesc1(), amount))
        {
            error = "Failed to enter the 'Other Expenses Description 1'";
            return false;
        }
        narrator.stepPassed("Other Expenses Description 1 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesOther2()))
        {
            error = "Failed to clear the 'Other Expenses 2'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesOther2(), amount))
        {
            error = "Failed to enter the 'Other Expenses 2'";
            return false;
        }
        narrator.stepPassed("Other Expenses 2 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesOtherDesc2()))
        {
            error = "Failed to clear for the 'Other Expenses Description 2'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesOtherDesc2(), amount))
        {
            error = "Failed to enter the 'Other Expenses Description 2'";
            return false;
        }
        narrator.stepPassed("Other Expenses Description 2 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesOther3()))
        {
            error = "Failed to clear the 'Other Expenses 3'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesOther3(), amount))
        {
            error = "Failed to enter the 'Other Expenses 3'";
            return false;
        }
        narrator.stepPassed("Other Expenses 3 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseExpensesOtherDesc3()))
        {
            error = "Failed to clear the 'Other Expenses Description 3'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseExpensesOtherDesc3(), amount))
        {
            error = "Failed to enter the 'Other Expenses Description 3'";
            return false;
        }
        narrator.stepPassed("Other Expenses Description 3 : " + amount);

        SeleniumDriverInstance.pressKey(Keys.TAB);

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.spouseExpensesTotal()))
        {
            error = "Failed to wait for the 'Podium Details'";
            return false;
        }

        narrator.stepPassed("Successfully validated the total monthly income field is not editable");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.spouseExpensesDisposalIncome()))
        {
            error = "Failed to wait for the 'Podium Details'";
            return false;
        }

        narrator.stepPassed("Successfully validated the disposable field is not editable");

        return true;
    }

}
