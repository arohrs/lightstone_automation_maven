/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Marketing details",
        createNewBrowserInstance = false
)
public class MarketingDetails extends BaseClass
{

    String error = "";
    String answer = getData("Answer");
    String marketingMethod = getData("Method");

    public TestResult executeTest()
    {
        if (!marketingDetails())
        {
            narrator.testFailed("Failed to enter the marketing details -"+error);
        }

        return narrator.finalizeTest("Successfully entered the marketing details");

    }

    public boolean marketingDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.marketingTab()))
        {
            error = "Failed to scroll  the 'Marketing' tab.";
            return false;
        }
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.telemarketingCampaign(),answer))
        {
            error = "Failed to select response for Telemarketing Campaign";
            return false;
        }
        narrator.stepPassed("Include in Telemarketing Campaign : "+answer);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.includeMarketingList(),answer))
        {
            error = "Failed to select response for Telemarketing list";
            return false;
        }
          narrator.stepPassed("Include in Marketing List : "+answer);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.marketingMethod(),marketingMethod))
        {
            error = "Failed to select Telemarketing method.";
            return false;
        }
          narrator.stepPassed("Marketing Communication Method : "+marketingMethod);
          if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.massDistribution(),answer))
        {
            error = "Failed to select response for mass distribution";
            return false;
        }
          narrator.stepPassed("Include in Mass Distribution : "+answer);  
           if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.throughReconnect(),answer))
        {
            error = "Failed to select response for through Reconnect.";
            return false;
        }
          narrator.stepPassed("Through Reconnect : "+answer);    
             if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.comprehensiveInsurance(),answer))
        {
            error = "Failed to select response for Comprehensive Insurance.";
            return false;
        }
          narrator.stepPassed("Comprehensive Insurance Quote : "+answer);     
          
        
        return true;

    }
}
