/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Inbox_TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Doc Podium",
        createNewBrowserInstance = false
)

public class DocPodium extends BaseClass
{

    String error = "";

    public TestResult executeTest()
    {
        if (!docPodium())
        {
            return narrator.testFailed("Failed to validate the DOC Podium -" + error);
        }

        return narrator.finalizeTest("Successfully validated Inbox dashboard page.");
    }

    public boolean docPodium()
    {        // Get Current tab title
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.columnLinkXpath("DOC", 19)))
        {
            error = "Failed to wait  for the 'Doc' column";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated that 'DOC' button is pressent.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.columnLinkXpath("DOC", 19)))
        {
            error = "Failed to click on the 'Doc' column";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.docTransaction(), 3))
        {

            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.docShowButton()))
            {
                error = "Failed to wait for 'Doc transaction show ' button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.docShowButton()))
            {
                error = "Failed to click on 'Doc transaction show ' button";
                return false;
            }

        }

        // Switch New tab
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to Switch to a new tab.";
            return false;
        }

        // Validate that tab open is from column
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.DocPage()))
        {
            error = "Failed to Validate that tab open is from 'DOC' column";
            return false;
        }

        //extracting new tab Title page name to extends report
        testData.extractParameter("DOC page Title", SeleniumDriverInstance.Driver.getTitle(), "");
        narrator.stepPassedWithScreenShot("Successfully opened the 'DOC' column.");

        // Close the new Tab and switch back to original tab
        if (!SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle()))
        {
            error = "Failed to close the current tab.";
            return false;
        }
        // End Validating table
        return true;
    }

}
