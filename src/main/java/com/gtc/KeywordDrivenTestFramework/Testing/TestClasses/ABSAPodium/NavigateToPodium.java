/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Inbox_TestClasses.Inbox;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.WebElement;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Details",
        createNewBrowserInstance = false
)
public class NavigateToPodium extends BaseClass
{

    String error = "";
    String disabled = getData("Disable Fields");
    String fieldName = getData("Fields");
    Inbox inboxObject = new Inbox();

    public NavigateToPodium()
    {
    }

    public TestResult executeTest()
    {
        if (!navigateToPodium())
        {
            return narrator.testFailed("Failed to navigate to a declined application - " + error);
        }
        return narrator.finalizeTest("Successfully navigated to a declined application");
    }

    public boolean navigateToPodium()

    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());
        inboxObject.setup();
        findPodium();
//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch to the new tab";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.podiumDetailsTab()))
//        {
//            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.podiumDetailsTab(), 120))
//            {
//                error = "Failed to wait for 'Podium Details'";
//                return false;
//            }
//        }
        List<String> disabledField = Arrays.asList(disabled.split(","));
        List<String> disabledFieldName = Arrays.asList(fieldName.split(","));

        for (String fieldName : disabledFieldName)
        {
            for (String field : disabledField)

            {
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.disabledInputField(field), 5))
                {
                    error = "Field " + field + " is not disabled";
                }

            }
            narrator.stepPassed("Successfully validated <b>" + fieldName + "</b> field is disabled");
        }

        return true;

    }

    public boolean findPodium()
    {
        try
        {
//            List<WebElement> podiumCount = SeleniumDriverInstance.retrieveWebElementByXpath(LightStonePageObjects.declinedApplicationList(6));
//            WebElement podiumClick = podiumCount.stream().filter(s -> s.getAttribute("value").equals("Declined")).findFirst().get();
//            if (!SeleniumDriverInstance.clickWebElementUsingActions(podiumClick))
//            {
//                error = "Failed to click declined button";
//                return false;
//            }
            int podiumCount = SeleniumDriverInstance.retrieveNumberOfElementsByXpath(LightStonePageObjects.declinedApplicationList(6));
            Search:
            for (int i = 1; i < podiumCount; i++)
            {
                if (SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.declinedApplicationListCount(i), "value").equals("Declined"))
                {
                    if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.declinedApplicationListCount(i)))
                    {
                        error = "Failed to wait for 'Declined' application";
                        return false;
                    }
                    if (!nestedClicks(LightStonePageObjects.declinedApplicationListCount(i), LightStonePageObjects.podiumDetailsTab()))
                    {
                        error = "Didn't find a  declined application";
                        return false;
                    }
                    if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.podiumDealerSummary(), 5))
                    {
                        if (!SeleniumDriverInstance.closeCurrentTab(LightStonePageObjects.getPageHandle()))
                        {
                            error = "Failed to wait for 'Podium Details'";
                            return false;

                        }
                        continue Search;

                    } else
                    {
                        break Search;
                    }
                }
            }

        } catch (Exception e)
        {
            error = "Failed to find a Declined application";
        }

        return true;

    }

    public boolean nestedClicks(String xpath, String searchxPath)
    {
        Search:
        for (int count = 0; count < 4; count++)
        {
            switch (count)
            {
                case 0:
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(xpath))
                    {
                        break;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 10))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;

                    } else
                    {
                        break Search;
                    }
                case 1:
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";
                        break;
                       
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 20))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                case 2:
                    if (!SeleniumDriverInstance.clickElementbyXpath(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";
                        break;
                   
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 30))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                case 3:
                    if (!SeleniumDriverInstance.roboClickLatestElementbyXpath(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";
                        break;                     
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 120))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                default:
                    return false;
            }
        }
        return true;
    }

}
