/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Company Negative",
        createNewBrowserInstance = false
)
public class NegativeCompanyDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public TestResult executeTest()
    {
        if (!companyDetails())
        {
            narrator.testFailed("Failed to perform company details container negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the company details container negative test");
    }

    public boolean companyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.companyDetailsTab())))
        {
            error = "Failed to scroll to Company Details tab.";
            return false;
        }
        String isSolevent = getData("isCompany solvent");
        if (!isSolevent.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isCompanySolvent(), isSolevent))
            {
                error = "Failed to select isSolvent option.";
                return false;
            }
        }
        narrator.stepPassed("Company solvent : " + isSolevent);
        String mortgageLoan = getData("isCredit secured");
        if (!mortgageLoan.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isCreditSecure(), mortgageLoan))
            {
                error = "Failed to select mortage Loan";
                return false;
            }
        }
        narrator.stepPassed("Mortgage loan : " + mortgageLoan);
        String cellType = getData("Cell type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mobileTypeDropDown(), cellType))
        {
            error = "Failed to select the 'Cell Type' from the list.";
            return false;
        }
        narrator.stepPassed("Cell Type : " + cellType);
        String cellNumber = getData("Cell number");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyCellNumber()))
        {
            error = "Failed to clear company Cell Phone number textfield.";
            return false;
        }
        if (!cellNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyCellNumber(), cellNumber))
            {
                error = "Failed to enter company Cell Phone number.";
                return false;
            }
        }
        narrator.stepPassed("Cell Number : " + cellNumber);
        String email = getData("Email");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyEmail()))
        {
            error = "Failed to clear to Company email textfiled.";
            return false;
        }
        if (!email.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyEmail(), email))
            {
                error = "Failed to enter company email.";
                return false;
            }
        }
        narrator.stepPassed("E-Mail : " + email);
        String industrySector = getData("Sector of Industry");
        if (!industrySector.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sectorOfIndusrty(), industrySector))
            {
                error = "Failed to select industory Sector.";
                return false;
            }
        }
        narrator.stepPassed("Sector of Industry : " + industrySector);
        String contactDesignation = getData("Designation");
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.businessContactDesignation()))
        {
            error = "Failed to scroll to Contact Designation textfield.";
            return false;
        }
        if (!contactDesignation.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.businessContactDesignation(), contactDesignation))
            {
                error = "Failed to select contact Designation.";
                return false;
            }
        }
        narrator.stepPassed("Business Contact Designation : " + contactDesignation);
        String contactName = getData("Contact Name");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessContactName()))
        {
            error = "Failed to clear business contact name.";
            return false;
        }
        if (!contactName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessContactName(), contactName))
            {
                error = "Failed to enter contact name.";
                return false;
            }
        }
        narrator.stepPassed("Business Contact Name : " + contactName);
        String preferredLanguage = getData("Preferred Language");
        if (!preferredLanguage.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyPreferredLanguage(), preferredLanguage))
            {
                error = "Failed to select preferred language.";
                return false;
            }
        }
        narrator.stepPassed("Preferred Language: " + preferredLanguage);
        String sourceIncome = getData("Source of Income");
        if (!sourceIncome.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companySourceOfIncome(), sourceIncome))
            {
                error = "Failed to select source of income.";
                return false;
            }
        }
        narrator.stepPassed("Source of Income: " + sourceIncome);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.companyDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);

            }
            narrator.stepPassedWithScreenShot("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
