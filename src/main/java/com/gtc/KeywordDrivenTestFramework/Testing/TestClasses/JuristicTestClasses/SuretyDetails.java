/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateDOB;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.ConstructID;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Surety",
        createNewBrowserInstance = false
)
public class SuretyDetails extends BaseClass
{

    String error = "";
    GeographicalDetails address = new GeographicalDetails();
    String gender = getData("Gender");

    public SuretyDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!suretyDetails())
        {
            narrator.testFailed("");
        }

        return narrator.finalizeTest("");
    }

    public boolean suretyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatorySuretyTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        String employmentType = getData("Employment Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyEmploymentType(), employmentType))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Employment Type : " + employmentType);
        String periodEmployed = getData("Period");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedYear(), periodEmployed))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedMonth(), periodEmployed))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Period Currently Employed (YY/MM) : " + periodEmployed + "/" + periodEmployed);
        String residentialStatus = getData("Residential Status");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyResidentialStatus(), residentialStatus))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Residential Status : " + residentialStatus);
        String spouseFirstName = getData("Spouse FirstName");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseFirstName(), spouseFirstName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse First Name : " + spouseFirstName);
        String spouseSurName = getData("Spouse Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseSurname(), spouseSurName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse Surname : " + spouseSurName);
        String dateOfBirth = "";
        GenerateDOB generateDOB = new GenerateDOB(21, 60);
        String dob = generateDOB.getIdDateOfBirth();
        String spouseIdNumber = "";
        try
        {

            ConstructID randID = new ConstructID();
            spouseIdNumber = randID.getSouthAfricanID(gender, dob, "South African");
        } catch (Exception e)
        {
            narrator.logDebug("Failed to generate south african ID - " + e);
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseSurname(), spouseIdNumber))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse ID Number : " + spouseIdNumber);
        String kinFirstName = getData("Next of Kin Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinFirstname(), kinFirstName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of Kin First Name : " + kinFirstName);
        String kinSurname = getData("Next of Kin Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinSurname(), kinSurname))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of Kin Surname : " + kinSurname);
        String relation = getData("Relation");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinRelation(), relation))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Applicant relation with next of kin : " + relation);
        String telephone = getData("Telephone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinTelephoneNumber(), telephone))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of kin telephone number : " + telephone);
        String addressLine = getData("Address");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine1(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Work Address Line 1: " + addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine2(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Work Address Line 2: " + addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyWorkAddressLookup()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        address.addressLookup();
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinWorkAddressLine1(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Address Line 1: " + addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinWorkAddressLine2(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Address Line 2: " + addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyNextofKinWorkAddressLookup()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        address.addressLookup();
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatorySuretyTab()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        return true;
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateDOB;
import com.gtc.keyworddriventestframework.utilities.IDGenerator.ConstructID;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Surety",
        createNewBrowserInstance = false
)
public class SuretyDetails extends BaseClass
{

    String error = "";
    GeographicalDetails address = new GeographicalDetails();
    String gender = getData("Gender");

    public SuretyDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!suretyDetails())
        {
            narrator.testFailed("");
        }

        return narrator.finalizeTest("");
    }

    public boolean suretyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatorySuretyTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        String employmentType = getData("Employment Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyEmploymentType(), employmentType))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Employment Type : " + employmentType);
        String periodEmployed = getData("Period");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedYear(), periodEmployed))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedMonth(), periodEmployed))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Period Currently Employed (YY/MM) : " + periodEmployed + "/" + periodEmployed);
        String residentialStatus = getData("Residential Status");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyResidentialStatus(), residentialStatus))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Residential Status : " + residentialStatus);
        String spouseFirstName = getData("Spouse FirstName");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseFirstName(), spouseFirstName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse First Name : " + spouseFirstName);
        String spouseSurName = getData("Spouse Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseSurname(), spouseSurName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse Surname : " + spouseSurName);
        String dateOfBirth = "";
        GenerateDOB generateDOB = new GenerateDOB(21, 60);
        String dob = generateDOB.getIdDateOfBirth();
        String spouseIdNumber = "";
        try
        {

            ConstructID randID = new ConstructID();
            spouseIdNumber = randID.getSouthAfricanID(gender, dob, "South African");
        } catch (Exception e)
        {
            narrator.logDebug("Failed to generate south african ID - " + e);
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseSurname(), spouseIdNumber))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse ID Number : " + spouseIdNumber);
        String kinFirstName = getData("Next of Kin Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinFirstname(), kinFirstName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of Kin First Name : " + kinFirstName);
        String kinSurname = getData("Next of Kin Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinSurname(), kinSurname))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of Kin Surname : " + kinSurname);
        String relation = getData("Relation");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinRelation(), relation))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Applicant relation with next of kin : " + relation);
        String telephone = getData("Telephone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinTelephoneNumber(), telephone))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of kin telephone number : " + telephone);
        String addressLine = getData("Address");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine1(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Work Address Line 1: " + addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine2(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Work Address Line 2: " + addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyWorkAddressLookup()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        address.addressLookup();
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinWorkAddressLine1(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Address Line 1: " + addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinWorkAddressLine2(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Address Line 2: " + addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyNextofKinWorkAddressLookup()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        address.addressLookup();
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatorySuretyTab()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        return true;
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateDOB;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.ConstructID;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Surety",
        createNewBrowserInstance = false
)
public class SuretyDetails extends BaseClass
{

    String error = "";
    GeographicalDetails address = new GeographicalDetails();
    String gender = getData("Gender");

    public SuretyDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!suretyDetails())
        {
            narrator.testFailed("");
        }

        return narrator.finalizeTest("");
    }

    public boolean suretyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.signatorySuretyTab())))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        String employmentType = getData("Employment Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyEmploymentType(), employmentType))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Employment Type : " + employmentType);
        String periodEmployed = getData("Period");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedYear(), periodEmployed))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyPeriodEmployedMonth(), periodEmployed))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Period Currently Employed (YY/MM) : " + periodEmployed + "/" + periodEmployed);
        String residentialStatus = getData("Residential Status");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.suretyResidentialStatus(), residentialStatus))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Residential Status : " + residentialStatus);
        String spouseFirstName = getData("Spouse FirstName");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseFirstName(), spouseFirstName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse First Name : " + spouseFirstName);
        String spouseSurName = getData("Spouse Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseSurname(), spouseSurName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse Surname : " + spouseSurName);
        String dateOfBirth = "";
        GenerateDOB generateDOB = new GenerateDOB(21, 60);
        String dob = generateDOB.getIdDateOfBirth();
        String spouseIdNumber = "";
        try
        {

            ConstructID randID = new ConstructID();
            spouseIdNumber = randID.getSouthAfricanID(gender, dob, "South African");
        } catch (Exception e)
        {
            narrator.logDebug("Failed to generate south african ID - " + e);
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretySpouseSurname(), spouseIdNumber))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Spouse ID Number : " + spouseIdNumber);
        String kinFirstName = getData("Next of Kin Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinFirstname(), kinFirstName))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of Kin First Name : " + kinFirstName);
        String kinSurname = getData("Next of Kin Surname");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinSurname(), kinSurname))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of Kin Surname : " + kinSurname);
        String relation = getData("Relation");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinRelation(), relation))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Applicant relation with next of kin : " + relation);
        String telephone = getData("Telephone");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinTelephoneNumber(), telephone))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Next of kin telephone number : " + telephone);
        String addressLine = getData("Address");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine1(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Work Address Line 1: " + addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyWorkAddressLine2(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Work Address Line 2: " + addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyWorkAddressLookup()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        address.addressLookup();
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinWorkAddressLine1(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Address Line 1: " + addressLine);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.suretyNextofKinWorkAddressLine2(), addressLine))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Address Line 2: " + addressLine);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suretyNextofKinWorkAddressLookup()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        address.addressLookup();
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatorySuretyTab()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        return true;
    }
}
