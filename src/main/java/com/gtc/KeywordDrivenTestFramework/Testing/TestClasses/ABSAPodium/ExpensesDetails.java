/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Expenses Details",
        createNewBrowserInstance = false
)
public class ExpensesDetails extends BaseClass
{

    String error = "";
    String rent = getData("Rent");
    String amount = getData("Amount");
    String totalTestPack = getData("Total");
   
    public ExpensesDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!expensesDetails())
        {
            return narrator.testFailed("Failed to enter the customer's expenses details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the customer's expenses details");
    }

    public boolean expensesDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.expensesDetailsContainer()))
        {
            error = "Failed to scroll the 'Expenses Details'";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.podiumRent()))
        {
            error = "Failed to clear the 'Bond Payment/Rent Total'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.podiumRent(), rent))
        {
            error = "Failed to enter the 'Bond Payment/Rent Total'";
            return false;
        }
        narrator.stepPassed("Bond Payment/Rent Total : " + rent);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.waterElectricity()))
        {
            error = "Failed to clear the 'Rates, Water & Electricity'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.waterElectricity(), amount))
        {
            error = "Failed to enter the 'Rates, Water & Electricity'";
            return false;
        }
        narrator.stepPassed("Rates, Water & Electricity : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.vehicleInstallment()))
        {
            error = "Failed to clear the 'Vehicle Instalments'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vehicleInstallment(), amount))
        {
            error = "Failed to enter the 'Vehicle Instalments'";
            return false;
        }
        narrator.stepPassed("Vehicle Instalments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.loanRepayment()))
        {
            error = "Failed to clear the 'Personal Loan Repayments'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.loanRepayment(), amount))
        {
            error = "Failed to enter the 'Personal Loan Repayments'";
            return false;
        }
        narrator.stepPassed("Personal Loan Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.creditCardRepayment()))
        {
            error = "Failed to clear the 'Credit Card Repayments'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.creditCardRepayment(), amount))
        {
            error = "Failed to enter the 'Credit Card Repayments'";
            return false;
        }
        narrator.stepPassed("Credit Card Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.furnitureAccount()))
        {
            error = "Failed to clear the 'Furniture Accounts'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.furnitureAccount(), amount))
        {
            error = "Failed to enter the 'Furniture Accounts'";
            return false;
        }
        narrator.stepPassed("Furniture Accounts : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.clothingAccounts()))
        {
            error = "Failed to clear the 'Clothing Accounts'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.clothingAccounts(), amount))
        {
            error = "Failed to enter the 'Clothing Accounts'";
            return false;
        }
        narrator.stepPassed("Clothing Accounts : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.overDraftRepayment()))
        {
            error = "Failed to clear the 'Overdraft Repayments'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.overDraftRepayment(), amount))
        {
            error = "Failed to enter the 'Overdraft Repayments'";
            return false;
        }
        narrator.stepPassed("Overdraft Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.insuranceRepayment()))
        {
            error = "Failed to clear the 'Policy/Insurance Payments'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.insuranceRepayment(), amount))
        {
            error = "Failed to enter the 'Policy/Insurance Payments'";
            return false;
        }
        narrator.stepPassed("Policy/Insurance Payments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.telephonePayments()))
        {
            error = "Failed to clear the 'Telephone Payments'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.telephonePayments(), amount))
        {
            error = "Failed to enter the 'Telephone Payments'";
            return false;
        }
        narrator.stepPassed("Telephone Payments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transportCosts()))
        {
            error = "Failed to clear the 'Transport Costs'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transportCosts(), amount))
        {
            error = "Failed to enter the 'Transport Costs'";
            return false;
        }
        narrator.stepPassed("Transport Costs : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.foodEntertainment()))
        {
            error = "Failed to clear the 'Food and Entertainment'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.foodEntertainment(), amount))
        {
            error = "Failed to enter the 'Food and Entertainment'";
            return false;
        }
        narrator.stepPassed("Food and Entertainment : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.educationCost()))
        {
            error = "Failed to clear the 'Education Costs'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.educationCost(), amount))
        {
            error = "Failed to enter the 'Education Costs'";
            return false;
        }
        narrator.stepPassed("Education Costs: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.maintenanceCost()))
        {
            error = "Failed to clear the 'Maintenance'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.maintenanceCost(), amount))
        {
            error = "Failed to enter the 'Maintenance'";
            return false;
        }
        narrator.stepPassed("Maintenance: " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.householdExpenses()))
        {
            error = "Failed to clear the 'House Hold Expenses'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.householdExpenses(), amount))
        {
            error = "Failed to enter the 'House Hold Expenses'";
            return false;
        }
        narrator.stepPassed("House Hold Expenses : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.securityPayment()))
        {
            error = "Failed to clear the 'Security Payment'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.securityPayment(), amount))
        {
            error = "Failed to clear the 'Security Payment'";
            return false;
        }
        narrator.stepPassed("Security Payment : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.securityPayment()))
        {
            error = "Failed to clear the 'Security Payment'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.securityPayment(), amount))
        {
            error = "Failed to enter the 'Security Payment'";
            return false;
        }
        narrator.stepPassed("Security Payment : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherExpenses1()))
        {
            error = "Failed to clear the 'Other Expenses 1'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherExpenses1(), amount))
        {
            error = "Failed to enter the 'Other Expenses 1'";
            return false;
        }
        narrator.stepPassed("Other Expenses 1 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherExpensesDesc1()))
        {
            if (!SeleniumDriverInstance.dismissAlert())
            {
                error = "Failed to handle the pop up message'";
                return false;
            }
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherExpensesDesc1(), amount))
        {
            error = "Failed to enter the 'Other Expenses Description 1'";
            return false;
        }
        narrator.stepPassed("Other Expenses Description 1 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherExpenses2()))
        {
            error = "Failed to clear the 'Other Expenses 2'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherExpenses2(), amount))
        {
            error = "Failed to enter the 'Other Expenses 2'";
            return false;
        }
        narrator.stepPassed("Other Expenses 2 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherExpensesDesc2()))
        {
            error = "Failed to clear the 'Other Expenses Description 2'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherExpensesDesc2(), amount))
        {
            error = "Failed to enter the 'Other Expenses Description 2'";
            return false;
        }
        narrator.stepPassed("Other Expenses Description 2 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherExpenses3()))
        {
            error = "Failed to clear the 'Other Expenses 3'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherExpenses3(), amount))
        {
            error = "Failed to enter the 'Other Expenses 3'";
            return false;
        }
        narrator.stepPassed("Other Expenses 3 : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.otherExpensesDesc3()))
        {
            error = "Failed to clear the 'Other Expenses Description 3'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.otherExpensesDesc3(), amount))
        {
            error = "Failed to enter the 'Other Expenses Description 3'";
            return false;
        }
        narrator.stepPassed("Other Expenses Description 3 : " + amount);

        return true;
    }

}
