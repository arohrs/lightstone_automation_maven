/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Alphera Info",
        createNewBrowserInstance = false
        
)
public class AlpheraSpecificInformation extends BaseClass
{
    String error="";
    
    public AlpheraSpecificInformation()
    {
    }
    
     public TestResult executeTest()
    {

        if (!alpheraInformation())
        {
            return narrator.testFailed("Failed to enter Alphera specific details. - " + error);
        }
        return narrator.finalizeTest("Successfully enter Alphera specific details.");
    }
    
    public boolean alpheraInformation()
    {
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.alpheraSpecificInfoForm()))
        {
            error = "Failed to scroll to the 'Alphera info Tab ' field.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.alpheraBrandCode()))
        {
            error = "Failed to wait for the 'Alphera branch code' field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.alpheraBrandCode(), getData("Brand codes")))
        {
            error = "Failed to select 'Alphera branch code' from the list.";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.alpheraPayDay()))
        {
            error = "Failed to wait for the 'Alphera pay day' field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.alpheraPayDay(), getData("PayDay")))
        {
            error = "Failed to select 'Alphera pay day' from the list.";
            return false;
        }
    
        return true;
    }
            
}
