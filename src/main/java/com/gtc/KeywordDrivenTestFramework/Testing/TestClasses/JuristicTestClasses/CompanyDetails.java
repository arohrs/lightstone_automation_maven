/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Company details",
        createNewBrowserInstance = false
)
public class CompanyDetails extends BaseClass
{

    String error = "";

    public CompanyDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!companyDetails())
        {
            narrator.testFailed("Failed to enter the company's details -" + error);
        }

        return narrator.finalizeTest("Successfully entered the company's details");
    }

    public boolean companyDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement((LightStonePageObjects.companyDetailsTab())))
        {
            error = "Failed to scroll to the 'Company Details' container.";
            return false;
        }
        String clientSubType = getData("Client sub type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.clientSubType(), clientSubType))
        {
            error = "Failed to select the 'Client sub type' from the list.";
            return false;
        }
        narrator.stepPassed("Client Sub Type : " + clientSubType);
        String isSolevent = getData("isCompany solvent");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isCompanySolvent(), isSolevent))
        {
            error = "Failed to select the 'Yes/No' from the list.";
            return false;
        }
        narrator.stepPassed("is the Company solvent : " + isSolevent);
        String mortgageLoan = getData("isCredit secured");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isCreditSecure(), mortgageLoan))
        {
            error = "Failed to select the 'Yes/No' from the list.";
            return false;
        }
        narrator.stepPassed("Is the Credit secured by a Mortgage loan : " + mortgageLoan);
        String blackOwned = getData("Black owned");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.blackOwned(), blackOwned))
        {
            error = "Failed to select the 'Yes/No' from the list.";
            return false;
        }
        narrator.stepPassed("Black Owned : " + blackOwned);
        String memberAge = getData("Member Age");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.principalMembersAge(), memberAge))
        {
            error = "Failed to enter the 'Average age' into the text field.";
            return false;
        }
        narrator.stepPassed("Principal Members Average Age : " + memberAge);
        String duration = getData("Duration of Directorship");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.durationOfDirectorship(), duration))
        {
            error = "Failed to enter the 'Duration of directorship' into the text field.";
            return false;
        }
        narrator.stepPassed("Duration of Current Directorship : " + duration);
        String taxPayer = getData("Tax payer");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.registeredTaxPayer(), taxPayer))
        {
            error = "Failed to select the 'Yes/No' from the list.";
            return false;
        }
        narrator.stepPassed("Registered Tax Payer : " + taxPayer);
        String ringFenced = getData("Ringfenced");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.profitRingfenced(), ringFenced))
        {
            error = "Failed to select the 'Yes/No' from the list.";
            return false;
        }
        narrator.stepPassed("Assets & Profits Ringfenced : " + ringFenced);
        String contactMethod = getData("Contact Method");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyPreferredContactMethod(), contactMethod))
        {
            error = "Failed to select the 'Contact Method' from the list.";
            return false;
        }
        narrator.stepPassed("Preferred Contact Method : " + contactMethod);
        String cellType = getData("Cell type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mobileTypeDropDown(), cellType))
        {
            error = "Failed to select the 'Cell Type' from the list.";
            return false;
        }
        narrator.stepPassed("Cell Type : " + cellType);
        String cellNumber = getData("Cell number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyCellNumber(), cellNumber))
        {
            error = "Failed to enter the 'cell number' in the text field.";
            return false;
        }
        narrator.stepPassed("Cell Number : " + cellNumber);
        String telephoneNumber = getData("Telephone number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.workNumber(), telephoneNumber))
        {
            error = "Failed to enter the 'telephone number' in the text field.";
            return false;
        }
        narrator.stepPassed("Telephone Number : " + telephoneNumber);
        String email = getData("Email");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyEmail(), email))
        {
            error = "Failed to enter the 'Email' in the text field.";
            return false;
        }
        narrator.stepPassed("E-Mail : " + email);
        String countryOfOperation = getData("Country of Operation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyCountryOfOperation(), countryOfOperation))
        {
            error = "Failed to select the 'Country of Operation' from the list.";
            return false;
        }
        narrator.stepPassed("Country of Operation : " + countryOfOperation);
        String headOffice = getData("Country of Head office");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyCountryOfHeadOffice(), headOffice))
        {
            error = "Failed to select the 'Country of Head Office' from the list.";
            return false;
        }
        narrator.stepPassed("Country of Head Office : " + headOffice);
        String industrySector = getData("Sector of Industry");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sectorOfIndusrty(), industrySector))
        {
            error = "Failed to select the 'Sector of Industry' from the list.";
            return false;
        }
        narrator.stepPassed("Sector of Industry : " + industrySector);
        String industryType = getData("Industry Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.industryType(), industryType))
        {
            error = "Failed to select the 'Industry Type' from the list.";
            return false;
        }
        narrator.stepPassed("Industry Type : " + industryType);
        String holdingName = getData("Holding Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfOperation2(), countryOfOperation))
        {
            error = "Failed to enter the 'Holding Company Name' text in the field.";
            return false;
        }
        narrator.stepPassed("Country of Operation 2 : " + countryOfOperation);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfOperation3(), countryOfOperation))
        {
            error = "Failed to enter the 'Holding Company Name' text in the field.";
            return false;
        }
        narrator.stepPassed("Country of Operation 3 : " + countryOfOperation);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfOperation4(), countryOfOperation))
        {
            error = "Failed to enter the 'Holding Company Name' text in the field.";
            return false;
        }
        narrator.stepPassed("Country of Operation 4 : " + countryOfOperation);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.countryOfOperation5(), countryOfOperation))
        {
            error = "Failed to enter the 'Holding Company Name' text in the field.";
            return false;
        }
        narrator.stepPassed("Country of Operation 5 : " + countryOfOperation);

        String holdingNumber = getData("Registration Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.holdingRegistrationNumber(), holdingNumber))
        {
            error = "Failed to enter the 'Company Registration Number' button.";
            return false;
        }
        narrator.stepPassed("Holding Company Registration Number : " + holdingNumber);
        String contactTitle = getData("Contact title");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.businessContactTitle(), contactTitle))
        {
            error = "Failed to select the 'Business Contact Title' from the list.";
            return false;
        }
        narrator.stepPassed("Business Contact Title : " + contactTitle);
        String contactDesignation = getData("Designation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.businessContactDesignation(), contactDesignation))
        {
            error = "Failed to select the 'Business Contact Designation' from the list.";
            return false;
        }
        narrator.stepPassed("Business Contact Designation : " + contactDesignation);
        String contactName = getData("Contact Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessContactName(), contactName))
        {
            error = "Failed to enter the 'Business Contact Name' in the text field.";
            return false;
        }
        narrator.stepPassed("Business Contact Name : " + contactName);
        String businessYear = getData("Time (Year)");
        String businessMonth = getData("Time(Month)");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.yearTimeInBusiness(), businessYear))
        {
            error = "Failed to enter the 'Time in Business (Year) '.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.monthTimeInBusiness(), businessMonth))
        {
            error = "Failed to enter the 'Time in Business (Month) '.";
            return false;
        }
        narrator.stepPassed("Time in Business  Year: " + businessYear + " Month: " + businessMonth + " ");
        String monthlyIncome = getData("Monthly Income");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.businessMonthlyIncome()))
        {
            error = "Failed to clear the 'Monthly Income' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessMonthlyIncome(), monthlyIncome))
        {
            error = "Failed to enter the 'Monthly Income' button.";
            return false;
        }
        narrator.stepPassed("Monthly Income : " + monthlyIncome);
        String companyExpenses = getData("Company Expenses");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.companyExpenses()))
        {
            error = "Failed to clear the 'Monthly Income' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyExpenses(), companyExpenses))
        {
            error = "Failed to enter the 'Monthly Income' button.";
            return false;
        }
        narrator.stepPassed("Company Expenses : " + companyExpenses);
        String preferredLanguage = getData("Preferred Language");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyPreferredLanguage(), preferredLanguage))
        {
            error = "Failed to select the 'Preferred Language' from the list.";
            return false;
        }
        narrator.stepPassed("Preferred Language: " + preferredLanguage);
        String sourceIncome = getData("Source of Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companySourceOfIncome(), sourceIncome))
        {
            error = "Failed to select the 'Source of Income' from the list.";
            return false;
        }
        narrator.stepPassed("Source of Income: " + sourceIncome);
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.regTrackDropdown()))
        {
            error = "Failed to scroll to the 'Request Regtrack Later' textfield.";
            return false;
        }
        String regTrack = getData("RegTrack");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.regTrackDropdown(), regTrack))
        {
            error = "Failed to select the 'Request Regtrack Later' from the list.";
            return false;
        }
        narrator.stepPassed("Request Regtrack Later : " + regTrack);
        String foreignTax = getData("Tax");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyForeignTaxObligation(), foreignTax))
        {
            error = "Failed to select the 'Tax obligations' from the list.";
            return false;
        }
        narrator.stepPassed("Tax residencies outside of South Africa: " + foreignTax);
        String usEntity = getData("US Entity");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.isUSEntity(), usEntity))
        {
            error = "Failed to select the 'Tax obligations' from the list.";
            return false;
        }
        narrator.stepPassed("Is the entity incorporated: " + usEntity);

        return true;
    }

}
