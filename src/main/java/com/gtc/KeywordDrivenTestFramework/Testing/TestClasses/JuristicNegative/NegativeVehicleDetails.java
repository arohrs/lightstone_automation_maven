/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Vehicle Negative",
        createNewBrowserInstance = false
)
public class NegativeVehicleDetails extends BaseClass
{

    String error = "";
    String articleCondition = getData("Article condition");
    String articleType = getData("Article type");
    String vehicleUse = getData("Vehicle use");
    String km = getData("Kilometers");
    String redField = getData("redField");
    String readOnly = getData("Read Only");
    String notEditable = getData("Not editable");
    String fieldType = getData("Field Type");
    String field = getData("Field");
    String submit = getData("Submit");

    public NegativeVehicleDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!vehicleDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");

    }

    public boolean vehicleDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionVehicleDetailsTab()))
        {
            error = "Failed to scroll to the 'Vehicle Details' tab.";
            return false;
        }
        List<String> readOnlyField = Arrays.asList(readOnly.split(","));
        for (String readField : readOnlyField)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.readOnlyInputField(readField)))
            {
                error = "Failed to validate '" + readField + "' is a read-only field .";
                return false;
            }

        }
        narrator.stepPassed("Successfully validated " + notEditable + " fields are not manually editable");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionArticleType(), articleType))
        {
            error = "Failed to select article type.";
            return false;
        }
        narrator.stepPassed("Article Type : " + articleType);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.transactionArticleCondition(), articleCondition))
        {
            error = "Failed to select article condition.";
            return false;
        }
        narrator.stepPassed("Article New/Used : " + articleCondition);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.transactionActualKilometers()))
        {
            error = "Failed to clear Actual Kilometers textfield.";
            return false;
        }
        if (!km.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.transactionActualKilometers(), km))
            {
                error = "Failed to enter Actual Kilometers.";
                return false;
            }
        }
        narrator.stepPassed("Actual Kilometers : " + km);

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.transactionVehicleDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "Failed to catch the '" + field + "' is Highlighted in red .";
                    return false;
                }

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to catch the '" + field + "' is Highlighted in red .";
                    return false;
                }
                pause(500);

            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))

                {
                    error = "The'" + field + "' field is highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to catch the '" + field + "' is Highlighted in red .";
                    return false;
                }
                pause(500);
                
            }

            narrator.stepPassedWithScreenShot("Successfully found the red highlighted <b>" + field + " </b> field");

        }

        return true;
    }

}
