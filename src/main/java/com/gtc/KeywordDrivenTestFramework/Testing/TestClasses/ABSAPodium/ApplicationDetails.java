/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Application Details",
        createNewBrowserInstance = false
)
public class ApplicationDetails extends BaseClass
{

    String error = "";
    String firstName = getData("First Name");
    String middleName = getData("Middle Name");
    String regTrack = getData("Reg Track");
    String fields = getData("Fields");
    String readOnly = getData("Disable Fields");

    public ApplicationDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!applicantDetails())
        {
            return narrator.testFailed("Failed to enter the applicant's details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the applicant's details");
    }

    public boolean applicantDetails()
    {
        if (!SeleniumDriverInstance.dismissAlert())
        {
            error = "Can't close pop up";

        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.applicantDetailsTab()))
        {
            error = "Failed to scroll to  the 'Application Details'";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.applicationFirstname()))
        {
            error = "Failed to clear the 'First Name'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationFirstname(), firstName))
        {
            error = "Failed to enter the 'First Name'";
            return false;
        }
        narrator.stepPassed("First Name: " + firstName);
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationMiddleName(), middleName))
        {
            error = "Failed to enter the 'Middle Name'";
            return false;
        }
        narrator.stepPassed("Middle Name: " + middleName);
        String surname = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.applicationSurname(), "value");
        if (!surname.isEmpty())
        {
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.applicationSurname()))
            {
                error = "Failed to clear  the 'Surname'";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationSurname(), surname))
            {
                error = "Failed to enter the 'surname'";
                return false;
            }
        } else
        {
            surname = getData("Surname");
            if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.applicationSurname()))
            {
                error = "Failed to clear  the 'Surname'";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.applicationSurname(), surname))
            {
                error = "Failed to enter the 'surname'";
                return false;
            }
        }
        narrator.stepPassed("Surname : " + surname);
        List<String> disabledField = Arrays.asList(readOnly.split(","));
        List<String> disabledFieldName = Arrays.asList(fields.split(","));

        for (String fieldName : disabledFieldName)
        {
            for (String field : disabledField)

            {
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.readOnlyInputField(field), 5))
                {
                    error = "Field " + field + " is not disabled";
                }

            }
            narrator.stepPassed("Successfully validated " + fieldName + " field is disabled");
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.regTrackDropdown(), regTrack))
        {
            error = "Failed to select the 'Request RegTrack'";
            return false;
        }
        narrator.stepPassed("Request Reg Track : " + regTrack);
        pause(500);

        return true;
    }
}
