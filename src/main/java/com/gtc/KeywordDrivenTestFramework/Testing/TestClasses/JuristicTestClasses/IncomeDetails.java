/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Income details",
        createNewBrowserInstance = false
)
public class IncomeDetails extends BaseClass
{

    String error = "";
    
    public IncomeDetails()
    {
        
    }

    public TestResult executeTest()
    {
        if (!incomeDetails())
        {
            narrator.testFailed("Failed to enter the income details -" + error);
        }
        
        return narrator.finalizeTest("Successfully entered the income details");
    }
    
    public boolean incomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryIncomeDetailsTab()))
        {
            error = "Failed to scroll  the 'Income Details 'tab.";
            return false;
        }
        String sourceOfIncome = getData("Source of Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySourceofIncomeDetails(), sourceOfIncome))
        {
            error = "Failed to select source of income.";
            return false;
        }
        narrator.stepPassed("Source of income : " + sourceOfIncome);
        String grossNet = getData("Gross Net");
        String income = getData("Income");
        String netTakeHome = getData("Net");
        
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryGrossRenumeration()))
        {
            error = "Failed to clear the Gross Remunaration textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryGrossRenumeration(), grossNet))
        {
            error = "Failed to enter Gross Remunaration";
            return false;
        }
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseGrossRenumeration()))
        {
            error = "Failed to clear the Spouse Gross Remunaration textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseGrossRenumeration(), grossNet))
        {
            error = "Failed to enter Spouse Gross Remunaration";
            return false;
        }
        narrator.stepPassed("Gross Remuneration : " + grossNet);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryMonthlyCommission()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryMonthlyCommission(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
           if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseMonthlyCommission()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseMonthlyCommission(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Monthly Commission : " + income);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryCarAllowance()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryCarAllowance(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseCarAllowance()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseCarAllowance(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Car allowance : " + income);
           if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryOverTime()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }  
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryOverTime(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Overtime : " + income);        
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryReimbursement()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
 if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryReimbursement(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }          
        narrator.stepPassed("Reimbursement : " + income);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryNetTakeHome()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        } 
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryNetTakeHome(), netTakeHome))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        } 
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseNetTakeHome()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseNetTakeHome(), netTakeHome))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        narrator.stepPassed("Net Take-home Pay : " + netTakeHome);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryOtherIncome()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        } 
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryOtherIncome(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        } 
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseOtherIncome()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        } 
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseOtherIncome(), income))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }        
        
        narrator.stepPassed("Other Income : " + income);
        
        return true;
    }
}
