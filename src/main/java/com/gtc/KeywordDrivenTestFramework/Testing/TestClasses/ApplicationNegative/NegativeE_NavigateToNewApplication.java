/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Navigate New_Application",
        createNewBrowserInstance = false
)
public class NegativeE_NavigateToNewApplication extends BaseClass
{

    String error = "";

    public NegativeE_NavigateToNewApplication()
    {
    }

    public TestResult executeTest()
    {
        if (!NavigateToNewApplication())
        {
            narrator.testFailed("Failed to navigate to New Application -" + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean NavigateToNewApplication()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.applicationDropDown()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.applicationDropDown(), 120))
            {
                error = "Failed to wait for Application Drop Down";
                return false;
            }

        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(LightStonePageObjects.applicationDropDown()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'Application' Menu");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.newApplication(), 120))
        {
            error = "Failed to wait for 'new application' from the list of applications.";
            return false;
        }
        if (!nestClicks(LightStonePageObjects.newApplication(), LightStonePageObjects.applicationFormTab()))
        {
            error = "Failed to click 'new application' from the list of applications.";
            return false;
        }
        pause(2000);
        //Test
        narrator.stepPassed("Successfully clicked on the 'New Application' from the list");

        return true;
    }

    public boolean nestClicks(String xpath, String searchxPath)
    {
        Search:
        for (int count = 0; count < 4; count++)
        {
            switch (count)
            {
                case 0:
                    if (!SeleniumDriverInstance.clickElementbyXpath(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 5))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;

                    } else
                    {
                        break Search;
                    }
                case 1:
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 10))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                case 2:
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 15))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                case 3:
                    if (!SeleniumDriverInstance.roboClickLatestElementbyXpath(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 120))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                default:
                    return false;
            }
        }
        return true;
    }
}
