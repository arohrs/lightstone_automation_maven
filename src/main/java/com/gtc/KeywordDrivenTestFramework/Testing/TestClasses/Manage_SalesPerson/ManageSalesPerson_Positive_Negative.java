/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Manage_SalesPerson;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Salesperson Positive and Negative Tests",
        createNewBrowserInstance = false
)

public class ManageSalesPerson_Positive_Negative extends BaseClass
{

    String error = "";
    String manageSalesPerson = testData.getData("Manage Salesperson");
    String dataToEnter = testData.getData("DataToEnter");
    String field = testData.getData("Field");
    String validateColor = testData.getData("ValidateColor");
    String xpathFieldToEnter = testData.getData("XpathFieldToEnter");
    String xpathToValidate = testData.getData("XpathToValidate");
    String message = testData.getData("Message");

    boolean isTestPositive;
    boolean isLastTest = (!testData.getData("isLastTest").isEmpty()) ? Boolean.parseBoolean(testData.getData("isLastTest").toLowerCase()) : false;

    public ManageSalesPerson_Positive_Negative()
    {
    }

    public TestResult executeTest()
    {

        isTestPositive = Boolean.parseBoolean(getData("isTestPositive"));
        String testType = isTestPositive ? "Positive" : "Negative";

        switch (field)
        {
            case "idNumber":
                //id specific
                if (!idNumberAndEmailTests())
                {
                    return narrator.testFailed("Failed to perform " + testType + " " + field + " test for the salesperson - " + error);
                }
                break;
            case "email":
                //email specific
                if (!idNumberAndEmailTests())
                {
                    return narrator.testFailed("Failed to perform " + testType + " " + field + " test for the salesperson - " + error);
                }
                break;

            default:
                if (!manageSalespersonTests())
                {
                    return narrator.testFailed("Failed to perform " + testType + " " + field + " test for the salesperson - " + error);
                }
                break;
        }
        return narrator.finalizeTest("Successfully performed " + testType + " " + field + " test.");
    }

    public boolean idNumberAndEmailTests()
    {  
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.closeButtonSalesPerson(), 2))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.closeButtonSalesPerson()))
            {
                error = "Failed to click on CLOSE .";
                return false;
            }
            if (!SeleniumDriverInstance.alertHandler())
            {
                error = "Failed to click OK.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to wait for the ADD NEW button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully landed on the SALESPERSONS page.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to click on ADD NEW button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientDetails()))
        {
            error = "Failed to wait for the ClENT DETAILS bar to appear.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully showing CLIENT DETAILS capturing fields");

        if (!SeleniumDriverInstance.enterTextByXpath(xpathFieldToEnter, dataToEnter))
        {
            error = "Failed to enter " + dataToEnter + " into the text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered :" + dataToEnter);
        testData.extractParameter(field + ":", dataToEnter, "");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.saveSalesPerson()))
        {
            error = "Failed to click on save SALESPERSON.";
            return false;
        }

        if (isTestPositive)
        {
           
            if (!validateUsingColor())
            {
                error = "Failed to validate testpack background color against the attribute.";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully showing all the relevant elements highlighted.");

            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.closeButtonSalesPerson()))
            {
                error = "Failed to click on CLOSE .";
                return false;
            }
            if (!SeleniumDriverInstance.alertHandler())
            {
                error = "Failed to click OK.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.addNewSalesPerson()))
            {
                error = "Failed to wait for the ADD NEW button.";
                return false;
            }

            if (isLastTest)
            {
                SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());
                narrator.stepPassed("Successfully moved back to the boardroom");

            }

            //Cont. positive test after this works.
        } else
        {//Negative Test
            if (!SeleniumDriverInstance.waitForElementByXpath(xpathFieldToEnter, 5))
            {
                error = "Failed to wait for the text field.";
                return false;
            }
            if (!SeleniumDriverInstance.hoverOverElementByXpath(xpathFieldToEnter))
            {
                error = "Failed to hover over the text field";
                return false;
            }
            pause(500);
            narrator.stepPassedWithScreenShot("Successfully verified that the message : " + message + " is present.");
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(xpathToValidate, 5))
            {
                error = "failed to wait for element to be present";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.closeButtonSalesPerson()))
            {
                error = "Failed to click on CLOSE .";
                return false;
            }
            if (!SeleniumDriverInstance.alertHandler())
            {
                error = "Failed to click OK.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.addNewSalesPerson()))
            {
                error = "Failed to wait for the ADD NEW button.";
                return false;
            }
            if (isLastTest)
            {
                SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());
                narrator.stepPassed("Successfully moved back to the boardroom");

            }

        }
        return true;
    }

    public boolean manageSalespersonTests()
    {
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.closeButtonSalesPerson(), 2))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.closeButtonSalesPerson()))
            {
                error = "Failed to click on CLOSE .";
                return false;
            }
            if (!SeleniumDriverInstance.alertHandler())
            {
                error = "Failed to click OK.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to wait for the ADD NEW button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully landed on the SALESPERSONS page.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewSalesPerson()))
        {
            error = "Failed to click on ADD NEW button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.clientDetails()))
        {
            error = "Failed to wait for the ClENT DETAILS bar to appear.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully showing CLIENT DETAILS capturing fields");

        if (!SeleniumDriverInstance.enterTextByXpath(xpathFieldToEnter, dataToEnter))
        {
            error = "Failed to enter " + dataToEnter + " into the text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered " + field + ":" + dataToEnter);
        narrator.stepPassed("Entered " + field + ": " + dataToEnter);
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.saveSalesPerson()))
        {
            error = "Failed to click on save SALESPERSON.";
            return false;
        }

        if (!validateUsingColor())
        {
            error = "Failed to validate that the relevant elements are highlighted.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully showing all the relevant elements highlighted.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.closeButtonSalesPerson()))
        {
            error = "Failed to click on CLOSE .";
            return false;
        }
        if (!SeleniumDriverInstance.alertHandler())
        {
            error = "Failed to handle the alert.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.addNewSalesPerson(), 5))
        {
            error = "Failed to wait for the ADD NEW button.";
            return false;
        }
        if (isLastTest)
        {
            SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());
            narrator.stepPassed("Successfully moved back to the boardroom");

        }

        return true;
    }

    private boolean validateUsingColor()
    {
        try
        {
            List<WebElement> clientDetailElements = SeleniumDriverInstance.Driver.findElements(By.xpath(LightStonePageObjects.ValidateUsingBackGroundColor())); //expecting 7
            Map<String, String> fieldDetailMap = new HashMap<String, String>();

            for (WebElement currentElement : clientDetailElements)
            {
                String name = currentElement.getAttribute("name");
                String attri = currentElement.getAttribute("style");
                fieldDetailMap.put(name, attri);
            }

            //removing items we don't want to validate
            fieldDetailMap.remove("memberIdentificationtype"); //colour is always white
            fieldDetailMap.remove(field);//whatever we are currently entering (not red)

            Set<String> keys = fieldDetailMap.keySet();
            int numberOfHighlightedElements = 0;
            for (String currentKey : keys)
            {
                String currentAttribute = fieldDetailMap.get(currentKey);
                if (!currentAttribute.contains(validateColor))
                {
                    error = "Failed to Validate colour of field: " + currentKey + " with value: " + currentAttribute;
                    return false;
                }
                numberOfHighlightedElements++;
            }
            narrator.stepPassed("Expected number of highlighted elements : 5");
            narrator.stepPassedWithScreenShot("Number of highlighted elements : " + numberOfHighlightedElements);
            return true;
        } catch (Exception e)
        {
            error = "Caught an exception while trying to validate color fields.";
            return false;
        }

    }
}
