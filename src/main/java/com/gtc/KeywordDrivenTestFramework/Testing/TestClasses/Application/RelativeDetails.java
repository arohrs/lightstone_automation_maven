/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Relative Details",
        createNewBrowserInstance = false
)

public class RelativeDetails extends BaseClass
{

    String error = "";

    public RelativeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!relativeDetails())
        {
            return narrator.testFailed("Failed to enter customers 'relative details' on the Form ." + error);
        }
        return narrator.finalizeTest("Successfully entered customers 'relative details' on the Form.");
    }

    public boolean relativeDetails()
    {
        /*Entering customers relative details*/
        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.relativeDetailsTab()))
        {
            error = "Failed to scroll to 'relative details ' container .";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.relativeFirstname()))
        {
            error = "Failed to wait for the 'relative firstname' text field to appear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeFirstname(), getData("Relative Firstname")))
        {
            error = "Failed to enter 'firstname' into the text field.";
            return false;
        }
        narrator.stepPassed("Firstname:" + " " + getData("Relative Firstname"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeSurname(), getData("Relative Surname")))
        {
            error = "Failed to enter 'surname' into the text field.";
            return false;
        }
        narrator.stepPassed("Surname:" + " " + getData("Relative Surname"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.relationDropDown(), getData("Relation")))
        {
            error = "Failed to select 'relation' from the list.";
            return false;
        }
        narrator.stepPassed("Relation:" + " " + getData("Relation"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.preferedContactMethodDropDown(), getData("Contact Method")))
        {
            error = "Failed to select the 'preferred contact method' from the list.";
            return false;
        }
        narrator.stepPassed("Contact Method:" + " " + getData("Contact Method"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeCellNumber(), getData("Cellphone Number")))
        {
            error = "Failed to enter 'cell number' into the text field.";
            return false;
        }
        narrator.stepPassed("Cellphone Number :" + " " + getData("Cellphone Number"));
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeAddress(), getData("Relative Address")))
        {
            error = "Failed to enter 'relative address' into the text field.";
            return false;
        }
        narrator.stepPassed("Relative Address :" + " " + getData("Relative Address"));
        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(LightStonePageObjects.relativeSuburb()))
        {
            error = "Failed to click 'suburb' lookup button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.relativeSuburbSearch()))
        {
            error = "Failed to wait for the search text field.";
            return false;
        }

        String relativeSuburbSearch = getData("Relative Suburb Search");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeSuburbSearch(), relativeSuburbSearch))
        {
            error = "Failed to enter 'suburb' name into the search text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.relativeSearchButton()))
        {
            error = "Failed to click 'suburb' search button.";
            return false;
        }
        String suburb = getData("Relative Suburb");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.relativeSuburbDropDown(), suburb))
        {
            error = "Failed to select 'suburb' from the list.";
            return false;
        }
        narrator.stepPassed("Relative Suburb  :" + " " + getData("Relative Suburb"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.relativePostalCodeDropDown(), getData("Relative Postal Code")))
        {
            error = "Failed to select 'relative postal address' from the list.";
            return false;
        }
        narrator.stepPassed("Relative Postal Code   :" + " " + getData("Relative Postal Code"));

        narrator.stepPassedWithScreenShot("Completed filling in Address Lookup");

        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.relativeOK()))
        {
            error = "Failed to click 'OK' button.";
            return false;
        }
        pause(1000);
        String[] relativeTownOrCity = suburb.split(",");
        String relativeTown = relativeTownOrCity[1];
        narrator.stepPassedWithScreenShot("Succesfully entered Relative Surbub");
      
        String relativeSuburb = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.relative_Suburb(),"value"); 
        String relativeCityTown = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.relativeCityTown(),"value");
        //validate 
        if (!relativeSuburbSearch.toLowerCase().contains(relativeSuburb.toLowerCase()))
        {
            narrator.stepFailed("Failed to validate Relative suburb" + relativeSuburb);

        } else
        {
            narrator.stepPassed("Relative Suburb Validated: " + relativeSuburb);
        }

        if (!relativeTown.toLowerCase().contains(relativeCityTown.toLowerCase()))
        {
            narrator.stepFailed("Failed to validate Relative City / Town "+relativeCityTown);

        } else
        {
            narrator.stepPassed("Relative City / Town Validated: " + relativeCityTown);
        }

        return true;
    }
}
