/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Spouse Income",
        createNewBrowserInstance = false
)
public class SpousalIncome extends BaseClass
{

    String error = "";
    String remunaration = getData("Remuneration");
    String net = getData("Net Take-home");
    String rent = getData("Rent Income");
    String commission = getData("Commission");
    String amount = getData("Amount");

    public SpousalIncome()
    {
    }

    public TestResult executeTest()
    {
        if (!spousalIncome())
        {
            return narrator.testFailed("Failed to enter the spousal income details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the spousal income  details");
    }

    public boolean spousalIncome()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.spousalIncomeTab()))
        {
            error = "Failed to scroll the 'Spousal Income'";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseGrossRenumeration()))
        {
            error = "Failed to enter the 'Gross Remuneration'";
            return false;
        }
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseGrossRenumeration(),remunaration))
        {
            error = "Failed to enter the 'Gross Remuneration'";
            return false;
        }
          narrator.stepPassed("Gross Remuneration : "+remunaration);
           if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spousalMonthlyCommission()))
        {
            error = "Failed to clear the 'Monthly Commission'";
            return false;
        }
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spousalMonthlyCommission(),commission))
        {
            error = "Failed to enter the 'Monthly Commission'";
            return false;
        }
          narrator.stepPassed("Monthly Commission : "+commission);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseCarAllowance()))
        {
            error = "Failed to clear the 'Car allowance'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseCarAllowance(), amount))
        {
            error = "Failed to enter the 'Car allowance'";
            return false;
        }
       narrator.stepPassed("Car allowance : " +amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spouseRentalIncome()))
        {
            error = "Failed to clear the 'Rent Income'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spouseRentalIncome(), rent))
        {
            error = "Failed to enter the 'Rent Income'";
            return false;
        }
        narrator.stepPassed("Rent Income : " + rent);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spousalNetTakeHome()))
        {
            error = "Failed to clear the 'Net Take-home Pay'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spousalNetTakeHome(), net))
        {
            error = "Failed to enter the 'Net Take-home Pay'";
            return false;
        }
        narrator.stepPassed("Net Take-home Pay : " + net);
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.spousalOtherIncome()))
        {
            error = "Failed to clear the 'Other Income'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.spousalOtherIncome(), amount))
        {
            error = "Failed to enter the 'Other Income'";
            return false;
        }
        narrator.stepPassed("Other Income : " + amount);
        SeleniumDriverInstance.pressKey(Keys.TAB);
       
        String netHome = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.spousalNetTakeHome(),"value");      
        String totalIncome = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.spousalTotalMonthlyIncome(),"value");
        double netHomeAmount = Double.parseDouble(netHome);

        double totalAmount = Double.parseDouble(totalIncome);

        if (netHomeAmount != totalAmount)
        {
            error = "Failed to validate the total monthly Income";
            return false;
        }
        narrator.stepPassed("Successfully validated the total monthly income field");
        
        
        return true;
    }

}
