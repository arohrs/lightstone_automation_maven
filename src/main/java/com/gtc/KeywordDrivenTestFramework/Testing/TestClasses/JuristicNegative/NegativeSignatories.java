
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Signatory Negative",
        createNewBrowserInstance = false
)
public class NegativeSignatories extends BaseClass
{

    String error = "";
    String firstName = getData("First Name");
    String surname = getData("Surname");
    String contactType = getData("Contact Type");
    String employerTime = getData("Employer");
    String preferredLanguage = getData("Language");
    String idNumber = getData("ID number");
    String title = getData("Title");
    String gender = getData("Gender");
    String ethnicity = getData("Ethnicity");
    String employmentSector = getData("Employment Sector");
    String maritalStatus = getData("Marital Status");
    String clientType = getData("Client Type");
    String industryType = getData("Industry Type");
    String residenceTime = getData("Residence");
    String birthDay = getData("Birth");
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public TestResult executeTest()
    {
        if (!signatoriesDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean signatoriesDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoriesTab()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryFirstName()))
        {
            error = "Failed to clear Name textfield.";
            return false;
        }
        if (!firstName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryFirstName(), firstName))
            {
                error = "Failed to enter First Name.";
                return false;
            }
        }
        narrator.stepPassed("First name : " + firstName);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySurName()))
        {
            error = "Failed to clear Surname textfield.";
            return false;
        }
        if (!surname.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySurName(), surname))
            {
                error = "Failed to enter Surname.";
                return false;
            }
        }
        narrator.stepPassed("Surname :" + surname);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactType(), contactType))
        {
            error = "Failed to select contact type.";
            return false;
        }
        narrator.stepPassed("Contact Type : " + contactType);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryIdNumber(), idNumber))
        {
            error = "Failed to select ID number.";
            return false;
        }
        narrator.stepPassed("ID Number : " + idNumber);
        SeleniumDriverInstance.pressKey(Keys.TAB);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryPreferredLanguage(), preferredLanguage))
        {
            error = "Failed to select preffered language.";
            return false;
        }
        narrator.stepPassed("Preferred language : " + preferredLanguage);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryGender(), gender))
        {
            error = "Failed to select gender.";
            return false;
        }
        narrator.stepPassed("Gender : " + gender);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactTitle(), title))
        {
            error = "Failed to select title.";
            return false;
        }
        narrator.stepPassed("Contact Title :" + title);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryEthnicity(), ethnicity))
        {
            error = "Failed to select ethnicity.";
            return false;
        }
        narrator.stepPassed("Ethnicity :" + ethnicity);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryEmploymentSector(), employmentSector))
        {
            error = "Failed to select employment sector.";
            return false;
        }
        narrator.stepPassed("Employment sector : " + employmentSector);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryClientType(), clientType))
        {
            error = "Failed to select client type.";
            return false;
        }
        narrator.stepPassed("Client type : " + clientType);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryMaritalStatus(), maritalStatus))
        {
            error = "Failed to select marital status.";
            return false;
        }
        narrator.stepPassed("Marital Status : " + maritalStatus);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryIndustryType(), industryType))
        {
            error = "Failed to select industry type.";
            return false;
        }
        narrator.stepPassed("Industry Type : " + industryType);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryResidenceYear()))
        {
            error = "Failed to clear 'Time at Residence - Year' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryResidenceMonth()))
        {
            error = "Failed to clear 'Time at Residence - Month' textfield.";
            return false;
        }
        if (!residenceTime.isEmpty())
        {

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryResidenceYear(), residenceTime))
            {
                error = "Failed to enter 'Time at Residence - Year'.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryResidenceMonth(), residenceTime))
            {
                error = "Failed to clear 'Time at Residence - Month'.";
                return false;
            }

        }
        narrator.stepPassed("Time at Residence (YY/MM):" + residenceTime + "/" + residenceTime);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryEmployerYear()))
        {
            error = "Failed to clear 'Time at Employer - Year' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryEmployerMonth()))
        {
            error = "Failed to clear 'Time at Employer - Month' textfield.";
            return false;
        }
        if (!employerTime.isEmpty())
        {

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerYear(), employerTime))
            {
                error = "Failed to enter 'Time at Employer - Year'.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerMonth(), employerTime))
            {
                error = "Failed to enter 'Time at Employer - Month'.";
                return false;
            }

        }
        narrator.stepPassed("Time at Employer (YY/MM):" + employerTime + "/" + employerTime);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoriesTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The <b>'" + field + "'</b> field is not highlighted in red .";
                    return false;
                }

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The <b>'" + field + "'</b> field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
