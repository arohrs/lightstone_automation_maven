/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Expense details",
        createNewBrowserInstance = false
)
public class ExpenseDetails extends BaseClass
{

    String error = "";
    String bond = getData("Bond");
    String amount = getData("Payment");
    String payDay = getData("Day");

    public ExpenseDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!expenseDetails())
        {

            narrator.testFailed("Failed to enter the expenses details -" + error);
        }
        if (!otherIncomeDetails())
        {
            narrator.testFailed("Failed to enter ");
        }

        return narrator.finalizeTest("Successfully entered the expenses details");
    }

    public boolean expenseDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryExpenseDetails()))
        {
            error = "Failed to scroll  the 'Expenses Details ' container.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryBondPayment()))
        {
            error = "Failed to clear 'Bond Payment 'textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryBondPayment(), bond))
        {
            error = "Failed to enter the 'Bond Payment'.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorSpouseBondPayment()))
        {
            error =  "Failed to clear 'Spouse Bond Payment 'textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorSpouseBondPayment(), bond))
        {
            error = "Failed to enter the 'Spouse Bond Payment'.";
            return false;
        }
        narrator.stepPassed("Bond Payment : " + bond);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryRent()))
        {
            error = "Failed to clear 'Rent' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryRent(), bond))
        {
            error = "Failed to enter the 'Rent'.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseRent()))
        {
            error = "Failed to clear 'Spouse Rent' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseRent(), bond))
        {
            error = "Failed to enter the 'Spouse Rent'.";
            return false;
        }
        narrator.stepPassed("Rent : " + bond);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryRatesAndWater()))
        {
            error = "Failed to clear 'Spouse Rates and Water' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryRatesAndWater(), amount))
        {
            error = "Failed to enter the 'Spouse Rates and Water'.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseRatesAndWater()))
        {
            error = "Failed to clear 'Rates and Water' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseRatesAndWater(), amount))
        {
            error = "Failed to enter the 'Rates and Water'.";
            return false;
        }
        narrator.stepPassed("Rates, Water & Electricity : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryVehicleInstalment()))
        {
            error = "Failed to clear 'Rates and Water' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryVehicleInstalment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseVehicleInstalment()))
        {
            error = "Failed to clear 'Rates and Water' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseVehicleInstalment(), amount))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        narrator.stepPassed("Vehicle Instalments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryPersonalLoan()))
        {
            error = "Failed to clear 'Personal Loan' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryPersonalLoan(), amount))
        {
            error = "Failed to enter 'Personal Loan'.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpousePersonalLoan()))
        {
            error = "Failed to clear 'Spouse Personal Loan' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpousePersonalLoan(), amount))
        {
            error = "Failed to enter 'Spouse Personal Loan'.";
            return false;
        }
        narrator.stepPassed("Personal Loan Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryCreditCardRepayment()))
        {
            error = "Failed to clear 'Credit Card Repayment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryCreditCardRepayment(), amount))
        {
            error = "Failed to enter 'Credit Card Repayment' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseCreditCardRepayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseCreditCardRepayment(), amount))
        {
            error = "Failed to clear 'Spouese Credit Card Repayment' textfield.";
            return false;
        }
        narrator.stepPassed("Credit Card Repayments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryFurnitureAccount()))
        {
            error = "Failed to clear 'Furniture Account' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryFurnitureAccount(), amount))
        {
            error = "Failed to enter 'Furniture Account' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseFurnitureAccount()))
        {
            error = "Failed to clear 'Spouse Furniture Account' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseFurnitureAccount(), amount))
        {
            error = "Failed to enter 'Spouse Furniture Account' amount.";
            return false;
        }
        narrator.stepPassed("Furniture Accounts : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryClothingAccount()))
        {
            error = "Failed to clear 'Clothing Account' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryClothingAccount(), amount))
        {
            error = "Failed to enter 'Clothing Account' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseClothingAccount()))
        {
            error = "Failed to clear 'Spouse Clothing Account' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseClothingAccount(), amount))
        {
            error = "Failed to enter 'Spouse Clothing Account' amount.";
            return false;
        }
        narrator.stepPassed("Clothing Accounts : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryOverdraftRepayment()))
        {
            error = "Failed to clear 'OverDraft repayment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryOverdraftRepayment(), amount))
        {
            error = "Failed to enter 'OverDraft repayment' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseOverdraftRepayment()))
        {
            error = "Failed to clear 'Spouse OverDraft repayment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseOverdraftRepayment(), amount))
        {
            error = "Failed to enter 'Spouse OverDraft repayment' amount.";
            return false;
        }
        narrator.stepPassed("Overdraft Repayments : " + amount);
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryInsurancePayment()))
        {
            error = "Failed to scroll  the 'geograpical 'tab.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Filled in details so far");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryInsurancePayment()))
        {
            error = "Failed to clear 'Insurance Payment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryInsurancePayment(), amount))
        {
            error = "Failed to enter 'Insurance Payment' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseInsurancePayment()))
        {
            error = "Failed to clear 'Spouse Insurance Payment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseInsurancePayment(), amount))
        {
            error = "Failed to enter 'Spouse Insurance Payment' amount.";
            return false;
        }
        narrator.stepPassed("Policy/Insurance Payments : " + amount);

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryTelephonePayment()))
        {
            error = "Failed to clear 'Telephone Payment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryTelephonePayment(), amount))
        {
            error = "Failed to enter 'Telephone Payment' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseTelephonePayment()))
        {
            error = "Failed to clear 'Spouse Telephone Payment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseTelephonePayment(), amount))
        {
            error = "Failed to enter 'Spouse Telephone Payment' amount.";
            return false;
        }
        narrator.stepPassed("Telephone Payments : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryTransportCost()))
        {
            error = "Failed to clear 'Transport Cost' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryTransportCost(), amount))
        {
            error = "Failed to enter 'Transport Costt' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseTransportCost()))
        {
            error = "Failed to clear 'Spouse Transport Cost' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseTransportCost(), amount))
        {
            error = "Failed to enter 'Spouse Transport Costt' amount.";
            return false;
        }
        narrator.stepPassed("Transport Costs : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryFoodEntertainment()))
        {
            error = "Failed to clear 'Food Entertainment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryFoodEntertainment(), amount))
        {
            error = "Failed to enter 'Food Entertainment' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseFoodEntertainment()))
        {
            error = "Failed to clear 'Spouse Food Entertainment' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseFoodEntertainment(), amount))
        {
            error = "Failed to enter 'Spouse Food Entertainment' amount.";
            return false;
        }
        narrator.stepPassed("Food and Entertainment : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryEducationCost()))
        {
            error = "Failed to clear 'Education Cost' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEducationCost(), amount))
        {
            error ="Failed to enter 'Education Cost' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseEducationCost()))
        {
            error = "Failed to clear 'Spouse Education Cost' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseEducationCost(), amount))
        {
            error = "Failed to enter 'Spouse Education Cost' amount.";
            return false;
        }
        narrator.stepPassed("Education Costs : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryMaintenance()))
        {
            error = "Failed to clear 'Maintenence' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryMaintenance(), amount))
        {
            error = "Failed to enter 'Maintenence' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseMaintenance()))
        {
            error = "Failed to clear 'Spouse Maintenence' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseMaintenance(), amount))
        {
            error = "Failed to enter 'Spouse Maintenence' amount.";
            return false;
        }
        narrator.stepPassed("Maintenance : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryHouseholdExpenses()))
        {
            error = "Failed to clear 'Household expenses' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryHouseholdExpenses(), amount))
        {
            error = "Failed to enter 'HouseHold expenses' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseHouseholdExpenses()))
        {
            error ="Failed to clear 'Spouse Household expenses' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseHouseholdExpenses(), amount))
        {
            error = "Failed to enter 'Spouse HouseHold expenses' amount.";
            return false;
        }
        narrator.stepPassed("Household Expenses : " + amount);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryOtherExpenses()))
        {
            error = "Failed to clear 'Other expenses' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryOtherExpenses(), amount))
        {
            error = "Failed to enter 'Other expenses' amount.";
            return false;
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatorySpouseOtherExpenses()))
        {
            error = "Failed to clear 'Spouse Other expenses' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySpouseOtherExpenses(), amount))
        {
            error = "Failed to enter 'Spouse Other expenses' amount.";
            return false;
        }
        narrator.stepPassed("Other Expenses : " + amount);
        narrator.stepPassedWithScreenShot("Successfully enter all the expenses details");

        return true;
    }

    public boolean otherIncomeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryOtherIncomeTab()))
        {
            error = "Failed to scroll  the 'Other Income' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorypayDay(), payDay))
        {
            error = "Failed to select Pay Day";
            return false;
        }
        narrator.stepPassed("Pay Day : " + payDay);
        narrator.stepPassedWithScreenShot("Successfully entered the other income details ");
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryExpenseDetails()))
        {
            error = "Failed to scroll  the 'Expense Details' tab.";
            return false;
        }
        return true;
    }
}
