/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Finance Calculator",
        createNewBrowserInstance = false
        
)
public class FinanceCalculator extends BaseClass
{

    String error = "";

    public FinanceCalculator()
    {

    }

    public TestResult executeTest()
    {
        if (!financeCalculatorDetails())
        {

            return narrator.testFailed("Failed to enter the finance calculator's details " + error);
        }
        return narrator.finalizeTest("Successfully entered finance calculator's details in the Form.");
    }

    public boolean financeCalculatorDetails()
    {
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
          if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financeCalculatorForm()))
        {
            error = "Failed to scroll to the 'Finance Calculator details'.";
            return false;
        }
          
        Date date = new Date();
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd");
        String instalmentDate = dateTimeFormat.format(date);
        

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.commencementDate(), instalmentDate))
        {
            error = "Failed to enter the 'commencement date' in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.instalmentDate()))
        {
            error = "Failed to click the 'Instalment date' in the text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.instalmentDate(), instalmentDate))
        {
            error = "Failed to enter the 'Instalment date'in the text field.";
            return false;
        }
 
        return true;
    }
}
