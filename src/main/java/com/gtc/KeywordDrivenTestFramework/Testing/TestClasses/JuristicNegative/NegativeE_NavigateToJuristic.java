/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Navigate To Juristic",
        createNewBrowserInstance = false
)
public class NegativeE_NavigateToJuristic extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeE_NavigateToJuristic()
    {
    }

    public TestResult executeTest()
    {
        if (!navigateToJuristic())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean navigateToJuristic()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.applicationDropDown()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.applicationDropDown(), 10))
            {
                error = "Failed to wait for Application Drop Down";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(LightStonePageObjects.applicationDropDown()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.juristicApplication(), 8))
        {
            error = "Failed to wait for  the ' Juristic application' button.";
            return false;
        }

        if (!nestedClicks(LightStonePageObjects.juristicApplication(), LightStonePageObjects.companyName()))
        {
            error = "Failed to click the ' Juristic application' button.";
            return false;
        }
        pause(1000);
//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch to new browser window.";
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'Juristic Application' from the list");

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyName()))
        {
            error = "Failed to wait for Company Details.";
            return false;

        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Juristic Application'.");

        return true;
    }

    public boolean nestedClicks(String xpath, String searchxPath)
    {
        Search:
        for (int count = 0; count < 4; count++)
        {
            switch (count)
            {
                case 0:
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(xpath))
                    {
                        break;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 5))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;

                    } else
                    {
                        break Search;
                    }
                case 1:
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(xpath))
                    {
                        break;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 10))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                case 2:
                    if (!SeleniumDriverInstance.clickElementbyXpath(xpath))
                    {
                        break;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 15))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                case 3:
                    if (!SeleniumDriverInstance.roboClickLatestElementbyXpath(xpath))
                    {
                       break;
                    }
                    if (!SeleniumDriverInstance.switchToTabOrWindow())
                    {
                        error = "Failed to switch to new browser window.";
                        return false;
                    }
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 120))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                default:
                    return false;
            }
        }
        return true;
    }

}
