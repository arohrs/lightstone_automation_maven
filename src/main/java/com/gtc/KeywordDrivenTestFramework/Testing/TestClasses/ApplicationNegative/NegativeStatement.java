/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Statement Negative",
        createNewBrowserInstance = false
)
public class NegativeStatement extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeStatement()
    {
    }

    public TestResult executeTest()
    {
        if (!statementDetails())
        {
            return narrator.testFailed("Failed to perform the negative test -"+error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean statementDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.statementTab()))
        {
            error = "Failed to scroll to the 'Statement' container.";
            return false;
        }
        
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.statementDeliveryMethod(),getData("Statement Method")))
        {
            error = "Failed to select 'Electronic statement 'from the list";

        }
        narrator.stepPassed("Statement Delivery Method: " +getData("Statement Method"));

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.preferredEmailAddress()))
        {
            error = "Failed to clear the 'preferred email'detail in the text field";

        }
        String firstEmail = getData("Email");
        if (!firstEmail.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.preferredEmailAddress(), firstEmail))
            {
                error = "Failed to enter the '" + firstEmail + "'detail in the text field";

            }
        }
        narrator.stepPassed("Preferred Email: " + firstEmail);

        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.statementTab()))
            {
                error = "Failed to scroll to the 'Statement' container ";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                 if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

               
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                 if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
               

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
