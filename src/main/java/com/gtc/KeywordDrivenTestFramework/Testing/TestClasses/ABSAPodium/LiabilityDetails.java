/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Liability Details",
        createNewBrowserInstance = false
)
public class LiabilityDetails extends BaseClass
{

    String error = "";
    String option = getData("Option");

    public LiabilityDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!liabilityDetails())
        {
            return narrator.testFailed("Failed to enter the liabilities details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the liabilities details");
    }

    public boolean liabilityDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.liabilittDetailsTab()))
        {
            error = "Failed to scroll the 'Liability Details'";
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.surety(),option))
        {
            error = "Failed to select the 'Surety'";
            return false;
        }
        narrator.stepPassed("Surety : " +option);
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.guarantor(),option))
        {
            error = "Failed to select the 'Guarantor'";
            return false;
        }
        narrator.stepPassed("Guarantor : "+option);
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.coDebtor(),option))
        {
            error = "Failed to select the 'Co-Debtor'";
            return false;
        }
        narrator.stepPassed("Co-Debtor : "+option);


        return true;
    }

}
