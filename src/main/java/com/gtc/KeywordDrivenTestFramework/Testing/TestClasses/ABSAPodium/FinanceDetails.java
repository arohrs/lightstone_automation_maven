/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Finance Details",
        createNewBrowserInstance = false
)
public class FinanceDetails extends BaseClass
{

    String error = "";
    String agreement = getData("Agreement");
    String repayment = getData("Repayment period");
    String interest = getData("Interest rate");
    String rateIndicator = getData("Rate Indicator");
    String residualBallon = getData("Residual Value");
    String financeInitiation = getData("Finance Initiation Fees");
    String deposit = getData("Deposit");
    String fordScheme = getData("Ford Scheme");
    String packageIndicator = getData("Package Indicator");
    String packageNumber = getData("Package Number");

    public FinanceDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!vehicleDetails())
        {
            return narrator.testFailed("Failed to enter the finance's details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the finance's details");
    }

    public boolean vehicleDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.financeDetailsContainer()))
        {
            error = "Failed to scroll the 'Finance Details'";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.agreement(), agreement))
        {
            error = "Failed to select the 'Agreement'";
            return false;
        }
        narrator.stepPassed("Agreement : " + agreement);
        if (!SeleniumDriverInstance.dismissAlert())
        {
            error = "Failed to wait for the 'Podium Details'";
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.repaymentPeriod(), repayment))
        {

            error = "Failed to select the 'Repayment Period '";
            return false;
        }
        if (!SeleniumDriverInstance.dismissAlert())
        {
            error = "Failed to wait for the 'Podium Details'";

        }
        narrator.stepPassed("Repayment Period : " + repayment);
        SeleniumDriverInstance.pressKey(Keys.TAB);

        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.financeInterestRatePodium()))
        {
            error = "Failed to clear the 'Interest Rate'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.financeInterestRatePodium(), interest))
        {
            error = "Failed to enter the 'Interest Rate'";
            return false;
        }
        narrator.stepPassed("Interest Rate: " + interest);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.rateIndicatorDropDown(), rateIndicator))
        {
            error = "Failed to select the 'Rate Indicator'";
            return false;
        }
        narrator.stepPassed("Rate Indicator: " + rateIndicator);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.residualBalloonValue()))
        {
            error = "Failed to clear the 'Residual/Balloon Amount'";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.residualBalloonValue(), residualBallon))
        {
            error = "Failed to enter the 'Residual/Balloon Amount'";
            return false;
        }
        if (!SeleniumDriverInstance.alertHandler())
        {
            error = "Failed to handle alert.";

        }
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }

        narrator.stepPassed("Residual/Balloon Amount : " + residualBallon);
        String residualPercentage = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.residualPercentage(), "value");

        narrator.stepPassed("Residual/Balloon % : " + residualPercentage);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.financeInitiationFees(), financeInitiation))
        {
            error = "Failed to select the 'Finance Initiation Fees'";
            return false;
        }
        if (!SeleniumDriverInstance.alertHandler())
        {
            error = "Failed to handle alert.";

        }
        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }

        narrator.stepPassed("Finance Initiation Fees : " + financeInitiation);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.carDeposit()))
        {
            error = "Failed to clear the 'Deposit'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.carDeposit(), deposit))
        {
            error = "Failed to enter the 'Deposit'";
            return false;
        }
        narrator.stepPassed("Deposit : " + deposit);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fordSchemeDeal(), fordScheme))
        {
            error = "Failed to select the 'Ford Scheme Deal'";
            return false;
        }
        SeleniumDriverInstance.pressKey(Keys.TAB);

        if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }
        narrator.stepPassed("Ford Scheme Deal : " + fordScheme);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.packageIndicator(), packageIndicator))
        {
            error = "Failed to select the 'Package Indicator'";
            return false;
        }
        narrator.stepPassed("Package Indicator : " + packageIndicator);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.packageNumber(), packageNumber))
        {
            error = "Failed to enter the 'Package Number'";
            return false;
        }
        narrator.stepPassed("Package Number : " + packageNumber);

        return true;
    }

}
