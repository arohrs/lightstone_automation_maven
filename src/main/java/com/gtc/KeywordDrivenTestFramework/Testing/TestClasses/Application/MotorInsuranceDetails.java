/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Motor Insurance",
        createNewBrowserInstance = false
)
public class MotorInsuranceDetails extends BaseClass
{

    String error = "";

    public MotorInsuranceDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!motorInsurance())
        {
            return narrator.testFailed("Failed to enter the Motor Insurance details. - " + error);
        }

        return narrator.finalizeTest("Successfully updated the Motor Insurance details detail.");
    }

    public boolean motorInsurance()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.motorInsuranceDetailsheader()))
        {
            error = "Failed to Scroll to 'Motor Insuranc details' Header";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.carRadio()))
        {
            error = "Failed to wait for 'car radio' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.carRadio(), getData("Radio")))
        {
            error = "Failed to enter the 'car radio' detail";
            return false;

        }
        narrator.stepPassed("Car Radio : " + getData("Radio"));
       
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.claimFreeYear()))
        {
            error = "Failed to wait for 'claim free year' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.claimFreeYear(), getData("Claim")))
        {
            error = "Failed to enter the 'claim free year' detail";
            return false;

        }
        narrator.stepPassed("Claim free year : " + getData("Claim"));
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.motorAlarm()))
        {
            error = "Failed to wait for 'Alarm' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.motorAlarm(), getData("Alarm")))
        {
            error = "Failed to enter the 'Alarm' detail";
            return false;

        }
        narrator.stepPassed("Alarm : " + getData("Alarm"));
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.gearLock()))
        {
            error = "Failed to wait for 'gearLock' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.gearLock(), getData("Gearlock")))
        {
            error = "Failed to enter the 'gearLock' detail";
            return false;

        }
        narrator.stepPassed("Gearlock : " + getData("Gearlock"));
          if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.yearsInsured()))
        {
            error = "Failed to wait for 'Years Insured' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.yearsInsured(), getData("Years Insured")))
        {
            error = "Failed to enter the 'Years Insured' detail";
            return false;

        }
        narrator.stepPassed("Years Insured : " + getData("Years Insured"));
          if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.typeOfCover()))
        {
            error = "Failed to wait for 'Type of cover' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeOfCover(), getData("Type of cover")))
        {
            error = "Failed to enter the 'type of cover' detail";
            return false;

        }
        narrator.stepPassed("Type of cover : " + getData("Type of cover"));
          if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.overnightParking()))
        {
            error = "Failed to wait for 'overnight Parking' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.overnightParking(), getData("Overnight")))
        {
            error = "Failed to enter the 'overnight' detail";
            return false;

        }
        narrator.stepPassed("Overnight Parking : " + getData("Overnight"));
           if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.immobiliser()))
        {
            error = "Failed to wait for 'immobiliser' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.immobiliser(), getData("Immobiliser")))
        {
            error = "Failed to enter the 'immobiliser' detail";
            return false;

        }
        narrator.stepPassed("Immobiliser : " + getData("Immobiliser"));
           if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.motorTrackingDevice()))
        {
            error = "Failed to wait for 'Tracking device' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.motorTrackingDevice(), getData("Tracking device")))
        {
            error = "Failed to enter the 'Tracking device' detail";
            return false;

        }
        narrator.stepPassed("Tracking device : " + getData("Tracking device"));
           if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.sendQuote()))
        {
            error = "Failed to wait for 'Send Quote to' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.sendQuote(), getData("Send quote")))
        {
            error = "Failed to enter the 'Send Quote to' detail";
            return false;

        }
        narrator.stepPassed("Send Quote to : " + getData("Send quote"));
           if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.driverLicenseCode()))
        {
            error = "Failed to wait for the 'driver license code' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.driverLicenseCode(), getData("License code")))
        {
            error = "Failed to enter the 'driver license code' detail";
            return false;

        }
        narrator.stepPassed("Driver License code : " + getData("License code"));
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.driverDetails()))
        {
            error = "Failed to wait for the 'driver license code' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.driverDetails(), getData("Driver details")))
        {
            error = "Failed to enter the 'driver license code' detail";
            return false;

        }
        narrator.stepPassed("Driver details : " + getData("Driver details"));
             if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.licenseDate()))
        {
            error = "Failed to wait for the 'license date' to appear";
            return false;

        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.licenseDate(), getData("License date")))
        {
            error = "Failed to enter the 'license date' detail";
            return false;

        }
        narrator.stepPassed("License date : " + getData("License date"));

        
        return true;
    }

}
