/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses.GeographicalDetails;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic PhysicalAddress Negative",
        createNewBrowserInstance = false
)
public class NegativePhysicalAddress extends BaseClass
{

    GeographicalDetails address = new GeographicalDetails();
    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public TestResult executeTest()
    {
        if (!physicalDetails())
        {
            narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean physicalDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryAddressLine1()))
        {
            error = "Failed to scroll to the 'Address Line1' textfield.";
            return false;
        }
        String addressLine = getData("Address");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryAddressLine1()))
        {
            error = "Failed to clear the 'Address Line1' textfield.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAddressLine1(), addressLine))
        {
            error = "Failed to enter 'Address Line1'.";
            return false;
        }
        narrator.stepPassed("<h5> Physical Address </h5>");
        narrator.stepPassed("Address Line 1 : " + addressLine);

        if (!addressLine.isEmpty())
        {
            if (!getData("Select Suburb").isEmpty())
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.signatoryAddressLookup()))
                {
                    error = "Failed to click 'Select Suburb' button.";
                    return false;
                }
            }
            address.addressLookup();
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.signatoryPopulateAddressButton()))
            {
                error = "Failed to click the 'Populate with Physical Address' button.";
                return false;
            }
            narrator.stepPassed("<h5> Postal Address </h5>");
            narrator.stepPassed("Postal Address Line 1 : " + addressLine);
            narrator.stepPassed("Suburb:" + " " + getData("Select Suburb"));
            narrator.stepPassed("Code :" + " " + getData("Select Code"));

        }

        String cellPhone = getData("Cell number");

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryCellPhone()))
        {
            error = "Failed to clear the 'Cell Phone' textfield.";
            return false;
        }
        if (!cellPhone.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryCellPhone(), cellPhone))
            {
                error = "Failed to enter Cell Phone number.";
                return false;
            }
        }
        narrator.stepPassed("Cell Phone : " + cellPhone);
        String email = getData("Email");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryEmailAddress()))
        {
            error = "Failed to clear the 'Email Address' textfield.";
            return false;
        }
        if (!email.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmailAddress(), email))
            {
                error = "Failed to enter  'Email Address'.";
                return false;
            }
        }
        narrator.stepPassed("Email : " + email);
        String ownership = getData("Type of ownership");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryTypeOfOwnership(), ownership))
        {
            error = "Failed to select type of Ownership.";
            return false;
        }
        narrator.stepPassed("Type of Ownership : " + ownership);

        String bank = getData("Bank Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryBankName(), bank))
        {
            error = "Failed to select Bank Name.";
            return false;
        }
        narrator.stepPassed("Bank Name : " + bank);
        String accType = getData("Acc Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryAccountType(), accType))
        {
            error = "Failed to select account type";
            return false;
        }
        narrator.stepPassed("Account Type : " + accType);

        String accNumber = getData("Acc Number");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryAccNumber()))
        {
            error = "Failed to clear 'Account Number' textfield.";
            return false;
        }
        if (!accNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryAccNumber(), accNumber))
            {
                error = "Failed to enter 'Account Number'.";
                return false;
            }
        }
        narrator.stepPassed("Account Number : " + accNumber);
        String branchCode = getData("Branch Code");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryBranchCode()))
        {
            error = "Failed to clear 'Branch Code' textfield.";
            return false;
        }
        if (!branchCode.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryBranchCode(), branchCode))
            {
                error = "Failed to enter 'Branch Code'.";
                return false;
            }
        }
        narrator.stepPassed("Branch Code : " + branchCode);
        String occupation = getData("Occupation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryOccupation(), occupation))
        {
            error = "Failed to select Occupation.";
            return false;
        }
        narrator.stepPassed("Occupation : " + occupation);
        String employerName = getData("Employer Name");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.signatoryEmployerName()))
        {
            error = "Failed to clear the 'Employer Name' textfield.";
            return false;
        }
        if (!employerName.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatoryEmployerName(), employerName))
            {
                error = "Failed to enter 'Employer Name'.";
                return false;
            }
        }
        narrator.stepPassed("Employer Name : " + employerName);

        String sourceOfIncome = getData("Source of Income");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatorySourceofIncome(), sourceOfIncome))
        {
            error = "Failed to select source of income.";
            return false;
        }
        narrator.stepPassed("Source of Income : " + sourceOfIncome);

        String percentage = getData("Share Percentage");
//        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySharePercentage(), percentage))
//        {
//            error = "Failed to enter share percentage.";
//            return false;
//        }
        if (!percentage.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.signatorySharePercentage(), percentage))
            {
                error = "Failed to enter share percentage.";
                return false;
            }
        }
        narrator.stepPassed("Share Percentage : " + percentage);

        String designation = getData("Designation");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.signatoryContactDesignation(), designation))
        {
            error = "Failed to select contact designation.";
            return false;
        }
        narrator.stepPassed("Contact Designation : " + designation);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplicationButton()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.signatoryResidenceYear()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }
   
}
