/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Credit Provider Details",
        createNewBrowserInstance = false
)
public class CreditProviderDetails extends BaseClass
{

    String error = "";
    String option = getData("Option");
    String bankName = getData("Bank Name");
    String account1 = getData("Account Number1");
    String account2 = getData("Account Number2");
    String instalment = getData("Instalment");
    String balance = getData("Balance");
    

    public CreditProviderDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!totalIncomeExpenses())
        {
            return narrator.testFailed("Failed to entere the credit provider details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the credit provider details");
    }

    public boolean totalIncomeExpenses()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.creditProviderTab()))
        {
            error = "Failed to scroll to the 'Credit Provider Details' container";
            return false;
        }
        narrator.stepInfo("<h5> Account to Settle 1 </h5> ");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.settleAccount1(),option))
        {
            error = "Failed to select the 'Settle Existing Instalment 1' option";
            return false;
        }
        narrator.stepPassed("Settle Existing Instalment 1 : "+option);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.settleBankName1(),bankName))
        {
            error = "Failed to select  the 'Bank Name' option";
            return false;
        }
        narrator.stepPassed("Bank Name : "+bankName);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.settleAcountNumber1(),account1))
        {
            error = "Failed to enter the 'Account Number'";
            return false;
        }
        narrator.stepPassed("Account Number : "+account1);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.settleMonthlyInstalment1()))
        {
            error = "Failed to clear the 'Monthly Installment'";
            return false;
        }
        
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.settleMonthlyInstalment1(),instalment))
        {
            error = "Failed to enter the 'Monthly Installment'";
            return false;
        }
        narrator.stepPassed("Monthly Instalment : "+instalment);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.settleOutstandingBalance1()))
        {
            error = "Failed to clear the 'Outstanding balance'";
            return false;
        }
        
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.settleOutstandingBalance1(),balance))
        {
            error = "Failed to enter the 'Outstanding balance'";
            return false;
        }
        narrator.stepPassed("Outstanding Balance : "+balance);
        narrator.stepInfo("<h5> Account to Settle 2 </h5> ");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.settleAccount2(),option))
        {
            error = "Failed to select the 'Settle Existing Instalment 2 ' option";
            return false;
        }
        narrator.stepPassed("Settle Existing Instalment 2 : "+option);

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.settleBankName2(),bankName))
        {
            error = "Failed to select the 'Bank Name' option";
            return false;
        }
        narrator.stepPassed("Bank Name : "+bankName);

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.settleAcountNumber2(),account2))
        {
            error = "Failed to enter the 'Account Number'";
            return false;
        }
        narrator.stepPassed("Account Number : "+account2);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.settleMonthlyInstalment2()))
        {
            error = "Failed to clear the 'Monthly Instalment'";
            return false;
        }
        
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.settleMonthlyInstalment2(),instalment))
        {
            error = "Failed to enter the 'Monthly Instalment'";
            return false;
        }
        narrator.stepPassed("Monthly Instalment : "+instalment);
          if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.settleOutstandingBalance2()))
        {
            error = "Failed to clear the 'Outstanding Balance'";
            return false;
        }
        
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.settleOutstandingBalance2(),balance))
        {
            error = "Failed to enter the 'Outstanding Balance'";
            return false;
        }
        narrator.stepPassed("Outstanding Balance 2 : "+balance);

        return true;
    }

}
