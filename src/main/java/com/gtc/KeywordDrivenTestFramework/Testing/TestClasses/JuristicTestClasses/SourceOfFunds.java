/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Source Of Funds",
        createNewBrowserInstance = false
)
public class SourceOfFunds extends BaseClass
{
    String error="";
    String commission = getData("Commission");
    public SourceOfFunds()
    {
    }
    
    public TestResult executeTest()
    {
        if(!sourceOfFunds())
        {
        
            narrator.testFailed("Failed to select the source of funds -"+error);
        }
    
    
        return narrator.finalizeTest("Successfully selected the source of funds");
    }
    
    public boolean sourceOfFunds()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.sourceOfFundsTab()))
        {
            error = "Failed to scroll to the 'Source of funds' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.developmentFunding()))
        {
            error = "Failed to click Development Funding.";
            return false;
        }
        narrator.stepPassed("Ticked 'Development Funding'");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.commissionCheckBox()))
        {
            error = "Failed to click the Commission checkbox.";
            return false;
        }
         narrator.stepPassed("Ticked 'Commission'");
         if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.commissionAmount()))
        {
            error = "Failed to clear commission amount.";
            return false;
        }
          if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.commissionAmount(), commission))
        {
            error = "Failed to enter commission amount";
            return false;
        }
        narrator.stepPassed("Commission : "+commission);
    
        return true;
    }
    
}
