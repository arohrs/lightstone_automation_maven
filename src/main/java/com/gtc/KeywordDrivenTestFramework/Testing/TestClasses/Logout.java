/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author nkelechi
 * @Internal Review sbachan 2018-09-14
 */
@KeywordAnnotation(
        Keyword = "Logout",
        createNewBrowserInstance = false
         
)

public class Logout extends BaseClass
{

    String error = "";

    public Logout()
    {
    }

    public TestResult executeTest()
    {
        if (!logout())
        {
            return narrator.testFailed("Failed to log out - " + error);
        }
        return narrator.finalizeTest("Successfully Logged out of SIGNIO.");
    }

    public boolean logout()
    {
        // Waiting for Logout button
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
         if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.logoutButtonXpath()))
        {
            error = "Failed to wait for logout button";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.logoutButtonXpath()))
        {
            error = "Failed to wait for logout button";
            return false;
        }
        pause(500);

        // Clicking on Logout button
        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(LightStonePageObjects.logoutButtonXpath()))
        {
            error = "Failed to click on logout button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.username()))
        {
            error = "Failed to validate Logout was successful";
            return false;
        }

        return true;
    }
}
