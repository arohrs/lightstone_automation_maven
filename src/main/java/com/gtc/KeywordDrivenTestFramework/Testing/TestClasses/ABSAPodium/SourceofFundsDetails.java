/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Source Funds",
        createNewBrowserInstance = false
)
public class SourceofFundsDetails extends BaseClass
{

    String error = "";

    public SourceofFundsDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!sourceOfFunds())
        {
            return narrator.testFailed("Failed to select a source of funds -" + error);
        }
        return narrator.finalizeTest("Successfully selected a source of funds");
    }

    public boolean sourceOfFunds()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.sourceOfFundsDetails()))
        {
            error = "Failed to scroll to the 'Source of Funds Details'";
            return false;
        }
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.salary()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.salary()))
            {
                error = "Failed to click the 'Salary' checkBox";
                return false;
            }
        }
        if (SeleniumDriverInstance.isSelected(LightStonePageObjects.salary()))
        {
              narrator.stepPassed("Source of Funds : Salary");
        }
      

        return true;
    }

}
