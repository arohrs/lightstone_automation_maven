/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Contract Details",
        createNewBrowserInstance = false
         
)

public class MyContractDetailsMFC extends BaseClass
{

    String error = "";

    public MyContractDetailsMFC()
    {
    }

    public TestResult executeTest()
    {

        if (!myContract())
        {
            return narrator.testFailed("MFC Contract Details-" + error);
        }
        return narrator.finalizeTest("Successfully updated 'MFC Contract Details' on the Form.");
    }

    public boolean myContract()
    {
        /*updating mfc my contract details*/
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }

        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.myContract(), getData("My Contract")))
        {
            error = "Failed to select 'Yes/No' for my contract.";
            return false;
        }
        narrator.stepPassed("My Contract : " + getData("My Contract"));
        return true;
    }
}
