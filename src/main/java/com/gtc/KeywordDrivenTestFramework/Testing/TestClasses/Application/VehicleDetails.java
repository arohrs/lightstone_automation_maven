/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author gndaye
 */
@KeywordAnnotation(
        Keyword = "Vehicle Details",
        createNewBrowserInstance = false
)
public class VehicleDetails extends BaseClass
{

    String error = "";
    boolean isAfterMarket = (!testData.getData("isAfterMarket").isEmpty()) ? Boolean.parseBoolean(testData.getData("isAfterMarket").toLowerCase()) : false;

    public VehicleDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!enterVehicleDetails())
        {
            return narrator.testFailed("Failed to enter 'vehicle details' - " + error);
        }
        if (!isAfterMarket)
        {
            if (!enterVehicleInfo())
            {

                return narrator.testFailed("Failed to enter 'vehicle details' - " + error);
            }
        }
        return narrator.finalizeTest("Successfully entered customers 'vehicle details' on the Form.");
    }

    public boolean enterVehicleDetails()
    {

        if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(), 2))
        {
            error = "Test Failed due to Wesbank Search Failure - " + SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }

        /*Entering vehicle details*/
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsTab()))
        {
            error = "Failed to scroll to the 'Vehicle Details'.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.articleType(), getData("Article Type")))
        {
            error = "Failed to select 'Article Type'from the list";
            return false;
        }
        narrator.stepPassed("Article Type: " + getData("Article Type"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.articleNewUsedDrop(), getData("Article new/used")))
        {
            error = "Failed to select 'Used/New' from the list";
            return false;
        }
        narrator.stepPassed("Article: " + " " + getData("Article new/used"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.useOfArticle(), getData("Use of Article")))
        {
            error = "Failed to select 'Use of article' from the list";
            return false;
        }
        narrator.stepPassed("Use of article: " + getData("Use of Article"));
        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(LightStonePageObjects.selectVehicleButton()))
        {
            error = "Failed to click on the 'select vehicle' button.";
            return false;
        }
        narrator.stepPassed("Clicked on select vehicle button");
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.vehicleLookup()))
        {
            error = "Failed to wait for the'Vehicle lookup' to appear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully opened the vehicle lookup modal");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.mnMCode()))
        {
            error = "Failed to wait for the 'M&M code' field to apear.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mnMCode(), getData("M&M")))
        {
            error = "Failed to enter the 'M&M code'data.";
            return false;
        }
        narrator.stepPassed("M&M Code : " + getData("M&M"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.searchMMcode()))
        {
            error = "Failed to wait for the 'M&M search' button to apear.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.searchMMcode()))
        {
            error = "Failed to click the 'M&M search'button.";
            return false;
        }
        narrator.stepPassed("Clicked on the search button");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.ok()))
        {
            error = "Failed to wait for the 'OK' button to apear.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully searched the vehicle from M&M code");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.ok()))
        {
            error = "Failed to click  the 'OK' button to apear.";
            return false;
        }
        narrator.stepPassed("Clicked on the OK button");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.selectVehicleButton()))
        {
            error = "Failed to wait fot the select vehicle button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectVehicleButton()))
        {
            error = "Failed to click on the select vehicle button.";
            return false;
        }
        narrator.stepPassed("Clicked on the Select vehicle button");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.manufacturerDropdown()))
        {
            error = "Failed to wait for the 'manufacturer' field to apear.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.manufacturerDropdown(), getData("Manufacturer")))
        {
            error = "Failed to select the 'manufacturer' from the list.";
            return false;
        }
        narrator.stepPassed("Manufacturer: " + getData("Manufacturer"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.yearDropdown(), getData("Year")))
        {
            error = "Failed to select the 'year' from the calender.";
            return false;
        }
        narrator.stepPassed("Year: " + " " + getData("Year"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.modelDropdown(), getData("Model")))
        {
            error = "Failed to select the 'model' from the list";
            return false;
        }
        narrator.stepPassed("Model: " + getData("Model"));
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okButtonOnVehicleInfo()))
        {
            error = "Failed to click on the 'OK' button";
            return false;
        }
        narrator.stepPassed("Clicked on the Ok button");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.purchasePriceTextfield()))
        {
            error = "Failed to wait for 'Purchase Price' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.purchasePriceTextfield(), getData("Purchase Price")))
        {
            error = "Failed to enter the 'purchase price' into the text field";
            return false;
        }
        if (!SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.purchasePriceTextfield(), "value").equals(getData("Purchase Price")))
        {
            error = "Wrong purchase price entered";
            return false;
        }
        narrator.stepPassed("Purchase Price: " + getData("Purchase Price"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.retailPrice()))
        {
            error = "Failed to wait for 'Retail Value' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.retailPrice(), getData("Retail Value")))
        {
            error = "Failed to enter the 'Retail Value' into the text field";
            return false;
        }
        narrator.stepPassed("Retail Value: " + " " + getData("Retail Value"));
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.actualKilometersTextfield()))
        {
            error = "Failed to click the 'actual kilometers' field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.actualKilometersTextfield(), getData("Actual Kilometers")))
        {
            error = "Failed to enter the 'actual kilometers' into the text field ";
            return false;
        }
        if (!SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.actualKilometersTextfield(), "value").equals(getData("Actual Kilometers")))
        {
            error = "Wrong 'actual kilometers' entered";
            return false;
        }
        narrator.stepPassed("Actual Kilometers: " + " " + getData("Actual Kilometers"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.articleColour()))
        {
            error = "Failed to wait for 'Article colour' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.articleColour(), getData("Article Colour")))
        {
            error = "Failed to enter the 'Article colour' into the text field";
            return false;
        }
        narrator.stepPassed("Article Colour: " + " " + getData("Article Colour"));
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.actualKilometersTextfield()))
        {
            error = "Failed to scroll to the 'Kilometer Category' text field";
            return false;
        }

        String chassisNumber = getData("Chassis Number");
        if (!chassisNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.chassisNumberTextfield()))
            {
                error = "Failed to wait for the 'Chassis Number' to appear";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.chassisNumberTextfield(), getData("Chassis Number")))
            {
                error = "Failed to enter the 'Chassis Number' into the text field";
                return false;
            }
            narrator.stepPassed("Chassis Number: " + " " + getData("Chassis Number"));
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.stockNumberTextfield()))
            {
                error = "Failed to wait for 'Stock Number' to appear";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.stockNumberTextfield(), getData("Stock Number")))
            {
                error = "Failed to enter the 'Stock Number' into the text field";
                return false;
            }
            narrator.stepPassed("Stock Number: " + " " + getData("Stock Number"));
            String modelCode = getData("Model Code");
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.shortModelCode(), modelCode))
            {
                error = "Failed to click on the 'OK' button";
                return false;
            }
            narrator.stepPassed("Vehicle Short Model Code : " + modelCode);
        }

        if (!SeleniumDriverInstance.pressKeyWithRobot("tab"))
        {
            error = "Failed to Press Tab Key";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(LightStonePageObjects.actualKilometersTextfield()))
        {
            error = "Failed to click the 'Kilometer Category' into the text field";
            return false;
        }

        int timeout = 10;
        int counter = 0;
        boolean isKilometerFound = false;

        while (!isKilometerFound && counter < timeout)
        {
            String currentValue = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.kilometersCategoryTextfield(), "value");

            if (!currentValue.equals(""))
            {
                isKilometerFound = true;
            }

            pause(1000);
            counter++;
        }

        if (!isKilometerFound)
        {
            error = "Failed to retrieve the Kilometer Category";
            return false;
        }

        String kilometerCatagoryAmount = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.kilometersCategoryTextfield(), "value");
        if (!kilometerCatagoryAmount.equals(getData("Kilometer Category")))
        {
            error = "Failed to validate 'kilometers category amount' present - " + kilometerCatagoryAmount;
            return false;
        }

        narrator.stepPassed("Kilometers Category Validated: " + " " + getData("Kilometer Category"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.registrationNumber()))
        {
            error = "Failed to wait for 'Registration Number' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.registrationNumber(), getData("Registration Number")))
        {
            error = "Failed to enter the 'Registration Number' into the text field";
            return false;
        }
        narrator.stepPassed("Registration Number: " + getData("Registration Number"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.engineNumber()))
        {
            error = "Failed to wait for 'Stock Number' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.engineNumber(), getData("Stock Number")))
        {
            error = "Failed to enter the 'Stock Number' into the text field";
            return false;
        }
        narrator.stepPassed("Stock Number: " + getData("Stock Number"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.serialNumber()))
        {
            error = "Failed to wait for 'Stock Number' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.serialNumber(), getData("Stock Number")))
        {
            error = "Failed to enter the 'Stock Number' into the text field";
            return false;
        }
        narrator.stepPassed("Serial Number: " + getData("Stock Number"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.motorBikeSize()))
        {
            error = "Failed to wait for 'Stock Number' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.motorBikeSize(), getData("Engine Size")))
        {
            error = "Failed to enter the 'Stock Number' into the text field";
            return false;
        }
        narrator.stepPassed("Engine size: " + getData("Engine Size"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.numberOfSeat()))
        {
            error = "Failed to wait for 'Stock Number' to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.numberOfSeat(), getData("Number of Seat")))
        {
            error = "Failed to enter the 'Stock Number' into the text field";
            return false;
        }
        narrator.stepPassed("Number of seats: " + getData("Number of Seat"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeOfSale(), getData("Vehicle Type")))
        {
            error = "Failed to select the 'Vehicle type' ";
            return false;
        }
        narrator.stepPassed("Vehicle Type: " + getData("Vehicle Type"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vehicleTrackingDevice(), getData("Tracking Device")))
        {
            error = "Failed to select the 'Tracking device hardware' ";
            return false;
        }
        narrator.stepPassed("Tracking device fee: " + getData("Tracking Device"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.annualMileage()))
        {
            error = "Failed to wait for the'Annual mileage' field to appear";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.annualMileage(), getData("Mileage")))
        {
            error = "Failed to enter the 'Annual mileage' into the text field";
            return false;
        }
        narrator.stepPassed("Maximum annual mileage : " + getData("Mileage"));
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsTab()))
        {
            error = "Failed to wait for the 'article' new/used drop down.";
            return false;
        }

        return true;
    }

    public boolean enterVehicleInfo()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.regTrackDropdown()))
        {
            error = "Failed to scroll to 'request reg track'";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.regTrackDropdown()))
        {
            error = "Failed to wait to 'request reg track'";
            return false;
        }

        String regNumber = getData("Request Reg Track");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.regTrackDropdown(), regNumber))
        {
            error = "Failed to select the 'request reg track' ";
            return false;
        }

        pause(1000);
        if (regNumber.toLowerCase().equals("yes"))
        {
            if (!SeleniumDriverInstance.alertHandler())
            {
                error = "Failed to handle alert";
                return false;
            }
        }

        pause(1000);

        narrator.stepPassed("Request Reg Track: " + getData("Request Reg Track"));
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsTab()))
        {
            error = "Failed to scroll to the 'Vehicle Details '";
            return false;
        }

        return true;
    }
}
