/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Bank VAPS",
        createNewBrowserInstance = false
)
public class BankVAPS extends BaseClass
{
    
    String error = "";
    String option = getData("Option");
    String employeeNumber = getData("Employee Number");
    String supplier = getData("Supplier");
    String product = getData("Product");
    
    public BankVAPS()
    {
    }
    
    public TestResult executeTest()
    {
        if (!bankVAPSDetails())
        {
            return narrator.testFailed("Failed to enter the Bank VAPS details -" + error);
        }
        return narrator.finalizeTest("Successfully entered the Bank VAPS details");
    }
    
    public boolean bankVAPSDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.bankVAPSTab()))
        {
            error = "Failed to scroll to the 'Bank VAPS'";
            return false;
        }
        
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaEmployeeIndicator(), option))
        {
            error = "Failed to select  the 'Barclays/ABSA Employee' option";
            return false;
        }
        narrator.stepPassed("Barclays/Absa Employee : " + option);
         if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(),5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }
        narrator.stepPassed("Employee Number : " + employeeNumber);
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.academyCertified(), option))
        {
            error = "Failed to select the 'Are you Auto Dealer Academy Certified' option";
            return false;
        }
        narrator.stepPassed("Are you Auto Dealer Academy Certified? : " + option);
         if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.okConsentModal(),5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.okConsentModal()))
            {
                error = "Failed to click the 'OK' button";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewAbsaVap()))
        {
            error = "Failed to click  the 'Add New Bank VAP' button";
            return false;
        }
        narrator.stepPassed("Clicked 'Add New Bank VAP' button");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.absaEmployeeDeleteButton()))
        {
            error = "Failed to click the 'X' button";
            return false;
        }
        narrator.stepPassed("Clicked 'X' button ");
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewAbsaVap()))
        {
            error = "Failed to click  the 'Add New Bank VAP' button";
            return false;
        }
        narrator.stepPassed("Clicked 'Add New Bank VAP' button");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vapsSupplier(), supplier))
        {
            error = "Failed to select the 'supplier' option";
            return false;
        }
        narrator.stepPassed("Supplier : " + option);
        ArrayList <String> productOptions = new ArrayList<>(SeleniumDriverInstance.retrieveMultipleTextByXpath(LightStonePageObjects.vapsSupplierOptions()));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vapsProduct(), productOptions.get(1)))
        {
            error = "Failed to select the 'Product' option";
            return false;
        }
        narrator.stepPassed("Product : " + productOptions.get(1));
        
        String vapsTotal = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.bankVAPSTotalAmount(),"value");
        narrator.stepPassed("Total Vap Amount : " + vapsTotal);
        
        return true;
    }
    
}
