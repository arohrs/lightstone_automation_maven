/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.JuristicTestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application.E_ApplicationDetails;
import org.openqa.selenium.Keys;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Juristic Application details",
        createNewBrowserInstance = false
)
public class E_ApplicationForm extends BaseClass
{

    String error = "";

    public E_ApplicationForm()
    {
    }

    public TestResult executeTest()
    {
        if (!eApplicationform())
        {
            return narrator.testFailed("Failed to enter the company's details on the E-Application form -" + error);
        }

        return narrator.finalizeTest("Successfully entered the company's details on the E-Application form");
    }

    public boolean eApplicationform()
    {
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());
        E_ApplicationDetails  nestedClick = new E_ApplicationDetails();
        if (!nestedClicks(LightStonePageObjects.applicationDropDown(),LightStonePageObjects.juristicApplication()))
        {
            error = "Failed to click  the 'application' button.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.juristicApplication()))
        {
            error = "Failed to wait for  the ' Juristic application' button.";
            return false;
        }
        if (!nestedClick.nestClicks(LightStonePageObjects.juristicApplication(),LightStonePageObjects.companyName()))
        {
            error = "Failed to click the ' Juristic application' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'Juristic Application' from the list");
//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch to new browser window.";
//            return false;
//        }
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.companyName(), 120))
        {
            error = "Failed to wait 'company name'.";
            return false;
        }
        String companyName = getData("Company Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.companyName(), companyName))
        {
            error = "Failed to enter the 'company name'.";
            return false;
        }
        narrator.stepPassed("Company name : " + companyName);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.tradingName())))
        {
            error = "Failed to wait for the 'trading name' to appear.";
            return false;
        }
        String tradingName = getData("Trading Name");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.tradingName(), tradingName))
        {
            error = "Failed to enter the 'trading name'.";
            return false;
        }
        narrator.stepPassed("Trading name : " + tradingName);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.establishedDate())))
        {
            error = "Failed to wait for the 'established date' to appear.";
            return false;
        }
        String establishedDate = getData("Established date");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.establishedDate()))
        {
            error = "Failed to clear 'established date' textfield .";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.establishedDate()))
        {
            error = "Failed to click the 'established date'.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.establishedDate(), establishedDate))
        {
            error = "Failed to enter the 'established date'.";
            return false;
        }
        narrator.stepPassed("Company established date : " + establishedDate);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.companyYearEnd())))
        {
            error = "Failed to wait for the 'Year end' to appear.";
            return false;
        }
        String yearEnd = getData("Year end");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyYearEnd(), yearEnd))
        {
            error = "Failed to select the 'Company year end ' from the list.";
            return false;
        }
        narrator.stepPassed("Company year end : " + yearEnd);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.companyType())))
        {
            error = "Failed to wait for the 'Company type' to appear.";
            return false;
        }
        String companyType = getData("Company Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyType(), companyType))
        {
            error = "Failed to select the 'company type' from the list.";
            return false;
        }
        narrator.stepPassed("Company type : " + companyType);
        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.businessRegistrationNumber())))
        {
            error = "Failed to wait for the 'trading name' to appear.";
            return false;
        }
        String registrationNumber = getData("Registration Number");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessRegistrationNumber(), registrationNumber))
        {
            error = "Failed to enter the 'Registration number' in the text field.";
            return false;
        }
        narrator.stepPassed("Registration number : " + registrationNumber);

        if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.vatIndicator())))
        {
            error = "Failed to wait for the 'VAT Indicator' to appear.";
            return false;
        }
        String vatIndicator = getData("Vat Indicator");
        SeleniumDriverInstance.pressKey(Keys.TAB);
        if (!SeleniumDriverInstance.dismissAlert())
        { //cancells an alert
            error = "Failed to handle an alert.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vatIndicator(), vatIndicator))
        {
            error = "Failed to select  the 'Vat Indicator' from the list .";
            return false;
        }
        narrator.stepPassed("Vat Indicator : " + vatIndicator);
        if (!vatIndicator.equalsIgnoreCase("No"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath((LightStonePageObjects.businessVatNumber())))
            {
                error = "Failed to wait for the 'VAT Number' to appear.";
                return false;
            }
            String vatNumber = getData("Vat Number");

            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.businessVatNumber(), vatNumber))
            {
                error = "Failed to enter the 'Vat Number'.";
                return false;
            }
            narrator.stepPassed("Vat Number : " + vatNumber);
        }
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.clickAbsaBank()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickAbsaBank()))
            {
                error = "Failed to click the 'absa bank' check-box.";
                return false;
            }
        }
        narrator.stepPassed("Clicked ABSA BANK checkBox");
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.clickMfc()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickMfc()))
            {
                error = "Failed to click the 'mfc' check-box.";
                return false;
            }
        }
        narrator.stepPassed("Clicked MFC checkBox");
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.clickStandardBank()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickStandardBank()))
            {
                error = "Failed to click the 'standard bank' check-box.";
                return false;
            }
        }
        narrator.stepPassed("Clicked STANDARD BANK checkBox");
        if (!SeleniumDriverInstance.isSelected(LightStonePageObjects.clickWesbank()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.clickWesbank()))
            {
                error = "Failed to click the 'wesbank' check-box.";
                return false;
            }
        }
        narrator.stepPassed("Clicked WESBANK checkBox");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaDealerCodeDropDown(), getData("ABSA")))
        {
            error = "Failed to select 'absa dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected ABSA - Dealer Code: " + " " + getData("ABSA"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.mfcDealerCodeDropDown(), getData("MFC")))
        {
            error = "Failed to select the 'mfc dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected MFC - Dealer Code :" + " " + getData("MFC"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.standardBankDealerCodeDropDown(), getData("Standard Bank")))
        {
            error = "Failed to select the 'standard dealer code' from the list.";
            return false;
        }
        narrator.stepPassed("Selected STANDARD BANK - Dealer Code: " + " " + getData("Standard Bank"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.wesBankDealerCodeDropDown(), getData("Wesbank")))
        {
            error = "Failed to select the 'wesbank dealer code from the list.";
            return false;
        }
        narrator.stepPassed("Selected WESBANK - Dealer Code: " + " " + getData("Wesbank"));
        if (!SeleniumDriverInstance.waitForOneElementLeftByXpath(LightStonePageObjects.branchDropDown()))
        {
            error = "Failed to wait for the 'branch Code' from the list.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.branchDropDown(), getData("Branch Code")))
        {
            error = "Failed to select the 'branch Code' from the list.";
            return false;
        }
        narrator.stepPassed("WESBANK - Branch Code: " + " " + getData("Branch Code"));

        return true;
    }

    public boolean nestedClicks(String xpath, String searchxPath)
    {
        Search:
        for (int count = 0; count < 4; count++)
        {
            switch (count)
            {
                case 0:
                    if (!SeleniumDriverInstance.clickElementbyXpath(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";
                    }
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 5))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;

                    } else
                    {
                        break Search;
                    }
                case 1:
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";

                    }
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 10))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                case 2:
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingJavascript(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";

                    }
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 15))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                case 3:
                    if (!SeleniumDriverInstance.roboClickLatestElementbyXpath(xpath))
                    {
                        error = "Failed to select 'new application' from the list of applications.";

                    }
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(searchxPath, 120))
                    {
                        error = "Failed to wait for the 'application form tab' to appear.";
                        break;
                    } else
                    {
                        break Search;
                    }
                default:
                    return false;
            }
        }
        return true;
    }
}
