/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Dealer VAPS",
        createNewBrowserInstance = false
        
)

public class DealerVAPS extends BaseClass
{

    String error = "";

    public DealerVAPS()
    {
    }

    public TestResult executeTest()
    {
        if (!dealerVAPS())
        {
            return narrator.testFailed("Failed to enter customers details on the 'Dealer VAPS' Form - " + error);
        }
        return narrator.finalizeTest("Successfully entered customers details on the 'Dealer VAPS' Form.");
    }

    public boolean dealerVAPS()
    {

        /*Entering Dealer VAPS details*/
         if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.dealerVapsTab()))
        {
            error = "Failed to scroll to the 'Dealer VAPS'.";
            return false;
        }
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fspNumber(), getData("FSP")))
        {
            error = "Failed to select 'FSP'.";
            return false;
        }
         narrator.stepPassed("FSP: "+getData("FSP"));
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.dofRegistrationDropDown()))
        {
            error = "Failed to click the 'date of first registration' text field.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.monthDropDown(), getData("Month")))
        {
            error = "Failed to select a 'month' from a calender.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.yearDropDown(), getData("Year")))
        {
            error = "Failed to select a 'year' from a calender.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.selectDay()))
        {
            error = "Failed to select a 'day' from a calender.";
            return false;
        }
        narrator.stepPassed("Date of First Registration : " + " " + getData("Date Of First Registration"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.manufacturerWarrantyDropDown(), getData("Manufacturer Warranty")))
        {
            error = "Failed to select 'manufacturer warranty' from the list.";
            return false;
        }
        narrator.stepPassed("Manufacturer Warranty : " + " " + getData("Manufacturer Warranty"));
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.manufactureService(), getData("Service")))
        {
            error = "Failed to select 'manufacturer maintenance /service' detail from the list.";
            return false;
        }
         narrator.stepPassed("Maintenance/Service: "+ getData("Service"));
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fullServiceHistory(), getData("Service History")))
        {
            error = "Failed to select 'Full service history' detail from the list.";
            return false;
        }
         narrator.stepPassed("Full Service History: "+ getData("Service History"));
         if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maintenanceOrServicePlan(), getData("Plan")))
        {
            error = "Failed to select 'Maintenance /service Plan' detail from the list.";
            return false;
        }
         narrator.stepPassed("Maintenance/Service Plan: "+getData("Plan"));
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.maintenanceOrServiceExpiryDate(), getData("Expiry")))
        {
            error = "Failed to enter the 'manufacturer maintenance /service plan expiry' detail from the list.";
            return false;
        }
         narrator.stepPassed("Maintenance/Service Plan expiry date: "+getData("Expiry"));
         if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.maintenanceOrServiceExpiryKm(), getData("KM Expiry")))
        {
            error = "Failed to enter 'manufacturer maintenance /service plan expiry Kilometers' detail from the list.";
            return false;
        }
         narrator.stepPassed("Maintenance/Service Plan expiry KM: "+getData("KM Expiry"));
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addButton()))
        {
            error = "Failed to click the 'add'  button.";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.categoryDropDown(), getData("Category")))
        {
            error = "Failed to select 'cetegory' from the list.";
            return false;
        }
        narrator.stepPassed("Category : " + " " + getData("Category"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.supplierDropDown()))
        {
            error = "Failed to wait for 'supplier' dropdown.";
            return false;
        }
        // Waiting for 'supplier' dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.supplierDropDown()))
        {
            error = "Failed to wait for 'supplier' dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.supplierDropDown(), getData("Supplier")))
        {
            error = "Failed to select 'supplier' from the list.";
            return false;
        }
        narrator.stepPassed("Supplier: "+ getData("Supplier"));
        // Waiting for 'product' dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.productDropDown()))
        {
            error = "Failed to wait for 'product' dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.productDropDown(), getData("Product")))
        {
            error = "Failed to select 'product' from the list.";
            return false;
        }
        narrator.stepPassed("Product : " + " " + getData("Product"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.productOptionDropDown(), getData("Product Option")))
        {
            error = "Failed to select 'product option' from the list.";
            return false;
        }
        narrator.stepPassed("Product option : " + getData("Product Option"));
        // IGF Provider drop down has no value
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealerVAPSComment()))
        {
            error = "Failed to wait for 'Comment' text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.dealerVAPSComment(), getData("Comment")))
        {
            error = "Failed to enter the 'comment' into the text field";
            return false;
        }
        narrator.stepPassed("Comment :"+getData("Comment"));
         if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.priceList()))
        {
            error = "Failed to wait for 'Comment' text field";
            return false;
        }
          if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.priceList()))
        {
            error = "Failed to click the 'price list' button";
            return false;
        }
         narrator.stepPassed("Clicked on the Price List button");
          if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.expandAll()))
        {
            error = "Failed to click the 'Expand All' button";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'Expand All' button");
           if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.collapseAll()))
        {
            error = "Failed to click for 'Collapse All' button";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked on the 'Collapse All' button");
           if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.resizableIcon()))
        {
            error = "Failed to wait for 'Resizable' Icon";
            return false;
        }
            if (!SeleniumDriverInstance.dragDropElementByXpathToPixel(LightStonePageObjects.resizableIcon(),180,120))
        {
            error = "Failed to drag n drop the 'treeView resizable 'Icon";
            return false;
        }    
           if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.closeTreeview()))
        {
            error = "Failed to wait for 'Close' button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully expanded the 'Tree View windown' from the resizable icon");   
           if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.closeTreeview()))
        {
            error = "Failed to click for 'Close' button";
            return false;
        }
           pause(500);
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.dealerVapsTab()))
        {
            error = "Failed to scroll to the 'Dealer VAPS'.";
            return false;
        }   
           
         
        return true;
    }
}
