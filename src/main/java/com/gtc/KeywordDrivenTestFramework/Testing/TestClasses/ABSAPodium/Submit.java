/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;


/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Submit",
        createNewBrowserInstance = false
)
public class Submit extends BaseClass
{

    String error = "";

    public Submit()
    {
    }

    public TestResult executeTest()
    {
        if (!submitButton())
        {
            return narrator.testFailed("Failed to submit the podium form -" + error);
        }
        return narrator.finalizeTest("Successfully submitted the podium form");
    }

    public boolean submitButton()
    {
        
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.submitPodiumApplication()))
        {
            error = "Failed to scroll to the 'Submit' button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitPodiumApplication()))
        {
            error = "Failed to wait for the 'Submit' button";
            return false;
        }
         if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitPodiumApplication()))
        {
            error = "Failed to click the 'Submit' button";
            return false;
        }
        
          if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.podiumSubmissionMessage()))
        {
            error = "Failed to wait for the 'Submission message'";
            return false;
        }
          narrator.stepPassedWithScreenShot("Successfully submitted the Podium form ");
             if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumSubmissionMessage()))
        {
            error = "Failed to scroll to the 'Submission message'";
            return false;
        }
       SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle());
        narrator.stepPassed("Successfully switched back to the Inbox  Tab");

       return true;
    }

}
