/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Banking Negative",
        createNewBrowserInstance = false
)
public class NegativeBankingDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeBankingDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!bankingDetails())
        {
            return narrator.testFailed("Failed to perform the negative test -" + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test  ");
    }

    public boolean bankingDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.bankingDetailsTab()))
        {
            error = "Failed to scroll to the 'banking details' table.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.accountHolder()))
        {
            error = "Failed to enter the'account holder' details into the text field.";
            return false;
        }
        if (!getData("Account Holder").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountHolder(), getData("Account Holder")))
            {
                error = "Failed to enter 'account holder' details into the text field.";
                return false;
            }
        }
        narrator.stepPassed("Account Holder  :" + " " + getData("Account Holder"));
        if (!getData("Bank Name").isEmpty())
        {
            if (!getData("Bank Name").equalsIgnoreCase("Not a Bank"))
            {
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.bankNameDropDown(), getData("Bank Name")))
                {
                    error = "Failed to select the 'bank name' from the list.";
                    return false;
                }
                narrator.stepPassed("Bank Name :" + " " + getData("Bank Name"));
            } else
            {
                if (SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountType(), getData("Bank Name"))) //If true, fail it. Should not be able to enter text
                {
                    error = "Text Field should not accept text.";
                    return false;
                } else
                {
                    narrator.stepPassed("Entered text:" + " " +getData("Bank Name")+" into Bank Name textfield." );
                    narrator.stepPassedWithScreenShot("User cannot put in bank name manually");
                }

            }

        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.branchCode()))
        {
            error = "Failed to enter the 'Branch code' into the text field.";
            return false;
        }
        if (!getData("Branch Code").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.branchCode(), getData("Branch Code")))
            {
                error = "Failed to enter the 'Branch code' into the text field.";
                return false;
            }
        }
        narrator.stepPassed(" Branch Code:" + " " + getData("Branch Code"));

        if (!getData("Bank Branch").isEmpty())
        {
            if (!getData("Bank Name").equalsIgnoreCase("Not selected"))
            {
                if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.bankBranch()))
                {
                    error = "Failed to clear the 'Bank Branch' field.";
                    return false;
                }
            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.bankBranch(), getData("Bank Branch")))
            {
                error = "Failed to enter the 'Bank Branch' into the text field.";
                return false;
            }
        }
        narrator.stepPassed("Bank Branch: " + getData("Bank Branch"));
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.accountNumber()))
        {
            error = "Failed to clear the 'account number'  text field.";
            return false;
        }
        if (!getData("Account Number").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.accountNumber(), getData("Account Number")))
            {
                error = "Failed to enter the 'account number' into the text field.";
                return false;
            }
        }
        narrator.stepPassed(" Account Number :" + " " + getData("Account Number"));
        if (!getData("Bank Name").equalsIgnoreCase("Not selected"))
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.accountType(), getData("Account Type")))
            {
                error = "Failed to select 'account type' from the list.";
                return false;
            }

            narrator.stepPassed("Account Type :" + " " + getData("Account Type"));

        }
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.bankingDetailsTab()))
            {
                error = "Failed to scroll to the 'banking details' table.";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("input"))
            {

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(1000);

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
