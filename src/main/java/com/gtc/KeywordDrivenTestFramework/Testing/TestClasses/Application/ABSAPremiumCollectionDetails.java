/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "ABSA Premium Collection Details",
        createNewBrowserInstance = false
        
)

public class ABSAPremiumCollectionDetails extends BaseClass
{

    String error = "";

    public ABSAPremiumCollectionDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!absaPremiumCollectionDetails())
        {
            return narrator.testFailed("Failed to update 'absa dealer premium collection details' on the Form - " + error);
        }
        return narrator.finalizeTest("Successfully updated 'absa dealer premium collection details' on the Form.");
    }

    public boolean absaPremiumCollectionDetails()
    {

        /*updating absa premium collection details*/
          if (SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.wesbankErrorMessage(),2))
        {
            error ="Test Failed due to Wesbank Search Failure - "+SeleniumDriverInstance.retrieveTextByXpath(LightStonePageObjects.wesbankErrorMessage());
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.absaPremiumCollectionTab()))
        {
            error = "Failed to scroll to 'absa dealer premium collection' details";
            return false;
        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaPremiumCollectionDropDown(), getData("Dealer Collect Premium")))
        {
            error = "Failed to select 'absa dealer premium collection details' from the list.";
            return false;
        }
        narrator.stepPassed("Dealer Collect Premium :" + getData("Dealer Collect Premium"));
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaPremiumVendorDropDown(), getData("Premium Vendor")))
        {
            error = "Failed to select 'absa dealer premium vendor' from the list.";
            return false;
        }
        narrator.stepPassed("Premium Vendor :" + getData("Premium Vendor"));
        return true;
    }
}
