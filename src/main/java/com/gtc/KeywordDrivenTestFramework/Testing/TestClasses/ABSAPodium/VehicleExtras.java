/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ABSAPodium;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Podium Vehicle Extras",
        createNewBrowserInstance = false
)
public class VehicleExtras extends BaseClass
{

    String error = "";
    String description = getData("Description");
    String amount = getData("Amount");

    public VehicleExtras()
    {
    }

    public TestResult executeTest()
    {
        if (!vehicleExtras())
        {
            return narrator.testFailed("Failed to enter the vehicle's extras -" + error);
        }
        return narrator.finalizeTest("Successfully entered the vehicle's extras");
    }

    public boolean vehicleExtras()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.podiumVehicleExtrasTab()))
        {
            error = "Failed to scroll to the 'Vehicle Extras'";
            return false;
        }
        List extras;
        int extrasList = 0;
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.transactionExtraDescription(), 5))
        {
            if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.addNewExtra()))
            {
                error = "Failed to click the 'Add New Extra' button";
                return false;
            }
            narrator.stepPassed("Clicked 'Add New Extra' button");
            if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.removeExtraButton()))
            {
                error = "Failed to click the 'X' button";
                return false;
            }
            narrator.stepPassed("Clicked 'X' button");
            if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.addNewExtra()))
            {
                error = "Failed to click the 'Add New Extra' button";
                return false;
            }
            narrator.stepPassed("Clicked 'Add New Extra' button");
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleExtrasDescription()))
            {

                error = "Failed to wait for the 'Description' to appear";
                return false;

            }
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vehicleExtrasDescription(), description))
            {
                error = "Failed to select the 'Description'";
                return false;
            }
            narrator.stepPassed("Description: " + description);
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vehicleExtrasAmount(), amount))
            {
                error = "Failed to enter the 'Amount'";
                return false;
            }
            narrator.stepPassed("Amount: " + amount);

        } else if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.transactionExtraDescription(), 5))

        {
            extras = SeleniumDriverInstance.retrieveWebElementByXpath(LightStonePageObjects.transactionExtraDescription());
            extrasList = extras.size();

            if (extrasList < 9)
            {

                if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.addNewExtra()))
                {
                    error = "Failed to click the 'Add New Extra' button";
                    return false;
                }
                narrator.stepPassed("Clicked 'Add New Extra' button");
                if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.removeExtraButton()))
                {
                    error = "Failed to click the 'X' button";
                    return false;
                }
                narrator.stepPassed("Clicked 'X' button");
                if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.addNewExtra()))
                {
                    error = "Failed to click the 'Add New Extra' button";
                    return false;
                }
                narrator.stepPassed("Clicked 'Add New Extra' button");

                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleExtrasDescription()))
                {

                    error = "Failed to wait for the 'Description' to appear";
                    return false;

                }
                if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vehicleExtrasDescription(), description))
                {
                    error = "Failed to select the 'Description'";
                    return false;
                }
                if (SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.invalidSelection()))
                {
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.okButton()))
                    {
                        error = "Failed to click 'OK' button";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpathUsingActions(LightStonePageObjects.removeExtraButton()))
                    {
                        error = "Failed to click the 'X' button";
                        return false;
                    }

                }
                narrator.stepPassed("Description: " + description);
                if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vehicleExtrasAmount(), amount))
                {
                    error = "Failed to enter the 'Amount'";
                    return false;
                }
                narrator.stepPassed("Amount: " + amount);
            }

        }

        String totalExtra = SeleniumDriverInstance.retrieveAttributeByXpath(LightStonePageObjects.total(),"value");
        narrator.stepPassed("Total Extras Amount: " + totalExtra);

        return true;
    }

}
