/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Application;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Absa VAPS",
        createNewBrowserInstance = false
)
public class AbsaBankVaps extends BaseClass
{

    String error = "";

    public AbsaBankVaps()
    {
    }

    public TestResult executeTest()
    {
        if (!absaVaps())
        {
            return narrator.testFailed("Failed to enter ABSA Bank Vaps details. - " + error);
        }

        return narrator.finalizeTest("Successfully updated ABSA Bank Vaps details.");
    }

    public boolean absaVaps()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.absaBankVaps()))
        {
            error = "Failed to Scroll to 'ABSA Vaps ' Header";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.absaEmployeeIndicator()))
        {
            error = "Failed to wait for 'Barclays/ABSA employee' dropdown";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaEmployeeIndicator(), getData("Employee")))
        {
            error = "Failed to select 'Yes/No' from the Barclays/ABSA employeelist  ";
        }
        narrator.stepPassed("Barclays/ABSA employee: " + getData("Employee"));
        String employeeNumber = getData("Employee Number");
        if (!employeeNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.absaEmployeeNumber()))
            {
                error = "Failed to wait for 'Employee number' field";
                return false;

            }
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.absaEmployeeNumber(), employeeNumber))
            {
                error = "Failed to enter the'Employee number' ";
                return false;

            }
        }
        narrator.stepPassed("Employee Number: " + employeeNumber);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.academyCertified()))
        {
            error = "Failed to wait for 'Barclays/ABSA employee' dropdown";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.academyCertified(), getData("Academy Certified")))
        {
            error = "Failed to select 'Yes/No' from the Barclays/ABSA employeelist  ";
        }
        narrator.stepPassed("Academy Certified: " + getData("Academy Certified"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.addNewAbsaVap()))
        {
            error = "Failed to wait for 'Add new Bank Vap' button";
            return false;

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.addNewAbsaVap()))
        {
            error = "Failed to click the 'Add new Bank Vap' button";
            return false;

        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.absaVapsSupplier()))
        {
            error = "Failed to wait for 'Absa vaps supplier' to appear ";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaVapsSupplier(), getData("Supplier")))
        {
            error = "Failed to wait for 'Absa vaps supplier' to appear ";
            return false;

        }
        narrator.stepPassed("Supplier: " + getData("Supplier"));
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.absaVapsProduct()))
        {
            error = "Failed to wait for 'Absa vaps Product' to appear ";
            return false;

        }
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.absaVapsProduct(), getData("Product")))
        {
            error = "Failed to wait for 'Absa vaps Product' to appear ";
            return false;

        }
        narrator.stepPassed("Product: " + getData("Product"));

        return true;
    }

}
