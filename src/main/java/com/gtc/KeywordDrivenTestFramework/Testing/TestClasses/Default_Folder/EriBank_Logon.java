package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Default_Folder;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestEntity;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.Eribank_PageObject;
import com.gtc.KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;

@KeywordAnnotation(
        Keyword = "Eribank",
        createNewBrowserInstance = false
)
public class EriBank_Logon extends BaseClass {

    public TestEntity testData;
    String error = "";
    Narrator narrator;

    public EriBank_Logon() {
  
    
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest() {
        if (!Login()) {
            return narrator.testFailed("Failed to login - " + error);
        }
        return narrator.finalizeTest("Process: Logged on successfully ");
    }

    public boolean Login() {
        if (!AppiumDriverInstance.enterTextByXpath(Eribank_PageObject.username(),testData.getData("Username")))
        {
            error="Failed to enter username";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        if (!AppiumDriverInstance.enterTextByXpath(Eribank_PageObject.password(),testData.getData("Password")))
        {
            error="Failed to enter password";
            return false;
        }
        AppiumDriverInstance.pause(2000);
        if (!AppiumDriverInstance.clickElementbyXpath(Eribank_PageObject.loginButton()))
        {
            error="Failed to click login button";
            return false;
        }
        
        AppiumDriverInstance.pause(1000);
        return true;

    }

}
