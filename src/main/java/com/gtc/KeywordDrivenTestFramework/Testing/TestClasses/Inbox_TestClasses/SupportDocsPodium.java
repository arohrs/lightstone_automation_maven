/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.Inbox_TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;


/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Support Docs",
        createNewBrowserInstance = false
)

public class SupportDocsPodium extends BaseClass
{

    String error = "";

    public TestResult executeTest()
    {
        if (!suppPodium())
        {
            return narrator.testFailed("Failed to validate the Support Document Podium -" + error);
        }

        return narrator.finalizeTest("Successfully validated Inbox dashboard page.");
    }

    public boolean suppPodium()
    {
        // Get Current tab title
        LightStonePageObjects.setPageHandle(SeleniumDriverInstance.getWindowHandle());

        // Clicking on 'Supp Docs'
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.suppDocs("SUPP DOCS", 15)))
        {
            error = "Failed to wait for the 'Supp Docs' button.";
            return false;
        }

        narrator.stepPassed("Successfully validated that the 'SUPP DOCS' button is present.");

        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.suppDocs("SUPP DOCS", 15)))
        {
            error = "Failed to click on 'Supp Docs'";
            return false;
        }

        // Switch New tab
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to Switch to a new tab";
            return false;
        }

        // Validate that tab open is from column
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(LightStonePageObjects.supportDocumentTitle()))
        {
            error = "Failed to Validate that tab open is from 'Support Document' column";
            return false;
        }

        //extracting new tab Title page name to extends report
        testData.extractParameter("Supporting Document page Title", SeleniumDriverInstance.Driver.getTitle(), "");
        narrator.stepPassedWithScreenShot("Successfully opened the 'Supporting Document' column.");

        // Close the new Tab and switch back to original tab
        if (!SeleniumDriverInstance.closeTab(LightStonePageObjects.getPageHandle()))
        {
            error = "Failed to close the current tab.";
            return false;
        }

        return true;
    }

}
