/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Personal Negative",
        createNewBrowserInstance = false
)
public class NegativePersonalDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativePersonalDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!personalDetails())
        {
            return narrator.testFailed("Failed to perform the negative test for - " + error);
        }

        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean personalDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.personalDetailsTab()))
        {
            error = "Failed to scroll to 'Personal Details' drop down selection";
            return false;
        }
        String race = getData("Race");
        if (!race.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.raceDropDown(), race))
            {
                error = "Failed to select 'Race' from the drop down list.";
                return false;
            }
        }
        narrator.stepPassed("Race :" + race);
        String title = getData("Title");
        if (!title.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.titleDropDown(), title))
            {
                error = "Failed to select a 'Title' from the list.";
                return false;
            }
        }
        narrator.stepPassed("Title : " + title);
        String maritalStatus = getData("Marital Status");
        if (!maritalStatus.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.maritalStatusDropDown(), maritalStatus))
            {
                error = "Failed to select the 'marital status' from the list.";
                return false;
            }
        }
        narrator.stepPassed("Marital Status : " + maritalStatus);

        String preferredContact = getData("Contact Method");
        if (!preferredContact.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.companyPreferredContactMethod(), preferredContact))
            {

                error = "Failed to select the 'preferred contact method'";
                return false;
            }
            narrator.stepPassed("Preferred Contact Method : " + preferredContact);
        }
        String mobileNumber = getData("Mobile Number");
        String homeNumber = getData("Home Number");
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.phoneNumber()))
        {
            error = "Failed to clear the 'phone number'";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.phoneNumber(), homeNumber))
        {
            error = "Failed to enter the 'phone number (H) '";
            return false;
        }
        narrator.stepPassed("Phone Number (H) : " + homeNumber);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.mobileNumber()))
        {
            error = "Failed to clear 'mobile number' field";
            return false;
        }

        if (!mobileNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.mobileNumber(), mobileNumber))
            {
                error = "Failed to enter 'mobile number' from the drop down list.";
                return false;
            }
        }
        narrator.stepPassed("Mobile Number :" + mobileNumber);
        String phoneNumber = getData("Phone Number Type");
        if (!phoneNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.phoneTypeDropDown(), phoneNumber))
            {
                error = "Failed to select 'phone number type' from the drop down list.";
                return false;
            }
        }
        narrator.stepPassed("Phone Number Type:" + phoneNumber);
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.workNumber()))
        {
            error = "Failed to clear 'work phone number' field.";
            return false;
        }
        String workNumber = getData("Work Number");
        if (!workNumber.isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.workNumber(), workNumber))
            {
                error = "Failed to enter 'work phone number' from the list.";
                return false;
            }
        }
        narrator.stepPassed("Phone Number (W) :" + workNumber);
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.personalDetailsTab()))
            {
                error = "Failed to scroll to 'Personal detail' container";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
