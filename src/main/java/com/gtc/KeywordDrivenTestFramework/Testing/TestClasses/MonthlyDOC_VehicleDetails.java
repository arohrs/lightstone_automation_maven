/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "Doc-Vehicle Details",
        createNewBrowserInstance = false
)

public class MonthlyDOC_VehicleDetails extends BaseClass
{

    String error = "";
    ArrayList<String> details;
    String rowDetails = "";
    String monthlyDocValidation = getData("Monthly DOC");

    public MonthlyDOC_VehicleDetails()
    {
    }

    public TestResult executeTest()
    {
        MonthlyDOC mon = new MonthlyDOC();

        if (!captureVehicleDetails())
        {
            return narrator.testFailed("Failed capture Vehicle Details- " + error);
        }
        if (!captureVehicleAccessories())
        {
            return narrator.testFailed("Failed to capture Vehicle Accessories' Details- " + error);
        }
        if (!captureDOCDetails())
        {
            return narrator.testFailed("Failed to capture DOC Details- " + error);
        }

        return narrator.finalizeTest("Successfully entered customer's details on the 'e-application' Form.");
    }

    public boolean captureVehicleDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleDetailsHeader()))
        {
            error = "Failed to wait for the 'Vehicle details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.articleCondition()))
        {
            error = "Failed to wait for the 'article condition' .";
            return false;
        }
        String vehicleCondition = getData("Condition");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.articleCondition(), vehicleCondition))
        {
            error = "Failed to select 'used' from the list.";
            return false;
        }
        narrator.stepPassed("Selected vehicle condition: " + vehicleCondition);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.yearModel()))
        {
            error = "Failed to wait for the 'Year Model'.";
            return false;
        }
        String yearModel = getData("Year Model");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.yearModel(), yearModel))
        {
            error = "Failed to select '2010' from the list.";
            return false;
        }
        narrator.stepPassed("Selected year model: " + yearModel);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleMake()))
        {
            error = "Failed to wait for the 'vehicle make' .";
            return false;
        }
        String vehicleMake = getData("Vehicle Make");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vehicleMake(), vehicleMake))
        {
            error = "Failed to select for the 'vehicle make' from the list.";
            return false;
        }
        narrator.stepPassed("Selected vehicle make:  " + vehicleMake);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleModel()))
        {
            error = "Failed to wait for the 'vehicle model' .";
            return false;
        }
        String vehicleModel = getData("Vehicle Model");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.vehicleModel(), vehicleModel))
        {
            error = "Failed to select for the 'vehicle model' from the list.";
            return false;
        }
        narrator.stepPassed("Selected vehicle model:  " + vehicleModel);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.mmCode()))
        {
            error = "Failed to wait for the 'M&M Code' .";
            return false;
        }
        String mmCode = getData("M&M");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.mmCode(), mmCode))
        {
            error = "Failed to enter for the 'M&M Code' .";
            return false;
        }
        narrator.stepPassed("Entered M&M code: " + mmCode);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vinNumber()))
        {
            error = "Failed to wait for the 'M&M Code' .";
            return false;
        }
        String vinNum = getData("Vin Number");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.vinNumber(), vinNum))
        {
            error = "Failed to enter for the 'Vin Number' .";
            return false;
        }
            narrator.stepPassedWithScreenShot("Entered Vin Number: " + vinNum);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleRetailPrice()))
        {
            error = "Failed to wait for the 'retail Price'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.vehicleRetailPrice()))
        {
            error = "Failed to  clear Retail Price text field.";
            return false;
        }
        String vehicleRetailPrice = getData("Retail Price");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vehicleRetailPrice(), vehicleRetailPrice))
        {
            error = "Failed to enter for the 'retail Price'.";
            return false;
        }
        narrator.stepPassed("Entered Retail Price: " + vehicleRetailPrice);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.costPrice()))
        {
            error = "Failed to wait for the 'cost Price'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.costPrice()))
        {
            error = "Failed to  clear 'Cost Price' text field";
            return false;
        }
        String vehicleCostPrice = getData("Cost Price");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.costPrice(), vehicleCostPrice))
        {
            error = "Failed to enter for the 'costPrice'.";
            return false;
        }
        narrator.stepPassed("Entered cost price: " + vehicleCostPrice);
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.overAllowance()))
        {
            error = "Failed to wait for the 'over Allowance'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.overAllowance()))
        {
            error = "Failed to clear 'Factory Claim Price' text field.";
            return false;
        }
        String overAllowance = getData("Over Allowance");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.overAllowance(), overAllowance))
        {
            error = "Failed to enter for the 'over Allowance' .";
            return false;
        }
        narrator.stepPassed("Entered Over Allowance: " + overAllowance);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.factoryClaim()))
        {
            error = "Failed to wait for the 'factory Claim' .";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.factoryClaim()))
        {
            error = "Failed to clear 'Factory Claim Price' text field.";
            return false;
        }
        String factoryClaim = getData("Factory Claim");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.factoryClaim(), factoryClaim))
        {
            error = "Failed to enter for the 'factory Claim' header.";
            return false;
        }
        narrator.stepPassed("Entered Factory claim: " + factoryClaim);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehiclePurchasePrice()))
        {
            error = "Failed to wait for the 'purchase Price'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.vehiclePurchasePrice()))
        {
            error = "Failed to clear Purchase Price text field.";
            return false;
        }
        String purchasePrice = getData("Purchase Price");
        if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.vehiclePurchasePrice(), purchasePrice))
        {
            error = "Failed to enter for the 'purchase Price'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Entered purchase price: " + purchasePrice);
        return true;
    }

    public boolean captureVehicleAccessories()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.vehicleAccessories()))
        {
            error = "Failed to wait for the 'vehicle Accessories' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.accessoriesRetailPrice()))
        {
            error = "Failed to wait for the 'accessories RetailPrice'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.accessoriesRetailPrice()))
        {
            error = "Failed to clear Purchase Price text field.";
            return false;
        }
        String accessoriesRetailPrice = getData("Accessories Retail Price");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.accessoriesRetailPrice(), accessoriesRetailPrice))
        {
            error = "Failed to enter for the 'accessories RetailPrice'.";
            return false;
        }
        narrator.stepPassed("Entered Accessories retail price: " + accessoriesRetailPrice);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.accessoriesCostPrice()))
        {
            error = "Failed to wait for the 'accessories Cost Price'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.accessoriesCostPrice()))
        {
            error = "Failed to clear Purchase Price text field.";
            return false;
        }
        String accessoriesCostPrice = getData("Accessories cost Price");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.accessoriesCostPrice(), accessoriesCostPrice))
        {
            error = "Failed to enter for the 'accessories Cost Price' header.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Entered Accessories cost price: " + accessoriesCostPrice);

        return true;
    }

    public boolean captureDOCDetails()
    {

        Date date = new Date();
        Calendar now = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        now.add(Calendar.DATE, 9);

        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.docDetailsHeader()))
        {
            error = "Failed to wait for the 'doc Details' header.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.sightedCheckBox()))
        {
            error = "Failed to wait for the 'sighted' CheckBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.sightedCheckBox()))
        {
            error = "Failed to click for the 'sighted' CheckBox.";
            return false;
        }
        narrator.stepPassed("'Is Sighted' checkbox selected successfully");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.repeatCustomer()))
        {
            error = "Failed to wait for the 'repeat customer' CheckBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.repeatCustomer()))
        {
            error = "Failed to click for the 'repeat customer' CheckBox.";
            return false;
        }
        narrator.stepPassed("'Repeat Customers' checkbox selected successfully");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.docBankName()))
        {
            error = "Failed to wait for the 'Bank Name' .";
            return false;
        }
        String bankName = getData("Bank Name");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.docBankName(), bankName))
        {
            error = "Failed to select 'Cash' from the list.";
            return false;
        }
        narrator.stepPassed("Selected Bank name: " + bankName);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dealer()))
        {
            error = "Failed to wait for the 'dealer' .";
            return false;
        }
        String dealer = getData("Dealer");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.dealer(), dealer))
        {
            error = "Failed to select for the 'dealer' .";
            return false;
        }
        narrator.stepPassed("Selected dealer: " + dealer);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.fAndIName()))
        {
            error = "Failed to wait for the 'F&I'.";
            return false;
        }
        String fandI = getData("F&I6");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.fAndIName(), fandI))
        {
            error = "Failed to select for the 'F&I' list.";
            return false;
        }
        narrator.stepPassed("Selected F&I: " + fandI);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.typeOfSale()))
        {
            error = "Failed to wait for the 'type Of Sale'.";
            return false;
        }
        String typeOfSale = getData("Type of Sale");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.typeOfSale(), typeOfSale))
        {
            error = "Failed to select for the 'type Of Sale'.";
            return false;
        }
        narrator.stepPassed("Selected Type Of Sale: " + typeOfSale);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.jointVenture()))
        {
            error = "Failed to wait for the 'joint Venture'.";
            return false;
        }
        String jointAdventure = getData("Joint Venture");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.jointVenture(), jointAdventure))
        {
            error = "Failed to select for the 'joint venture' .";
            return false;
        }
        narrator.stepPassed("Selected Joint Adventure: " + jointAdventure);
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.stockNumber()))
        {
            error = "Failed to wait for the 'stock Number'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.stockNumber()))
        {
            error = "Failed to clear 'Stock Number' text field.";
            return false;
        }
        String stockNumber = getData("Stock Number");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.stockNumber(), stockNumber))
        {
            error = "Failed to enter for the 'stock Number' .";
            return false;
        }
        narrator.stepPassed("Entered text: " + stockNumber + " into Stock Number text field");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.interestRate()))
        {
            error = "Failed to wait for the 'interest Rate'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.interestRate()))
        {
            error = "Failed to clear 'interest Rate'text field.";
            return false;
        }
        String interestRate = getData("Interest Rate");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.interestRate(), interestRate))
        {
            error = "Failed to enter for the 'interest Rate' .";
            return false;
        }
        narrator.stepPassed("Entered text: " + interestRate + " into Interest Rate text field");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.principalDebt()))
        {
            error = "Failed to wait for the 'principal Debt'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.principalDebt()))
        {
            error = "Failed to clear 'Principle Debt'text field.";
            return false;
        }
        String principalDebt = getData("Principal Dept");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.principalDebt(), principalDebt))
        {
            error = "Failed to enter for the 'principal Debt'.";
            return false;
        }
        narrator.stepPassed("Entered text: " + principalDebt + " into Principal Debt text field");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.invoiceAmount()))
        {
            error = "Failed to wait for the 'Invoice Amount'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.invoiceAmount()))
        {
            error = "Failed to clear 'Invoice Amount' text field.";
            return false;
        }
        String invoiceAmount = getData("Invoice Amount");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.invoiceAmount(), invoiceAmount))
        {
            error = "Failed to enter for the 'Invoice Amount'.";
            return false;
        }
        narrator.stepPassed("Entered text: " + invoiceAmount + " into Inoice Amount text field");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.invoicedDate()))
        {
            error = "Failed to wait for the 'invoiced Date'.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.invoicedDate(), dateFormat.format(date)))
        {
            error = "Failed to enter for the 'invoiced Date'.";
            return false;
        }
            narrator.stepPassed("Entered text: " + dateFormat.format(date) + " into Invoiced Date text field");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.payoutDate()))
        {
            error = "Failed to wait for the 'invoiced Date' .";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.payoutDate(), dateTimeFormat.format(now.getTime())))
        {
            error = "Failed to enter for the 'invoiced Date'.";
            return false;
        }
        narrator.stepPassed("Entered text: " + dateFormat.format(now.getTime()) + " into Payout Date text field");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.signedDate()))
        {
            error = "Failed to wait for the 'signed Date'.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.signedDate(), dateTimeFormat.format(date)))
        {
            error = "Failed to enter for the 'signed Date' .";
            return false;
        }
        if (!SeleniumDriverInstance.pressKeyWithRobot("tab"))
        {
            error = "Failed to click Enter button";
            return false;
        }
        narrator.stepPassed("Entered text: " + dateFormat.format(date) + " into Signed Date text field");

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.specialDic()))
        {
            error = "Failed to wait for the 'special Dic'.";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.specialDic()))
        {
            error = "Failed to clear 'Special DIC'text field.";
            return false;
        }
        String specialDic = getData("Special DIC");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.specialDic(), specialDic))
        {
            error = "Failed to enter for the 'special Dic'.";
            return false;
        }
        narrator.stepPassed("Entered text: " + specialDic + " into Special DIC text field");

        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.vehicleDeposit()))
        {
            error = "Failed to wait for the 'vehicle Deposit' .";
            return false;
        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.vehicleDeposit()))
        {
            error = "Failed to clear 'Vehicle Deposit'text field.";
            return false;
        }
        String deposit = getData("Deposit");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.vehicleDeposit(), deposit))
        {
            error = "Failed to enter for the 'vehicle Deposit' header.";
            return false;
        }
        narrator.stepPassed("Entered text: " + deposit + " into Deposit text field");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.dic()))
        {
            error = "Failed to wait for the 'dic'.";
            return false;
        }
        String dic = getData("DIC");
        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(LightStonePageObjects.dic(), dic))
        {
            error = "Failed to enter for the 'dic'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Entered text: " + dic + " into DIC text field");
        if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.rateType()))
        {
            error = "Failed to wait for the 'rateType'.";
            return false;
        }
        String rateType = getData("Rate Type");
        if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.rateType(), rateType))
        {
            error = "Failed to select for the 'Linked' from the list.";
            return false;
        }
        narrator.stepPassed("Selected: " + rateType);
        return true;
    }

}
