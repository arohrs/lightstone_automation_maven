/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.TestClasses.ApplicationNegative;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.narrator;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Testing.PageObjects.LightStonePageObjects;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author pkankolongo
 */
@KeywordAnnotation(
        Keyword = "Relative Negative",
        createNewBrowserInstance = false
)
public class NegativeRelativeDetails extends BaseClass
{

    String error = "";
    String redField = getData("redField");
    String readOnly = getData("Read Only");
    String field = getData("Field");
    String fieldType = getData("Field Type");
    String submit = getData("Submit");

    public NegativeRelativeDetails()
    {
    }

    public TestResult executeTest()
    {
        if (!relativeDetails())
        {
            return narrator.testFailed("Failed to perform the negative test -" + error);
        }
        return narrator.finalizeTest("Successfully performed the negative test");
    }

    public boolean relativeDetails()
    {
        if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.relativeDetailsTab()))
        {
            error = "Failed to scroll for to the 'relative details ' tab to appear.";
            return false;
        }
        List<String> readOnlyField = Arrays.asList(readOnly.split(","));
        for (String readField : readOnlyField)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.readOnlyInputField(readField)))
            {
                error = "Failed to validate '" + readField + "' is a read-only field .";
                return false;
            }
            narrator.stepPassed("Successfully validated " + readField + " field is not manually editable");

        }
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relativeFirstname()))
        {
            error = "Failed to enter 'firstname' into the text field.";
            return false;
        }
        if (!getData("Relative Firstname").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeFirstname(), getData("Relative Firstname")))
            {
                error = "Failed to enter 'firstname' into the text field.";
                return false;
            }
        }
        narrator.stepPassed("Firstname:" + " " + getData("Relative Firstname"));
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relativeSurname()))
        {
            error = "Failed to enter 'surname' into the text field.";
            return false;
        }
        if (!getData("Relative Surname").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeSurname(), getData("Relative Surname")))
            {
                error = "Failed to enter 'surname' into the text field.";
                return false;
            }
        }
        narrator.stepPassed("Surname:" + " " + getData("Relative Surname"));
        if (!getData("Relation").equalsIgnoreCase("Not a relation"))
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.relationDropDown(), getData("Relation")))
            {
                error = "Failed to select 'relation' from the list.";
                return false;
            }
            narrator.stepPassed("Relation:" + " " + getData("Relation"));
        } else
        {
            if (SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.preferedContactMethodDropDown(), getData("Relation")))
            {
                error = "Failed to select the 'preferred contact method' from the list.";
                return false;
            }
            narrator.stepPassed("Entered Relation:" + " " + getData("Relation"));
            narrator.stepPassed("'Relation' textfield does not accept text.");
        }

        if (!getData("Contact Method").equalsIgnoreCase("Not a contact"))
        {
            if (!SeleniumDriverInstance.selectFromDropDownListUsingXpath(LightStonePageObjects.preferedContactMethodDropDown(), getData("Contact Method")))
            {
                error = "Failed to select the 'preferred contact method' from the list.";
                return false;
            }
            narrator.stepPassed("Contact Method:" + " " + getData("Contact Method"));
        } else
        {
            if (SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.preferedContactMethodDropDown(), getData("Contact Method")))
            {
                error = "Failed to select the 'preferred contact method' from the list.";
                return false;
            }
            narrator.stepPassed("Entered contact Method:" + " " + getData("Contact Method"));
            narrator.stepPassed("'Preferred contact' textfield does not accept text.");
        }

        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relativeCellNumber()))
        {
            error = "Failed to enter 'cell number' into the text field.";
            return false;
        }
        if (!getData("Cellphone Number").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeCellNumber(), getData("Cellphone Number")))
            {
                error = "Failed to enter 'cell number' into the text field.";
                return false;
            }
        }
        narrator.stepPassed("Cellphone Number :" + " " + getData("Cellphone Number"));
        if (!SeleniumDriverInstance.clearTextByXpath(LightStonePageObjects.relativeAddress()))
        {
            error = "Failed to enter 'relative address' into the text field.";
            return false;
        }
        if (!getData("Relative Address").isEmpty())
        {
            if (!SeleniumDriverInstance.enterTextByXpath(LightStonePageObjects.relativeAddress(), getData("Relative Address")))
            {
                error = "Failed to enter 'relative address' into the text field.";
                return false;
            }
        }
        narrator.stepPassed("Relative Address :" + " " + getData("Relative Address"));
        if (submit.equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to wait for the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(LightStonePageObjects.submitApplication()))
            {
                error = "Failed to click the 'submit application' button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(LightStonePageObjects.relativeDetailsTab()))
            {
                error = "Failed to scroll for to the 'relative details ' tab to appear.";
                return false;
            }
            if (fieldType.equalsIgnoreCase("input"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedInputField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }

                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedInputField(redField)))
                {
                    error = "Failed to hover over the '" + field + "' field .";
                    return false;
                }
                pause(500);

            }
            if (fieldType.equalsIgnoreCase("select"))
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(LightStonePageObjects.highlightedSelectField(redField), 5))
                {
                    error = "The '" + field + "' field is not highlighted in red .";
                    return false;
                }
                if (!SeleniumDriverInstance.hoverOverElementByXpath(LightStonePageObjects.highlightedSelectField(redField)))
                {
                    error = "Failed to hover over the '" + field + "'field .";
                    return false;
                }
                pause(500);

            }
            narrator.stepPassed("Successfully found the red highlighted " + field + " field ");

        }

        return true;
    }

}
