package com.gtc.KeywordDrivenTestFramework.Testing.PageObjects;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;

public class Eribank_PageObject extends BaseClass
{

    public static String username()
    {
        return "//android.widget.EditText[@text='Username']";
    }

    public static String password()
    {
        return "//android.widget.EditText[@resource-id='com.experitest.ExperiBank:id/passwordTextField']";
    }

    public static String loginButton()
    {
        return "//android.widget.Button[@resource-id='com.experitest.ExperiBank:id/loginButton']";
    }

    public static String BalanceAmount()
    {
        return "//android.webkit.WebView[@resource-id='com.experitest.ExperiBank:id/balanceWebView']";
    }

    public static String PaymentButton()
    {
        return "//android.widget.Button[@resource-id='com.experitest.ExperiBank:id/makePaymentButton']";
    }

    public static String PhoneNo()
    {
        return "//android.widget.EditText[@text='Phone']";
    }

    public static String Name()
    {
        return "//android.widget.EditText[@text='Name']";
    }

    public static String Amount()
    {
        return "//android.widget.EditText[@text='Amount']";
    }

    public static String SelectCountry()
    {
        return "//android.widget.Button[@text='Select']";
    }

    public static String CountrySelection(String country)
    {
        return "//android.widget.TextView[@text='" + country + "']";
    }

    public static String SendPayments()
    {
        return "//android.widget.Button[@text='Send Payment']";
    }

    public static String PaymentYes()
    {
        return "//android.widget.Button[@text='Yes']";
    }

    public static String Logout()
    {
        return "//android.widget.Button[@text='Logout']";
    }

    public static String SetTimeButton()
    {
        return "//android.widget.ImageButton[@resource-id='com.calc.chadcook.parkopay:id/starttimebutton']";
    }

    public static String EndTimeButton()
    {
        return "//android.widget.ImageButton[@resource-id='com.calc.chadcook.parkopay:id/endtimebutton']";
    }

    public static String Time()
    {
        return "android:id/radial_picker";
    }

    public static String SetHour()
    {
        return "android.widget.RadialTimePickerView$RadialPickerTouchHelper";
    }

    public static String buttonOkaySetTimes()
    {
        return "//android.widget.Button[@resource-id='android:id/button1']";
    }

    public static String TextPayment()
    {
        return "//android.widget.TextView[@resource-id='com.calc.chadcook.parkopay:id/paymentfield']";
    }

    public static String FieldPayment(int payAmount)
    {
        return "//android.widget.EditText[@resource-id='com.calc.chadcook.parkopay:id/editTextR"+payAmount+"']";
    }

    public static String imageOkPayment()
    {
        return "//android.widget.ImageButton[@resource-id='com.calc.chadcook.parkopay:id/okaybutton']";
    }

    public static String imagePayment()
    {
        return "//android.widget.ImageButton[@resource-id='com.calc.chadcook.parkopay:id/paymentbutton']";
    }
   
}
