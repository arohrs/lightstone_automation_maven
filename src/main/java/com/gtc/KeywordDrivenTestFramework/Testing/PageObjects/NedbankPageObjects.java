/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing.PageObjects;

import static com.gtc.KeywordDrivenTestFramework.Core.BaseClass.currentEnvironment;

/**
 *
 * @author mjivan
 */
public class NedbankPageObjects
{
//    public static String RoboAdvisorURL()
//    {
//        // Use ENUM
////        return currentEnvironment.RoboAdvisorAzureURL;
//    }
    
    //sikuli images:
    public static String UCTInstitutionImage()
    {
        return "UCT.PNG";
    }
    
    public static String SACSInstitutionImage()
    {
        return "SACS.png";
    }
    
    public static String EvaSelectThisSchool()
    {
        return "//a[@id='map-selectSchool']";
    }
    
    public static String ZoomOutButton()
    {
        return "ZoomOut.PNG";
    }
    
    public static String SchoolMarkers()
    {
        return "SchoolMarkers.PNG";
    }
    
    public static String SelectThisSchool()
    {
        return "//a[contains(text(),'Select this school')]";
    }
  
  
    public static String LandingPageRoboDiv()
    {
        return "//div[@id='botDiv']";
    }

    public static String EVAMessageDiv(String messageText)
    {
        return "//div[text()=\"" + messageText + "\"]";
    }
    
    public static String EVAMessageDivContains(String messageText)
    {
        return "//div[contains(text(),'"+messageText+"')]";
    }

    public static String EvaChatButtonXpath(String buttonText)
    {
        return "//a[text()=\""+buttonText+"\"][@href]";
    }//a[text()=\"Let's use average fees\"][@href]
    
    public static String EvaChatButtonContainsXpath()
    {
        return "//a[contains(text(), '')][@href]";
    
    }
    

    public static String EvaChatInputFieldXpath()
    {
        return "//input";
    }
    
    public static String EvaChatInputTextXpath()
    {
        return "//input//..//..//../div";
    }
    
    
    public static String LetsUseAverageFeesButton()
    {
        return "//a[contains(text(),'use average fees')][@href]";
    }
    
    public static String EVAStatesHiImEvaYourPersonalDigitalAdviser()
    {
        return "//div[contains(text(),'Hi, I')][contains(text(),'m Eva, your digital investment advisor.')]";
    }
    
    public static String EVAStatesIWorkWithNedgroupInvestements()
    {
        return "//div[contains(text(),'I work with Nedgroup Investments and my job is to help you invest for whatever future you want. It')]";
    }
    
    public static String EVAAsksWouldYouLikeMeToGuideYouOrAreYouExperiencedEnough()
    {
        return "//div[contains(text(),'Would you like me to guide you through the process or are you experienced enough to go it alone?')]";
    }
    
    public static String EVAStatesGreatWeCanHelpYouInvestForYourRetirementAnEducationOrYourOwnPersonalGoal()
    {
        return "//div[contains(text(),'Great! We can help you invest for your retirement, an education or your own personal goal.')]";
    }
    
    public static String EVAAsksWhichOfTheseWouldYouLikeToPlanFor()
    {
        return "//div[contains(text(),'Which of these would you like to plan for?')]";
    }


    
    public static String EVAAtatesWithTheHelpOfNedgroupInvestmentsICanHelpYou()
    {
        return EVAMessageDiv("With the help of Nedgroup Investments, I can help you plan for your retirement, an education or to just save.");
    }
    
    public static String EVAAsksWhatWouldYouLikeToStartInvestingFor()
    {
        return EVAMessageDiv("What would you like to start investing for?");
    }

    public static String EVAIntroducesHerselfMessageText()          
    {
        return EVAMessageDiv("Hi, I'm Eva, your digital adviser. Investing and preparing for a better future can be easy and fun. Let me ask you a few questions and show how I can help you. Do you need help with investing your money?");
    }
    
    public static String EVAAsksDoYouWantToSaveForYouKidsEducation()
    {
        return EVAMessageDiv("Do you want to save for your kids education?");
    }
    
    public static String EVAStatesGreatILoveKidsEspeciallyWhenTheyAreEducated()
    {
        return EVAMessageDiv("Great, I love kids, especially when they are educated!");
    }
    
    public static String EVAStatesGreat()
    {
        return EVAMessageDiv("Great.");
    }
    
    public static String EVAStatesForMoreQuestionsForRightFunds()
    {
        return "//div[contains(text(),'Now I need to ask you few more questions')]";
    }
    
    public static String EVAAsksWhenWillYouNeedTheFunds()
    {
        return EVAMessageDiv("How long till you need the funds?");
    }
    
    public static String EVAAsksDoYouKnowWhichInstitute()
    {
        return EVAMessageDiv("Do you know which institute?");
    }
    
    public static String EVAAsksHighSchoolOrUniversity()
    {
        return EVAMessageDiv("High school or university?");
    }
    
    public static String EVAAsksToHelpChooseOrUseExampleTuition()
    {
        return "//div[contains(text(),'Do you want me to help you choose or we')]";
    }
    
    public static String EVAAsksIfAimingForStandardOrBestSchool()
    {
        return "//div[contains(text(),'Are you aiming for a standard school or')]";
    }
    
    public static String EVAAskIfYouHaveMoreChildren()
    {
        return "//div[contains(text(),'Do you have more children')]";
    }
    
    public static String EVAASksIfYouCanAffordTheSuggestedAmount()
    {
        return EVAMessageDiv("Can you afford the suggested amount?");
    }
    
    //Expaths not implemented:
    public static String EVAAsksHaveYouStartedSavingTowardsYourRetirement()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAStatesThatsGreatNowLetsSeeHowICanMaximiseYourRetirementSavings()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAAsksDoYouKnowHowMuchYourSavingsAreWorthToday()
    {
        return EVAMessageDivContains("That's great! Do you know how much\nthose savings are worth today?");
    }
    
    public static String EVAAsksHowMuchHaveYouSavedAlready()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAAsksDoYouWantToStartWithAOnceOffAmount()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAAsksHowMuchWouldYouWantToContribute()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAAsksNowINeedToKnowIsYourAgeSoThatICanCalculateRetirementPicture()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAAsksWasICorrect()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAStatesSorryAboutThat()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAAsksWantMeYoRetakePictureOrWillYouTellMe()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAAsksWhatIsYourAge()
    {
         return EVAMessageDiv("");
    }
    
    public static String EVAStatesThanks()
    {
         return EVAMessageDiv("Great, thanks :)");
    }
    
    public static String EVAStatesThisIsImportantBecauseICancalculateYourTaxBenefit()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAAsksWhatIsYourAnnualIncomeBeforeTaxes()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAStatesThanksIveCreatedYourFirstInvestmentRecommendation()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAStatesDontWorryYouCanTellMeTheNumberLater()
    {
        return EVAMessageDiv("");
    }
    
    public static String EVAStatesNoProblemIllHelpYouMaximiseYourInvestment()
    {
        return EVAMessageDiv("");
    }
    
    //---------------
    
    public static String EVAAsksIfYouCanKickStartYourFund()
    {
        return "//div[contains(text(),'Can you kick start your fund with an initial')]";
    }
    
    public static String EVAAsksIfIAmReadyToPlanMyRetirement()          
    {
        return EVAMessageDiv("Let's start with planning for your retirement. Happy?");
    }

    public static String EVAAsksIfIAmAlreadySavingTowardsMyRetirement()          
    {
        return EVAMessageDiv("Have you already started saving towards your retirement?");
    }

    public static String EvaAsksIfIKnowHowMuchMySavingsAreWorthToday()
    {
        return EVAMessageDiv("Do you know how much your savings are worth today?");
    }

    public static String EvaAsksHowMuchIHaveSavedAlready()
    {
        return "//div[contains(text(), 'How much have you saved already?')]";
    }

    public static String EVAGetStartedMessageText()          
    {
        return EVAMessageDiv("Good, then you are in the right place to get started.");

    }

    public static String EVALetsStartPlanningForRetirementMessageText()          
    {
        return EVAMessageDiv("Let's start with planning for your retirement. Happy?");

    }


    public static String EVAHaveYouStartedSavingForRetirementMessageText()          
    {
        return EVAMessageDiv("Have you already started saving towards your retirement?");

    }


  
    public static String EVADoYouKnowHowMuchYourSavingsAreWorthMessageText()          
    {
        return EVAMessageDiv("Do you know how much your savings are worth today?");

    }

    public static String EVAHowMuchHaveYouSavedMessageText()          
    {
        return EVAMessageDiv("How much have you saved already?");

    }
  
    public static String EVALetMeTakeAPhotoOfYouMessageText()          
    {
        return EVAMessageDiv("Let me take a photo of you and I'll guess your age.");

    }
  
    public static String EVAWhatIsYourAgeMessageText()          
    {
        return EVAMessageDiv("What's your age?\n" +
"                        ");

    }
    public static String EVAWhatIsYourCurrentGrossAnnualIncomeMessageText()          
    {
        return EVAMessageDiv("What is your current gross annual income?\n" +
"                        ");

    }
    
    public static String EVAWouldYouNeedAccessToYourMoneyBefore55MessageText(int age)          
    {
        return EVAMessageDiv("Would you need access to your money before age " + age + "?");

    }
    
    public static String EVASummaryMessageText()          
    {
        return EVAMessageDiv("[Summary]");

    }
    
    
    public static String EVAAsksWhatWouldYouSayYourAnnualIncomeIs()
    {
        return "//div[contains(text(),'What would you say you make')][contains(text(),'annually before tax? An')][contains(text(),'approximate amount is fine...')]";
    }
    
    public static String EVAStatesPleaseTypeInYourAge()
    {
        return EVAMessageDivContains("Okay, please type in your age now.");
    }
    
    public static String EVAAsksCanIGuessYouAgeByTakingAPhotoOfYou()
    {
        return "//div[contains(text(),'Thanks. Now for your age...if you want, I can take a photo of you and try and guess. How about that?')]";
    }
    
    public static String EVAStatesGreatGladToHearIt()
    {
        return EVAMessageDiv("Great, glad to hear it :)");
    }
    
    public static String EVAAskThatsGreatDoYouKnowHowMuchThoseSavingsAreWorthToday()
    {
        return "//div[contains(text(),'That')][contains(text(),'s great! Do you know how much')]";
    }
    
    public static String EVAAsksIfYoureAlreadyInvestingForYourRetirement()
    {
        return "//div[contains(text(),'Now I need to know if you')][contains(text(),'re already investing in any funds for retirement – it')][contains(text(),'s important for calculating tax benefits.')]";
    }
    
    public static String EVAAsksCanYouGiveMeAnIdeaOfTheAmount()
    {
        return EVAMessageDivContains("Can you give me an idea of the");
    }
    
    public static String EVAStatesInvestmentRecommendationForYouMonthlyContribution()
    {
        return "//div[contains(text(),'Okay... based on what I know so far, I')][contains(text(),'ve got a first investment recommendation for you with a suggested monthly contribution.')]";
    }
    
    
    public static String EVAStatesTimeForMoreQuestions()
    {
        return EVAMessageDivContains("//div[contains(text(),'I hope you like this')][contains(text(),'recommendation, but it')][contains(text(),'s still quite')][contains(text(),'generic.')]");
    }
    
    //education SO 1:
    
    public static String EVAStatesGreatChoiceWhetherYoureInvestingForYourChildsEducstionOrYourOwnItsAPositiveStepForTheFinancialFuture()
    {
        return "//div[contains(text(),'Great choice – education is always an investment in the financial future.')]";
        
    }
    //Let's start by giving your investment a name. How about something personal, like 'Jonny's School Fund'?
    
    public static String EVAAsksEducationIsVeryPersonalLetsStartByGivingYourInvestmentAName()
    {
        return "//div[contains(text(),'Let')][contains(text(),'s start by giving your investment a name. How about something personal, like ')][contains(text(),'Ben')]";
    }
    
    public static String EVAAsksDoYouHaveAParticularSchoolUniversityOrCollegeInMind()
    {
        return "//div[contains(text(),'Great, thanks! Do you have a particular study institution in mind?')]";
    }
    
    //education SO 3:
    
    public static String EVAAsksWhatTypeOfEducationDoYouWantToSaveFor()
    {
        return "//div[contains(text(),'That')][contains(text(),'s fine – you don')][contains(text(),'t need one to start investing. Can you tell me what kind of education you')]";
    }
    
    public static String EVAStatesThatsGoodToKnowAsTheCostOfSchoolingCanVaryWidelyInSouthAfrica()
    {
        return "//div[contains(text(),'Good to know. The cost of schooling in South Africa can vary widely.')]";
    }
    
    public static String EVAAsksWillYouBeSavingForAPrivateSchoolEducation()
    {
        return "//div[contains(text(),'Will you be saving for a private school education?')]";
    }
    
    public static String EVAAsksWantMeToHelpYouChoseTheSchoolOrWeCanProceedWithAverageTuition()
    {
        return "//div[contains(text(),'or should we just use average tuition fees?')]";
    }//                                or should we just use average tuition fees?
    
    public static String EVAAsksOkayWhichOne()
    {
        return "//div[contains(text(),'Okay, what is it called?')]";
    }
    
    //retirement SO 1:
    
    public static String EVAStatesToStartWithAgeAndIncome()
    {
        return  "//div[contains(text(),'Alright, in order to work out when you')][contains(text(),'re likely to retire and what you')][contains(text(),'ll be able to invest, I need to know your age and income.')]";
    }
    
    public static String EVAStatesNoProblemIDontNeedTheValueToProceed()
    {
        return "//div[contains(text(),'That')][contains(text(),'s no problem. I don')][contains(text(),'t need the')][contains(text(),'value right now – you can tell me')][contains(text(),'later.')]";
    }
    
    public static String EVAStatesIHopeYouLikeThisRecommendationButItsNotVeryPersonalised()
    {
        return "//div[contains(text(),'I hope you like this recommendation, but it')][contains(text(),'s not very personalised.')]";
    }
    
    public static String EVAStatesIfYouveGotTimeForAnotherOrQuestionsICanTailorItMore()
    {
        return "//div[contains(text(),'If you')][contains(text(),'ve got time for another 2 or 3 questions I can tailor it more to your life. It won')][contains(text(),'t take long...')]";
    }
    
    public static String EVAAsksDoYouWantToKickstartYourRetirementWithAOnceOffPayment()
    {
        return "//div[contains(text(),'Firstly, do you want to kickstart your investment with a once-off payment – it')][contains(text(),'s not essential, but will affect the monthly contribution?')]";
    }
    
    public static String EVAAsksGreaHowMuchWouldYouLikeToStartWith()
    {
        return "//div[contains(text(),'Great. How much would you like to')]";
    }
    
    public static String EVAAsksWillYouLikelyNeedToAccessThisMoneyBeforeYouRetire()
    {
        return "//div[contains(text(),'Next question: do you think you')][contains(text(),'re likely to need some of this money before you retire?')]";
    }
    
    public static String EVAStatesIfYesItsPerfectlyFineButIfNoIdRecommendAFund()
    {
        return "//div[contains(text(),'If yes, it')][contains(text(),'s perfectly fine, but if no, I')][contains(text(),'d recommend a fund that gives lots of tax benefits, but no access to the money before age 55.')]";
    }
    
    public static String EVAStatesGoodToKnowIllMakeSureYouCanAccessYourMoneyAnytime()
    {
        return "//div[contains(text(),'Good to know. I')][contains(text(),'ll make sure you can access your money anytime.')]";
    }
    
    public static String EVAStatesNowLastQuestion()
    {
        return "//div[contains(text(),'Now, last question...')]";
    }
    
    public static String EVAAsksYouAreAllowedToInvestUpToRATaxFreeHaveYouUsedThatFullAllowance()
    {
        return "//div[contains(text(),'You')][contains(text(),'re allowed to invest up to R33 000 a year tax-free – have you used that full allowance this year?')]";
    }
    
    public static String EVAAksHowWouldYouPreferYourInvestmentToChangeOverTime()
    {
        return "//div[contains(text(),'Lastly, of these two options, how')][contains(text(),'would you like, or prefer, your')][contains(text(),'investment to change over time?')]";
    }
    
    public static String EVAAksHowWouldYouPreferYourInvestmentToChangeOverTime2()
    {
        return "//div[contains(text(),'Good to know. Now, of these two options, how would you prefer your investment to change over time?')]";
    }
    
    
    public static String EvaStatesNowHeresMyRecommendationBasedOnEverythingWeveDiscussed()
    {
        return "//div[contains(text(),'here')][contains(text(),'s my recommendation based on everything we')][contains(text(),'ve discussed.')]";
    }
    
    
    public static String EVAStatesSummary()
    {
        return "-";
    }
    
    public static String EVAStatesTheAverageTuitionFeesAreRPerYearSoThat()
    {
        return "//div[contains(text(),'Alright...average tuition fees are Rx per year, so that')]";
    }
    
    public static String EVAAsksWhenDoYouNeedToStartPayingForThisEducation()
    {
        return "//div[contains(text(),'What year will this education, and the fee payments, start?')]";
    }
    
    public static String EVAAsksIsTheFirstRecommendationOnTheLefTDoableForYouHaveALook()
    {
        return "//div[contains(text(),'Education is always surprisingly expensive, but take a look on the left – does this look doable for you?')]";
    }
    
    public static String EVAStatesGreatThatMeansYourGoalShouldBePrettyAchievableToo()
    {
        return "//div[contains(text(),'Good! That means your goal should be pretty achievable too.')]";
    }
    
    public static String EVAAsksGreatHowMuchDoYouWantToContribute()
    {
        return "//div[contains(text(),'Great! how much would you like to put in, once-off?')]";
    }
    
    public static String EVAAsksInCaseOfATemporaryDropCanYouDelayAccessingYourMoneyForSixMonthsOrSo()
    {
        return "//div[contains(text(),'The market has its ups and downs so, in the case of a temporary drop, do you think you')]";
    }
    
    
    public static String EVAStatesThatsNotAProblemStartingIsSoMuchMoreImportantThanTheAmountYouInvest()
    {
        return "//div[contains(text(),'That')][contains(text(),'s not a problem. Starting is so much more important than the amount you invest.')]";
    }
    
    public static String EVAAsksHowMuchDoYouThinkYouCanInvestPerMonthAtTheMoment()
    {
        return "//div[contains(text(),'s find a more comfortable number and you can always increase. How much do you think you can currently manage per month?')]";
    }
    
    public static String EVAAsksDoYouWantToKickStartYourEducationWithAOnceOffPayment()
    {
        return "//div[contains(text(),'Do you want to kickstart your investment with a once-off payment – it')]";
    }
    
    public static String EVAStatesOkayIllHelpYouMaximiseYourInvestmentInOtherWays()
    {
        return "//div[contains(text(),'ll help you maximise your investment in other ways :)')]";
    }
    
    public static String EVAStatesNotAProblemIllMakeSureYouCanAccessTheMoneyAtAnyTimeYouShouldNeedIt()
    {
        return "//div[contains(text(),'ll make sure you can access the money at any time should you need it')]";
    }
    
    public static String EVAAsksAreYouLookingForANearbySchoolOrTheBestSchoolsInTheCountry()
    {
        return "//div[contains(text(),'s nearby, or should I show you a list of the best in the country?')]";
    }
    
    public static String EVAStatesAlrightAverageTuitionFeesAreRPerYearSoThatsWhatWellWorkTowardsForYourInvestment()
    {
        return "//div[contains(text(),'Alright...average tuition fees are Rx per year, so that')]";
    }
    
    public static String EVAStatesOkayIllKeepThatInMind()
    {
        return "//div[contains(text(),'Okay')][contains(text(),'ll keep that in mind.')]";
    }
    
    public static String EVAStatesAlmostThereJustACoupleMoreQuestions()
    {
        return "//div[contains(text(),'Almost there now. just a couble more questions...')]";
    }
    
    public static String EVAStatesThatsAlwaysAGoodMoveImGoingToNeedYourAgeAndIncome()
    {
        return "//div[contains(text(),'That')][contains(text(),'s always a good move. To start with, I')][contains(text(),'m going to need your age and income to work out when you')][contains(text(),'m going to need your age and income to work out when you')][contains(text(),'re likely to retire and what you could comfortably invest.')]";
    }
    
    public static String EVAAsksHowOldAreYouCurrently()
    {
        return "//div[contains(text(),'Okay, how old are you currently?')]";
    }
    
    public static String EVAStatesOkayBasedOnWhatIKnowSoFarIveGotABasicInvestmentPlanForYouTakeALook()
    {
        return "//div[contains(text(),'Okay...based on what I know so far, I')][contains(text(),'ve got a basic investment plan for you. Take a look…')]";
    }
    
    public static String EVAStatesThisPlanIsStillQuiteGenericIfYouveGotTimeICanPersonaliseItFurther()
    {
        return "//div[contains(text(),'This plan is still quite generic. If you')][contains(text(),'ve got time for 2 or 3 more questions I can personalise it further, and help you save even more.')]";
    }
    
    public static String EVAAsksDoYouWantToKickstartYourInvestmentWithALumpsum()
    {
        return "//div[contains(text(),'This plan is still quite generic, but I')][contains(text(),'can personalise it more with another')][contains(text(),'2 or 3 questions... Firstly, do you')][contains(text(),'want to kickstart your investment with')][contains(text(),'')]";
    }
    
    public static String EVAStatesGoodToKnowIllMakeSureYouCanAccessTheMoneyAnytime()
    {
        return "//div[contains(text(),'Good to know. I')][contains(text(),'ll make sure you can access the money anytime.')]";
    }
    
    public static String EVAStatesThanksIllKeepThatInMindNowHeresMyDetailedPlanBasedOnEverythingWeveDiscussed()
    {
        return "//div[contains(text(),'Thanks, I')][contains(text(),'ll keep that in mind. Now, here')][contains(text(),'s my detailed plan based on everything we')][contains(text(),'ve discussed.')]";
    }
    
    public static String EVAStatesPerfectAverageTuitionFeesAreRPerYearSoThatsWhatWellWorkTowards()
    {
        return "//div[contains(text(),'Perfect...average tuition fees are R40 000 per year, so that')][contains(text(),'s what we'll work towards.')]";
    }
    
    public static String EVAStatesThatsNotAProblemAtAllWithInvestingStartingIsSoMuchMoreImportantThanTheAmount()
    {
        return "//div[contains(text(),'That')][contains(text(),'s not a problem at all. With investing, starting is so much more important than the amount.')]";
    }
    
    public static String EVAStatesThanksIllKeepThatInMind()
    {
        return "//div[contains(text(),'Thanks, I')][contains(text(),'ll keep that in mind.')]";
    }
    
    public static String EVAStatesNowheresMyDetailedPlanBasedOnEverythingWeveDiscussed()
    {
        return "//div[contains(text(),'Now here')][contains(text(),'s my detailed plan based on')][contains(text(),'everything we')][contains(text(),'ve discussed.')]";
    }
    
    public static String EVAStatesNoProblemWeDontNeedTheValueToMoveOn()
    {
        return "//div[contains(text(),'No problem. We don')][contains(text(),'t need the value to move on.')]";
    }
    
    public static String EVAAsksWhatIsTheAmount()
    {
        return "//div[contains(text(),'Okay, what is the amount?')]";
    }
    
    public static String EVAAsksDoYouHaveADateForYourGoalOrWhatYouWouldLikeToInvestPerMonth()
    {
        return "//div[contains(text(),'Great. Do you have a date in mind for your goal? If not, can you define what you')][contains(text(),'d like to invest each month?')]";
    }
    
    public static String EVAAsksWhatIsTheDate()
    {
        return "//div[contains(text(),'Perfect, what is it?')]";
    }
    
    public static String EVAStatesThanksBasedOnWhatIKnowSoFarIveGotABasicInvestmentPlanForYouTakeALook()
    {
        return "//div[contains(text(),'Perfect, what is it?')]";
    }




    
    //Personal:
    
    public static String EVAAsksDoYouKnowTheAmountYouAreSavingFor()
    {
        return "//div[contains(text(),'Let')][contains(text(),'s see if we can make your goal a reality. Do you know how much you')][contains(text(),'ll need to make it happen?')]";
    }

    public static String EVAAsksHowMuchWillYouNeed()
    {
        return "";
    }
    
    public static String EVAAsksDoYouHaveADueDateForYourGoalOrMaximalMonthlyContribution()
    {
        return "";
    }
    
    public static String EVAAsksByWhenWillYouNeedThisAmount()
    {
        return "";
    }
    
    public static String EVAStatesThanksBasedOnWhatIKnowIveCreatedYourFirstInvestmentRecommendationHaveALookOnTheLeft()
    {
        return "";
    }
    
    public static String EVAAsksItsNotEssentialButDoYouWantToKickstartYourInvestmentWithAOnceOffLumpSum()
    {
        return "";
    }
    
    public static String EVAAsksHowMuchWouldYouLikeToPutInOnceOff()
    {
        return "";
    }
    
    public static String EVAAsksWhenWillYouNeedThisMoney()
    {
        return "";
    }
    
    public static String EVAAsksHowWouldYouExpectOrLikeYourInvestmentToChangeOverTime()
    {
        return "";
    }
    
    public static String EVAAsksHowMuchDoYouLikeToInvestMonthly()
    {
        return "";
    }
    
    public static String EVAAsksDoYouThinkYouMayWantToUseTheseSavingsWithinTheNextYears()
    {
        return "";
    }
    
    public static String EVAAsksHaveYouUsedYourFullRAllowanceThisYear()
    {
        return "";
    }
    
    public static String EVAStatesOkayIfYouWishYouCanSelectTheGoalAnytimeLater()
    {
        return "";
    }
    
    public static String EVAAsksHowMuchCanYouSavePerMonth()
    {
         return "";
    }
    
    public static String EVAStatesRecommendationWillBeBasedOnLessThanYearsInvestmentAndValueWillBeShownAfterYears()
    {
        return "";
    }
    
    //>>>>>>>>>
    
    public static String EVAStatesSureWeCanHelpYouInvestForYourRetirementAnEducationOrYourOwnPersonalGoalYouCanAlsoJustInvest()
    {
        return "//div[contains(text(),'Sure. We can help you invest for your retirement, an education or your own personal goal. You can also just invest...')]";
    }
    
    public static String EVAStatesPerfectToStartImGoingToNeedYourAgeAndIncomeToWorkOutWhenYoullLikelyRetireAndWhatYouCouldComfortablyInvest()
    {
        return "//div[contains(text(),'Perfect. To start, I')][contains(text(),'m going to need your age')][contains(text(),'and income to work out when you')][contains(text(),'ll likely retire')][contains(text(),'and what you could comfortably invest.')]";
    }
    
    public static String EVAAsksHowOldAreYou()
    {
        return "//div[contains(text(),'Okay, how old are you?')]";
    }
    
    public static String EVAStatesPerfectIllMakeSureYouCanAccessTheMoneyAnytime()
    {
        return "//div[contains(text(),'Perfect. I')][contains(text(),'ll make sure you can access the money anytime.')]";
    }
    
    public static String EVAAsksYouAreAllowedToInvestUpToRaYearTaxFreeHaveYouUsedThatFullAllowance()
    {
        return "//div[contains(text(),'You')][contains(text(),'re allowed to invest up to R33 000 a')][contains(text(),'year tax-free. Have you used that full')][contains(text(),'allowance this year? It affects what I')]";
    }
    
    public static String EVAAsksHowWouldYouExpectOrPreferYourInvestmentToChangeOverTime()
    {
        return "//div[contains(text(),'Good to know. Lastly, of these two options, how would you expect, or prefer, your investment to change over time?')]";
    }
    
    public static String EVAStatesGoodChoiceEducationIsAlwaysAnInvestmentInTheFinancialFuture()
    {
        return "//div[contains(text(),'Good choice')][contains(text(),'education is always an investment in the financial future.')]";
    }
    
    public static String EVAStatesPerfectAverageTuitionFeesAreRperYearSoThatsWhatWellWorkTowards()
    {
        return "//div[contains(text(),'Perfect. Average tuition fees are R40 000 per year, so that')][contains(text(),'s what we')][contains(text(),'ll work towards.')]";
    }
    
    public static String EVAStatesGreatThatMeansYourGoalShouldBeComfortablyInReach()
    {
       return "//div[contains(text(),'Great. That means your goal should be comfortably in reach.')]";
    }
    
    public static String EVAStatesThatsFineWithInvestmentsStartingIsSOMuchMoreImportantThanTheAmount()
    {
       return "//div[contains(text(),'That')][contains(text(),'s fine...with investments, starting is SO much more important than the amount.')]";
    }
    
    public static String EVAStatesThanksForLettingMeKnowIllMakeSureYouCanAccessTheMoneyAnytime()
    {
        return "//div[contains(text(),'Thanks for letting me know. I')][contains(text(),'ll make sure you can access the money anytime.')]";
    }
    
    public static String EVAStatesOkayTheCostOfSchoolingInSouthAfricaCanVaryQuiteWidely()
    {
        return "//div[contains(text(),'Okay... the cost of schooling in South Africa can vary quite widely.')]";
    }
    
    public static String EVAStatesSureYouCanChooseASchoolBasedOnNameOrLocationOrYouCanUseMyListOfTheBestInTheCountry()
    {
        return "//div[contains(text(),'Sure. You can choose a school based on name or location. Or you can use my list of the best in the country...')]";
    }
    
    public static String EVAAsksCantFindTheOneYouAreLookingForTypeInTheNameBelow()
    {
        return "//div[contains(text(),'Can')][contains(text(),'t find the one you')][contains(text(),'re looking for? Type in the name below and we')][contains(text(),'ll add it for you.')]";
    }
    
    public static String EVAStatesGotIt()
    {
        return "//div[contains(text(),'Got it')]";
    }
    
    public static String EVAStatesSureYoUCanChooseAUniversityBasedOnNameOrLocation()
    {
        return "//div[contains(text(),'Sure. You can choose a university based on name or location')]";
    }
    
    public static String EVAStatesThank()
    {
        return "//div[contains(text(),'Thanks.')]";
    }
    
    public static String EVAStatesGotItThanks()
    {
        return "//div[contains(text(),'Got it thanks')]";
    }
    
    public static String EVAAsksLetsStartByGivingYourInvestmentAName()
    {
        return "//div[contains(text(),'Excellent. Let')][contains(text(),'s start by giving your')][contains(text(),'investment a name. How about something')][contains(text(),'personal, like')]";
    }

    
    public static String EVASsksHowMuchDoYouThinkIsNeededToAchieveThis()
    {
        return "//div[contains(text(),'Now let')][contains(text(),'s work out how to make it happen.')][contains(text(),'How much do you think it will take to')][contains(text(),'achieve this?')]";
    }
    
    public static String EVAAsksWhatTheMaximumYouCanInvestPerMonth()
    {
        return "//div[contains(text(),'Perfect. What')][contains(text(),'s the maximum you think you can manage per month?')]";
    }
    
    public static String EVAStatesGreatS()
    {
        return "//div[contains(text(),'Great.')]";
    }
    
    public static String EVAAsksAllocateThisSavingToATaxFreeInvestment()
    {
        return "//div[contains(text(),'Are you happy for me to allocate this saving to a tax free investment?')]";
    }
    
    public static String EVAStatesOkayIllMakeSureYouCanAccessTheMoneyAnytime()
    {
        return "//div[contains(text(),'Okay, I')][contains(text(),'ll make sure you can access the money anytime.')]";
    }
    
    public static String EVAAsksHowMuchDoYouWantToInvestPerMonth()
    {
        return "//div[contains(text(),'Perfect. To start, I just need to know')][contains(text(),'how much you want to invest per')][contains(text(),'month.')]";
    }
    
    public static String EVAStatesOkayBasedOnThisAndAnEstimatedYearsToSave()
    {
        return "//div[contains(text(),'Okay, based on this and an estimated')][contains(text(),'3 years to save')]";
    }
    
    public static String EVASchoolInput()
    {
        return "//form[@id='schoolForm']//input";
    }
    
    public static String EVAStatesAverageFees(String input)
    {
       return "//div[contains(text(),'Perfect. Average tuition fees are R')][contains(text(),'"+input+"')][contains(text(),'per year, so that')]";
    }
    
    public static String EVAAsksWhenWilLYouNeedThisMoney()
    {
        return "//div[contains(text(),'Next question... in how long do')][contains(text(),'you think you')][contains(text(),'ll need this money')]";
    }
    
    public static String eva_asks_do_you_think_you_may_want_to_use_these_savings_within_the_next_years()
    {
        return "//div[contains(text(),'It sounds like you')][contains(text(),'re investing for the longer')]";
    }
    
    public static String EVAAsksWhatTypeOfInstituteIsIt()
    {
        return "//div[contains(text(),'Great! What type of institute is it?')]";
    }






















    
   
    

  
}
