/*
 * Changed made by James Joubert
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Testing;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.gtc.KeywordDrivenTestFramework.Entities.TestEntity;
import com.gtc.KeywordDrivenTestFramework.Entities.TestResult;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Reporting.ReportGenerator;
import com.gtc.KeywordDrivenTestFramework.Reporting.TestReportEmailerUtility;
import com.gtc.KeywordDrivenTestFramework.Utilities.*;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.gtc.KeywordDrivenTestFramework.Entities.QASConfig.QASymphonyUpload.RunQTestUpload;
//import com.sun.org.apache.bcel.internal.util.ClassPath;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import java.io.*;
import static java.lang.System.err;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.zeroturnaround.zip.ZipUtil;

/**
 *
 * @author fnell
 * @editir jjoubert
 */
public class TestMarshall extends BaseClass
{

    ExcelReaderUtility excelInputReader;
    CSVReportUtility cSVReportUtility;
    TestReportEmailerUtility reportEmailer;
    boolean lastTestStatus = true;
    private List<TestResult> currentResultList;
    String networkReportZip;
    private boolean copyToNetwork = false;

    
    {
        LightstoneScriptingInstance = new LightstoneScriptingUtility();
    }

    // Implementation  for Appium only
    public TestMarshall(TestEntity testdata, boolean cucumber)
    {

        testData = testdata;
        // For screenshot
        testCaseId = testdata.TestCaseId;
        isCucumberTesting = cucumber;

        reportGenerator = new ReportGenerator();
        this.generateReportDirectory();
        //No Browser selected using default - Chrome
        currentBrowser = Enums.BrowserType.Chrome;
        AppiumDriverInstance = new AppiumDriverUtility();

        performanceMonitor = new PerformanceMonitor();

    }

    public TestMarshall(String inputFilePathIn)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        //No Browser selected using default - Chrome
        currentBrowser = Enums.BrowserType.Chrome;
        reportGenerator = new ReportGenerator(inputFilePath, baseReportDirectory);
        SeleniumDriverInstance = new SeleniumDriverUtility(currentBrowser);
        DataBaseInstance = new DataBaseUtility();
//        testRailInstance = new TestRailUtility(ApplicationConfig.projectName(), ApplicationConfig.runName());
        performanceMonitor = new PerformanceMonitor();
        this.generateReportDirectory();

    }

    /**
     * Constructor for Selenium
     *
     * @param featureName
     * @param browserTypeOverride
     * @param cucumber
     */
    public TestMarshall(String featureName, Enums.BrowserType browserTypeOverride, boolean cucumber)
    {
        inputFilePath = featureName;
        currentBrowser = browserTypeOverride;
        reportGenerator = new ReportGenerator();
        SeleniumDriverInstance = new SeleniumDriverUtility(currentBrowser);
        DataBaseInstance = new DataBaseUtility();
        isCucumberTesting = cucumber;
        performanceMonitor = new PerformanceMonitor();
        this.generateReportDirectory();

    }

    public TestMarshall(String inputFilePathIn, Enums.BrowserType browserTypeOverride)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        currentBrowser = browserTypeOverride;
        reportGenerator = new ReportGenerator(inputFilePath, baseReportDirectory);
        SeleniumDriverInstance = new SeleniumDriverUtility(currentBrowser);
        DataBaseInstance = new DataBaseUtility();
        performanceMonitor = new PerformanceMonitor();
        this.generateReportDirectory();

    }

    public TestMarshall(String inputFilePathIn, Enums.BrowserType browserTypeOverride, String isCopyToNetwork)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        currentBrowser = browserTypeOverride;
        reportGenerator = new ReportGenerator(inputFilePath, baseReportDirectory);
        SeleniumDriverInstance = new SeleniumDriverUtility(currentBrowser);
        DataBaseInstance = new DataBaseUtility();
        performanceMonitor = new PerformanceMonitor();
        copyToNetwork = Boolean.parseBoolean(isCopyToNetwork);
        this.generateReportDirectory();

    }

    public void runKeywordDrivenTests() throws FileNotFoundException
    {

        narrator = new Narrator();
        List<List<TestEntity>> testDataRegressionList = loadTestData(inputFilePath);
        for (List<TestEntity> currentTestDataList : testDataRegressionList)
        {
            if (currentTestDataList.size() < 1)
            {
                Narrator.logError("Test data object is empty - spreadsheet not found or is empty");
            } else
            {
                narrator.startTestPack(currentTestDataList);
                int numberOfTest = 0;

                try
                {
                    // Each case represents a test keyword found in the excel spreadsheet
                    for (TestEntity testData : currentTestDataList)
                    {
                        BaseClass.testData = testData;

                        numberOfTest++;

                        testCaseId = testData.TestCaseId;

                        performanceMonitor.startMonitoring("This Report was generated for the following scenario - " + resolveScenarioName());

                        // Skip test methods and test case id's starting with ';'
                        if (!testData.TestMethod.startsWith(";") && !testData.TestCaseId.startsWith(";"))
                        {
                            Narrator.logDebug("Executing test - " + testData.TestMethod + " | " + numberOfTest + " of " + currentTestDataList.size());

                            try
                            {
                                ClassPath.ClassInfo testClassInfo = getKeywordTestClass(testData.TestMethod);

                                Class testClass = testClassInfo.load();

                                Constructor constructor = testClass.getConstructor();

                                Object testClassInstance = constructor.newInstance();

                                narrator.startTest();

                                if (isBlocked(testClassInstance))
                                {
                                    narrator.BlockedTest();
                                    continue;
                                }

                                if (testClassInstance.getClass().getAnnotation(KeywordAnnotation.class).createNewBrowserInstance())
                                {
                                    ensureNewBrowserInstance();
                                } else if (testClassInstance.getClass().getAnnotation(KeywordAnnotation.class).appiumTest())
                                {
                                    ensureAppium();
                                } else if (testClassInstance.getClass().getAnnotation(KeywordAnnotation.class).winAppTest())
                                {
                                    ensureWinAppDriver();
                                }

                                // Set up the Performance Monitoring
                                performanceMonitor.newPage(testData.TestCaseId);

                                Method executeTestMethod = testClassInstance.getClass().getMethod("executeTest");

                                TestResult result = (TestResult) executeTestMethod.invoke(testClassInstance);
                                lastTestStatus = (result.testStatus == Enums.ResultStatus.PASS) ? true : false;
                                reportGenerator.addResult(result);
                                performanceMonitor.endPage();

                            } catch (Exception ex)
                            {
                                err.println("[ERROR] Critical Error during keyword execution - " + testData.TestMethod + " - error: " + ex.getMessage());
                                ex.printStackTrace();
                            }

                            Narrator.logDebug("Continuing to next test method");
                        } else
                        {
                            //Test was disabled in the test pack                       
                            narrator.skipTest_TestPack_Disabled();

                        }
                    }
                } catch (Exception e)
                {

                } finally
                {

                    performanceMonitor.endMonitoring();

                    if (SeleniumDriverInstance != null && SeleniumDriverInstance.isDriverRunning())
                    {
                        SeleniumDriverInstance.shutDown();
                    }

                    //Moved to AppiumDriverutility class as it could be used in future - reload Appium server method
                    AppiumDriverUtility.stopAppiumServerIfExists();

                    if (WinDriverInstance != null)
                    {
                        WinDriverInstance.shutDown();
                    }

                    narrator.flushReports();
                    reportGenerator.generateTestReport();

                    if (requiresQAS)
                    {
                        File cucumberFile = reportGenerator.generatePayload();

                        //Config file version still exists 
                        //RunQTestUpload upload = new RunQTestUpload("Configurations\\QAConfig.properties");
                        RunQTestUpload upload = new RunQTestUpload(Enums.QAS.Placeholder);
                        upload.setStartTime();
                        upload.setEndTime();
                        upload.tdd_run(cucumberFile);
                    }

//                reportEmailer = new TestReportEmailerUtility(reportGenerator.testResults);
//                reportEmailer.SendResultsEmail();
                }

                //Generate performance log report
            }
        }
        if (copyToNetwork)
        {
            copyReportsDirectoryToNetworkDrive();
        }

    }

    public boolean isBlocked(Object testClassInstance)
    {
        if (testClassInstance.getClass().getAnnotation(KeywordAnnotation.class).blockable() && !lastTestStatus)
        {
            return true;
        }

        return false;
    }

    public ClassPath.ClassInfo getKeywordTestClass(String keywordName)
    {
        try
        {

            //Get list of all loaded classes for the package - defined at runtime - we need to be able to isolate just the TestClasses
            // in order to extract the one matching the keyword to be executed
            ClassPath classPath = ClassPath.from(Thread.currentThread().getContextClassLoader());

            //Next we set up Predicates (a type of query in java) to isolate the list of classes to only those pertaining to the framework
            // i.e. no dependencies included 
            ImmutableSet<ClassPath.ClassInfo> allClasses = classPath.getTopLevelClassesRecursive("com.gtc.KeywordDrivenTestFramework.Testing");

            //We then filter the classes to only those who have the required annotations - annotations used to add meta
            //data to the TestClasses sothat we can scan them to read their Keywords - this uses Lambda notation only available in Java 8 and above.
            Predicate<ClassPath.ClassInfo> hasAnnotationPredicate = c -> c.load().isAnnotationPresent(KeywordAnnotation.class);
            Stream<ClassPath.ClassInfo> annotatedClasses = allClasses.stream().filter(hasAnnotationPredicate);

            //The filtered list is then queried a second time in order to retrieve the valid TestClass based on the keywordName
            Predicate<ClassPath.ClassInfo> checkKeywordPredicate = c -> c.load().getAnnotation(KeywordAnnotation.class).Keyword().equals(keywordName);
            ClassPath.ClassInfo testClass = annotatedClasses.filter(checkKeywordPredicate).findFirst().get();

            if (testClass == null)
            {
                err.println("[ERROR] Failed to resolve TestClass for keyword - " + keywordName + " - error: Keyword not found");
            }

            return testClass;

        } catch (Exception ex)
        {
            err.println("[ERROR] Failed to resolve TestClass for keyword - " + keywordName + " - error: " + ex.getMessage());

            return null;
        }
    }

//    private List<TestEntity> loadTestData(String inputFilePath)
//    {
//        return excelInputReader.getTestDataFromExcelFile(inputFilePath);
//    }
    private List<List<TestEntity>> loadTestData(String inputFilePath)
    {
        return excelInputReader.getRegressionTestDataFromExcelFile(inputFilePath);
    }

    public void loadCucumberData(DataTable tableData, String keyword, Scenario scenario)
    {
        TestEntity data = new TestEntity();
        data.TestCaseId = scenario.getName();
        data.TestMethod = keyword;
        data.TestDescription = getFeaturename(scenario.getId()).concat(" - ").concat(scenario.getName());

        if (tableData != null)
        {
            for (int i = 0; i < tableData.raw().size() - 1; i++)
            {
                for (int j = 0; j < tableData.raw().get(i).size(); j++)
                {
                    data.addParameter(tableData.raw().get(0).get(j), tableData.raw().get(i + 1).get(j));
                }
            }
        }

        if (testDataList == null)
        {
            testDataList = new ArrayList<>();
        }
        testDataList.add(data);

    }

    public static String getFeaturename(String scenarioId)
    {
        return scenarioId.split(";")[0].toUpperCase();
    }

    public static void CheckBrowserExists()
    {
        if (SeleniumDriverInstance == null)
        {
            SeleniumDriverInstance = new SeleniumDriverUtility(currentBrowser);
            SeleniumDriverInstance.startDriver();
        }

        if (!SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.startDriver();
        }
    }

    public static void ensureAppium()
    {
        if (AppiumDriverInstance == null)
        {
            AppiumDriverInstance = new AppiumDriverUtility();
        }
    }

    public static void ensureWinAppDriver()
    {
        if (WinDriverInstance == null)
        {
            if (currentDesktopApplication.filePath != null)
            {
                WinDriverInstance = new WindowsDriverUtility(currentDesktopApplication.filePath);
            } else
            {
                Narrator.logError("No filepath specified! - Set the currentDesktopApplication variable!");
            }
        }

    }

    public static void ensureNewBrowserInstance()
    {
        if (SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.shutDown();
        }
        SeleniumDriverInstance.startDriver();
    }

    public void generateReportDirectory()
    {

        reportDirectory = baseReportDirectory + resolveScenarioName() + "_" + this.generateDateTimeString();
        String[] reportsFolderPathSplit = BaseClass.reportDirectory.split("\\\\");
        BaseClass.currentTestDirectory = baseReportDirectory + "\\" + reportsFolderPathSplit[reportsFolderPathSplit.length - 1];
        networkReportZip = reportsFolderPathSplit[reportsFolderPathSplit.length - 1] + ".zip";

    }

    public void copyReportsDirectoryToNetworkDrive()
    {
        //Set network folder path
        String networkdir = "\\\\GTC-FS01\\OneDrive\\OneDrive - Dynamic Visual Technologies (Pty) Ltd\\Reports\\Lightstone\\";
        String runDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String networkFolderPath = networkdir + new SimpleDateFormat("MMM").format(Calendar.getInstance().getTime()) + " " + runDate.substring(0, 4) + "\\" + runDate + "\\";
        String HTMLFolderPath = "HTMLTestReport\\extentReport.zip";

        try
        {
            //Create network folders
            File networkReportFolder = new File(networkFolderPath);
            networkReportFolder.mkdirs();

            ZipUtil.pack(new File(this.reportDirectory), new File(networkFolderPath + this.networkReportZip));
            ZipUtil.pack(new File(this.reportDirectory), new File(HTMLFolderPath));

            System.out.println("\n\nReport folder zipped and copied to the network folder with file path - " + networkFolderPath + this.networkReportZip);
        } catch (Exception e)
        {
            System.err.println("Unable to copy the report directory to the network path '" + networkFolderPath + "', fault - " + e.getMessage());
        }
    }

}
