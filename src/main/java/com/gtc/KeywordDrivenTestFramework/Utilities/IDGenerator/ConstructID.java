<<<<<<< HEAD
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator;

import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateRandBetween;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateDOB;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateGender;

/**
 *
 * @author Micah de Villiers
 */
public class ConstructID
{

    String gender;
    String CSVOutput;
    int min;
    int max;

    public ConstructID(String gender, int min, int max)
    {
        this.gender = gender;
        this.min = min;
        this.max = max;

    }

    public ConstructID()
    {

    }

    public String getID()
    {

        String id = ""; //without checksum
        String dob = "";
        String genderNumber = "";
        String idp13 = ""; //checksum
        String idp11 = ""; // 0 - Native or 1 - Foreign ; Position 11
        String idp12 = ""; // 8 or 9 ; Posiion 12 
        String nationality = null;

        GenerateRandBetween genRand = new GenerateRandBetween();
        //DOB
        GenerateDOB genDOB = new GenerateDOB(min, max);
        dob = genDOB.getIdDateOfBirth();

        CSVOutput = genDOB.getCSVOuput();

        //Gender Number
        GenerateGender genGender = new GenerateGender(gender);
        genderNumber = genGender.getGenderNumber();

        // IDP11
        if (nationality.equals("South African"))
        {
            idp11 = "0";
        } else
        {
            idp11 = "1";
        }

        // IDP12
        idp12 = Integer.toString(genRand.getRandBetween(8, 9));

        // ID12 ; Thats the ID without the checksum
        id = dob + genderNumber + idp11 + idp12;

        //Checksum
        CheckSum checkSum = new CheckSum(id);
        idp13 = checkSum.getCheckSum();

        id += idp13;

        return id;
    }

    public String getSouthAfricanID(String gender,String dob,String nationality)
    {
       
        String id = ""; //without checksum
        String genderNumber = "";
        String idp13 = ""; //checksum
        String idp11 = ""; // 0 - Native or 1 - Foreign ; Position 11
        String idp12 = ""; // 8 or 9 ; Posiion 12 

        GenerateRandBetween genRand = new GenerateRandBetween();

        //Gender Number
        GenerateGender genGender = new GenerateGender(gender);
        genderNumber = genGender.getGenderNumber();
     
        // IDP11
        // idp11 = Integer.toString(genRand.getRandBetween(0, 1));
        if (nationality.equals("South African"))
        {
            idp11 = "0";
        } else
        {
            idp11 = "1";
        }
        // IDP12
        idp12 = Integer.toString(genRand.getRandBetween(8, 9));
//        idp12 = "8";

//         ID12 ; Thats the ID without the checksum
        id = dob + genderNumber + idp11 + idp12;

        //Checksum
        CheckSum checkSum = new CheckSum(id);
        idp13 = checkSum.getCheckSum();

        id += idp13;

        return id;
    }

}
=======
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator;

import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateRandBetween;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateDOB;
import com.gtc.KeywordDrivenTestFramework.Utilities.IDGenerator.GenerateGender;

/**
 *
 * @author Micah de Villiers
 */
public class ConstructID
{

    String gender;
    String CSVOutput;
    int min;
    int max;

    public ConstructID(String gender, int min, int max)
    {
        this.gender = gender;
        this.min = min;
        this.max = max;

    }

    public ConstructID()
    {

    }

    public String getID()
    {

        String id = ""; //without checksum
        String dob = "";
        String genderNumber = "";
        String idp13 = ""; //checksum
        String idp11 = ""; // 0 - Native or 1 - Foreign ; Position 11
        String idp12 = ""; // 8 or 9 ; Posiion 12 
        String nationality = null;

        GenerateRandBetween genRand = new GenerateRandBetween();
        //DOB
        GenerateDOB genDOB = new GenerateDOB(min, max);
        dob = genDOB.getIdDateOfBirth();

        CSVOutput = genDOB.getCSVOuput();

        //Gender Number
        GenerateGender genGender = new GenerateGender(gender);
        genderNumber = genGender.getGenderNumber();

        // IDP11
        if (nationality.equals("South African"))
        {
            idp11 = "0";
        } else
        {
            idp11 = "1";
        }

        // IDP12
        idp12 = Integer.toString(genRand.getRandBetween(8, 9));

        // ID12 ; Thats the ID without the checksum
        id = dob + genderNumber + idp11 + idp12;

        //Checksum
        CheckSum checkSum = new CheckSum(id);
        idp13 = checkSum.getCheckSum();

        id += idp13;

        return id;
    }

    public String getSouthAfricanID(String gender,String dob,String nationality)
    {
       
        String id = ""; //without checksum
        String genderNumber = "";
        String idp13 = ""; //checksum
        String idp11 = ""; // 0 - Native or 1 - Foreign ; Position 11
        String idp12 = ""; // 8 or 9 ; Posiion 12 

        GenerateRandBetween genRand = new GenerateRandBetween();

        //Gender Number
        GenerateGender genGender = new GenerateGender(gender);
        genderNumber = genGender.getGenderNumber();
     
        // IDP11
        // idp11 = Integer.toString(genRand.getRandBetween(0, 1));
        if (nationality.equals("South African"))
        {
            idp11 = "0";
        } else
        {
            idp11 = "1";
        }
        // IDP12
        idp12 = Integer.toString(genRand.getRandBetween(8, 9));
//        idp12 = "8";

//         ID12 ; Thats the ID without the checksum
        id = dob + genderNumber + idp11 + idp12;

        //Checksum
        CheckSum checkSum = new CheckSum(id);
        idp13 = checkSum.getCheckSum();

        id += idp13;

        return id;
    }

}
>>>>>>> d07de976829ca921ddd915f7730068c7a5e2d2f4
