/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Utilities.PayloadGenerator;


import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zglenn
 */
public class GenerateNewPayload {

    private DateFormat df;
    private File QASFolder;
    private File payloadFile;

    public GenerateNewPayload() {
        df = new SimpleDateFormat("yyyyMMddHHmmss");
        QASFolder = new File("QAS");
        payloadFile = new File(QASFolder, String.format("results-%s", df.format(new Date())));
        if (!QASFolder.exists()) {
            try {
                QASFolder.mkdirs();
                payloadFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(TDD_Payload.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                payloadFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(TDD_Payload.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public File generateNewPayloadFile() {
        return payloadFile;
    }

}
