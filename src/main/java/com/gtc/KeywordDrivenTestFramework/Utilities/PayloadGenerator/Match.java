/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Utilities.PayloadGenerator;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Match {

    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("arguments")
    @Expose
    private List<Argument> arguments = null;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Match withLocation(String location) {
        this.location = location;
        return this;
    }

    public List<Argument> getArguments() {
        return arguments;
    }

    public void setArguments(List<Argument> arguments) {
        this.arguments = arguments;
    }

    public Match withArguments(List<Argument> arguments) {
        this.arguments = arguments;
        return this;
    }

}
