/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.KeywordDrivenTestFramework.Utilities.PayloadGenerator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Argument {

    @SerializedName("val")
    @Expose
    private String val;
    @SerializedName("offset")
    @Expose
    private Integer offset;

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public Argument withVal(String val) {
        this.val = val;
        return this;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Argument withOffset(Integer offset) {
        this.offset = offset;
        return this;
    }
}