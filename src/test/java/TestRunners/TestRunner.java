/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestRunners;

import TestRunners.TestSuites.Default_Suite;
import TestRunners.TestSuites.JenkinsSuite;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Stan
 */
public class TestRunner extends BaseClass
{
    // <editor-fold defaultstate="collapsed" desc="TestRunner Instructions">
    /*
        1. Create your Test Suites (without any '@Test' annotations above the methods.)
        2. Create an instance of that test suite method inside of the below @Test method.
        3. When Maven builds, it executes anything that is in the test TestRunner. 
        4. Keep your test suite instances Commented out if you don't want them 
            to be run during clean and build.
     */
// </editor-fold>

    @Test
    public void defaultTest() throws FileNotFoundException, IOException
    {
        new Default_Suite().gmailTest();
//        new Default_Suite().eribankTest();

        //navigate to the repo dir in CMD. Paste Command in and run the test
        //mvn test -DEnvironment="PROD" -DTestPack="GMAIL TEST PACK.xlsx" -DBrowser="Chrome"
        //mvn test -Dtest=JenkinsSuite -DEnvironment="Lightstone" -DTestPack="TempFolder/001_JUST Login.xlsx" -DBrowser="Chrome"
//        new JenkinsSuite().executeJenkins();

    }
}
