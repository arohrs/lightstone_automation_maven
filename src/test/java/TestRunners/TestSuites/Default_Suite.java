/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestRunners.TestSuites;

import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author sbachan
 */
public class Default_Suite extends BaseClass
{

    TestMarshall instance;

    public void gmailTest() throws FileNotFoundException
    {
        Narrator.logDebug("GMAIL - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\GMAIL TEST PACK.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    public void eribankTest() throws FileNotFoundException
    {

        Narrator.logDebug("Eribank - Scenario 1  - Test Pack");
        currentDevice = Enums.Device.Emulator;
        currentDeviceConfig = Enums.DeviceConfig.Eribank;
        instance = new TestMarshall("TestPacks\\eribank.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }

}
