/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestRunners.TestSuites;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.TestReportEmailerUtility;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.lang.System.out;
import java.util.Properties;
import org.junit.Test;

/**
 *
 * @author fnell
 */
public class JenkinsSuite extends BaseClass
{
 static TestMarshall instance;
    
    @Test
    public void executeJenkins() throws FileNotFoundException, IOException
    { 
        
        Properties props = System.getProperties();
//        
        String browser = props.getProperty("Browser");        
        String testpack = props.getProperty("TestPack");
        String environment = props.getProperty("Environment");
        String zipToRemote = props.getProperty("ZipToRemote");
        zipToRemote = (zipToRemote == null) ? "false" : zipToRemote.toLowerCase();
        
        
        
        
        out.println(props.stringPropertyNames());
                
        out.println("[INFO] Executing tests: Browser - " + browser + ", Environment - " + environment + ", Test Pack - " + testpack + ",ZipToRemote = " + zipToRemote);
        
        instance = new TestMarshall("TestPacks\\" + testpack , Enums.resolveBrowserType(browser),zipToRemote);
        
        TestMarshall.currentEnvironment = Enums.resolveTestEnvironment(environment);
        
        
        out.println("[INFO] Executing tests: Browser - " + browser + ", Environment - " + environment + ", Test Pack - " + testpack);
        
        
        instance.runKeywordDrivenTests();
    }   
}
