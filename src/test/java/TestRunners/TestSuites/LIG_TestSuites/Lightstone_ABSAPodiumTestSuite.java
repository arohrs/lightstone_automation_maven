package TestRunners.TestSuites.LIG_TestSuites;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class Lightstone_ABSAPodiumTestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_ABSAPodiumTestSuite()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void executeLightStoneABSAPodium() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\LightstoneRegressionFolder\\005_LIG-126 ABSA Podium.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
}
