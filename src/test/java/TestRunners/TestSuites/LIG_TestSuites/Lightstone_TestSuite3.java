
package TestRunners.TestSuites.LIG_TestSuites;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author nkelechi
 */
public class Lightstone_TestSuite3 extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_TestSuite3()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void ArchiveTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Archive Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\Archive Test Pack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}

