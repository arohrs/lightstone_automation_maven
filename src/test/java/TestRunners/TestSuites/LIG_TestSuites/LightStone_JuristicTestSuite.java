/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestRunners.TestSuites.LIG_TestSuites;


import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author pkankolongo
 */
public class LightStone_JuristicTestSuite extends BaseClass
{
    static TestMarshall instance;
    public static Enums.DeviceConfig test;
    
    public LightStone_JuristicTestSuite()
    {
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
        
    
    }
    @Test
    public void executeLightStoneJuristic() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStoneJuristic Application  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-79 JuristicApplication.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
    
    }
    @Test
    public void executeLightStoneJuristicIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStoneJuristic Application  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-79 JuristicApplication.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
        
    
    }
    
    @Test
    public void executeLightStoneJuristicNegative() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStoneJuristic Application (Negative)  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-80 JuristicApplication_Negative.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
    
    }
     @Test
     public void executeLightStoneJuristicNegativeIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStoneJuristic Application (Negative)  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-80 JuristicApplication_Negative.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
        
    
    }
    
    
    
}
