package TestRunners.TestSuites.LIG_TestSuites;


import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class Lightstone_Regression extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_Regression()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
//        TestReportEmailerUtility.setEmailRecipients("sbachan@dvt.co.za;tknoetzen@dvt.co.za;pkankologo@dvt.co.za;");
    }

    @Test
    public void executeLightStoneRegressionFolder() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");    
        instance = new TestMarshall("TestPacks\\LightstoneRegressionFolder", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void executeLightStoneRegressionTestPack() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");    
        instance = new TestMarshall("TestPacks\\NegativeRegressionFolder", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }

}
