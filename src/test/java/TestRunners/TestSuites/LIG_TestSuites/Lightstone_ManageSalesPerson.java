
package TestRunners.TestSuites.LIG_TestSuites;


import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;
/**
 *
// * @author nkelechi
 */
public class Lightstone_ManageSalesPerson extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_ManageSalesPerson()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void ManageSalepersonTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Manage Salesperson Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-52 ManageSalesPerson.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void ManageSalepersonTestSuiteIE() throws FileNotFoundException
    {
        Narrator.logDebug("Manage Salesperson Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-52 ManageSalesPerson.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
}

