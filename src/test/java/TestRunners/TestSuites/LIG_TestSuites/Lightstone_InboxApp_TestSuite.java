package TestRunners.TestSuites.LIG_TestSuites;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;
/**
 *
 * @author dluis
 */
public class Lightstone_InboxApp_TestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_InboxApp_TestSuite()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void inboxTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Inbox Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\LightstoneRegressionFolder\\005_LIG-126 ABSA Podium.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void executeLightStone_SignioApplication() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-57 SignioApp-New Application_Negative.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
     @Test
    public void executeLightStone_SignioApplicationIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-57 SignioApp-New Application_Negative.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void executeLightStone_WesbankReApplication() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Wesbank Re-Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-43 Wesbank Re-Application.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void executeLightStone_WesbankReApplicationIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Wesbank Re-Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-43 Wesbank Re-Application.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void executeLightStone_DVTApplication() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");   
        instance = new TestMarshall("TestPacks\\LIG-54 Complete All Fields Application.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
      @Test
    public void executeLightStone_DVTApplicationIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-54 Complete All Fields Application.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
     @Test
    public void executeLightStone_DVTApplicationNegative() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-57 SignioApp-New Application_Negative.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
      @Test
    public void executeLightStone_DVTApplicationNegativeIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-57 SignioApp-New Application_Negative.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
     @Test
    public void executeLightStone_AfterMarket() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-55 AfterMarket.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void executeLightStone_AfterMarketIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-55 AfterMarket.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
     @Test
    public void executeLightStone_AfterMarketNegative() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-61 AfterMarket_Negative.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
     @Test
    public void executeLightStone_AfterMarketNegativeIE() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("Dvt Application - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-61 AfterMarket_Negative.xlsx", Enums.BrowserType.IE);
        instance.runKeywordDrivenTests();
    }
    
}
