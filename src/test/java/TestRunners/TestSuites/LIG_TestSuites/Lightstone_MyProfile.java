package TestRunners.TestSuites.LIG_TestSuites;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class Lightstone_MyProfile extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_MyProfile()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void executeLightStonePOC() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\MyProfile.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
