
package TestRunners.TestSuites.LIG_TestSuites;


import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;
/**
 *
// * @author nkelechi
 */
public class Lightstone_ArchiveTestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_ArchiveTestSuite()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void ArchiveTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Archive Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\Archive Test Pack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void LIG_45_ArchiveTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Archive Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\LIG-45 Archive_TestPack.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}

