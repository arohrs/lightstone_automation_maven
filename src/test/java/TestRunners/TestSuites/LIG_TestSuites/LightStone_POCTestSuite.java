package TestRunners.TestSuites.LIG_TestSuites;


import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class LightStone_POCTestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public LightStone_POCTestSuite()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void executeLightStonePOC() throws FileNotFoundException
    {
        requiresBrowser = true;
        Narrator.logDebug("LightStonePOC  - Test Pack");
        instance = new TestMarshall("TestPacks\\LightstoneRegressionFolder\\002 LIG-44 LightStonePOC.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
