
package TestRunners.TestSuites.LIG_TestSuites;

import com.gtc.KeywordDrivenTestFramework.Core.BaseClass;
import com.gtc.KeywordDrivenTestFramework.Entities.Enums;
import com.gtc.KeywordDrivenTestFramework.Reporting.Narrator;
import com.gtc.KeywordDrivenTestFramework.Testing.TestMarshall;
import java.io.FileNotFoundException;
import org.junit.Test;
/**
 *
 * @author nkelechi
 */
public class Lightstone_LoginTestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public Lightstone_LoginTestSuite()
    {
        
        TestMarshall.currentEnvironment = Enums.Environment.LightStone;
    }

    @Test
    public void loginTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Login Scenario - Test Pack");
        instance = new TestMarshall("TestPacks\\LightstoneRegressionFolder\\001_LIG-34 Lightstone Login.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}

